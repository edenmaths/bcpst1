if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="PolyStats21-1";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

import graph;

graphicrules(xunit=.4cm, yunit=3cm,xmin=1978, xmax=2009,ymin=3, ymax=6);
xaxis("Ann\'ee",BottomTop,
Ticks(Step=5,step=1,
pTick=blue, ptick=dashed+grey,
extend=true));
yaxis("Extension de la banquise (en $\overline{M}$ de km\textsuperscript{2})",LeftRight,
Ticks(Step=1,step=1,
pTick=blue, ptick=dashed+grey,
extend=true));

// Tableau des annees
real[] tabxi={1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008};
// Tableau des extensions
real[] tabyi={5.3,5.5,4.95,5.13,5.37,4.7,5,5.37,5.3,5.2,4.8,4.62,4.5,5.05,4.45,4.85,4.4,5.25,4.9,4.26,4.2,4.14,4.55,4.05,4.12,4.3,4.05,4.06,3.75,3.8};

// Tracé du polygone
path polygone=graph(tabxi,tabyi);
draw(polygone,1bp+red);
dot(polygone);

//shipout(bbox(3mm,white));



