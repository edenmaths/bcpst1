import sympy as sp
from sympy.abc import x

y = sp.log(1 + x**2)

i = sp.integrate(y, (x, 0, 1))
sp.pprint(i)
