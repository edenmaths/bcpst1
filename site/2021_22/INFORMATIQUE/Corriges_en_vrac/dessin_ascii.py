from typing import List

# On découpe en 3 fonctions:
#
# histo_liste(notes: List[int]) -> List[int]
# qui renvoie la liste des fréquences
#
# trace(freqs: List[int]) -> None
# qui trace freqs[i] # sur la ligne i
# 
# trace_histo(notes: List[int]) -> None
# qui compose les deux


def histo_liste(notes: List[int]) -> List[int]:
    # m = max(notes) # en utilisant le max de Python
    m = notes[0]
    for note in notes:
        if note > m:
            m = note
    freqs = [0]*(m + 1)
    for note in notes:
        freqs[note] += 1
    return freqs

def trace(freqs: List[int]) -> None:
    for note in range(len(freqs)):
        print(f"{note} : {'#'*freqs[note]}")

def trace_histo(notes: List[int]) -> None:
    trace(histo_liste(notes))
