def racineDicho(prec: float):
    cpt: int = 0
    inf: float = 1
    sup: float = 2
    while sup - inf > prec:
        mil = (sup + inf) / 2
        if mil*mil < 2:
            inf = mil
        else:
            sup = mil
        cpt += 1
    return sup,cpt