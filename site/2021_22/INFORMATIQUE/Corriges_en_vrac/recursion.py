# quizz inverse
def inverse(xs):
    if len(xs) <= 1:
        return xs # me dit que cette fonction renvoie une liste
        # à retenir : le cas terminal donne le type des objets renvoyés
    else:
        return inverse(xs[1:]) + [xs[0]]
    
# somme des n premiers entiers

def somme(n: int) -> int:
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)
    
# somme des éléments d'une liste de nombres

def somme2(xs):
    assert len(xs) > 0, "On en peut, jeune Padawan, calculer la somme des éléments d'une lsite vide"
    if len(xs) == 1:
        return xs[0] # on doit renvoyer un objet du même type que celui des éléments de xs 
    else:
        # la tête plus la somme de la liste décapitée
        return xs[0] + somme2(xs[1:])

# nombre d'éléments d'une collection

def compte(sac):
    if sac == []:
        return 0
    else:
        return 1 + compte(sac[1:])
    
    
    