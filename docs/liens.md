# Quelques liens

## Autres enseignements

- Le cours d'[ECS-1](https://edenmaths.gitlab.io/ecs1/)
- Le dépôt GIT des cours de [NSI](https://gitlab.com/lyceeND/) :
     - [1ère](https://gitlab.com/lyceeND/1ere/-/tree/master/2019_20)
	 - [Tale](https://gitlab.com/lyceeND/tale/-/tree/master/2020_21)	 
-  Un [ancien  site](https://informathix.tuxfamily.org/)  avec  des cours  d'IUT
  Info, d'info  en MPSI/MP*,  de programmation en  divers langages  et bien
  d'autres choses, notamment :
    - [IUT INFO 1](https://informathix.tuxfamily.org/?q=node/5)
	- [IUT INFO 2](https://informathix.tuxfamily.org/?q=node/16)
	- [Info MP et MP*](https://informathix.tuxfamily.org/?q=node/14)
	- Des activités dans [divers langages de programmation](https://informathix.tuxfamily.org/?q=node/11)
- Le dépôt GIΤ  du [DU NSI](https://gitlab.com/GiYoM/du/-/tree/master/) que j'ai
  animé à l'UCO.
- Le dépôt GIT du cours de [Python en L1](https://github.com/Informathix/Complements_Info_L1_UCO_Angers)
  en [L2](https://github.com/Informathix/UCO_L2) et en [L3](https://github.com/Informathix/UCO_L3)
- le cours des [BCPST2](https://mathsinfo-bcpst2-eden.fr/)
-  Un   [catalogue](https://natb_nsi.forge.aeif.fr/ressources/)  des  cours
  d'informatique en ligne.
