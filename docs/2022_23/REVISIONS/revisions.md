# Révisions estivales

!!! exo ""
	![Placeholder](../COURS/IMG/KermitBeach.jpg){ align=right width=300}
	Que  faire  cet  été  sur  la   plage  sinon  un  peu  de  mathématiques  et
	d'informatique ! Ne  perdez pas de temps  à lire des romans  ennuyeux : [Visa
	pour la Prépa 2023-2024](https://www.dunod.com/prepas-concours/maths-et-informatique-visa-pour-prepa-2023-2024-mpsi-mp2i-pcsi-ptsi-bcpst-ecg-tsi)  est le seul livre que vous  devez emporter. Mais si
	vous n'avez  que votre  smartphone, voici une  petite série  d'activités qui
	devraient égayer vos journées.

![Placeholder](./IMG/tintin9.png){ align=left width=370} 

1. [Espace d'échanges](../1_element)

2. [Pourquoi travailler cet été](../2_travail)

3. [Python](../3_python)

4. [Fractions](../4_fractions)

5. [Puissances](../5_puissances)

6. [Calcul littéral](../6_litteral)

7. [Racines carrées](../7_racines)

8. [Expressions alébriques](../8_expressions)

9. [Équations du 2nd degré](../9_second_degre)

10. [Exponentielle et logarithme](../10_expolog)

11. [Trigonométrie](../11_trigo)

12. [Dérivation](../12_derivation)

13. [Intégration](../13_integration)

14. [Inégalités](../14_inegalites)
