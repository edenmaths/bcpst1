{{ chapitre(8, "Exponentielle et logarithme", "Rappel N°")}}





## Logarithmes
![Placeholder](./IMG/tintin_colle.png){ align=right width=290}

!!! {{ exercice()}}

    === "Énoncé"

        Calculer les  nombres suivants en  fonction de $\ln  (2), \ln (3)$  et $\ln(5)$

		1. $\ln (16)       $
		1.  $\dfrac{1}{8}  \ln  \left(\dfrac{1}{4}\right)-\dfrac{1}{4}  \ln
		   \left(\dfrac{1}{8}\right)$ 
		1. $\ln (512)        $
		1. $\ln (72)-2 \ln (3)       $
		1. $\ln (0,125)      $
		1. $\ln (36)       $

	=== "Corrigé"

         1.  On a $16=4^{2}=2^{4}$ donc $\ln (16)=4 \ln (2)$.
		 1. RAS
		 1.  On a $0,125=\frac{1}{8}$ donc $\ln 0,125=-\ln 8=-3 \ln 2$.
		 1. RAS
		 1. On a $72=8 \times 9=2^{3}  \times 3^{2}$ donc $\ln (72)-2 \ln (3)=(3
		    \ln (2)+2 \ln (3))-2 \ln (3)=3 \ln (2)$.
			

	
!!! {{ exercice()}}

    === "Énoncé"

        Calculer les nombres suivants en fonction de $\ln (2), \ln (3)$ et $\ln(5)$.

		1. $\ln \left(\dfrac{1}{12}\right)$
		1. $\ln (500)$
		1. $\ln (2,25)$
		1. $\ln \left(\dfrac{16}{25}\right)$
		1. $\ln (21)+2 \ln (14)-3 \ln (0,875)$.
		1. $\ln (6,25)$

	=== "Corrigé"

         1. RAS
		 1. RAS
		 1. On a $0,875=\frac{7}{8}$ donc $ \begin{aligned} \ln (21)+2 \ln (14)-3
		    \ln (0,875)  & =(\ln  (3)+\ln (7))+2(\ln (2)+\ln  (7))-3(\ln (7)-\ln
		    (8)) \\ & =\ln (3)+2 \ln (3)+3 \times 3 \ln (2)=3 \ln (3)+11 \ln (2)
		    \end{aligned}$



!!! {{ exercice()}}

    === "Énoncé"

        Calculer la somme suivante en fonction de $\ln (2), \ln (3)$ et $\ln (5)$.

        $$
		\ln                                   \left(\dfrac{1}{2}\right)+\ln
		\left(\dfrac{2}{3}\right)+\cdots+\ln
		\left(\dfrac{98}{99}\right)+\ln \left(\dfrac{99}{100}\right)
		$$


	=== "Corrigé"

         On  appelle $A$  ce nombre.  On a  $ A=(\ln  (1)-\ln (2))+(\ln  (2)-\ln
         (3))+\cdots+(\ln (98)-\ln (99))+(\ln (99)-\ln (100))$
		 
		 donc en simplifiant les termes deux par deux finalement il reste $A=\ln
		 1-\ln 100$, c'est-à-dire $A=-\ln 100$ où $100=2^{2} \times 5^{2}$, d'où
		 le résultat $A=-2(\ln  2+\ln 5)$ On peut écrire  plus rigoureusement ce
		 calcul    :    $    \begin{aligned}   A    &    =\sum_{k=1}^{99}    \ln
		 \left(\frac{k}{k+1}\right)=\sum_{k=1}^{99}(\ln  (k)-\ln   (k+1))  \\  &
		 =\sum_{k=1}^{99} \ln (k)-\sum_{k=1}^{99}  \ln (k+1)=\sum_{k=1}^{99} \ln
		 (k)-\sum_{j=2}^{100} \ln (j) \end{aligned}$
		 
		 en effectuant le changement d'indice $j=k+1$ d'où finalement $A=\ln (1)-\ln (100)=-2(\ln (2)+\ln (5))$.


## Exponentielles


!!! {{ exercice()}}

    === "Énoncé"

        Écrire les nombres suivants le plus simplement possible.

		1. $\mathrm{e}^{3 \ln (2)}$
		1. $\mathrm{e}^{-2 \ln (3)}$
		1. $\ln (\sqrt{e})$
		1. $\ln \left(\mathrm{e}^{-\dfrac{1}{2}}\right)$
		1. $\ln \left(\mathrm{e}^{\dfrac{1}{3}}\right)$
		1. $\mathrm{e}^{\ln (3)-\ln (2)}$
	    1. $-\mathrm{e}^{-\ln \left(\dfrac{1}{2}\right)}$
		1. $\ln \left(\dfrac{1}{\mathrm{e}^{17}}\right)$
		1. $\mathrm{e}^{-\ln (\ln (2))}$
		1. $\ln \left(\sqrt{\mathrm{e}^{4}}\right)-\ln \left(\sqrt{\mathrm{e}^{2}}\right)$

	=== "Corrigé"

         1. RAS
		 1. RAS
		 1. RAS
		 1. RAS
		 1. RAS
		 1. RAS
		 1. RAS
		 1. On a $\mathrm{e}^{-\ln (\ln (2))}=\mathrm{e}^{(-1) \ln (\ln (2))}=(\ln (2))^{-1}=\frac{1}{\ln (2)}$.


## Étude de fonctions

!!! {{ exercice("Parité")}}

    === "Énoncé"

        Étudier la parité des fonctions suivantes.

		1. $f_{1}: x \longmapsto \ln \left(\dfrac{2022+x}{2022-x}\right)$
		1. $f_{2}: x \longmapsto \ln \left(x+\sqrt{x^{2}+1}\right)$
		1. $f_{3}: x \longmapsto \dfrac{\mathrm{e}^{2 x}-1}{\mathrm{e}^{2 x}+1}$
		1.               $f_{4}:                x               \longmapsto
		   \dfrac{\mathrm{e}^{x}-\mathrm{e}^{-x}}{\mathrm{e}^{x}+\mathrm{e}^{-x}}$ 

	=== "Corrigé"

         1.  $\quad  f_{1}$ est définie sur  ] - 2022,+2022[ qui  est symétrique
             par  rapport  à  0  et $  \forall  x  \in]-2022,+2022\left[,  \quad
             f(-x)=\ln                    \left(\frac{2022-x}{2022+x}\right)=\ln
             \left(\frac{1}{\frac{2022+x}{2022-x}}\right)=-\ln
             \left(\frac{2022+x}{2022-x}\right)=-f_{1}(x) .\right.$ 
		 1. On a $\forall x \in \mathbb{R}, \quad x \leqslant|x|<\sqrt{x^{2}+1}$
		    donc $f_{2}$ est définie sur $\mathbb{R}$ et pour tout réel $x$ on a
		    $\begin{aligned}f_{2}(-x)  & =\ln  \left(-x+\sqrt{(-x)^{2}+1}\right)
		    \\&      =\ln      \left(-x+\sqrt{x^{2}+1}\right)      \\&      =\ln
		    \left(\frac{\left(-x+\sqrt{x^{2}+1}\right)\left(x+\sqrt{x^{2}+1}\right)}{x+\sqrt{x^{2}+1}}\right)
		    \\&                                                             =\ln
		    \left(\frac{-x^{2}+\left(x^{2}+1\right)}{x+\sqrt{x^{2}+1}}\right)=\ln
		    \left(\frac{1}{x+\sqrt{x^{2}+1}}\right)=-f_{2}(x) .\end{aligned}$ 



!!! {{ exercice("Limite d'une fonction")}}

    === "Énoncé"

        On            note           $f:            x           \longmapsto
        \dfrac{\mathrm{e}^{x}-\mathrm{e}^{-x}}{\mathrm{e}^{x}+\mathrm{e}^{-x}}$. 

		1. Déterminer la limite de $f$ en $+\infty$,
		1. Déterminer la limite de $f$ en $-\infty$,


	=== "Corrigé"

         sol


!!! {{ exercice()}}

    === "Énoncé"

        On note $f: x \longmapsto \ln (1+x)$.

        Calculer et simplifier  les expressions suivantes pour  tout $x \in
        \mathbb{R}$ pour lequel elles sont définies. 

		1. $f\left(2 \mathrm{e}^{x}-1\right)$
		1. $\dfrac{1}{2} f\left(x^{2}-2 x\right)$
		1. $\mathrm{e}^{x-\dfrac{1}{2} f(x)}$
		1. $x f^{\prime}(x)-1$

	=== "Corrigé"

         sol

## Équations, inéquations

!!! {{ exercice()}}

    === "Énoncé"

        Résoudre les équations et inéquations suivantes (d'inconnue $x$ ).

		1. $\mathrm{e}^{3 x-5} \geqslant 12$

		1. $1 \leqslant \mathrm{e}^{-x^{2}+x}$

		1. $\mathrm{e}^{1+\ln x} \geqslant 2$

		1. $\mathrm{e}^{-6 x} \leqslant \sqrt{\mathrm{e}}$ 
		

	=== "Corrigé"

         sol




	
## Réponses mélangées



$${\small
\begin{aligned}
& \text { impaire impaire } \quad-17 \quad \ln |x-1| \quad-\dfrac{1}{1+x} \quad-2 \ln (5)+4 \ln (2) \quad 8 \quad 4 \ln (2) \\
& x \geqslant \dfrac{\ln (12)+5}{3} \quad-\ln (3)-2 \ln (2) \quad 2 \ln (5)-2 \ln (2) \quad x \geqslant \dfrac{2}{e} \quad \text { impaire } \quad \text { impaire } \\
& \dfrac{1}{2} \ln (2) \quad \dfrac{1}{\ln (2)} \quad 1 \quad x \in[0,1] \quad-1 \quad 3 \ln (5)+2 \ln (2) \quad 2 \ln (2)+2 \ln (3) \\
& 3 \ln (2) \quad \dfrac{3}{2} \quad-\dfrac{1}{2} \quad \dfrac{1}{3} \quad 9 \ln (2) \quad x \geqslant-\dfrac{1}{12} \quad \dfrac{1}{9} \quad-2 \ln (2)-2 \ln (5) \\
& \dfrac{\mathrm{e}^{x}}{\sqrt{1+x}} \quad 2 \ln (3)-2 \ln (2) \quad \dfrac{1}{2} \quad 1 \quad x+\ln 2 \quad-2 \quad \ln (3)+11 \ln (2) \quad-3 \ln (2)
\end{aligned}}
$$



## Exercices supplémentaires


!!! {{ exercice()}}

    === "Énoncé"

	    Développez et réduisez au maximum les expressions suivantes :
		
		- $e^{x}e^{-x}$
		- $e^{x}e^{-x+1}$
		- $ee^{-x}$
		- $\left(e^{-x}\right)^2$
		- $\dfrac{e^{2x}}{e^{2-x}}$
		- $\dfrac{\left(e^x\right)^3}{e^{2x}}$
		- $e^x \left(e^x+e^{-x}\right)$
		- $\left(e^x\right)^5 \left(e^{-2x}\right)^2$
		- $e^{-3x+1}\left(e^x\right)^3$
		- $\sqrt{e^{-2x}}$
		- $\dfrac{e^{-4x}e}{\left(e^{-x}\right)^2}$
		- $\left(e^x+e^{-x}\right)^2-\left(e^x-e^{-x}\right)^2$
		- $\left(e^x-e^{-x}\right)^2-e^{-x}\left(e^{3x}-e^{-x}\right)$
		- $\left(e^x-e^{-x}\right)\left(e^{2x}+e^x+1\right)$

	=== "Corrigé"

        - $e^xe^{-x}=e^{x-x}=1$
		- $e^xe^{-x+1}=e^{x-x+1}=e^1=e$
		- $ee^{-x}=e^{1-x}$
		- $(e^{-x})^2=e^{-2x}$
		- $e^{2x}/e^{2-x}=e^{2x-2+x}=e^{3x-2}$
		- $(e^x)^3/e^{2x}=e^{3x-2x}=e^x$
		- $e^x(e^x+e^{-x})=e^{2x}+1$
		- $\left(e^x\right)^5 \left(e^{-2x}\right)^2=e^{5x}e^{-4x}=e^{5x-4x}=e^x$
		- $e^{-3x+1}\left(e^x\right)^3=e^{-3x+1+3x}=e$
		- $\sqrt{e^{-2x}}=e^{-2x/2}=e^{-x}$
		- $\dfrac{e^{-4x}e}{\left(e^{-x}\right)^2}=e^{-4x+1+2x}=e^{-2x+1}$
		- $\left(e^x+e^{-x}\right)^2-\left(e^x-e^{-x}\right)^2=(e^x+e^{-x}+e^x-e^{-x})(e^x+e^{-x}-e^x+e^{-x})=(2e^x)(2e^{-x})=4e^{x-x}=4$
		- $\left(e^x-e^{-x}\right)^2-e^{-x}\left(e^{3x}-e^{-x}\right)=e^{2x}-2e^{x-x}+e^{-2x}-e^{3x-x}+e^{-x-x}=2e^{-2x}-2$
		- $\left(e^x-e^{-x}\right)\left(e^{2x}+e^x+1\right)=e^{3x}+e^{2x}+e^x-e^{x}-1-e^{-x}=e^{3x}+e^{2x}-1-e^{-x}$


!!! {{ exercice()}}

    === "Énoncé"

        Calculez  et factorisez  les dérivées  et les  limites quand  elles
        existent aux bornes des 
		ensembles de définition des fonctions définies par les expressions suivantes :
		
		- $f_1(x)=e^x+x^2+1$
		- $f_2(x)=5e^x+5xe^x$
		- $f_3(x)=\dfrac{e^x+1}{e^x-1}$
		- $f_4(x)=\dfrac{3x+1-e^x}{e^{x}}$
		- $f_5(x)=x^3e^{-x}$
		- $f_6(x)=\dfrac{x^2e^{x}}{x+1}$
		- $f_7(x)=\dfrac{e^{x}}{x}$
		- $f_8(x)=\dfrac{1}{e^x}$
		- $f_{9}(x)=\left(e^x\right)^2+\dfrac{1}{e^x}$
		- $f_{10}(x)=e^{-x}$
		- $f_{11}(x)=e^{4x+1}$
		- $f_{12}(x)=e^{5x^3+7x+4}$
		- $f_{13}(x)=(x+1)e^{-x+1}$
		- $f_{14}(x)=\dfrac{e^{2x}-1}{x}$
		- $f_{15}(x)=e^{\cos(x)}$


	=== "Corrigé"

         Vérifiez avec Python. Par exemple :
		 
		 ```python
		 In [169]: f1 = lambda x : sy.exp(x) + x**2 + 1

         In [170]: sy.limit(f1(x), x, sy.oo)
         Out[170]: oo

         In [171]: sy.limit(f1(x), x, -sy.oo)
         Out[171]: oo

         In [172]: sy.diff(f1(x), x)
         Out[172]: 
                x
         2.x + e 
		 ```





