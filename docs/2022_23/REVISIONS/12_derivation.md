{{ chapitre(11, "Dérivation", "Rappel N°")}}





## Application des formules usuelles
![Placeholder](./IMG/gaston4.png){ align=right width=270}

!!! {{ exercice("Avec des produits.")}}

    === "Énoncé"

        Déterminer l'expression de $f^{\prime}(x)$ pour $f$ définie par :

		1. $x \in \mathbb{R}$ et $f(x)=\left(x^{2}+3 x+2\right)(2 x-5)$.

		1. $x \in \mathbb{R}$ et $f(x)=\left(x^{3}+3 x+2\right)\left(x^{2}-5\right)$.

		1. $x \in \mathbb{R}$ et $f(x)=\left(x^{2}-2 x+6\right) \exp (2 x)$.

		1. $x \in] 2,+\infty\left[\right.$ et $f(x)=\left(3 x^{2}-x\right) \ln (x-2)$

	=== "Corrigé"

         1. $ f^{\prime}(x)=(2 x+3)(2 x-5)+\left(x^{2}+3 x+2\right)
            \times 2=6 x^{2}+2 x-11$.
		 1.                        $                       f^{\prime}(x)=\left(3
		    x^{2}+3\right)\left(x^{2}-5\right)+\left(x^{3}+3 x+2\right) \times 2
		    x=5 x^{4}-6 x^{2}+4 x-15$.
		 1.  $f^{\prime}(x)=(2 x-2) \exp (2 x)+\left(x^{2}-2 x+6\right) \times 2
		     \exp (2 x)=\left(2 x^{2}-2 x+10\right) \exp (2 x)$. 
		 1. $f^{\prime}(x)=(6 x-1) \ln (x-2)+\left(3 x^{2}-x\right) \times \frac{1}{x-2}=(6 x-1) \ln (x-2)+\frac{3 x^{2}-x}{x-2}$.
			 
!!! {{ exercice("Avec des puissances.")}}

    === "Énoncé"

        Déterminer l'expression de $f^{\prime}(x)$ pour $f$ définie par :

		1. $x \in \mathbb{R}$ et $f(x)=\left(x^{2}-5 x\right)^{5}$.

		1. $x \in \mathbb{R}$ et $f(x)=\left(2 x^{3}+4 x-1\right)^{2}$.

		1. $x \in \mathbb{R}$ et $f(x)=(\sin (x)+2 \cos (x))^{2}$

		1. $x \in \mathbb{R}$ et $f(x)=(3 \cos (x)-\sin (x))^{3}$

	=== "Corrigé"

         1.  $f^{\prime}(x)=5\left(x^{2}-5 x\right)^{4}(2 x-5)$. 
		 1.       $f^{\prime}(x)=2\left(2        x^{3}+4       x-1\right)\left(6
		    x^{2}+4\right)=4\left(2 x^{3}+4 x-1\right)\left(3 x^{2}+2\right)$.
		 1. On calcule 
		 
		    $ \begin{aligned}f^{\prime}(x) & =2(\sin (x)+2 \cos (x))(\cos (x)-2
		    \sin (x))\\ &=2\left(\sin (x) \cos (x)-2 \sin ^{2}(x)+2 \cos ^{2}(x)-4
		    \cos (x) \sin (x)\right. \\& =-6  \cos (x) \sin (x)-4 \sin ^{2}(x)+4
		    \cos  ^{2}(x)\\&=-6 \cos  (x)  \sin (x)-4\left(1-\cos  ^{2}(x)\right)+4
		    \cos ^{2}(x) \\& =8 \cos ^{2}(x)-6 \cos (x) \sin (x)-4\end{aligned}$
		  1.  $  f^{\prime}(x)=3(3  \cos   (x)-\sin  (x))^{2}(-3  \sin  (x)-\cos
		     (x))$
			 
			 $f'(x) =-3(3 \cos (x)-\sin (x))^{2}(3 \sin (x)+\cos (x))$.
			 
			 En développant,  on trouve  : $f^{\prime}(x)=-54 \cos  ^{2}(x) \sin
			 (x)-78 \cos ^{3}(x)-9 \sin (x)+51 \cos (x)$.
			 



!!! {{ exercice("Avec des quotients")}}

    === "Énoncé"

        Déterminer l'expression de $f^{\prime}(x)$ pour $f$ définie par :

		1. $x \in \mathbb{R}$ et $f(x)=\dfrac{x^{2}+3 x}{2 \sin (x)+3}            $

		1. $x \in] 0,+\infty\left[\right.$ et $f(x)=\dfrac{\sqrt{x}}{3 x+2}           $

		1. $x \in \mathbb{R}$ et $f(x)=\dfrac{\cos (2 x+1)}{x^{2}+1}$.

		1. $x \in] 1,+\infty\left[\right.$ et $f(x)=\dfrac{2 x^{2}+3 x}{\ln (x)}$.

	=== "Corrigé"

         1.  $f^{\prime}(x)=\frac{(2 x+3)(2  \sin (x)+3)-\left(x^{2}+3  x\right)
            \times  2   \cos  (x)}{(2  \sin  (x)+3)^{2}}$.   En  développant  le
            numérateur, on  trouve $  f^{\prime}(x)=\frac{-2 x^{2} \cos  (x)+4 x
            \sin (x)-6 x \cos (x)+6 \sin (x)+6 x+9}{(2 \sin (x)+3)^{2}}$ 
		 1.  $ f^{\prime}(x)=\frac{\frac{1}{2  \sqrt{x}}(3 x+2)-\sqrt{x}  \times
		    3}{(3  x+2)^{2}}=\frac{\frac{3   x+2}{2  \sqrt{x}}-\frac{3  \sqrt{x}
		    \times  2  \sqrt{x}}{2  \sqrt{x}}}{(3 x+2)^{2}}=\frac{3  x+2-6  x}{2
		    \sqrt{x}(3 x+2)^{2}}=\frac{2-3 x}{2 \sqrt{x}(3 x+2)^{2}}$
		 1. $f^{\prime}(x)=\frac{-2 \sin (2 x+1) \times\left(x^{2}+1\right)-\cos
		    (2      x+1)      \times      2      x}{\left(x^{2}+1\right)^{2}}=-2
		    \frac{\left(x^{2}+1\right)     \sin     (2    x+1)+x     \cos     (2
		    x+1)}{\left(x^{2}+1\right)^{2}}$. 
		 1.  $  f^{\prime}(x)=\frac{(4  x+3) \ln  (x)-\left(2  x^{2}+3  x\right)
		    \frac{1}{x}}{(\ln  (x))^{2}}=\frac{(4   x+3)  \ln   (x)-2  x-3}{(\ln
		    (x))^{2}}$
			


## Avec des fonctions composées


!!! {{ exercice()}}

    === "Énoncé"

        Déterminer l'expression de $f^{\prime}(x)$ pour $f$ définie par :

		1. $x \in \mathbb{R}$ et $f(x)=\ln \left(x^{2}+1\right)$

		1. $x \in] 1,+\infty[$ et $f(x)=\ln (\ln (x))$

		1. $x \in \mathbb{R}$ et $f(x)=(2-x) \exp \left(x^{2}+x\right)$.

		1. $x \in \mathbb{R}$ et $f(x)=\exp (3 \sin (2 x))$.

		1. $x \in] 0, \pi[$ et $f(x)=\sqrt{\sin (x)}$.

		1. $x \in] 0,+\infty[$ et $f(x)=\sin (\sqrt{x})$.

	=== "Corrigé"

        1. $f^{\prime}(x)=\frac{2 x}{x^{2}+1}$. C'est une application directe de
           la formule de dérivation quand $f=\ln \circ u$.
		1.  $f^{\prime}(x)=\frac{1 / x}{\ln (x)}=\frac{1}{x \ln (x)}$
		1. $\begin{aligned}f^{\prime}(x) & =(-1) \exp \left(x^{2}+x\right)+(2-x)
		   \exp  \left(x^{2}+x\right)   \times(2  x+1)=(-1+(2-x)(2   x+1))  \exp
		   \left(x^{2}+x\right)  \\&   =\left(-1+4  x+2-2   x^{2}-x\right)  \exp
		   \left(x^{2}+x\right)=\left(-2      x^{2}+3       x-1\right)      \exp
		   \left(x^{2}+x\right) .\end{aligned}$ 
	    1. $f^{\prime}(x)=\exp (3  \sin (2 x))(3 \times 2 \cos  (2 x))=6 \cos (2
	       x) \exp (3 \sin (2 x))$.
	    1.   $f^{\prime}(x)=\frac{1}{2  \sqrt{\sin   (x)}}  \cos  (x)=\frac{\cos
	        (x)}{2 \sqrt{\sin (x)}}$.
		1.     $     f^{\prime}(x)=\cos     (\sqrt{x})     \times     \frac{1}{2
		   \sqrt{x}}=\frac{\cos (\sqrt{x})}{2 \sqrt{x}}$
		   


!!! {{ exercice()}}

    === "Énoncé"

        Déterminer l'expression de $f^{\prime}(x)$ pour $f$ définie par :

		1. $x \in \mathbb{R}^{*}$ et $f(x)=x^{2} \sin \left(\dfrac{1}{x}\right)           $

		1. $x \in]-3,3\left[\right.$ et $f(x)=\dfrac{x}{\sqrt{9-x^{2}}}$

		1. $x \in] 1,+\infty\left[\right.$ et $f(x)=\ln \left(\sqrt{\dfrac{x+1}{x-1}}\right)$

		1. $x \in] 0, \pi\left[\right.$ et $f(x)=\ln \left(\dfrac{\sin x}{x}\right)$.

	=== "Corrigé"

         1.    $f^{\prime}(x)=2  x   \sin  \left(\frac{1}{x}\right)+x^{2}   \cos
             \left(\frac{1}{x}\right)   \times\left(\frac{-1}{x^{2}}\right)=2  x
             \sin \left(\frac{1}{x}\right)-\cos \left(\frac{1}{x}\right)$
		 1.
		 1.    On  a   trois   fonctions   composées  à   la   suite  :   $f=\ln
		     (\sqrt{u}))$.  Donc on  a, en  appliquant deux  fois la  formule de
		     dérivée   d'une  fonction   composée  :   $f^{\prime}(x)=\frac{1}{2
		     \sqrt{u-x)}} \times u^{\prime}(x) \times \frac{1}{\sqrt{u(x)}}$. On
		     calcule   :   $   \begin{aligned}   f^{\prime}(x)   &   =\frac{1}{2
		     \sqrt{\frac{x+1}{x-1}}}     \times    \frac{1(x-1)-(x+1)     \times
		     1}{(x-1)^{2}}    \times     \frac{1}{\sqrt{\frac{x+1}{x-1}}}    \\&
		     =\frac{1}{2         \times          \frac{x+1}{x-1}}         \times
		     \frac{-2}{(x-1)^{2}}=\frac{-1}{(x+1)(x-1)}                      \\&
		     =\frac{-1}{x^{2}-1}=\frac{1}{1-x^{2}} .\end{aligned}$
		 1.  $f^{\prime}(x)=\frac{\cos (x)  \times x-\sin  (x) \times  1}{x^{2}}
		    \times \frac{x}{\sin (x)}=\frac{x \cos (x)-\sin (x)}{x \sin (x)}$.
			




## Dériver pour étudier une fonction


!!! {{ exercice()}}

    === "Énoncé"

        Calculer $f^{\prime}(x)$ et écrire le résultat sous forme factorisée.

		1. $x \in \mathbb{R} \backslash 3,-2$ et $f(x)=\dfrac{1}{3-x}+\dfrac{1}{2+x} .        $

		1. $x \in]-1,+\infty\left[\right.$ et $f(x)=x^{2}-\ln (x+1)$

		1. $x \in] 1,+\infty\left[\right.$ et $f(x)=\ln \left(x^{2}+x-2\right)-\dfrac{x+2}{x-1}$.

		1. $x \in]-1,+\infty\left[\right.$ et $f(x)=\dfrac{x}{x+1}+x-2 \ln (x+1)$.

		1. $x \in] 0, e[\cup] e,+\infty\left[\right.$ et $f(x)=\dfrac{1+\ln (x)}{1-\ln (x)}$. Réponses mélangées

	=== "Corrigé"

         1. $f^{\prime}(x)=\frac{-(-1)}{(3-x)^{2}}+\frac{-1}{(2+x)^{2}}=\frac{(2+x)^{2}-(3-x)^{2}}{(3-x)^{2}(2+x)^{2}}=\frac{10
            x-5}{(3-x)^{2}(2+x)^{2}}$.
		 1.   $f^{\prime}(x)=2  x-\frac{1}{x+1}=\frac{2   x(x+1)-1}{x+1}=\frac{2
		    x^{2}+2  x-1}{x+1}$. Pour  le trinôme  $2 x^{2}+2  x-1$, on  calcule
		    $\Delta=4-4   \times  2   \times(-1)=12$.  On   a  deux   racines  :
		    $x_{1}=\frac{-2-\sqrt{12}}{2           \times          2}=\frac{-2-2
		    \sqrt{3}}{4}=\frac{-1-\sqrt{3}}{2}   \quad  \text   {  et   }  \quad
		    x_{2}=\frac{-1+\sqrt{3}}{2} . $
			Enfin,                              on                             a
			$f^{\prime}(x)=\frac{2\left(x-\frac{-1-\sqrt{3}}{2}\right)\left(x-\frac{-1+\sqrt{3}}{2}\right.}{x+1}=\frac{2}{x+1}\left(x+\frac{1+\sqrt{3}}{2}\right)\left(x+\frac{1-\sqrt{3}}{2}\right)$.
			
			


	
## Réponses mélangées



$${\small
\begin{aligned}
& -2 \ \dfrac{\left(x^{2}+1\right)\ \sin (2 x+1)+x\ \cos (2 x+1)}{\left(x^{2}+1\right)^{2}} \quad 5\left(x^{2}-5 x\right)^{4}(2 x-5) \quad \dfrac{2}{x+1}\left(x+\dfrac{1+\sqrt{3}}{2}\right)\left(x+\dfrac{1-\sqrt{3}}{2}\right) \\
& \dfrac{1}{1-x^{2}} \quad \dfrac{9}{\left(9-x^{2}\right) \sqrt{9-x^{2}}} \quad\left(2 x^{2}-2 x+10\right) \exp (2 x) \quad \dfrac{(4 x+3) \ln (x)-2 x-3}{(\ln (x))^{2}} \quad \dfrac{x^{2}}{(x+1)^{2}} \\
& 2 x \sin \left(\dfrac{1}{x}\right)-\cos \left(\dfrac{1}{x}\right) \quad(6 x-1) \ln (x-2)+\dfrac{3 x^{2}-x}{x-2} \quad 8 \cos ^{2}(x)-6 \cos (x) \sin (x)-4 \\
& \dfrac{\cos (\sqrt{x})}{2 \sqrt{x}} \quad \dfrac{x \cos (x)-\sin (x)}{x \sin (x)} \quad \dfrac{(2 x+3)(2 \sin (x)+3)-\left(x^{2}+3 x\right) \times 2 \cos (x)}{(2 \sin (x)+3)^{2}} \quad 5 x^{4}-6 x^{2}+4 x-15 \\
& 6 \cos (2 x) \exp (3 \sin (2 x)) \quad-3(3 \cos (x)-\sin (x))^{2}(3 \sin (x)+\cos (x)) \quad \dfrac{2 x^{2}+2 x+5}{(x+2)(x-1)^{2}} \\
& \dfrac{\cos (x)}{2 \sqrt{\sin (x)}} \quad \dfrac{1}{x \ln (x)} \quad 4\left(2 x^{3}+4 x-1\right)\left(3 x^{2}+2\right) \quad \dfrac{2}{x(1-\ln (x))^{2}} \quad \dfrac{10 x-5}{(3-x)^{2}(2+x)^{2}} \\
& \left(-2 x^{2}+3 x-1\right) \exp \left(x^{2}+x\right) \quad \dfrac{2-3 x}{2 \sqrt{x}(3 x+2)^{2}} \quad 6 x^{2}+2 x-11 \quad \dfrac{2 x}{x^{2}+1}
\end{aligned}
}$$




## Exercice supplémentaire


!!! {{ exercice()}}

    === "Énoncé"

	    Calculez les dérivées des fonction définies par:
		
		- $f_1(x)=(x^2+x)^3$ 
		- $f_2(x)=(3x^2+4x-6)^4$
		- $f_3(x)=(3x^2+4x-6)^{-4}$
		- $f_4(x)=\frac{1}{(3x^2-5x+1)}$
		- $f_5(x)=\cos(\sqrt{x})$
		- $f_6(x)=\sqrt{\cos(x)}$
		- $f_7(x)=\bigl( \cos(x^2+5x+1) \bigr)^2$
		- $f_8(x)=\cos \bigl( (x^2+5x+1)^2 \bigr)$
		- $f_9(x)=\frac{1}{\cos \bigl( (x^2+5x+1)^2 \bigr)}$
		- $f_{10}(x)=\tan(x)$
		- $f_{11}(x)=\tan \left(\sqrt{x}\right)$
		- $f_{12}(x)=\sqrt{x+\sqrt{x^2+2x+1}}$
		- $f_{13}(x)=(\sqrt{x}+4)(\sqrt{x}-4)$
		- $f_{14}(x)=\frac{\sin(2x)}{\cos(3x)}$
		- $f_{15}(x)=\sqrt{\sqrt{\sqrt{x}}}$
		- $f_{16}(x)=\ln(\ln(\ln(\ln(x))))$

	=== "Corrigé"

         Vous  pourrez  vérifier  vos  calculs   à  la  machine.  On  obtient  facilement
         l'expression formelle de la dérivée d'une fonction: 
		 
		 ```python
		 In [1]: import sympy as sy
		 
		 In [2]: x,t = sy.symbols('x t')
		 
     	 In [3]: f = lambda x : (x**2 + 3*x)**2
		 
		 In [4]: sy.diff(f(x), x)
         Out[4]: (4*x + 6)*(x**2 + 3*x)

		 ```
 
         On  a deux  solutions  pour faire  de l'expression  de  la dérivée  une
         fonction:
		 
		 ```python
		 In [5]: fp1 = lambda x : sy.diff(f(t), t).subs(t,x)

	     In [6]: fp1(x)
         Out[6]: (4*x + 6)*(x**2 + 3*x)


         In [7]: fp1(t)
         Out[7]: (4*t + 6)*(t**2 + 3*t)

         In [8]: fp1(2)
         Out[8]: 140  
		
		 ```
		 
	    ou

	    ```python
		In [9]: fp2 = sy.lambdify(x, sy.diff(f(x),x))

        In [10]: fp2(x)
        Out[10]: (4*x + 6)*(x**2 + 3*x)

        In [11]: fp2(2)
        Out[11]: 140  
		```


!!! {{ exercice()}}

    === "Énoncé"

        Calculez  et  factorisez  les  dérivées et  les  limites  quand  elles existent aux bornes des
		ensembles de définition des fonctions définies par les expressions suivantes :
		
		- $f_1(x)=e^x+x^2+1$
		- $f_2(x)=5e^x+5xe^x$
		- $f_3(x)=\dfrac{e^x+1}{e^x-1}$
		- $f_4(x)=\dfrac{3x+1-e^x}{e^{x}}$
		- $f_5(x)=x^3e^{-x}$
		- $f_6(x)=\dfrac{x^2e^{x}}{x+1}$
		- $f_7(x)=\dfrac{e^{x}}{x}$
		- $f_8(x)=\dfrac{1}{e^x}$
		- $f_{9}(x)=\left(e^x\right)^2+\dfrac{1}{e^x}$
		- $f_{10}(x)=e^{-x}$
		- $f_{11}(x)=e^{4x+1}$
		- $f_{12}(x)=e^{5x^3+7x+4}$
		- $f_{13}(x)=(x+1)e^{-x+1}$
		- $f_{14}(x)=\dfrac{e^{2x}-1}{x}$
		- $f_{15}(x)=e^{\cos(x)}$


	=== "Corrigé"

         Vérifiez avec Python. Par exemple :
		 
		 ```python
		 In [169]: f1 = lambda x : sy.exp(x) + x**2 + 1

         In [170]: sy.limit(f1(x), x, sy.oo)
         Out[170]: oo

         In [171]: sy.limit(f1(x), x, -sy.oo)
         Out[171]: oo

         In [172]: sy.diff(f1(x), x)
         Out[172]: 
                x
         2.x + e 
		 ```

