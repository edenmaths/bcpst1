
![Placeholder](./IMG/python.jpeg){ align=right width=350}
{{ chapitre(1, "Python", "Rappel N°")}}




## Les prérequis

Vous  *devriez*  arriver  du  lycée   avec  de  nombreuses  compétences  en
Python. N'hésitez  pas à revoir  vos cours  car vous devriez  maîtriser les
notions suivantes:

- **en 2nde** :

    - variables de type entier, booléen, flottant, chaînes de caractères,
    - séquences d'instructions.
    - instruction conditionnelle (`if` `else`).
    - boucles `for` et `while`.
    - fonctions à un ou plusieurs paramètres.
    - Utilisation de `random` et `randint`.

- **en 1ère**:

    - générer une liste par ajouts successifs ou par compréhension,
      manipuler les éléments d'une liste, itérer sur les éléments d'une
      liste,...
    - les notions relatives aux types de variables et à l'affectation sont
       consolidées.
    - calcul des termes d'une suite, de sommes de termes, de seuil, calcul
      de factorielle,...
    - approche du nombre dérivé en écrivant la liste des coefficients
      directeurs des sécantes,...
    - contruction de l'exponentielle par la méthode d'Euler, détermination
      d'une valeur approchée à l'aide de la suite de terme général
      $(1+1/n)^n$,...
    - approximation de $\pi$ par la méthode d'Archimède, approximation de
      $\pi$ par la méthode de Monte-Carlo,...
    - calcul de l'espérance, la variance ou l'écart-type d'une variable
      aléatoire, simulation d'une variable aléatoire,...

- **en Maths complémetaires (terminale)**:

    - simulation d'une variable de Bernoulli ou d'un lancer de dé, de la
      somme de de $n$ variables aléatoires indépendantes et de même loi.
    - Méthode des rectangles et des trapèzes, de Monte-Carlo pour le calcul
      d'intégrales.
    - Méthodes de recherche de valeurs approchées d'une solution d'équation
      du type $f(x)=k$ : balayage, dichotomie, méthode de Newton.
    - Algorithme de Briggs pour le calcul de logarithmes.
    - Recherche de seuils. Pour une suite récurrente $u_{n+1}=f(u_n)$, calcul
      des termes successifs.
    - Résolution d'équations différentielles par la méthode d'Euler.
    - Calcul des coefficients binomiaux (triangle de Pascal), simulation
      d'une variable aléatoire de loi géométrique à partir du schéma de
      Bernoulli, simulation d'une loi exponentielle à partir d'une loi
      uniforme,...
	 
	 

## Environnement de travail et entraînement

![Placeholder](./IMG/martine2.png){ align=left width=250}
Il  existe  de  très  nombreux environnements  de  travail  adaptés  à
`Python` que vous devez avoir sur vos machines:

- [`Pyzo`](https://www.pyzo.org/)
- [`Thonny`](https://thonny.org/)

le plus souvent disponibles aux  concours et dans tous les lycées. 

Voici             le             [formulaire             du             concours
A-TB](https://www.concours-agro-veto.net/IMG/pdf/commandespythonecritatb_-_pub.pdf)
Agro-Véto et [celui du concours BCPST](https://www.concours-agro-veto.net/IMG/pdf/polypython_nb_141019.pdf).


Pour des exercices interactifs, rendez-vous sur
[cette             page](https://diraison.github.io/Pyvert/)             et
[celle-là](https://e-nsi.gitlab.io/pratique/N0/).



