{{ chapitre(11, "Intégration", "Rappel N°")}}






## Intégrales et aires algébriques


!!! info "Rappel"

    On  rappelle que  $\displaystyle\int_{a}^{b} f(x)  \mathrm{d} x$  est l'aire
    algébrique
	entre  la courbe  représentative de  $f$ et  l'axe des  abscisses du  repère
	lorsque les bornes sont dans le sens croissant.


![Placeholder](./IMG/martine3.png){ align=right width=300}
!!! {{ exercice()}}

    === "Énoncé"

        Sans chercher à calculer les intégrales suivantes, donner leur signe.

		1. $\displaystyle\int_{-2}^{3} x^{2}+\mathrm{e}^{x} \mathrm{~d} x$
		1. $\displaystyle\int_{5}^{-3}|\sin (7 x)| d x$
		1. $\displaystyle\int_{0}^{-1} \sin (x) \mathrm{d} x$

	=== "Corrigé"

         1. On intègre  une fonction positive et  les bornes sont «  dans le bon
            sens ».
		 1.  $\int_{5}^{-3}|\sin (7 x)|  \mathrm{d} x=-\int_{-3}^{5}|\sin (7 x)|
		     \mathrm{d} x$. Cette dernière intégrale a  ses bornes « dans le bon
		     sens », on peut l'interpréter comme une aire. Elle est positive car
		     on intègre une fonction positive.
		  1.  $\int_{0}^{-1}  \sin  (x)  \mathrm{d}  x=-\int_{-1}^{0}  \sin  (x)
		     \mathrm{d} x$. Cette dernière intégrale a  ses bornes « dans le bon
		     sens ", on peut l'interpréter comme  une aire. sin est négative sur
		     $[-\pi, 0]$ donc  sur $[-1,0], \int_{-1}^{0} \sin  x \mathrm{~d} x$
		     est donc négative.
			 


!!! {{ exercice()}}

    === "Énoncé"

        En se ramenant à des aires, calculer de tête les intégrales suivantes.

		1. $\displaystyle\int_{1}^{3} 7 \mathrm{~d} x  $
		1. $\displaystyle\int_{0}^{7} 3 x \mathrm{~d} x$
		1. $\displaystyle\int_{-2}^{2} \sin (x) \mathrm{d} x $
		1. $\displaystyle\int_{7}^{-3}-5 \mathrm{~d} x $
		1. $\displaystyle\int_{2}^{8}(1-2 x) d x$
		1. $\displaystyle\int_{-2}^{1}|x| \mathrm{d} x$

	=== "Corrigé"

         1. Il s'agit de l'aire d'un rectangle de largeur 2 et de longueur 7 .
		 1.   On  commence  par  mettre  les  bornes «  dans  le  bon  sens  $»:
		     \int_{7}^{-3}-5    \mathrm{~d}    x=-\int_{-3}^{7}-5    \mathrm{~d}
		     x=\int_{-3}^{7}  5 \mathrm{~d}  x$.  Cette  dernière intégrale  est
		     l'aire d'un rectangle dont les côtés mesurent 10 et 5 .
		 1. Il s'agit de l'aire du triangle dont les sommets sont l'origine $O$,
		    le point $A(7 ; 0)$ et $B(7 ; 21)$. Ce triangle est rectangle en $A$
		    et son aire est $\frac{1}{2} \times A O \times A B$.
		 1.  Les bornes  sont «dans  le  bon sens  », on  peut donc  interpréter
		    l'intégrale comme une aire  algébrique. Sur l'intervalle $[2,8]$, la
		    courbe de $f(x)=1-2  x$ est située sous l'axe  des abscisses, l'aire
		    algébrique sera  négative. Il s'agit  de calculer l'aire  du trapèze
		    rectangle dont les sommets  sont $A(2 ; 0), B(8 ;  0), C(8 ;-15)$ et
		    $D(2 ;-3)$. L'aire de ce trapèze rectangle est $\frac{1}{2} \times A
		    B \times(A D+B C)=\frac{1}{2} \times 6 \times(3+15)$.
		 1.   Avec  la  relation  de  Chasles,  on  a  $\int_{-2}^{2}  \sin  (x)
		     \mathrm{d} x=\int_{-2}^{0} \sin  (x) \mathrm{d} x+\int_{0}^{2} \sin
		     (x)  \mathrm{d}  x$.La  fonction  sinus étant  impaire,  les  aires
		     algébriques $\int_{-2}^{0} \sin (x)  \mathrm{d} x$ et $\int_{0}^{2}
		     \sin (x)  \mathrm{d} x$ sont opposées,  il suit que leur  somme est
		     nulle.
		  1. Les bornes étant «dans le bon sens », on interprète cette intégrale
		     comme  une  aire  algébrique.  Cette  aire  est  composée  de  deux
		     triangles rectangles (les intégrales de -2 à 0 et de 0 à 1 ).
			 


## Calcul d'intégrales


On rappelle  que si $F$ est  une primitive de $f$  alors $\displaystyle\int_{a}^{b} f(x)
\mathrm{d} x=F(b)-F(a)$, que l'on note $[F(x)]_{a}^{b}$. 
 

!!! {{ exercice("Polynômes")}}

    === "Énoncé"

        Calculer les intégrales suivantes.

		1. $\displaystyle\int_{-1}^{3} 2 \mathrm{~d} x       $
		1. $\displaystyle\int_{-1}^{1}\left(3 x^{5}-5 x^{3}\right) \mathrm{d} x$
		1. $\displaystyle\int_{1}^{3}(2 x-5) \mathrm{d} x$
		1. $\displaystyle\int_{0}^{1}\left(x^{5}-x^{4}\right) d x$
		1. $\displaystyle\int_{-2}^{0}\left(x^{2}+x+1\right) \mathrm{d} x$
		1. $\displaystyle\int_{1}^{-1} x^{100} \mathrm{~d} x$


	=== "Corrigé"

         1.  Les bornes étant «dans le bon sens », on interprète cette intégrale
             comme une aire algébrique d'un rectangle.
		 1.     $     \int_{1}^{3}(2     x-5)     \mathrm{d}     x=\left[x^{2}-5
		    x\right]_{1}^{3}=\left(3^{2}-15\right)-\left(1^{2}-5\right)=-2$.
		 1.    $\int_{-2}^{0}    x^{2}+x+1    \mathrm{~d}    x=\left[\frac{1}{3}
		    x^{3}+\frac{1}{2}
		    x^{2}+x\right]_{-2}^{0}=0-\left(\frac{1}{3}(-2)^{3}+\frac{1}{2}(-2)^{2}-2\right)=\frac{8}{3}$.
		 1.  La fonction  intégrée est  impaire,  son intégrale  sur un  segment
		    symétrique par rapport à 0 est donc nulle.
		 1.     $\int_{0}^{1}   x^{5}-x^{4}    \mathrm{~d}   x=\left[\frac{1}{6}
		     x^{6}-\frac{1}{5}
		     x^{5}\right]_{0}^{1}=\frac{1}{6}-\frac{1}{5}=-\frac{1}{30}$
		 1.    $\int_{1}^{-1}    x^{100}    \mathrm{~d}    x=\left[\frac{1}{101}
		    x^{101}\right]_{1}^{-1}=-\frac{2}{101}$
			

!!! {{ exercice("Fonctions usuelles")}}

    === "Énoncé"

        Calculer.

		1. $\displaystyle\int_{-\frac{\pi}{6}}^{\frac{\pi}{6}} \sin (x) \mathrm{d} x $
		1. $\displaystyle\int_{1}^{2} \dfrac{\mathrm{d} x}{x^{2}}$
		1. $\displaystyle\int_{-3}^{2} e^{x} d x$
		1. $\displaystyle\int_{-\frac{\pi}{6}}^{\frac{\pi}{6}} \cos (x) \mathrm{d} x$
		1. $\displaystyle\int_{1}^{100} \dfrac{1}{\sqrt{x}} \mathrm{~d} x $
		1. $\displaystyle\int_{-3}^{-1} \dfrac{\mathrm{d} x}{x}$

	=== "Corrigé"

         1.   La fonction  intégrée est  impaire, son  intégrale sur  un segment
             symétrique par rapport à 0 est donc nulle.
		 1.  $\int_{-\frac{\pi}{6}}^{\frac{\pi}{6}} \cos  x \mathrm{~d}  x=[\sin
		    x]_{-\frac{\pi}{6}}^{\frac{\pi}{6}}=2                           \sin
		    \left(\frac{\pi}{6}\right)=1$
		 1.                     $\int_{1}^{2}                   \frac{\mathrm{d}
		     x}{x^{2}}=\left[-\frac{1}{x}\right]_{1}^{2}=-\frac{1}{2}+1=\frac{1}{2}$.
		 1.    $    \int_{1}^{100}     \frac{1}{\sqrt{x}}    \mathrm{~d}    x=[2
		    \sqrt{x}]_{1}^{100}=18$
		 1.          $\int_{-3}^{2}          \mathrm{e}^{x}          \mathrm{~d}
		    x=\left[\mathrm{e}^{x}\right]_{-3}^{2}=\mathrm{e}^{2}-\mathrm{e}^{-3}$.
		 1. $\int_{-3}^{-1} \frac{\mathrm{d} x}{x}=[\ln |x|]_{-3}^{-1}=-\ln 3$.
		 


!!! {{ exercice()}}

    === "Énoncé"

        Calculer les intégrales suivantes.

		1. $\displaystyle\int_{-1}^{2}(2 x+1)^{3} \mathrm{~d} x$
		1. $\displaystyle\int_{-\frac{\pi}{12}}^{\frac{\pi}{6}} \sin (3 x) \mathrm{d} x$
		1. $\displaystyle\int_{-2}^{4} \mathrm{e}^{\dfrac{1}{2} x+1} \mathrm{~d} x$
		1. $\displaystyle\int_{0}^{33} \dfrac{1}{\sqrt{3 x+1}} d x$
		1. $\displaystyle\int_{0}^{1} \dfrac{\mathrm{d} x}{\pi x+2}$
		1. $\displaystyle\int_{-\pi}^{\frac{\pi}{2}} \cos \left(\dfrac{\pi}{3}-x\right) \mathrm{d} x$


	=== "Corrigé"

         1.    $\int_{-1}^{2}(2   x+1)^{3}   \mathrm{~d}   x=\left[\frac{1}{8}(2
             x+1)^{4}\right]_{-1}^{2}=\frac{625}{8}-\frac{1}{8}=78$.
		 1.  $\int_{-2}^{4} \mathrm{e}^{\frac{1}{2}  x+1} \mathrm{~d}  x=\left[2
		    \mathrm{e}^{\frac{1}{2}
		    x+1}\right]_{-2}^{4}=2\left(\mathrm{e}^{3}-1\right)$.
		 1. $\int_{0}^{1}  \frac{\mathrm{d} x}{\pi  x+2}=\left[\frac{1}{\pi} \ln
		    |\pi              x+2|\right]_{0}^{1}=\frac{1}{\pi}              \ln
		    \left(\frac{\pi+2}{2}\right)$.
		 1.  $  \int_{-\frac{\pi}{12}}^{\frac{\pi}{6}}  \sin  (3  x)  \mathrm{d}
		    x=\left[-\frac{1}{3}                     \cos                     (3
		    x)\right]_{-\frac{\pi}{12}}^{\frac{\pi}{6}}=\frac{1}{3}
		    \frac{\sqrt{2}}{2}$.
		 1.       $\int_{0}^{33}     \frac{1}{\sqrt{3      x+1}}     \mathrm{~d}
		     x=\left[\frac{2}{3}                                         \sqrt{3
		     x+1}\right]_{0}^{33}=\frac{2}{3}(10-1)=6$.
		 1.   $\int_{-\pi}^{\frac{\pi}{2}}   \cos   \left(\frac{\pi}{3}-x\right)
		    \mathrm{d}                                             x=\left[-\sin
		    \left(\frac{\pi}{3}-x\right)\right]_{-\pi}^{\frac{\pi}{2}}=-\sin
		    \left(-\frac{\pi}{6}\right)+\sin                       \left(\frac{4
		    \pi}{3}\right)=\frac{1}{2}-\frac{\sqrt{3}}{2}$.
			







	
## Réponses mélangées (il y en a en trop...)




$${\small
\begin{aligned}
&\dfrac{\sqrt{2}}{6}\  \dfrac{2 \pi}{9}\  {\left(1-\dfrac{1}{2}\right)}\ 8\  50\
\dfrac{99}{\ln   10}  \  0 \  \dfrac{\pi}{4}  \  \dfrac{1}{2} \  \dfrac{1}{384}\
\dfrac{5}{2} \ \mathrm{ Négatif }\ \\
&\dfrac{2}{3}  \      \dfrac{1}{2}-\dfrac{\sqrt{3}}{2} \       \mathrm{e}^{2}\
\dfrac{147}{2}\ -\dfrac{1}{30} \ \mathrm{ Positif }\ 0 \ 78\ 0\ 0\ 0\ \mathrm{e}^{2}-\mathrm{e}^{-3} \\
&\dfrac{5}{8}  \    -\dfrac{1}{3}  \    14 \      3 \     \mathrm{e}-4   \   \ln
\left(\dfrac{2}{\sqrt{3}}\right) \ \dfrac{1}{2}-\dfrac{1}{\mathrm{e}+1} \ -2\
\mathrm{Positif}\ 18\ 6\ \dfrac{\mathrm{e}-\dfrac{1}{\mathrm{e}}}{2} \\
& -\dfrac{2}{101}\  1\  0\  -54\  -\ln 3\  2\left(\mathrm{e}^{3}-1\right)\   \dfrac{7}{48}  \dfrac{1}{\pi}\ \ln \left(1+\dfrac{\pi}{2}\right)\  \dfrac{17}{2} \ \dfrac{8}{3}
\end{aligned}}
$$


