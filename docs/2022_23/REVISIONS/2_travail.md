# Pourquoi et comment travailler cet été ?




![Placeholder](./IMG/spirou.png){ align=left width=250} Certains d'entre vous avaient peut-être choisi l'option mathématiques
complémentaires au lycée. Nul n'est parfait. Ne vous inquiétez pas, il vous
reste deux mois sans cours cet été pour vous remettre à flot. Vous serez la
troisième vague d'étudiantes et d'étudiants issus de la réforme du lycée. 
Suite à  ces «  aménagements »La  situation a été  assez critique  pour les
BCPST en maths au niveau national. Il 
faut donc  cette année prendre  les devants  pour éviter de  passer l'année
avec 3/20 de moyenne de classe. Mais  vous êtes à l'EDEN maintenant et vous
aurez  droit chaque  semaine  à  **2 heures  de  soutien supplémentaires**  par
rapport aux autres prépas. 

Contrairement aux 15  années récédentes, le **travail estival** ne  va pas être
optionnel mais  **vital** pour vous.  Le mois de  juillet 2023 marque  le début
d'une période d'intense travail qui se  terminera dans deux ans. Il ne faut
pas arriver  en septembre avec  d'énormes lacunes car chaque  semaine, vous
recevrez un flot de cours qu'il faudra assimiler vite et bien chaque jour (27
chapitres - 700 pages).  Le secret sera dans l'organisation de votre travail.  Les
premières semaines serviront  à mettre au point votre  plan hebdomadaire de
travail auquel vous devrez obéir aveuglément jusqu'au 30 juin 2024. 

![Placeholder](./IMG/gaston2.png){ align=right width=250} C'est
le secret de votre réussite. 
En attendant, il va falloir combler les nombreuses lacunes en mathématiques
qui remontent souvent au collège :

- d'un point de vue capacités de calcul ;
- d'un point de vue rigueur des raisonnements ;
-  d'un point  de vue  prise de  conscience  de la  nature et  du rôle  des
  démonstrations qui sont le cœur du travail ;
- d'un  point de vue prise  de conscience du  rôle vital du travail  sur le
  cours  : vous  pouvez jeter  un  coup d'œil  sur les  chapitres qui  vous
  attendent pour vous rendre compte de l'étendue et du contenu des cours.
- d'un  point de  vue informatique  : ce  n'est plus  du tout  une activité
  optionnelle mais bien un domaine qui représentera un pan important de vos
  notes aux concours et de votre future carrière scientifique.
