{{ chapitre(2, "Fractions", "Rappel N°")}}


## Au collège
![Placeholder](./IMG/martine_nulle.png){ align=right width=350}


!!! info "Questions, indications et réponses"

    Chaque exercice est corrigé mais surtout ne regardez le corrigé qu'en
    dernier  recours !  La plupart  du  temps, vous  trouverez  à la  fin de  la
    première série d'énoncés les réponses mélangées.
	
	Pour les QCM, vous cocherez vos  réponses et obtiendrez votre score. Dans un
	second onglet vous  pourrez vérifier chaque réponse puis  ensuite vous aurez
	les corrigés.


!!! note "Petits rappels"

	Pour tous réels non nuls $a, b, c, d, k$ :

	$$
	\dfrac{a}{b} \times \dfrac{c}{d}=\dfrac{a \times  c}{b \times d}, \quad
	\dfrac{k a}{k b}=\dfrac{a}{b}
	$$
	
	$$
	\dfrac{1}{\left(\dfrac{c}{d}\right)}=\dfrac{d}{c},                \quad
	\dfrac{\left(\dfrac{a}{b}\right)}{\left(\dfrac{c}{d}\right)}=\dfrac{a
	d}{b c}
	$$
	
	$$
	 \dfrac{a}{d}+\dfrac{c}{d}=\dfrac{a+c}{d}. 
	$$


!!! 



## Visualiser une fraction

No comment...Mais bon, vouys pouvez toujours aller voir [ceci](https://phet.colorado.edu/sims/html/fractions-intro/latest/fractions-intro_all.html?locale=fr)...


## Calculs dans l'ensemble des rationnels


!!! {{ exercice("Simplification de fractions")}} 

    === "Énoncé"
         
         Simplifier  les  fractions suivantes  (la  lettre  $k$ désigne  un
         entier naturel non nul). 
		 
		 1. $\dfrac{32}{40}$
		 2. $\dfrac{27^{-1} \times 4^{2}}{3^{-4} \times 2^{4}}$
		 3. $8^{3} \times \dfrac{1}{4^{2}}$
		 4. $\dfrac{(-2)^{2 k+1} \times 3^{2 k-1}}{4^{k} \times 3^{-k+1}}$

	=== "Réponses"

         1. $\frac{32}{40}=\frac{8 \times 4}{8 \times 5}=\frac{4}{5}$
		 2.  $ 8^{3} \times \frac{1}{4^{2}}=(2 \times 4)^{3} \times 
		    \frac{1}{4^{2}}=2^{3} \times 4^{3} \times \frac{1}{4^{2}}=2^{3} 
	 	    \times 4=2^{5}$ 
		 3.  $\frac{27^{-1} \times 4^{2}}{3^{-4} \times 
		     2^{4}}=\frac{\left(3^{3}\right)^{-1}
		     \times\left(2^{2}\right)^{2}}{3^{-4} \times 
		     2^{4}}=\frac{3^{4}}{3^{3}}=3$ 
		 4. On a $: \frac{(-2)^{2 k+1} \times 3^{2 k-1}}{4^{k} \times 3^{-k+1}}=\frac{(-2) \times(-2)^{2 k} \times 3^{2 k} \times 3^{-1}}{4^{k} \times 3^{-k} \times 3}=\frac{(-2) \times 4^{k} \times 3^{2 k} \times 3^{k}}{4^{k} \times 3^{2}}=-2 \times 3^{3 k-2}$
	
!!! {{ exercice("Sommes, produits, quotients, puissances")}}

    === "Énoncé"

         Écrire les nombres suivants sous forme d'une fraction irréductible.
		 
		 1.  $\dfrac{2}{4}-\dfrac{1}{3}$
		 1.  $\dfrac{36}{25} \times \dfrac{15}{12} \times 5$
		 1.  $\dfrac{2}{3}-0,2$
		 1. $-\dfrac{2}{15} \div\left(-\dfrac{6}{5}\right)$


	=== "Réponses"

         1.  On  met  au même  dénominateur  :  $\frac{2}{4}-\frac{1}{3}=\frac{2
            \times    3}{4     \times    3}-\frac{1    \times     4}{3    \times
            4}=\frac{6}{12}-\frac{4}{12}=\frac{6-4}{12}=\frac{2}{12}=\frac{1}{6}$.
			
		 2. On transforme 0,2 en fraction et on met au même dénominateur : $\frac{2}{3}-0,2=\frac{2}{3}-\frac{2}{10}=\frac{2 \times 10}{3 \times 10}-\frac{2 \times 3}{10 \times 3}=\frac{20}{30}-\frac{6}{30}=\frac{20-6}{30}=\frac{14}{30}=\frac{7 \times 2}{15 \times 2}=\frac{7}{15}$

	     3. Pour  multiplier des fractions,  on multiplie les  numérateurs entre
	        eux et les dénominateurs entre eux : $\frac{36}{25} \times \frac{15}{12} \times 5=\frac{36}{25} \times \frac{15}{12} \times \frac{5}{1}=\frac{36 \times 15 \times 5}{25 \times 12 \times 1}=\frac{12 \times 3 \times 5 \times 3 \times 5}{5 \times 5 \times 12 \times 1}=\frac{3 \times 3}{1}=\frac{9}{1}=9$
		 4.  Pour  diviser une fraction  par une autre,  on la multiplie  par la
		     fraction  inverse   de  la   deuxième  fraction   :  $-\frac{2}{15}
		     \div\left(-\frac{6}{5}\right)=-\frac{2}{15}
		     \times\left(-\frac{5}{6}\right)=\frac{2}{15}                 \times
		     \frac{5}{6}=\frac{2  \times  5}{15  \times 6}=\frac{2  \times  5}{3
		     \times 5 \times 2 \times 3}=\frac{1}{9}$
			 
			 
!!! {{ exercice("Sommes, produits, quotients, puissances")}}

    === "Énoncé"

         Écrire les nombres suivants sous forme d'une fraction irréductible.

	     1.        $(2      \times       3       \times      5       \times
	         7)\left(\dfrac{1}{2}+\dfrac{1}{3}+\dfrac{1}{5}+\dfrac{1}{7}\right)$ 
		 2. $\left(\dfrac{136}{15}-\dfrac{28}{5}+\dfrac{62}{10}\right) \times \dfrac{21}{24}$
		 3.  $\dfrac{5^{10}  \times 7^{3}-25^{5} \times  49^{2}}{(125 \times
		     7)^{3}+5^{9} \times 14^{3}}$ 
		 4. $\dfrac{1978 \times 1979+1980 \times 21+1958}{1980 \times 1979-1978 \times 1979}$


	=== "Réponses"

         1. On développe : $
			 \begin{aligned}(2      \times     3      \times     5      \times
		     7)\left(\frac{1}{2}+\frac{1}{3}+\frac{1}{5}+\frac{1}{7}\right)    &
		     =\frac{2 \times 3  \times 5 \times 7}{2}+\frac{2 \times  3 \times 5
		     \times 7}{3}+\frac{2 \times 3  \times 5 \times 7}{5}+\frac{2 \times
		     3 \times 5 \times 7}{7} \\& =3  \times 5 \times 7+2 \times 5 \times
		     7+2  \times  3  \times   7+2  \times  3  \times  5\\&=105+70+42+30=247
		     .\end{aligned}$ 
			 
		  2.  On simplifie  d'abord, puis  on applique  les règles  de calcul  :
		     $\begin{aligned}\left(\frac{136}{15}-\frac{28}{5}+\frac{62}{10}\right)
		     \times                        \frac{21}{24}                       &
		     =\left(\frac{136}{15}-\frac{28}{5}+\frac{31}{5}\right)       \times
		     \frac{7}{8}  \\&   =\left(\frac{136}{15}+\frac{3}{5}\right)  \times
		     \frac{7}{8}=\left(\frac{136}{15}+\frac{9}{15}\right)         \times
		     \frac{7}{8}=\frac{145}{15}  \times \frac{7}{8}=\frac{29}{3}  \times
		     \frac{7}{8}=\frac{203}{24} .\end{aligned}$
		   3.  On  simplifie  d'abord  les termes  comportant  des  exposants  :
		      $\frac{5^{10}  \times  7^{3}-25^{5}   \times  49^{2}}{(125  \times
		      7)^{3}+5^{9}   \times  14^{3}}=\frac{5^{10}   \times  7^{3}-5^{10}
		      \times  7^{4}}{5^{9}   \times  7^{3}+5^{9}  \times   7^{3}  \times
		      2^{3}}=\frac{5^{10}      \times      7^{3}(1-7)}{5^{9}      \times
		      7^{3}\left(1+2^{3}\right)}=\frac{5 \times(-6)}{9}=\frac{-10}{3}$ 
		   4. On calcule : $\begin{aligned}\frac{1978 \times 1979+1980 \times 21+1958}{1980 \times 1979-1978 \times 1979} & =\frac{1978 \times 1979+1979 \times 21+21+1958}{1979 \times(1980-1978)} \\& =\frac{1979 \times(1978+21)+1979}{1979 \times 2}=\frac{1979 \times(1978+21+1)}{1979 \times 2}=\frac{1979 \times 2000}{1979 \times 2} \\& =1000 .\end{aligned}$

!!! {{ exercice("Des nombres décimaux et des fractions.")}}

    === "Énoncé"
	
		 Dans chaque cas, donner le résultat sous forme d'une fraction irréductible.
    
		 1.  0,2
		 2.  1,35
		 3. $\dfrac{1}{3}-0,3$
		 4. $0,36$
		 5. $1,5+\dfrac{2}{3}$
		 6. $\dfrac{13,5}{18,2-3,2}$

	=== "Indications"

         oooohhhh


!!! {{ exercice("Une grosse fraction")}}

    === "Énoncé"

         Écrire
         $\dfrac{0,5-\dfrac{3}{17}+\dfrac{3}{37}}{\dfrac{5}{6}-\dfrac{5}{17}+\dfrac{5}{37}}+\dfrac{0,5-\dfrac{1}{3}+\dfrac{1}{4}-0,2}{\dfrac{7}{5}-\dfrac{7}{4}+\dfrac{7}{3}-3,5}$ 
         sous forme d'une fraction irréductible. 
		 
	=== "Corrigé"

         Et un petit calcul...
		 
		 $\begin{aligned}\frac{0,5-\frac{3}{17}+\frac{3}{37}}{\frac{5}{6}-\frac{5}{17}+\frac{5}{37}}+\frac{0,5-\frac{1}{3}+\frac{1}{4}-0,2}{\frac{7}{5}-\frac{7}{4}+\frac{7}{3}-3,5} & =\frac{\frac{3}{6}-\frac{3}{17}+\frac{3}{37}}{\frac{5}{6}-\frac{5}{17}+\frac{5}{37}}+\frac{\frac{1}{2}-\frac{1}{3}+\frac{1}{4}-\frac{1}{5}}{\frac{7}{5}-\frac{7}{4}+\frac{7}{3}-\frac{7}{2}} \\& =\frac{3\left(\frac{1}{6}-\frac{1}{17}+\frac{1}{37}\right)}{5\left(\frac{1}{6}-\frac{1}{17}+\frac{1}{37}\right)}+\frac{\frac{1}{2}-\frac{1}{3}+\frac{1}{4}-\frac{1}{5}}{-7\left(\frac{1}{2}-\frac{1}{3}+\frac{1}{4}-\frac{1}{5}\right)}=\frac{3}{5}-\frac{1}{7}=\frac{16}{35} .\end{aligned}$


!!! {{ exercice("Le calcul littéral à la rescousse.")}}

    === "Énoncé"

         En  utilisant les  identités remarquables  et le  calcul littéral,
         calculer les nombres suivants. 

	     1. $\dfrac{2022}{(-2022)^{2}+(-2021)(2023)}$
		 1.  $\dfrac{1235 \times 2469-1234}{1234 \times 2469+1235}$
		 1. $\dfrac{2021^{2}}{2020^{2}+2022^{2}-2}$
		 1.  $\dfrac{4002}{1000 \times 1002-999 \times 1001}$

	=== "Corrigé"

         1. On  connaît l'identité remarquable :  $(a-b)(a+b)=a^{2}-b^{2}$. Donc
            $:
            \frac{2022}{(-2022)^{2}+(-2021)(2023)}=\frac{2022}{(2022)^{2}+(1-2022)
            \times(1+2022)}=\frac{2022}{(2022)^{2}+1-2022^{2}}=2022$.
			
		  2.  On  fait apparaître  2021 dans  2020 et 2022  au dénominateur  : $
		      \begin{aligned}\frac{2021^{2}}{2020^{2}+2022^{2}-2}              &
		      =\frac{2021^{2}}{(2021-1)^{2}+(2021+1)^{2}-2}                  \\&
		      =\frac{2021^{2}}{2021^{2}-2  \times   2021  \times  1+1+2021^{2}+2
		      \times 2021  \times 1+1-2} \\&  =\frac{2021^{2}}{2021^{2}-2 \times
		      2021      \times     1+2021^{2}+2      \times     2021      \times
		      1}=\frac{2021}{2021-2+2021+2}=\frac{1}{2} .\end{aligned}$
			  
		  3.  En posant $a=1234$, on a : $1235=a+1$ et $2469=2 a+1$. Donc : $\frac{1235 \times 2469-1234}{1234 \times 2469+1235}=\frac{(a+1)(2 a+1)-a}{a(2 a+1)+a+1}=\frac{2 a^{2}+2 a+1}{2 a^{2}+2 a+1}=1$

          4. En posant $a=1000$, on a : $999=a-1,1001=a+1,1002=a+2$ et $4002=2 a+2$. Donc : $\frac{4002}{1000 \times 1002-999 \times 1001}=\frac{4 a+2}{a(a+2)-(a-1)(a+1)}=\frac{2(2 a+1)}{a^{2}+2 a-\left(a^{2}-1\right)}=\frac{2(2 a+1)}{2 a+1}=2$.

!!! {{ exercice("Les fractions et le calcul littéral.")}}

    === "Énoncé"

         Mettre sous  la forme d'une  seule fraction, qu'on écrira  sous la
         forme la plus simple possible. 
		 
		 1. $\dfrac{1}{(n+1)^{2}}+\dfrac{1}{n+1}-\dfrac{1}{n}$ pour $n \in \mathbb{N}^{*}$

	     1.    $\dfrac{a^{3}-b^{3}}{(a-b)^{2}}-\dfrac{(a+b)^{2}}{a-b}$   pour
	         $(a, b) \in \mathbb{Z}^{3}$, distincts deux à deux. 

	     1.           $\dfrac{\dfrac{6(n+1)}{n(n-1)(2          n-2)}}{\dfrac{2
	        n+2}{n^{2}(n-1)^{2}}}$     pour    $n     \in    \mathbb{N}^{*}
	        \backslash\{1,2\}$ 


	=== "Corrigé"

         1. On met au même dénominateur. Cela donne : 
		    
			$\begin{aligned}\frac{1}{(n+1)^{2}}+\frac{1}{n+1}-\frac{1}{n}      &
			=\frac{n}{n(n+1)^{2}}+\frac{n(n+1)}{n(n+1)^{2}}-\frac{(n+1)^{2}}{n(n+1)^{2}}=\frac{n+n(n+1)-(n+1)^{2}}{n(n+1)^{2}}
			\\&                                   =\frac{n+n^{2}+n-\left(n^{2}+2
			n+1\right)}{n(n+1)^{2}}=\frac{-1}{n(n+1)^{2}} .\end{aligned}$
		 2.     On    rappelle    la   formule    $:    a^{3}-b^{3}=(a-b)\left(a
		     b+a^{2}+b^{2}\right)$.            Cela           donne            :
		     $\frac{a^{3}-b^{3}}{(a-b)^{2}}-\frac{(a+b)^{2}}{a-b}=\frac{(a-b)\left(a
		     b+a^{2}+b^{2}\right)}{(a-b)^{2}}-\frac{(a+b)^{2}}{a-b}=\frac{a
		     b+a^{2}+b^{2}}{a-b}-\frac{a^{2}+2 a b+b^{2}}{a-b}=-\frac{a b}{a-b}$
		 3.  Pour $n \in \mathbb{N}^{*} \backslash\{1,2\}$, on a : $ \frac{\frac{6(n+1)}{n(n-1)(2 n-2)}}{\frac{2 n+2}{n^{2}(n-1)^{2}}}=\frac{6(n+1)}{n(n-1)(2 n-2)} \times \frac{n^{2}(n-1)^{2}}{2 n+2}=\frac{6(n+1)}{2(n-1)} \times \frac{n(n-1)}{2(n+1)}=\frac{3}{2} n$


	
!!! {{ exercice("Le quotient de deux sommes de Gauss.")}}

    === "Énoncé"

         Simplifier  $\dfrac{\displaystyle\sum_{k=0}^{n^{2}}  k}{\displaystyle\sum_{k=0}^{n} k}$  pour
         tout   $n   \in   \mathbb{N}^{*}$,   en   utilisant   la   formule
         $1+2+\cdots+p=\dfrac{p(p+1)}{2} \ldots \ldots$ 

	=== "Corrigé"

         $\operatorname{De} \sum_{k=0}^{n} k=\frac{n(n+1)}{2}$, on a $: \frac{\sum_{k=0}^{n^{2}} k}{\sum_{k=0}^{n} k}=\frac{\frac{n^{2}\left(n^{2}+1\right)}{2}}{\frac{n(n+1)}{2}}=\frac{n^{2}\left(n^{2}+1\right)}{2} \frac{2}{n(n+1)}=\frac{n\left(n^{2}+1\right)}{n+1}=\frac{n^{3}+n}{n+1}$.


!!! {{ exercice(" Décomposition en somme d'une partie entière et d'une partie décimale.")}}

    === "Énoncé"

         Soit  $k  \in \mathbb{R}  \backslash\{1\}$  et  $x \in  \mathbb{R}
         \backslash\{2\}$.  Écrire les  fractions suivantes  sous la  forme
         $a+\dfrac{b}{c}$ avec $b<c$. 
	     
		 1. $\dfrac{29}{6}$
		 1. $\dfrac{k}{k-1}$
		 1. $\dfrac{3 x-1}{x-2}$


	=== "Corrigé"

         1.  On trouve $\frac{29}{6}=\frac{4 \times 6+5}{6}=4+\frac{5}{6}$. 
		 2. On trouve $\frac{k}{k-1}=\frac{k-1+1}{k-1}=1+\frac{1}{k-1}$.
		 
		 3.  On trouve $\frac{3 x-1}{x-2}=\frac{3(x-2)+5}{x-2}=3+\frac{5}{x-2}$.
		 

!!! {{ exercice("Un produit de fractions")}}

    === "Énoncé"

         Soit    $t   \in    \mathbb{R}    \backslash\{-1\}$.   On    donne
         $A=\dfrac{1}{1+t^{2}}-\dfrac{1}{(1+t)^{2}}$                       et
         $B=\left(1+t^{2}\right)(1+t)^{2}$. 

	     Simplifier $A\times B$ autant que possible. 

	=== "Corrigé"

         Pour    $t     \in    \mathbb{R}    \backslash\{-1\}$,    on     a    :
         $A=\frac{1}{1+t^{2}}-\frac{1}{(1+t)^{2}}=\frac{(1+t)^{2}}{\left(1+t^{2}\right)(1+t)^{2}}-\frac{1+t^{2}}{\left(1+t^{2}\right)(1+t)^{2}}=\frac{1+2
         t+t^{2}-\left(1+t^{2}\right)}{\left(1+t^{2}\right)(1+t)^{2}}=\frac{2
         t}{\left(1+t^{2}\right)(1+t)^{2}}$ 
		 
		 Donc, $A B=\left(\frac{2 t}{\left(1+t^{2}\right)(1+t)^{2}}\right) \times\left(1+t^{2}\right)(1+t)^{2}=2 t$

	
### Comparaison
	
	
!!! {{ exercice("Règles de comparaison.")}}

    === "Énoncé"

         Comparer les  fractions suivantes  avec le  signe $«>»,  «<»$ ou
         $«=»$.
		 
	     1.  $\dfrac{3}{5} \ldots \dfrac{5}{9}$
		 2. $\dfrac{12}{11} \ldots \dfrac{10}{12}$
		 3. $\dfrac{125}{25} \ldots \dfrac{105}{21}$


	=== "Corrigé"

         1. $\frac{3}{5}=\frac{27}{45}>\frac{5}{9}=\frac{25}{45}$
		 1. 
		 1.  $\frac{125}{25}=5=\frac{105}{21}$
		 
		 
		 
!!! {{ exercice("Produit en croix.")}}

    === "Énoncé"

	     Les nombres $A=\dfrac{33215}{66317}$ et $B=\dfrac{104348}{208341}$
		 sont-ils égaux?

	=== "Corrigé"

         Nous allons étudier les produits en croix.

         On sait que $A=B$, si et seulement si $33215 \times 208341=66317 \times 104348$. Le nombre de gauche est le produit de deux nombres impair, il est impair. Par contre, le nombre de droite est le produit de deux nombres de parités différentes, il est pair. Par conséquent, l'égalité n'est pas vérifiée. $A$ et $B$ ne sont pas égaux.
		 
		 
!!! {{ exercice("Produit en croix.")}}

    === "Énoncé"

         On           pose          $A=\dfrac{100001}{1000001}$           et
         $B=\dfrac{1000001}{10000001}:$ a-t-on $A>B, A=B$ ou $A<B$ ? 

	=== "Corrigé"

         On          re-écrit          $A=\frac{10^{5}+1}{10^{6}+1}$          et
         $B=\frac{10^{6}+1}{10^{7}+1}$.  Nous  allons  étudier les  produits  en
         croix.
		 
		 D'une        part       calculons        :       $\left(10^{5}+1\right)
		 \times\left(10^{7}+1\right)=10^{12}+10^{7}+10^{5}+1$.
		 
		 D'autre part : $\left(10^{6}+1\right)^{2}=10^{12}+2 \times 10^{6}+1$.
		 
		 Comme $\left(10^{5}+1\right) \times\left(10^{7}+1\right)>\left(10^{6}+1\right) \times\left(10^{6}+1\right)$, on obtient $: A>B$. 


	
## Réponses mélangées

$${\small
\begin{aligned}
& 1000 \quad 2 t \quad \dfrac{13}{6} \quad 3 \quad \text { Non } \quad 2^{5} \quad \dfrac{-1}{n(n+1)^{2}} \quad \dfrac{3}{2} n \quad-\dfrac{a b}{a-b} \\
& \dfrac{12}{11}>\dfrac{10}{12} \quad \dfrac{n^{3}+n}{n+1} \quad \dfrac{1}{30} \quad 1+\dfrac{1}{k-1} \quad A>B \quad \dfrac{27}{20} \quad \dfrac{1}{9} \quad 2 \quad \dfrac{1}{5} \\
& \dfrac{9}{25} \quad \dfrac{203}{24} \quad \dfrac{-10}{3} \quad 1 \quad 247 \quad 4+\dfrac{5}{6} \quad \dfrac{1}{6} \quad \dfrac{3}{5}>\dfrac{5}{9} \quad \dfrac{9}{10} \quad \dfrac{7}{15} \\
& \dfrac{4}{5} \quad-2 \times 3^{3 k-2} \quad 2022 \quad \dfrac{16}{35} \quad 3+\dfrac{5}{x-2} \quad 9 \quad \dfrac{125}{25}=\dfrac{105}{21} \quad \dfrac{1}{2}
\end{aligned}}
$$

## QCM

!!! {{ exercice()}}

    === "QCM"
	      
	     Cliquez sur vos propositions aux 9 questions puis validez.
		 
		 {{ multi_qcm(
		 ["$\\dfrac{4 x^{2}}{\\dfrac{2}{x^{2}}}=$",["$2 x^{4}$", "2", "8"],[1]],
		 ["$\\dfrac{1}{2}-\\dfrac{x+1}{x}=$",["$-\\left(\\dfrac{x+2}{2 x}\\right)$", "$\\dfrac{x-2}{2x}$","$\\dfrac{-x+2}{2 x}$"], [1]],
		 ["$\\dfrac{1}{x(x+1)}-\\dfrac{2}{x}=$",["$\\dfrac{-2 x-1}{x^{2}(x+1)}$", "$\\dfrac{-2 x-1}{x(x+1)}$", "$\dfrac{-2 x+3}{x(x+1)}$"], [2]], 
		 ["$\\dfrac{1-x^{2}}{(x-1)^{4}}=$",["$-\dfrac{1+x}{(1-x)^{3}}$", " $\\dfrac{1+x}{(x-1)^{3}}$", "$\dfrac{1+x}{(1-x)^{3}}$"], [3]], 
		 ["$\\dfrac{e^{t}}{1+e^{t}}-1=$",["$\\dfrac{-1}{1+e^{t}}$", "$\\dfrac{2 e^{t}-1}{1+e^{t}}$", " $\dfrac{e^{t}-1}{1+e^{t}}$"], [1]], 
		 ["$\\dfrac{1}{\dfrac{1}{a}+\\dfrac{1}{b}}=$",["$a+b$", "$\\dfrac{1}{a+b}$", "$\\dfrac{a b}{a+b}$"], [3]], 
		 ["$2-\\dfrac{2 x+1}{x+2}=$",["$\\dfrac{3}{x+2}$", "$\\dfrac{5}{x+2}$", "$\\dfrac{4 x+3}{x+2}$"], [1]], 
		 ["$2-\\dfrac{(x+1)^{2}}{x}=$",["$\\dfrac{-x^{2}+1}{x}$", "$\\dfrac{-x^{2}-1}{x}$", "$-\\dfrac{x^{2}-1}{x}$"], [2]], 
		 ["$\\dfrac{1}{x-1}-\\dfrac{1}{x+1}=$",["$\\dfrac{2}{(x-1)}$","$\\dfrac{2}{x^{2}-1}$","$\\dfrac{2 x}{(x-1)(x+1)}$"], [2]]
	     )}}

    === "Réponses"
	
	     Cliquez et vous saurez tout de suite si vous avez vu juste :

		 1. $\dfrac{4 x^{2}}{\dfrac{2}{x^{2}}}=$
		    {{ qcm(["$2 x^{4}$", "2", "8"],[1], shuffle=True)}}
		 2. $\dfrac{1}{2}-\dfrac{x+1}{x}=$
		    {{ qcm(["$-\\left(\\dfrac{x+2}{2 x}\\right)$", "$\\dfrac{x-2}{2
		    x}$","$\\dfrac{-x+2}{2 x}$"], [1])}}
		 3. $\dfrac{1}{x(x+1)}-\dfrac{2}{x}=$
		     {{ qcm(["$\\dfrac{-2 x-1}{x^{2}(x+1)}$", "$\\dfrac{-2 x-1}{x(x+1)}$", "$\dfrac{-2 x+3}{x(x+1)}$"], [2])}} 
		 4. $\dfrac{1-x^{2}}{(x-1)^{4}}=$
		     {{ qcm(["$-\dfrac{1+x}{(1-x)^{3}}$", " $\\dfrac{1+x}{(x-1)^{3}}$", "$\dfrac{1+x}{(1-x)^{3}}$"], [3])}} 
		 5. $\dfrac{e^{t}}{1+e^{t}}-1=$
		     {{ qcm(["$\\dfrac{-1}{1+e^{t}}$", "$\\dfrac{2 e^{t}-1}{1+e^{t}}$", " $\dfrac{e^{t}-1}{1+e^{t}}$"], [1])}} 
		 6. $\dfrac{1}{\dfrac{1}{a}+\dfrac{1}{b}}=$
		     {{ qcm(["$a+b$", "$\\dfrac{1}{a+b}$", "$\\dfrac{a b}{a+b}$"], [3])}} 
		 7. $2-\dfrac{2 x+1}{x+2}=$
		     {{ qcm(["$\\dfrac{3}{x+2}$", "$\\dfrac{5}{x+2}$", "$\\dfrac{4 x+3}{x+2}$"], [1])}} 
		 8. $2-\dfrac{(x+1)^{2}}{x}=$
		     {{ qcm(["$\\dfrac{-x^{2}+1}{x}$", "$\\dfrac{-x^{2}-1}{x}$", "$-\\dfrac{x^{2}-1}{x}$"], [2])}} 
		 9. $\dfrac{1}{x-1}-\dfrac{1}{x+1}=$
		     {{ qcm(["$\\dfrac{2}{(x-1)}$",    "$\\dfrac{2}{x^{2}-1}$",
		     "$\\dfrac{2 x}{(x-1)(x+1)}$"], [2])}} 


	=== "Réponses et commentaires"

         1. $\mathrm{A}$;
		 2. A (mise au même dénominateur)
		 3. $\mathrm{B} \operatorname{car} \frac{1}{x(x+1)}-\frac{2}{x}=\frac{1}{x(x+1)}-\frac{2(x+1)}{x(x+1)}=\frac{-2 x-1}{x(x+1)}$.
		 4.                  $\mathrm{C}                 \operatorname{car}
		    \frac{1-x^{2}}{(x-1)^{4}}=\frac{(1-x)(1+x)}{(x-1)^{4}}=\frac{(1-x)(1+x)}{(1-x)^{4}}=\frac{1+x}{(1-x)^{3}}$. En
		    effet $(x-1)^{4}=(-(1-x))^{4}=(1-x)^{4}$ puisque 4 est pair.

		 5. A (mise au même dénominateur)
		 6. $\mathrm{C}$ (multiplier en haut et en bas par $a b)$
		 7. A (mise au même dénominateur)
		 8. B (mise au même dénominateur)
		 9. B (mise au même dénominateur, en utilisant que $\left.(x-1)(x+1)=x^{2}-1\right)$




<!---
{{ terminal() }}

{{ IDEv() }}

{{ IDE() }}
-->
