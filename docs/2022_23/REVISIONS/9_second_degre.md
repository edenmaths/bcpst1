{{ chapitre(7, "Équations du second degré", "Rappel N°")}}


!!! note "À noter"
    
	Dans cette fiche :
	 
	 - tous les trinômes considérés sont réels;
	 - on ne s'intéresse qu'à leurs éventuelles racines réelles;
	 - tous les paramètres sont choisis de telle sorte que l'équation considérée
	   soit bien de degré 2. 
	   
	Les  formules donnant  explicitement les  racines d'une  équation du  second
	degré en fonction du discriminant ne servent nulle part dans cette fiche d'exercices! 


## Recherche de racines
![Placeholder](./IMG/puffi.png){ align=right width=310}


!!! {{ exercice("Des racines vraiment évidentes.")}}

    === "Énoncé"

        Résoudre   mentalement  les   équations  suivantes.    Les  racines
        évidentes sont à chercher parmi $0,1,-1,2,-2$ ainsi éventuellement que 3 et -3.

		1. $x^{2}-6 x+9=0$
		1. $x^{2}-5 x=0$
		1. $9 x^{2}+6 x+1=0$
		1. $2 x^{2}+3 x=0$
		1. $x^{2}+4 x-12=0$
		1. $2 x^{2}+3=0$
		1. $x^{2}-5 x+6=0$
		1. $x^{2}+4 x-5=0$

	=== "Corrigé"

         1.  C'est une identité remarquable : $x^{2}-6 x+9=(x-3)^{2}$.
		 1. RAS
		 1. Le nombre 2 est racine évidente, l'autre est donc -6 en regardant le
		    produit des racines qui vaut -12 .
		 1. RAS
		 1. La  racine 0  est la  racine évidente par  excellence; la  somme des
		    racines valant ici 5 l'autre racine est 5 .
		 1. RAS
		 1.  La fonction $x \longmapsto  2 x^{2}+3$ est strictement postivie car
		     elle est minorée par 3 , donc elle ne s'annule pas.
		 

!!! {{ exercice(" Somme et produit.")}}

    === "Énoncé"
        
		Développer  $(x-a)(x-b)$  et  en   déduire  un  moyen  de  résoudre
		mentalement les équations suivantes:
		
		1. $x^{2}-(a+b) x+a b=0    $

		1. $x^{2}-13 x+42=0$

		1. $x^{2}-8 x-33=0 $

		1. $x^{2}+8 x+15=0       $

		1. $x^{2}+18 x+77=0      $

		1. $x^{2}-2 a x+a^{2}-b^{2}=0$

	=== "Corrigé"

         1. Ici on cherche  des racines un peu moins évidentes  : on remplace le
            problème  par le  problème équivalent  de la  détermination de  deux
            nombres $x_{1}, x_{2}$ dont  le produit vaut 42 et la  somme 13 . On
            teste donc les factorisations évidentes de  42 , ici $42=6 \times 7$
            et $13=6+7$.
		 1.  On cherche  deux nombres dont le  produit vaut 15 et la  somme -8 :
		     les nombres -3 et -5 conviennent. 

!!! {{ exercice("L'une grâce à l'autre.")}}

    === "Énoncé"

        Calculer la seconde racine des équations suivantes.

		1. $3 x^{2}-14 x+8=0 \quad$ sachant que $x=4$ est racine

		1. $7 x^{2}+23 x+6=0 \quad$ sachant que $x=-3$ est racine

		1. $m x^{2}+(2 m+1) x+2=0 \quad$ sachant que $x=-2$ est racine

		1. $(m+3)  x^{2}-\left(m^{2}+5 m\right) x+2 m^{2}=0  \quad$ sachant
		   que $x=m$ est racine 

	=== "Corrigé"

         RAS



!!! {{ exercice("Racine évidente")}}

    === "Énoncé"

        Trouver une racine  des équations suivantes et  calculer l'autre en
        utilisant les  relations entre les  coefficients du trinôme  et ses
        racines. 

		1. $(b-c) x^{2}+(c-a) x+(a-b)=0$

		1. $a(b-c) x^{2}+b(c-a)x+c(a-b)=0$

		1. $(x+a)(x+b)=(m+a)(m+b)$

	=== "Corrigé"

         RAS


## Recherche d'équations

!!! {{ exercice("À la recherche de l'équation")}}

    === "Énoncé"

        En utilisant la  somme et le produit des racines  d'une équation du
        second  degré, former  l'équation du  second degré  admettant comme
        racines les nombres suivants. 
		
		1. 9 et 13
		1. -11 et 17
		1. $2+\sqrt{3}$ et $2-\sqrt{3}$


	=== "Corrigé"

         1. La somme des racines vaut  22, leur produit 117. L'équation cherchée
            est donc $x^{2}-22 x+117=0$.
			

!!! {{ exercice()}}

    === "Énoncé"

        Déterminer  la  valeur  à  donner  à $m$  pour  que  les  équations
        suivantes admettent une racine double,  et préciser la valeur de la
        racine dans ce cas. 

	    1. $x^2-(2m+3)x +m^2=0$
		1. $(m+2) x^{2}-2(m-1) x+4=0$

	=== "Corrigé"

         1.  Une  équation du  second  degré  admet  une  racine double  si,  et
            seulement si,  son discriminant est  nul. Ici, le  discriminant vaut
            $\Delta=(2 m+3)^{2}-4  m^{2}=3(4 m-1)$. Ainsi, l'équation  admet une
            racine double si,  et seulement si, $m$  vaut $-3 / 4$  ce qui donne
            $x=3 / 4$.
		 1. Ici, le déterminant vaut $\Delta=4\left(m^{2}-6 m-7\right)$, donc une racine évidente est -1 donc l'autre vaut 7 . Pour $m=-1$ on trouve $x=-2$ et pour $m=7$ on trouve $x=2 / 3$.


## Factorisations et signe

!!! {{ exercice("Factorisation à vue")}}

    === "Énoncé"

        Déterminer de tête les valeurs des paramètres $a$ et $b$ pour que les égalités suivantes soient vraies pour tout $x$.

		1. $2 x^{2}+7 x+6=(x+2)(a x+b)$

		1. $-4 x^{2}+4 x-1=(2 x-1)(a x+b)$

		1. $-3 x^{2}+14 x-15=(x-3)(a x+b)$

	=== "Corrigé"

         RAS


!!! {{ exercice("Signe d'un trinôme.")}}

    === "Énoncé"

        Déterminer  l'ensemble  des  valeurs  de $x$  pour  lesquelles  les
        expressions suivantes sont positives ou nulles. 

	    1. $x^2-(\sqrt{2}+1)x+\sqrt{2}$
		
		1. $-x^{2}+2 x+15$

		1. $(x+1)(3 x-2)$

	=== "Corrigé"

         1. Un  trinôme est du  signe du  coefficient dominant à  l'extérieur de
            l'intervalle des racines, et du signe opposé entre les racines. Ici,
            les racines sont  $\sqrt{2}$ et 1 , le trinôme  est donc strictement
            positif  sur $]-\infty,  1[\cup]  \sqrt{2},+\infty[$ et  strictement
            négatif sur $] 1, \sqrt{2}[$.
		 1.  Les racines  sont -5 et 3. Le trinôme  est donc strictement négatif
		     sur  $]-\infty,-3[\cup]  5,+\infty[$  et  strictement  positif  sur
		     $]-3,5[$.
		 1. Ici, les racines sont -1 et $2 / 3$. Le trinôme est donc strictement
		    positif  sur  $]-\infty,-1[\cup]  2  /  3,+\infty[$  et  strictement
		    négatif sur ] - $1,2 / 3[$. 
			



	
## Réponses mélangées


$${\small
\begin{aligned}
& a, b \quad-3,-5 \quad x^{2}-6 x-187=0 \quad-7,-11 \quad-2 / 7 \quad \varnothing \quad 6,7 \quad 2,3 \\
& a=-2 \text { et } b=1 \quad a=2 \text { et } b=3 \quad 1 \text { donc }(a-		1. /(b-		1. \quad 2,-6 \quad 2 m /(m+3) \\
& 1 \text { donc } c(a-		1. /(a(b-c)) \quad-1 / m \quad a=-3 \text { et } b=5 \quad 1 \text { donc }-5 \quad-3,11 \\
& -1 / 3,-1 / 3 \quad a-b, a+b \quad 2 / 3 \quad]-\infty, 1] \cup\left[\sqrt{2},+\infty\left[\quad x^{2}-4 x+1=0\right.\right. \\
& ]-\infty,-1] \cup[2 / 3,+\infty[\quad 3,3 \quad m \text { donc }-(m+a+		1. \quad[-3,5] \quad m=-3 / 4 \text { et } x=3 / 4 \\
& m=-1 \text { et } x=-2, \text { ou } m=7 \text { et } x=2 / 3 \quad 0, \text { donc } 5 \quad 0, \text { donc }-3 / 2 \quad x^{2}-22 x+117=0
\end{aligned}
}
$$





