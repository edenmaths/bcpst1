{{ chapitre(6, "Expressions algébriques", "Rappel N°")}}


## Premiers calculs
![Placeholder](./IMG/natacha.png){ align=right width=310}


!!! {{ exercice("Cubique")}}

    === "Énoncé"

        Soit $a$ un nombre réel tel que $a^{3}-a^{2}+1=0$.

	    Exprimer les quantités  suivantes sous la forme $x  a^{2}+y a+z$ où
	    $x, y, z$ sont trois nombres rationnels.

		1. $(a+2)^{3}$
		1. $a^{12}$
		1. $a^{5}-a^{6}$
		1. $\dfrac{1}{a}+\dfrac{1}{a^{2}}$

	=== "Corrigé"

         1.  On développe  $(a+2)^{3}=a^{3}+6 a^{2}+12  a+8$, puis  on simplifie
            sachant que $a^{3}=a^{2}-1$.
		 1.           De           $a^{3}=a^{2}-1$,          on           déduit
		    $a^{6}=a^{3}\left(a^{2}-a\right)=a^{5}-a^{4}$         et        donc
		    $a^{5}-a^{6}=a^{4}$. De plus $a^{4}=a\left(a^{2}-1\right)$, etc.
		 1.                   On                   commence                  par
		     $a^{6}=\left(a^{3}\right)^{2}=\left(a^{2}-1\right)^{2}=a^{4}-2
		     a^{2}+1=-a^{2}-a$   puis  $a^{12}=\left(-a^{2}-a\right)^{2}=a^{4}+2
		     a^{3}+a^{2}$.
		 1. L'égalité $a^{3}-a^{2}+1$ peut s'écrire $a\left(a-a^{2}\right)=1$ ce
		    qui  montre   que  $a   \neq  0$  et   $\frac{1}{a}=a-a^{2}$.  Alors
		    $\frac{1}{a^{2}}=1-a$.
			

!!! {{ exercice("Inverse")}}

    === "Énoncé"

        Soit $x$ un réel non nul. On pose $a=x-\dfrac{1}{x}$.  Exprimer les
        quantités suivantes en fonction de $a$ uniquement. 

		1. $x^{2}+\dfrac{1}{x^{2}}$
		1. $x^{3}-\dfrac{1}{x^{3}}$
		1. $x^{4}+\dfrac{1}{x^{4}}$ 

	=== "Corrigé"

         1.          On          complète          le          carré          $:
            x^{2}+\frac{1}{x^{2}}=\left(x-\frac{1}{x}\right)^{2}+2 \cdot x \cdot
            \frac{1}{x}$.
		 1.      On      se     ramène     au     résultat      précédent     $:
		     x^{3}-\frac{1}{x^{3}}=x\left(x^{2}+\frac{1}{x^{2}}\right)-\frac{1}{x}\left(x^{2}+\frac{1}{x^{2}}\right)+\left(x-\frac{1}{x}\right)=a\left(a^{2}+2\right)+a$.
		 1.  De même $x^{4}+\frac{1}{x^{4}}=x^{2}\left(x^{2}+\frac{1}{x^{2}}\right)+\frac{1}{x^{2}}\left(x^{2}+\frac{1}{x^{2}}\right)-2=\left(a^{2}+2\right)^{2}-2$.
			

!!! {{ exercice()}}

    === "Énoncé"

         Soit $p$ un réel de $] 0,1[$. On pose $q=1-p$.

	     Simplifier les expressions suivantes.

		1. $p q \times \dfrac{2}{p^{3}}+\dfrac{1}{p}-\dfrac{1}{p^{2}}$

		1. $\dfrac{1}{p q}-\dfrac{1}{1-q}-\dfrac{1}{1-p}$

		1. $\dfrac{p q}{(1-p)^{2}}-\dfrac{1}{q}$

		1. $p^{3}+3 p q+q^{3}$

	=== "Corrigé"

         1.              $\quad              p             q              \times
            \frac{2}{p^{3}}+\frac{1}{p}-\frac{1}{p^{2}}=\frac{2    q+p-1}{p^{2}}
            \quad$       or      $p-1=-q$       donc      $p       q      \times
            \frac{2}{p^{3}}+\frac{1}{p}-\frac{1}{p^{2}}=\frac{q}{p^{2}}$
		 1.                            $\quad                            \frac{p
		    q}{(1-p)^{2}}-\frac{1}{q}=\frac{p}{q}-\frac{1}{q}=\frac{p-1}{q}=-1$
		 1.    $\quad    \frac{1}{p    q}-\frac{1}{1-q}-\frac{1}{1-p}=\frac{1}{p
		    q}-\frac{1}{p}-\frac{1}{q}=\frac{1}{p   q}-\frac{q}{p  q}-\frac{p}{p
		    q}=\frac{1-p-q}{p q}=0$
		 1.   $p+q=1$ donc  $p^{3}+3  p  q+q^{3}=p^{3}+3 p  q(p+q)+q^{3}=p^{3}+3
		     p^{2} q+3 p q^{2}+q^{3}=(p+q)^{3}=1$
			 


!!! {{ exercice("Résoudre une équation en physique.")}}

    === "Énoncé"

        Résoudre  les  équations  suivantes   en  exprimant  l'inconnue  en
        fonction des autres grandeurs. 

		1. $\dfrac{1}{k}=\dfrac{1}{k_{1}}+\dfrac{1}{k_{2}} \quad$ avec pour
		   inconnue  $k$ 

		1. $\dfrac{2  m g}{a} \rho-\dfrac{m C^{2}}{\rho^{3}}=0  \quad$ avec
		   pour inconnue $\rho \quad$ avec $\rho>0$ 

		1. $\dfrac{1}{2}  m v^{2}+\dfrac{m g d^{2}}{2  R}=\dfrac{m g R}{2}$
		   avec pour inconnue $v$. 

	=== "Corrigé"

         1.    $\quad   \frac{1}{k_{1}}+\frac{1}{k_{2}}=\frac{k_{2}+k_{1}}{k_{1}
             k_{2}}  \quad$  donc  $\frac{1}{k}=\frac{1}{k_{1}}+\frac{1}{k_{2}}$
             équivaut à $k=\frac{k_{1} k_{2}}{k_{1}+k_{2}}$
		 1.    $\quad   \frac{2    m   g}{a}   \rho-\frac{m   C^{2}}{\rho^{3}}=0
		     \Longleftrightarrow \frac{2  m g}{a}  \rho=\frac{m C^{2}}{\rho^{3}}
		     \Longleftrightarrow       \rho^{4}=\frac{a        C^{2}}{2       g}
		     \Longleftrightarrow     \rho=\sqrt[4]{\frac{a      C^{2}}{2     g}}
		     \quad(\operatorname{car} \rho>0)$
		 1. On a nécessairement $R-\frac{d^{2}}{R} \geqslant 0$.
		    $\frac{1}{2} m v^{2}+\frac{m g d^{2}}{2 R}=\frac{m g R}{2} \Longleftrightarrow v^{2}+\frac{g d^{2}}{R}=g R \Longleftrightarrow v^{2}=g\left(R-\frac{d^{2}}{R}\right) \Longleftrightarrow v=\sqrt{g\left(R-\frac{d^{2}}{R}\right)}$ ou $v=-\sqrt{g\left(R-\frac{d^{2}}{R}\right)}$ 


	
## Réponses mélangées


$${\small
\begin{array}{rcccccc}
\dfrac{k_{1} k_{2}}{k_{1}+k_{2}} &  & 1 & -a^{2}+1 & 7 a^{2}+12 a+7 & 4 a^{2}-a-3 \\
-1 & a^{2}+2 & a^{2}-a-1 & &  & 1 &  \\
a^{3}+3 a & 0 & 3 &  & \left(\dfrac{a C^{2}}{2 g}\right)^{\dfrac{1}{4}} &  & a^{4}+4 a^{2}+2 \\
\dfrac{q}{p^{2}} & 2197 & 1 & \sqrt{g\left(R-\dfrac{d^{2}}{R}\right)} & \text { ou }-\sqrt{g\left(R-\dfrac{d^{2}}{R}\right)} & 1
\end{array}}
$$


