# Element : notre espace d'échange



![Placeholder](./IMG/martine_equipe.png){  align=right width=250}  Durant cet
été et  toute l'année, vous pourrez  poser vos questions, répondre  à celle
des autres et proposer vos solutions sur notre groupe de discussion sur [Element](https://element.io/get-started).  Installez l'application sur
votre PC et votre téléphone intelligent.  Créez un compte et envoyez-moi un
message via `Element` (`@giyom:matrix.org`) pour que je vous inscrive sur nos
salons.      En      cas      de      problème,      contactez-moi      par
[e-mail](mailto:g.connan@externat-chavagnes.com). 


Il sera absolument vital de faire vivre cet espace. N'oubliez pas, tous les
conseils  prodigués  en Prépa  ne  sont  en  aucune  façon des  paroles  en
l'air. Vous n'êtes  plus des élèves passifs mais  des adultes scientifiques
guidés par  quelques adultes un peu  plus âgés qui vous  donneront quelques
clés  pour  survivre   :  ce  ne  sont  pas  des   paroles  en  l'air...Et,
contrairement au nouveau  lycée, il n'y aura pas de  happy ending magique :
**pas de travail, pas de résultats**!


