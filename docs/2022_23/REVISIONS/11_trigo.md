{{ chapitre(9, "Trigonométrie", "Rappel N°")}}




## Valeurs remarquables de cosinus et sinus
![Placeholder](./IMG/tintin8.png){ align=right width=320}


!!! {{ exercice()}}

    === "Énoncé"

        Donner les valeurs :

		1. $\cos \left(\dfrac{3 \pi}{4}\right)  $

		1. $\tan \left(\dfrac{5 \pi}{4}\right)  $

		1. $\cos \left(\dfrac{\pi}{6}\right)    $

		1. $\tan \left(\dfrac{\pi}{4}\right)$

		1. $\cos (7 \pi)$

		1. $\sin \left(\dfrac{7 \pi}{6}\right) $

	=== "Indications"

         sol


!!! {{ exercice()}}

    === "Énoncé"

        Simplifier :

		1. $\cos \left(\dfrac{\pi}{4}\right)+\cos \left(\dfrac{3 \pi}{4}\right)+\cos \left(\dfrac{5 \pi}{4}\right)+\cos \left(\dfrac{7 \pi}{4}\right) $

		1. $\cos ^{2}\left(\dfrac{4 \pi}{3}\right)+\sin ^{2}\left(\dfrac{4 \pi}{3}\right)$

		1. $\sin \left(\dfrac{5 \pi}{6}\right)+\sin \left(\dfrac{7 \pi}{6}\right)$

		1. $\cos ^{2}\left(\dfrac{4 \pi}{3}\right)-\sin ^{2}\left(\dfrac{4 \pi}{3}\right)$

		1. $\tan \left(\dfrac{2 \pi}{3}\right)+\tan \left(\dfrac{3 \pi}{4}\right)+\tan \left(\dfrac{5 \pi}{6}\right)+\tan \left(\dfrac{7 \pi}{6}\right) $

	=== "Indications"

         sol

## Signe du cosinus et du sinus

!!! {{ exercice()}}

    === "Énoncé"

        Donner le signe :

		1. $\cos \left(\dfrac{2 \pi}{5}\right)  $

		1. $\cos \left(\dfrac{8 \pi}{5}\right)  $

		1. $\sin \left(\dfrac{14 \pi}{5}\right)   $

		1. $\sin \left(\dfrac{7 \pi}{5}\right)  $

		1. $\tan \left(\dfrac{13 \pi}{5}\right)  $

		1. $\tan \left(-\dfrac{3 \pi}{5}\right) $

	=== "Indications"

         sol


## Propriétés remarquables de cosinus et sinus


!!! {{ exercice()}}

    === "Énoncé"

        Simplifier :

		1. $\sin (\pi-x)+\cos \left(\dfrac{\pi}{2}+x\right)$
		1. $\sin \left(\dfrac{\pi}{2}-x\right)+\sin \left(\dfrac{\pi}{2}+x\right)$
		1. $\sin (-x)+\cos (\pi+x)+\sin \left(\dfrac{\pi}{2}-x\right)$
		1. $\cos (x-\pi)+\sin \left(-\dfrac{\pi}{2}-x\right)$ 

	=== "Indications"

         sol


## Formules de duplication


On rappelle les formules suivantes : $\sin (2 x)=2 \sin (x) \cos (x) \quad$
et $\quad \cos (2 x)=\cos ^{2}(x)-\sin ^{2}(x)$ 


!!! {{ exercice()}}

    === "Énoncé"

        En remarquant qu'on a $\dfrac{\pi}{4}=2 \times \dfrac{\pi}{8}$, calculer :

		1. $\cos \left(\dfrac{\pi}{8}\right)$
		1. $\sin \left(\dfrac{\pi}{8}\right)$
		1. $\tan \left(\dfrac{\pi}{8}\right)$

	=== "Indications"

         1.      On      a     $\cos      \left(\frac{\pi}{4}\right)=2      \cos
            ^{2}\left(\frac{\pi}{8}\right)-1$             donc             $\cos
            ^{2}\left(\frac{\pi}{8}\right)=\frac{\frac{\sqrt{2}}{2}+1}{2}=\frac{\sqrt{2}+2}{4}$.
			
			De plus,  $\cos \left(\frac{\pi}{8}\right)  \geqslant 0$  donc $\cos
			\left(\frac{\pi}{8}\right)=\frac{\sqrt{2+\sqrt{2}}}{2}$. 
		 2.       On      a     $\sin      ^{2}\left(\frac{\pi}{8}\right)=1-\cos
		     ^{2}\left(\frac{\pi}{8}\right)=\frac{2-\sqrt{2}}{4}$    et    $\sin
		     \left(\frac{\pi}{8}\right)     \geqslant      0$     donc     $\sin
		     \left(\frac{\pi}{8}\right)=\frac{\sqrt{2-\sqrt{2}}}{2}$.
		 3.                                                                $\tan
		    \left(\frac{\pi}{8}\right)=\sqrt{\frac{2-\sqrt{2}}{2+\sqrt{2}}}=\sqrt{\frac{(2-\sqrt{2})^{2}}{4-2}}=\frac{2-\sqrt{2}}{\sqrt{2}}=\sqrt{2}-1$
			

!!! {{ exercice()}}

    === "Énoncé"

        Simplifier pour $x \in] 0, \dfrac{\pi}{2}[$,

		1. $\dfrac{1-\cos (2 x)}{\sin (2 x)}$
		1. $\dfrac{\cos (2 x)}{\cos (x)}-\dfrac{\sin (2 x)}{\sin (x)}$

	=== "Indications"

         1. On a $\cos (2 x)=1-2 \sin ^{2}(x)$ donc $\frac{1-\cos (2 x)}{\sin (2
            x)}=\frac{2 \sin ^{2}(x)}{2 \sin x \cos (x)}=\tan (x)$.
		 2.  $\frac{\cos (2 x)}{\cos (x)}-\frac{\sin (2 x)}{\sin (x)}=\frac{\cos
		      ^{2}(x)-\sin  ^{2}(x)}{\cos (x)}-\frac{2  \sin (x)  \cos (x)}{\sin
		      (x)}=\frac{\cos   ^{2}(x)-\sin  ^{2}(x)}{\cos   (x)}-\frac{2  \cos
		      ^{2}(x)}{\cos (x)}=-\frac{\cos ^{2}(x)+\sin ^{2}(x)}{\cos (x)}$
			  


## Équations et inéquations trigonométriques


!!! {{ exercice()}}

    === "Énoncé"

        Résoudre  dans   $[0,2  \pi]$,   dans  $[-\pi,  \pi]$,   puis  dans
        $\mathbb{R}$ les équations suivantes :

		1. $\cos (x)=\dfrac{1}{2}$
		1. $\tan (x)=-1$
		1. $\sin (x)=-\dfrac{\sqrt{3}}{2}$
		1. $\cos ^{2}(x)=\dfrac{1}{2}$
		1. $\sin (x)=\cos \left(\dfrac{2 \pi}{3}\right)$
		1. $|\tan (x)|=\dfrac{1}{\sqrt{3}}$

	=== "Indications"

        1. RAS
		1. RAS
		1. RAS
		1. RAS
		1.  Cela revient  à résoudre  « $\cos  (x)=\frac{\sqrt{2}}{2}$ ou  $\cos
		   (x)=-\frac{\sqrt{2}}{2}$ ».
		   


!!! {{ exercice()}}

    === "Énoncé"

        Résoudre dans $[0,2 \pi]$, puis dans $[-\pi, \pi]$, les inéquations
        suivantes :

		1. $\cos (x) \geqslant-\dfrac{\sqrt{2}}{2}$
		1. $|\sin (x)| \leqslant \dfrac{1}{2}$
		1. $\cos (x) \leqslant \cos \left(\dfrac{\pi}{3}\right)$
		1. $\tan (x) \geqslant 1$
		1. $\sin (x) \leqslant \dfrac{1}{2}$
		1. $\cos \left(x-\dfrac{\pi}{4}\right) \geqslant 0  $ 


	=== "Indications"

         1. RAS
		 1. RAS
		 1. RAS
		 1. Cela revient  à résoudre $-\frac{1}{2} \leqslant  \sin (x) \leqslant
		    \frac{1}{2}$.
		 1. RAS
		 1.      Si    $x     \in[0,2     \pi]$,    alors     $t=x-\frac{\pi}{4}
		     \in\left[-\frac{\pi}{4},  2  \pi-\frac{\pi}{4}\right]$.  On  résout
		     donc  $\cos (t)  \geqslant  0$ pour  $t \in\left[-\frac{\pi}{4},  2
		     \pi-\frac{\pi}{4}\right]$ ce qui  donne $t \in\left[-\frac{\pi}{4},
		     \frac{\pi}{2}\right]     \cup\left[\frac{3     \pi}{2},     \frac{7
		     \pi}{4}\right]$  et  donc  $x  \in\left[0,  \frac{3  \pi}{4}\right]
		     \cup\left[\frac{7 \pi}{4}, 2 \pi\right]$. 
			 





	
## Réponses mélangées



$${\small
\begin{aligned}
& {\left[-\dfrac{3 \pi}{4},-\dfrac{\pi}{2}\left[\cup \left[\dfrac{\pi}{4}, \dfrac{\pi}{2}\left[\quad\left[-\pi, \dfrac{\pi}{6}\right] \cup\left[\dfrac{5 \pi}{6}, \pi\right] \quad\left\{-\dfrac{5 \pi}{6},-\dfrac{\pi}{6}\right\} \quad\left\{-\dfrac{5 \pi}{6},-\dfrac{\pi}{6}, \dfrac{\pi}{6}, \dfrac{5 \pi}{6}\right\}\right.\right.\right.\right.} \\
& \dfrac{\sqrt{2+\sqrt{2}}}{2} \quad\left\{\dfrac{\pi}{4}+k \pi, k \in \mathbb{Z}\right\} \quad 1 \quad \tan x \quad \dfrac{\sqrt{3}}{2} \quad-\dfrac{1}{\cos (x)} \quad-\dfrac{1}{2} \\
& {\left[-\dfrac{\pi}{4}, \dfrac{3 \pi}{4}\right] \quad\left[0, \dfrac{\pi}{6}\right] \cup\left[\dfrac{5 \pi}{6}, 2 \pi\right] \quad\left\{\dfrac{\pi}{4}, \dfrac{5 \pi}{4}\right\} \quad\left\{\dfrac{\pi}{3}+2 k \pi, k \in \mathbb{Z}\right\} \cup\left\{-\dfrac{\pi}{3}+2 k \pi, k \in \mathbb{Z}\right\}} \\
& 0 \quad\left\{\dfrac{4 \pi}{3}+2 k \pi, k \in \mathbb{Z}\right\} \cup\left\{\dfrac{5 \pi}{3}+2 k \pi, k \in \mathbb{Z}\right\} \quad\left\{-\dfrac{3 \pi}{4}, \dfrac{\pi}{4}\right\} \quad\left\{\dfrac{\pi}{4}, \dfrac{3 \pi}{4}, \dfrac{5 \pi}{4} \dfrac{7 \pi}{4}\right\} \\
& \left\{\dfrac{\pi}{6}+k \pi, k \in \mathbb{Z}\right\} \cup\left\{\dfrac{5 \pi}{6}+k \pi, k \in \mathbb{Z}\right\} \quad\left[-\dfrac{3 \pi}{4}, \dfrac{3 \pi}{4}\right] \quad\left\{\dfrac{\pi}{4}+k \dfrac{\pi}{2}, k \in \mathbb{Z}\right\} \quad<0 \\
& {\left[0, \dfrac{3 \pi}{4}\right] \cup\left[\dfrac{7 \pi}{4}, 2 \pi\right] \quad\left[-\pi,-\dfrac{\pi}{3}\right] \cup\left[\dfrac{\pi}{3}, \pi\right] \quad\left[-\pi,-\dfrac{5 \pi}{6}\right] \cup\left[-\dfrac{\pi}{6}, \dfrac{\pi}{6}\right] \cup\left[\dfrac{5 \pi}{6}, \pi\right]>0} \\
& 2 \cos x \quad-2 \cos x \quad\left\{-\dfrac{\pi}{3}, \dfrac{\pi}{3}\right\} \quad \dfrac{\sqrt{2-\sqrt{2}}}{2} \quad\left[\dfrac{\pi}{3}, \dfrac{5 \pi}{3}\right] \quad\left\{\dfrac{\pi}{6}, \dfrac{5 \pi}{6}, \dfrac{7 \pi}{6}, \dfrac{11 \pi}{6}\right\} \\
& -\dfrac{1}{2} \quad\left\{\dfrac{-2 \pi}{3}, \dfrac{-\pi}{3}\right\} \quad\left[\dfrac{\pi}{4}, \dfrac{\pi}{2}\left[\cup \left[\dfrac{5 \pi}{4}, \dfrac{3 \pi}{2}\left[\begin{array}{llll}
0 & -1 & 0 & 1
\end{array}\right.\right.\right.\right. \\
& \left\{-\dfrac{3 \pi}{4},-\dfrac{\pi}{4}, \dfrac{\pi}{4}, \dfrac{3 \pi}{4}\right\} \quad\left\{\dfrac{7 \pi}{6}+2 k \pi, k \in \mathbb{Z}\right\} \cup\left\{\dfrac{11 \pi}{6}+2 k \pi, k \in \mathbb{Z}\right\} \quad\left\{\dfrac{7 \pi}{6}, \dfrac{11 \pi}{6}\right\} \\
& \sqrt{2}-1 \quad\left[0, \dfrac{3 \pi}{4}\right] \cup\left[\dfrac{5 \pi}{4}, 2 \pi\right] \quad\left\{\dfrac{\pi}{3}, \dfrac{5 \pi}{3}\right\} \quad\left\{\dfrac{4 \pi}{3}, \dfrac{5 \pi}{3}\right\} \quad>0 \quad<0 \quad 1 \\
& -\dfrac{\sqrt{2}}{2}-\sin x \quad\left[0, \dfrac{\pi}{6}\right] \cup\left[\dfrac{5 \pi}{6}, \dfrac{7 \pi}{6}\right] \cup\left[\dfrac{11 \pi}{6}, 2 \pi\right] \quad>0 \quad-1-\sqrt{3}
\end{aligned}}
$$


