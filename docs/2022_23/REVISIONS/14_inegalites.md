{{ chapitre(12, "Inégalités", "Rappel N°")}}




!!! note "Consignes pour les « Vrai ou Faux ? »"
    
	Pour répondre vrai, donner une démonstration.
	
	Pour répondre faux, montrer que c'est absurde ou donner un contre-exemple. 

## Pour s'échauffer
![Placeholder](./IMG/tintin3.png){ align=right width=310}


!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux : Soient deux réels $a$ et $b$ tels que : $1<a<2$ et $-5<b<-3$

		1. $-4<a+b<-1       $
		1. $\dfrac{-2}{3}<\dfrac{a}{b}<\dfrac{-1}{5}$
		1. $6<a-b<5$
		1. $\dfrac{\sqrt{a-1}}{b^{2}}<\dfrac{1}{25}$
		1. $-19<3 b-2 a<-11    $
		1. $a^{2} \leqslant a$
		1. $-5<a b<-6$
		1. $\forall n \in \mathbb{N},(-5)^{n}<b^{n}<(-3)^{n}$

	=== "Corrigé"

         1.  $1<a<2$  et   $-5<b<-3$  donc  (somme  membre  à   membre  de  deux
            encadrement) $1-5<a+b<2-3$
		 1.  On n'a jamais $6<a-b<5$ car  $6>5$. En effet, on ne peut soustraire
		     des inégalités, éventuellement multiplier  par -1 (avec précaution)
		     puis sommer, ce qui donne $4<a-b<7$
		 1. $1<a<2$ et $-5<b<-3$ donc $-4<-2 a<-2$ et $-15<3 b<-9$
		 donc (somme membre à membre de deux encadrements) $-4-15<3 b-2 a<-2-9$ 
		 1. On n'a jamais $-5<a b<-6$ car $-5>-6$
		 1.  $-5<b<-3$ donc  $\left(x \rightarrow-\frac{1}{x}\right.$ croissante
		     sur              $\left.\mathbb{R}_{-}^{*}\right)             \quad
		     \frac{1}{5}<-\frac{1}{b}<\frac{2}{3}$   de   plus   $1<a<2$,   donc
		     (produit  membre à  membre de  deux encadrements  avec des  nombres
		     positifs) $\frac{1}{5}<-\frac{a}{b}<\frac{2}{3}$ 
		 1.  D'une part  $1<a<2$ donc  $0<\sqrt{a-1}<1$, d'autre  part $-5<b<-3$
		    donc $0<\frac{1}{b^{2}}<\frac{1}{25}$ donc  (produit membre à membre
		    de     deux    encadrements     avec    des     nombres    positifs)
		    $\frac{\sqrt{a-1}}{b^{2}}<\frac{1}{25}$ 
		 1. $1<a<2$ donc  $a>1$ et $a-1>0$ ce qui donne  $a^{2}-a>0$ par produit
		    d'inégalités à termes positifs.
		 1. faux en prenant par exemple pour $n=2$


!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux : Soient deux réels $a$ et $b$

		1. $-a b \leqslant \dfrac{1}{2}\left(a^{2}+b^{2}\right)$
		1. $|a| \leqslant 1+a^{2}$
		1. $\dfrac{1}{2}\left(a^{2}+b^{2}\right) \leqslant a b$
		1. $a(1-		1. \leqslant \dfrac{1}{4}$

	=== "Corrigé"

         1.      $-a     b     \leqslant     \frac{1}{2}\left(a^{2}+b^{2}\right)
             \Longleftrightarrow(a+b)^{2} \geqslant 0$
		 1.  Il suffit de prendre comme contre-exemple $(a, b)=(1,0)$
		 1. 1er cas : Si $|a| \leqslant 1$, alors $|a| \leqslant 1+a^{2}, \quad$
		    2ème  cas  : Si  $|a|>1$  alors  $|a|^{2}>|a|$ donc  $|a|  \leqslant
		    1+a^{2}$
		 1.       $a(1-a)     \leqslant      \frac{1}{4}     \Longleftrightarrow
		     a^{2}-a+\frac{1}{4}                   \geqslant                   0
		     \Longleftrightarrow\left(x-\frac{1}{2}\right)^{2} \geqslant 0$
			 


!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux : Soit $x$ un réel

		1. $\sin (x) \leqslant(\sin (x))^{2}$
		1. $(\sin (x))^{2} \leqslant|\sin (x)|$
		1. $(\sin (x))^{2} \leqslant \sin (x)$
		1. $1-\cos (2 x) \leqslant 2|\sin (x)|$

	=== "Corrigé"

         1. Contre-exemple $: x=\frac{\pi}{6}$
		 1. Contre-exemple $: x=-\frac{\pi}{6}$
		 1.   $|\sin (x)|  \leqslant  1$  donc on  multipliant  par $|\sin  (x)|
		     \geqslant 0$ il vient $(\sin (x))^{2} \leqslant|\sin (x)|$
		 1.   On sait  que $1-\cos  (2 x)=2(\sin  (x))^{2}$ et  en utilisant  le
		     résultat  de la  question précédente  on  obtient :  $1-\cos (2  x)
		     \leqslant 2|\sin (x)|$
			 


## Pour comparer des intégrales

!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux  :  On note  $\left(u_{n}\right)$  la  suite définie  par
        $u_{n}=\displaystyle\int_{1}^{2} \dfrac{(\ln x)^{n}}{1+x} d x$. 

        Soient $x$ un réel tel que $1 \leqslant x \leqslant 2$ et $n$ un entier naturel.

		1. $0 \leqslant \dfrac{(\ln (x))^{n}}{1+x} \leqslant(\ln (2))^{n}$

		1. $0 \leqslant \dfrac{(\ln (x))^{n}}{1+x} \leqslant \dfrac{(\ln (x))^{n+1}}{1+x}$ 		1. $0 \leqslant \dfrac{(\ln (x))^{n+1}}{1+x} \leqslant \dfrac{(\ln (x))^{n}}{1+x} $

		1. La suite $\left(u_{n}\right)_{n}$ est décroissante. 

		1. $\dfrac{1}{3} \leqslant \dfrac{(\ln (x))^{n}}{1+x} \leqslant \dfrac{1}{2}      $

		1. La suite $\left(u_{n}\right)_{n}$ est bornée.

		1. La suite $\left(u_{n}\right)_{n}$ converge vers $0 $.

	=== "Corrigé"

         1. comme  $x \in[1,2]$, on  a $0 \leqslant  \ln (x) \leqslant  \ln (2)$
            donc  $0  \leqslant(\ln  (x))^{n}   \leqslant(\ln  (2))^{n}$  et  $0
            \leqslant \frac{1}{x+1}  \leqslant 1$ donc (produit  membre à membre
            de  deux  encadrements  avec  des  nombres  positifs)  $0  \leqslant
            \frac{(\ln (x))^{n}}{x+1} \leqslant(\ln (2))^{n}$ 
		 1.  Contre-exemple $: x=2$ et $n=1$ sachant que $0<\ln (2)<1$ donc $\ln
		     (2)^{2}<\ln (2)$
		 1.  comme  $x \in[1,2]$, on  a $0 \leqslant  \ln (x) \leqslant  \ln (2)
		     \leqslant  1$   donc  $0  \leqslant(\ln   (x))^{n+1}  \leqslant(\ln
		     (x))^{n}$ donc  $0 \leqslant \frac{(\ln  (x))^{n+1}}{1+x} \leqslant
		     \frac{(\ln (x))^{n}}{1+x}$
		 1.  Contre-exemple : $x=1$ et $n=1$
		 1. On a vu que pour tout $x \in[1,2], \quad \frac{(\ln (x))^{n+1}}{1+x}
		    \leqslant  \frac{(\ln  (x))^{n}}{1+x},  \quad$ donc  (croissance  de
		    l'intégrale) $u_{n+1} \leqslant u_{n}$
		 1.  On  a  vu  que  pour  tout  $x  \in[1,2],  0  \leqslant  \frac{(\ln
		    (x))^{n}}{1+x}  \leqslant(\ln (2))^{n},  \quad$ donc  (croissance de
		    l'intégrale)  $0  \leqslant  u_{n}  \leqslant(\ln  (2))^{n}$,  comme
		    $0<\ln (2)<1$, on a $\forall n \in \mathbb{N}^{*}, 0 \leqslant u_{n}
		    \leqslant 1$.
		 1.  On  a  vu  que  pour  tout  $x  \in[1,2],  0  \leqslant  \frac{(\ln
		    (x))^{n}}{1+x}  \leqslant(\ln (2))^{n},  \quad$ donc  (croissance de
		    l'intégrale)  $0  \leqslant  u_{n}  \leqslant(\ln  (2))^{n}$,  comme
		    $-1<\ln (2)<1$,  en utilisant le  théorème des gendarmes  on obtient
		    que $\left(u_{n}\right)$ converge vers 0 .
			



!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux  :  On note  $\left(u_{n}\right)$  la  suite définie  par
        $u_{n}=\displaystyle\int_{1 / 2}^{1} \dfrac{(\ln (x))^{n}}{1+x} d x$ .

        Soient $x$ un  réel tel que $\dfrac{1}{2} \leqslant  x \leqslant 1$
        et $n$ un entier naturel. 

		1. $(\ln (1 / 2))^{n} \leqslant \dfrac{(\ln (x))^{n}}{1+x}       $

		1. $0 \leqslant \dfrac{(\ln (x))^{n+1}}{1+x} \leqslant \dfrac{(\ln (x))^{n}}{1+x}$

		1. $0 \leqslant \dfrac{(\ln (x))^{n}}{1+x} \leqslant \dfrac{(\ln (x))^{n+1}}{1+x}$

		1. La suite $\left(u_{n}\right)_{n}$ est monotone. 

		1. La suite $\left(u_{n}\right)_{n}$ converge vers $0 . $

	=== "Corrigé"

         1.    Contre-exemple   $:    x=1$   et    $n=1$   sachant    que   $\ln
            \left(\frac{1}{2}\right)<0$
		 1.  Contre-exemple   :  $x=\frac{1}{2}$  et  $n=1$   sachant  que  $\ln
		    \left(\frac{1}{2}\right)<0$
		 1.  Contre-exemple   $:  x=\frac{1}{2}$  et  $n=1$   sachant  que  $\ln
		    \left(\frac{1}{2}\right)<0$
		 1. $u_{1}<0, u_{2}>0$ et $u_{3}<0$ donc $u_{1}<u_{2}$ et $u_{2}>u_{3}$.
	     1.  $\left|u_{n}\right|  \leqslant   \int_{1  /  2}^{1}\left|\frac{(\ln
	        (x))^{n}}{1+x}\right| \mathrm{d}  x \quad$ or  pour tout $x  \in[1 /
	        2 ;  1], 0 \leqslant|\ln (x)|  \leqslant \ln (2) \quad$  et $\quad 0
	        \leqslant  \frac{1}{1+x}  \leqslant   1$.  Donc  $\left|u_{n}\right|
	        \leqslant \frac{1}{2}(\ln  (2))^{n}$ de plus $-1<\ln  (2)<1$ donc ()
	        théorème des gendarmes $)\left(u_{n}\right)$ converge vers 0 .
			



## Pour comparer des sommes

!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux :  On note $\left(T_{n}\right)_{n \geqslant  1}$ la suite
        définie par $T_{n}=\displaystyle\sum_{k=n+1}^{2 n} \dfrac{1}{k}$ 

        Soit $n \in \mathbb{N}^{*}$.

		1. $\dfrac{1}{2} \leqslant T_{n} \leqslant 1        $

		1. La suite $\left(T_{n}\right)_{n}$ converge

		1. $T_{n+1} \leqslant T_{n}$


	=== "Corrigé"

         1. Chacun des $n$ termes de la somme est compris entre 0 et 1.
		 1.  Chacun des $n$ termes de la somme est compris entre $\frac{1}{2 n}$
		     et $\frac{1}{n+1}$.
		 1.               $T_{n+1}-T_{n}=\frac{1}{2              n+2}+\frac{1}{2
		    n+1}-\frac{1}{n+1}=\frac{1}{2    n+1}-\frac{1}{2    n+2}=\frac{1}{(2
		    n+1)(2 n+2)}>0$
		 1.          $         T_{n+1}-T_{n}=\frac{1}{2          n+2}+\frac{1}{2
		    n+1}-\frac{1}{n+1}=\frac{1}{2    n+1}-\frac{1}{2    n+2}=\frac{1}{(2
		    n+1)(2 n+2)} \geqslant 0$
		 1.  La  suite $\left(T_{n}\right)$  est  croissante  majorée donc  elle
		    converge.
			


!!! {{ exercice()}}

    === "Énoncé"

        Vrai-Faux :  On définit  la suite  $\left(u_{n}\right)_{n \geqslant
        1}$ par $u_{n}=-2 \sqrt{n}+\displaystyle\sum_{k=1}^{n} \dfrac{1}{\sqrt{k}}$. 

        Soient $n$ et $k$ deux entiers naturels non nuls.

		1.  $u_{n+1}  \leqslant  u_{n}   \cdot  $  1.  $\sqrt{k+1}-\sqrt{k}
		   \leqslant \dfrac{1}{2 \sqrt{k}} \leqslant \sqrt{k}-\sqrt{k-1} $ 

		1. $u_{n} \leqslant u_{n+1}        $

		1. La suite $\left(u_{n}\right)_{n}$ est bornée.

	=== "Corrigé"

         1. $u_{n+1}-u_{n}=-2(\sqrt{n+1}-\sqrt{n})+\frac{1}{\sqrt{n+1}}=\frac{1}{\sqrt{n+1}}-\frac{2}{\sqrt{n+1}+\sqrt{n}}$
            or   $\sqrt{n+1}+\sqrt{n}  \leqslant   2   \sqrt{n+1}  \quad$   donc
            $u_{n+1}-u_{n} \leqslant 0$
		 1.  $u_{n+1}-u_{n}=-2(\sqrt{n+1}-\sqrt{n})+\frac{1}{\sqrt{n+1}}=\frac{1}{\sqrt{n+1}}-\frac{2}{\sqrt{n+1}+\sqrt{n}}$
		      or $\sqrt{n+1}+\sqrt{n}<2 \sqrt{n+1} \quad$ donc $u_{n+1}-u_{n}<0$ 
	      1.   D'une  part :  $\sqrt{k+1}-\sqrt{k}=\frac{1}{\sqrt{k+1}+\sqrt{k}}
	          \leqslant     \frac{1}{2     \sqrt{k}}$.    D'autre     part     :
	          $\sqrt{k}-\sqrt{k-1}=\frac{1}{\sqrt{k}+\sqrt{k-1}}       \geqslant
	          \frac{1}{2 \sqrt{k}}$
		  1.   En sommant  les  encadrements précédents  et  en simplifiant  les
		      sommes   télescopiques,   il   vient  $   \sqrt{n+1}-1   \leqslant
		      \frac{1}{2} \sum_{k=1}^{n}  \frac{1}{\sqrt{k}} \leqslant \sqrt{n}$
		      donc $-2  \sqrt{n}+2 \sqrt{n+1}-2 \leqslant u_{n}  \leqslant 0$ et
		      comme $\sqrt{n+1}-\sqrt{n}  \geqslant 0$ donc $-2  \leqslant u_{n}
		      \leqslant 0$ 
			  





	
## Réponses mélangées

 Faux Vrai Vrai Vrai Vrai Faux Faux Vrai Vrai
Faux Vrai Faux Vrai Faux Faux Vrai Vrai Vrai
Vrai Vrai Faux Faux Faux Faux Faux Vrai Vrai
Faux Vrai Faux Vrai Vrai Vrai Vrai Faux Vrai
Vrai


