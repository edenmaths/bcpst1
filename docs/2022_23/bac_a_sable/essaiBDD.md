# Pense-bête `sqlite-console`


Penser au chemin relatif depuis docs

```
{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" init="2021_22/bac_a_sable/sql/les_Aliens.sql"}!}
```


{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" init="2021_22/bac_a_sable/sql/les_Aliens.sql"}!}

avec dans `les_Aliens.sql`

```sql
CREATE TABLE IF NOT EXISTS `Table_villes` (
	`Planete`	TEXT,
	`Ville`	TEXT NOT NULL,
	PRIMARY KEY(`Planete`)
);
INSERT INTO `Table_villes` VALUES ('Trantor','Terminus');
INSERT INTO `Table_villes` VALUES ('Euterpe','Arcturus');
INSERT INTO `Table_villes` VALUES ('Helicon','Kalgan');
INSERT INTO `Table_villes` VALUES ('Aurora','Hesperos');
INSERT INTO `Table_villes` VALUES ('Gaia','Siwenna');
INSERT INTO `Table_villes` VALUES ('Avonlea','Greengables');
INSERT INTO `Table_villes` VALUES ('Nexon','Solaris');

...
```

On peut rajouter une requête 

```
{!{ sqlide titre="Requête SQL préexistente" init="2021_22/bac_a_sable/sql/les_Aliens.sql", sql="2021_22/bac_a_sable/sql/requete1.sql"}!}
```


{!{ sqlide titre="Requête SQL préexistente" init="2021_22/bac_a_sable/sql/les_Aliens.sql", sql="2021_22/bac_a_sable/sql/requete1.sql"}!}


Ne pas oublier de rajouter le `pip install git+https://github.com/Epithumia/mkdocs-sqlite-console` dans le `.gitlab-ci.yml`
