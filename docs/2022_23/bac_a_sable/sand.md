- p24 
     - $(∀ x ∈ \mathbb R_+)(∀n ∈ \mathbb N)((1+x)^n \geqslant 1+nx)$
	 - $(∀x>0)((1+x)^0=1≥1+0\times x$ 
	 - $(∀x>0)(∀n∈\mathbb N)(\mathcal P(n)⟹ (1+x)^{n+1}=(1+x)(1+x)^n≥(1+x)(1+nx)=1+x+nx+nx^2=1+(n+1)x+nx^2≥1+(n+1)x)$
	 - $(∀n∈\mathbb N)(\mathcal P(n)⟹\mathcal P(n+1)$
  
- p27
     - d'une famille  de variables $(x_i)_{0 \leqslant i \leqslant n}$
	 - une  famille $(λ_i)_{0 \leqslant i \leqslant n}$
	 - Alors: $\Big(\forall \lambda\in\mathbb{R}\Big)\Big( \sum_{k=p}^n (x_k + \lambda y_k) = \sum_{k=p}^n x_k + \lambda\sum_{k=p}^n y_k\Big)$
	 - n veut par exemple calculer $S_n=\sum_{k=1}^{n} (k-1)^2$
	 - Soit $(u_i)_{i∈ \mathbb N}$ une suite de nombres.
  
- p28
     - Soit  $n$  un entier  naturel  et  $(x_i)_{i ∈  [[ 0,n ]]}$
 

- p 32
     - $\sum_{k=1}^{n}k^3  +  (n+1)^3=\sum_{k=1}^{n+1}k^3  $
     - $S_2=\sum_{k=1}^{n}\frac{(-1)^k}{2^{k-1}}$
  
- problème page 61 : il y a des e/2 et des e au lieu d'$ε$

- p73 : au-dessus de **application à la preuve de la divergence** : mettre le $n\to +\infty$ sous la limite : 
$\displaystyle\lim_{n\to+\infty}(v_{n+1}-v_n)=0$

- p82 : enlever les 2 lignes  **Attention** Pour vous amuser...

- p93 page XXX : c'est la page 82

- p114, les v de l'intégration par parties sont bizarrement grands

- p123 : une seule ligne  pour les équations ? (si besoin de place)

- p133/134  combinaisons et page 148, 170, 173

- p166 remplacer la dernière ligne du gros encadré par :
  
  ```python
  return f"Gagne en changeant : {100. * gagne_en_changeant/n}% Gagne sans changer : {100. * gagne_sans_changer/n}%" 
  ```
  
- p 206 dans le script, mettre partout  `List` et `Tuple` en minuscules `list` et `tuple`
  Enlever la ligne `from typing import List, Tuple`

- page 220 scripts des solutions du 6.3

```python
def smul(nb: float, xs: list[float]) -> list[float]:
    return [nb * x for x in xs]
```

```python
def vsom(xs: list[float], ys: list[float]) -> list[float]:

reste inchangé
```

idem pour `def vsom2` et `vdef dif`

- page 221 sol du 6.4

```python

def bords(m: list[list[int]]) -> list[list[int]]:

reste idem
```

- p222

```python
def som_chiffres(n: int, b: int) -> int:

reste idem
```

idem pour `som_rec(n: int, b: int) →int:`

- 6.6 

```python
def comptage(L: list[int], N: int) -> list[int]:

reste idem
```


```python
def tri(L: list[int], N: int) -> list[int]:

reste idem
```

- p224 6.7

```python
def nombreZeros(t: list[int], i:int) -> int:

reste idem
```


```python
def nombreZerosMax(t: list[int]) -> int:

reste idem
```

```python
def nombreZerosMax2(t: list[int]) -> int:

reste idem
```


- p225 6.8

```python
def nb_conflits() -> int:

reste idem
```

```python
def nb_vol_par_niveau_relatif(regulation: list[int]) -> list[int] :
```

```python
def cout_regulation(regulation: list[int]) -> int:
```

```python
def cout_RFL() -> int:
```

- p 226 6.9

```python
def pb1_1(borne: int) -> int :
```

et idem pour les autres `pb1`

```python
def somme_divisibles(nb: int, borne: int) -> int :
```

```python
def ens_div1(n: int) -> set[int]:
```

idem `ens_div`

- 6.11

```python
def crible(n: int) -> set[int] :
```

- 6.12
```python
def les_diviseurs_brute(n: int) -> set[int] :
```

```python
def les_diviseurs(n: int) -> set[int] :
```

```python
def est_premier(n: int) -> bool :
```


```python
def prochain_premier(n: int) -> int :
```

```python
def ppd(n: int) -> int:
```



```python
def est_premier(n: int) -> bool :
```


```python
def prochain_premier(n: int) -> int :
```


- 6.13 

```python
def huit(n: int) -> int :
```


- 6.14

```python
def piece(n: int) -> dict[int, float]:
```

```python
def boules1(n: int) -> dict[int, float]:
```


```python
def boules2(n: int) -> dict[int, float]:
```

- 6.15

```python
def EstimeProbaEchec (m: int, tmax: int) -> int :
```

- 6.16

```python
def mandelbrot(c: float) -> int :
```
