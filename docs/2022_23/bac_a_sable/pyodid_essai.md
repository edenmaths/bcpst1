
-  Vous  pouvez  taper votre  code  en  utilisant  la  touche ++tab++  pour  les
  indentations.
- Il  y a un  `#!python print` par  défaut de la  dernière commande donc  ne pas
  utiliser `#!python  print` dans  votre code  mais il  faut une  instruction en
  dernière ligne.
- le bouton `Nettoyer` efface la fenêtre des résultats.
- le  bouton `Afficher/Masquer` permet de  basculer entre l'affichage et  le non
  affichage des instructions dans la fenêtre des résultats.


{{ pyodide()}}



- Le javascript dans `/javascript/pyodid.js`:


```javascript
 //// coloration syntaxique

function update(text) {
  let result_element = document.querySelector("#highlighting-content");
  // MAJ du code
  result_element.innerHTML     =      text.replace(new     RegExp("&",     "g"),
  "&amp;").replace(new RegExp("<", "g"), "&lt;"); /* remplacement des tags html*/
  // Utilisation de SyntaxHighlight pour colorer
  hljs.highlightElement(result_element);
}

function sync_scroll(element) {
  /* on récupère la partie que l'utilisateur voit (colorée) */
  let result_element = document.querySelector("#highlighting-content");
  // On récupère les coords du caché pour en faire celles du visible
  result_element.scrollTop = element.scrollTop;
  result_element.scrollLeft = element.scrollLeft;
}

function check_tab(element, event) {
    let tabu = "\t".length;
    let code = element.value;
    if(event.key == "Tab") {
    /* si on appuie sur Tab */
    event.preventDefault(); // on neutralise le passage d'un cadre à l'autre
    let before_tab = code.slice(0, element.selectionStart); // texte avant la  tabulation
    let after_tab = code.slice(element.selectionEnd, element.value.length); // texte après  tab
    let cursor_pos = element.selectionEnd + tabu; // on avance le curseur de tabu (3)
    element.value = before_tab + "\t" + after_tab; // on transforme la chaine en intercalant la tabul
    // déplace curseur
    element.selectionStart = cursor_pos;
    element.selectionEnd = cursor_pos;
  }
}
//// fin coloration syntaxique

var output = document.getElementById("output"); // ce qui est répondu
const code = document.getElementById("editing"); // le code entré
const affiche = document.getElementById('affiche');  // savoir si on affiche les entrées dans la sortie 
let cpt = 0; // compteur de réponses

function addToOutput(s) {
   cpt += 1;
   output.value += 'Rés [' + cpt+ ']: ' + s + '\n';
}

function clearOutput(s) {
   output.value = '';
}

function afficheCommande(s) {
    cpt += 1;
    output.value += 'Ent ['+ cpt+ ']: ' + code.value + '\n';
    output.value += 'Rés [' + cpt+ ']: ' + s + '\n';
}

var clicked = false;
function clique(event){
    clicked = !clicked;
}

if (affiche) {
    affiche.addEventListener("click", clique)};

if (output){
    output.value = 'Un instant Dave, je réveille Python...\n';
}
else {
    output = "";
}
 // appel de  Pyodide
async function main(){
    // on attend la réponse de Pyodide
    await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.17.0/full/' });
    output.value = 'Je vous écoute Dave\n';
}

let pyodideReadyPromise = main();

// fonction principale appelée par le bouton Exécuter
async function evaluatePython() {
    await pyodideReadyPromise;
    // on attrappe les éventuelles erreurs
   try { // On affiche ou non les commandes dans la sortie
       let output = await pyodide.runPythonAsync(code.value);
       if (clicked) {
	   afficheCommande(output);
       } else {
	   addToOutput(output);
       }
   } catch(err) {
	 addToOutput(err);
   }
 }
```

- Pour avoir la `textarea` extensible (dans `/stylesheet/extra.css`):

```css
/// syntax highlight


/* Paragraphs */

p code {
  border-radius: 2px;
  background-color: #eee;
  color: #111;
}

/* cf
   https://css-tricks.com/creating-an-editable-textarea-that-supports-syntax-highlighted-code/
 */

#bout {
    text-align: center;
    display: block;
    margin-left: auto;
    margin-right: auto;
}

#editing, #highlighting-content, #output {
    /* On met les deux cadres dans les mêmes conditions pour la superposition*/
    margin: 10px;
    padding: 10px;
    /* border: 0; */
    width: calc(100% - 32px);
    height: 150px;
}
#editing, #highlighting-content{
  /* idem pour le texte*/
  font-size: 10pt;
  font-family: monospace;
    line-height: 15pt;
    font-weight: bold;
}


#editing {
  /* In the same place */
  position: relative;
  top: 0;
  left: 0;
}

#highlighting-content {
  /* on remonte le texte coloré*/
  position: relative;
  top: -180px;
  left: 0;
}


/* Move the textarea in front of the result */

#editing {
    z-index: 1;
}
#highlighting-content {
  z-index: 0;
}


/* le texte tapé est transparent*/

#editing {
  color: transparent;
  background: transparent;
  caret-color: white;
}

/*  */
#editing, #highlighting-content {
  overflow: auto;
}

/* Pas resize dans la zone de frappe*/
#editing {
  resize: none;
}

textarea {
    background-color:  rgba(0, 0, 0, 0.75);
    color: var(--md-code-fg-color);
    font-weight: bold;
    display: block;
    /*font-family: var(--md-code-font-family,_),SFMono-Regular,Consolas,Menlo,monospace;*/
}

textarea#output {
    background-color:  rgba(50, 0, 0, 0.75);
    overflow:auto;
}


/* pour les deux fenêtres en // */
.row {
  display: flex;
}

.col {
  flex: 50%;
}
```

- la macro dans `main.py`

```python
def pyodide0():
    s ="<div><button id='rouge' onclick='evaluatePython()'>Exécuter</button> <button id='rouge' onclick='clearOutput()'>Nettoyer</button> <button id='affiche'>Afficher/Masquer</button> </div> <div style=\"height: 200px;\"><textarea placeholder=\"Entrer le code Python ici\" id=\"editing\" spellcheck=\"false\" oninput=\"update(this.value); sync_scroll(this);\" onscroll=\"sync_scroll(this);\" onkeydown=\"check_tab(this, event);\"></textarea><pre id=\"highlighting-content\" aria-hidden=\"true\"><code class=\"language-python\" id=\"highlighting-content\"></code></pre> </div> <div>Résultat:<textarea id='output'  common\" ></textarea> </div>"
    return s
	
def define_env(env):
  @env.macro
  def pyodide():
      return pyodide0()
```

qui correspond à ce code `html`:

```html

<div id="bout">
 <button onclick='evaluatePython()'  id='rouge'>Exécuter</button>
 <button id='rouge' onclick='clearOutput()'>Nettoyer</button> 
 <button id='affiche'>Afficher/Masquer</button> 
</div>

<div class="row"> <div class="col">
<div style="height: 200px;">
<textarea placeholder="Entrer le code Python ici" id="editing" spellcheck="false" oninput="update(this.value); sync_scroll(this);" onscroll="sync_scroll(this);" onkeydown="check_tab(this, event);"></textarea>
<pre id="highlighting" aria-hidden="true">
<code class="language-python" id="highlighting-content">
</code>
</pre>
</div>
</div>
<div class="col">
<textarea id='output'  ></textarea>
</div>
</div>
```

dans `mkdocs.yml`:

```yaml
markdown_extensions:
  - pymdownx.highlight:
      linenums: true
      use_pygments: false
	  
extra_javascript:
  - javascripts/pyodid.js
  - https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js
  
extra_css:
  - stylesheets/extra.css
  - https://highlightjs.org/static/demo/styles/tomorrow-night-bright.css
```

dans `/overrides/main.html`:

```html
{% block libs %}
<script src="https://cdn.jsdelivr.net/pyodide/v0.17.0/full/pyodide.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script><script>hljs.initHighlightingOnLoad();</script>
{% endblock %}
```

et il ne reste qu'à taper `pyodide()` entre deux accolades pour avoir la console
dans son fichier `md`.
