{{ chapitre(4, "Dénombrement - Dictionnaires", "TP ", 0)}}


# Compteurs : avec des dictionnaires


## PI


### $π$ à volonté

Regardez ces premières décimales de $ π$. Y trouvez-vous une certaine symétrie?

$$3,14159\; 26\; 53589\; 79\; 32\; 38\; 46\; 26\; 43\; 38\; 32\; 79\;... $$


On peut aller un peu plus loin dans l'exploration en allant voir 100 000
décimales de $ π$ et compter combien il y en a de chaque sorte. On peut utiliser
 la constante `pi` et la fonction `N` de `sympy` pour afficher des décimales de $ π$ :

```python
>>> from sympy import N, pi
>>> N(pi, 100)
3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117068
```

!!! {{ exercice()}}

    === "Énoncé"

         Déterminer une fonction `stats_pi(n: int) -> list` qui donne les fréquences
         de sortie en pourcentage des chiffres constituant les `n` premiers chiffres de
         l'écriture décimale de $π$.
		
	     Par exemple :
		
		 ```python
		 >>> stats_pi(1000000)
		 Out[48]: 
		 [9.9959,  9.9757,  10.0026,  10.0230, 10.0230,  10.0359,  9.9548,  9.9800,
		 9.9985, 10.0106]
		 ```



### Découverte des dictionnaires Python


Nous  avions jusqu'à  maintenant  manipulé des  données à  l'aide  de listes  qui
étaient indexées par des  entiers : on y accède aux éléments  par leur rang dans
la liste.

Il  existe une  autre structure  de donnée  qui permet  de choisir  le type  des
indices qui peuvent notamment être des chaînes de caractères. Par exemple :

```python
In [1]: chaines = {'A': 5, 'C': 2, 'G': 24, 'T': 12}

In [2]: chaines['A']
Out[2]: 5

In [3]: bases = [cles for cles in chaines]

In [4]: bases
Out[4]: ['A', 'C', 'G', 'T']

In [5]: freqs = [chaines[cle] for cle in chaines]

In [6]: freqs
Out[6]: [5, 2, 24, 12]

In [7]: chaines['A'] += 10

In [8]: chaines
Out[8]: {'A': 15, 'C': 2, 'G': 24, 'T': 12}

In [9]: {base: 0 for base in 'ACGT'}
Out[9]: {'A': 0, 'C': 0, 'G': 0, 'T': 0}
```

Un dictionnaire  est donc une  structure mutable  associant des *valeurs*  à des
*clés*.  On  utilise des  accolades  comme  pour  les  ensembles car  c'est  une
structure par essence non ordonnée.



!!! {{ exercice()}}

    === "Énoncé"

         Déterminer une fonction `stats_pi(n: int) -> dict` qui donne les fréquences
         de sortie des chiffres constituant les `n` premiers chiffres de
         l'écriture décimale de $π$.
		
	     Par exemple :
		
		 ```python
		 >>> stats_pi(1000000)
		 Out[48]: 
		 {'0': 9.9959,
		  '1': 9.9757,
		  '2': 10.0026,
		  '3': 10.0230,
		  '4': 10.0230,
		  '5': 10.0359,
		  '6': 9.9548,
		  '7': 9.9800,
		  '8': 9.9985,
		  '9': 10.0106}
		 ```

	=== "Indications"

          On commencera par :
		 
     	  ```python
		  from sympy import N, pi

	      def stats_pi(n: int) -> dict[str, int]:
		 	 res = {str(i): 0 for i in range(...)}
			 pipi = '3' + str(N(pi, n))[2:]
			 for ... :
			     ...
			 return res
		  ```


!!! {{ exercice()}}

    === "Énoncé"

         On voudrait obtenir le diagramme en barre des fréquences obtenues. Vous
         allez  pour ce  faire utiliser  la  fonction `bar(Vals,  Freqs)` de  la
         bibliothèque `matplotlib.pyplot` où mystérieusement `Vals` est la liste
         des valeurs et `Freqs` la liste des....
		 
		 



!!! {{ exercice()}}

    === "Énoncé"

        On trouve tout dans $π$... Créer une fonction 
		`cherche(motif: str, n:  int) -> int` qui renvoie le  rang du début
		de la première occurence du  motif dans les `n` premières décimales
		de π.
		
		```python
		>>> cherche('1234567', 10000000)
        9470344
		```


	=== "Indications"

         ```python
		 def cherche(motif: str, n: int) -> int:
			 pipi = ...
			 taille = ...
			 for i in range(n - taille):
				 if ...:
					 return i
	         raise ValueError(f"{motif} n'a pas été trouvé dans les {n} premières décimales de π") 
		 ```
		 
		 Cet  algorithme  est  très  naïf.  De  nombreux  algorithmes  plus
		 efficaces sont couramment utilisés, en bioinformatique notamment.


!!! {{ exercice()}}

    === "Énoncé"

        Créer une fonction 
		`parse(chaine:  str,  long:  int)   ->  dict[str,  list[int]]`  qui
		parcourt une chaîne  et renvoie un dictionnaire où  les clés sont
		les motifs de longueur `n` et  les valeurs la liste de leurs positions
		dans la chaîne dans l'ordre croissant. 
		
		
		Par exemple :
		
		```python
		In [1]: parse("12312", 2)
		Out[1]: {'12': [0], '23': [1], '31': [2]}
		
		In [2]: pipi = '3' + str(N(pi, 1000))[2:]

        In [3]: parse(pipi, 3)
		Out[3]: 
		{'314': [0],
		 '141': [1, 295],
		 '415': [2, 392],
		 '159': [3, 730, 922],
		 '592': [4, 61],
		 '926': [5],
		 '265': [6],
		 '653': [7],
		 '535': [8, 141],
		 '358': [9],
		 '589': [10, 695],
		 '897': [11],
		 '979': [12],
		 '793': [13, 439, 486],
		 ...
		```
		
		
		Déterminer  alors le  motif  de  longueur 7  qui  apparaît le  plus
		souvent dans les $10^7$  premiers chiffres du développement décimal
		de $π$ en créant votre propre  algorithme de recherche de maximum adapté
		à la situation.
		
		Commentez  l'utilisation  de  cette   fonction  par  rapport  à  la
		recherche naïve.

	=== "Indications"
	
	    ```python
		def parse(chaine: str, n: int) -> dict[str, list[int]]:
			dic = {}
			lc = len(chaine)
			for i in range(...):
				motif = ...
				if motif ...:
					dic[motif] = ...
				else:
					...
	         return dic
		```





## Génôme du SARS COVID 2

### Récupération du génôme


Vous pouvez retrouver le génôme du *Severe acute respiratory syndrome coronavirus
2          isolate          Wuhan-Hu-1*          en          suivant          [ce
lien](https://www.ncbi.nlm.nih.gov/projects/sviewer/?id=NC_045512&tracks=%5Bkey:sequence_track,name:Sequence,display_name:Sequence,id:STD649220238,annots:Sequence,ShowLabel:false,ColorGaps:false,shown:true,order:1%5D%5Bkey:gene_model_track,name:Genes,display_name:Genes,id:STD3194982005,annots:Unnamed,Options:ShowAllButGenes,CDSProductFeats:true,NtRuler:true,AaRuler:true,HighlightMode:2,ShowLabel:true,shown:true,order:9%5D&v=1:29903&c=null&select=null&slim=0).


Nous n'allons travailler que la suite des G,  A, C et T au format `txt` à l'aide
de Python.

Elle commence ainsi:

```
CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGT
```

Nous allons commencer par récupérer ce fichier qui est en ligne :

```python
from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
with open("./genome.txt", "r") as gen:
	covid = gen.read().rstrip()
```


La méthode  `read`  va  permettre  de   transformer  ce  texte  en  chaîne  de
caractères.  La  méthode  `rstrip`  permet  d'effacer  les  caractères  spéciaux
indésirables : passage à la ligne, tabulation, espaces à la fin, etc.

```
>>> covid

'CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGTCCAAGACATGTGATCTGCACCTCTGAAGACATGCTTAACCCTA...'
```

Nous voudrions compter les bases azotées.

!!! {{ exercice()}}
    
	1. Déterminer une fonction qui va compter les bases azotées
       dans un génôme et renvoyer un dictionnaire `base: fréquence`:
       
	    ```python
        >>> compte_bases(gs)
        {'A': 2953, 'C': 1817, 'G': 1985, 'T': 3245}
        ```
		
	2. Quelle  est la(es) séquence(s) de  longueur 4 qui apparaît(issent)  le plus souvent
	   dans le génôme du covid sur lequel vous travaillez.

    2. Déterminer un dictionnaire des fréquences des n-uplets de bases.  Par exemple
       pour les doublets:

        ```python
        compte_nuplets(gs, 2)

 		{'CT': 694,
		 'TG': 935,
		 'GC': 396,
		 'TT': 1020,
		 'GT': 712,
		 'TC': 432,
		 'CA': 714,
		 'AT': 819,
		 'CG': 124,
		 'AA': 866,
		 'AG': 564,
		 'GG': 362,
		 'GA': 515,
		 'AC': 703,
		 'TA': 858,
		 'CC': 285}
		```


	3. On peut aussi ne s'occuper que des répétitions d'une même base :

		```python
		compte_repets(gs, 2)

		{'TT': 1020, 'AA': 866, 'GG': 362, 'CC': 285}
		```

		```python
		compte_repets(gs, 4)

		{'TTTT': 95, 'AAAA': 76, 'GGGG': 7}
		```




         




# Nombre de combinaisons 


##  Testons les propriétés des coefficients binomiaux

En utilisant les propriétés des coefficients binomiaux, déterminer
plusieurs fonctions calculant $p$ parmi $n$ et comparer leur efficacité.

Pour la comparaison, on pourra utiliser la fonction suivante :

```python
import timeit

def temps(commande: str) -> float:
    res =  timeit.timeit(stmt=commande, globals=globals(), number=10)
    print(f'{commande} a été exécuté en {res*10**6:.2f} μs')
```

Alors par exemple:

```python
temps('combi1(10, 5)')
temps('combi2(10, 5)')
```

donnent


```python
combi1(10, 5) a été exécuté en 1299.84 μs
combi2(10, 5) a été exécuté en 62.88 μs
```


## Avec un dictionnaire : comment la méthode la moins efficace devient la plus efficace

Que se passe-t-il ?

```python
def combi_cache(n, K, cache = {}):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    elif k == 0:
        return  1
    elif k == 1:
        return n
    if (n, k) in cache:
        #print("rappel via le cache de C(" + str((n, k)) + ")")
        return cache[(n, k)]
    else:
        cache[(n, k)] = combi_cache(n-1, k-1) + combi_cache(n-1, k) 
        #print("calcul via la définition de C(" + str((n,k)) + ")")
        return cache[(n,k)]
```









## Un petit jeu


```python
def triangle(n: int, symbole='*') -> None:
    for i in range(n + 1):
        ligne = ''
        for j in range(i + 1):
            if combi(i, j) % 2 == 0:
                ligne += ' '
            else:
                ligne += symbole
        print(ligne)
```
