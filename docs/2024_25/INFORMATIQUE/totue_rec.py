from turtle import *

def turec(n):
    left(45)
    forward(50 + n)
    if n < 50 :
        turec(n + 1)
    else:
        hideturtle()
        write("   FINI")
        exitonclick()
        
def flocandrea(n, cote):
    if n == 0:
        forward(cote)
    else:
        flocandrea(n - 1, cote/3)
        left(60)
        flocandrea(n - 1, cote/3)
        right(120)
        flocandrea(n - 1, cote/3)
        left(60)
        flocandrea(n - 1, cote/3)
        
def floc(n, cote):
    flocandrea(n, cote)
    right(120)
    flocandrea(n, cote)
    right(120)
    flocandrea(n, cote)