# MODÉLISATION MATHÉMATIQUE ET INFORMATIQUE - AGRO-VÉTO 2019

## Durée : 3 heures 30 minutes

*Chaque  candidat  est   responsable  de  la  vérification   de  son  sujet
d'épreuve : pagination et impression de  chaque page. Ce contrôle doit être
fait en début d'épreuve  : en cas de doute, il doit alerter  au plus tôt le
chef de centre qui contrôlera et éventuellement remplacera le sujet.*


*Si, au cours de  l'épreuve, un candidat repère ce qui  lui semble être une
erreur d'énoncé, il  le signale sur sa copie et  poursuit sa composition en
expliquant les raisons des initiatives qu'il a été amené à prendre.*


*L'usage d'une calculatrice est autorisé  pour cette épreuve. Les questions
d'informatique devront être rédigées en langage Python exclusivement.*


*Les différentes parties de l'épreuve sont indépendantes sauf les parties 4
et 5. Les questions d'informatique sont regroupées dans la partie 2.*


Le but de cette épreuve est d'étudier différentes méthodes pour modéliser des phénomènes qui dépendent du temps. Parmi les problématiques souvent étudiées, on peut par exemple citer l'étude de l'évolution de la population d'un pays au cours du temps à des fins de contrôles démographiques, l'analyse de l'évolution de la concentration en ozone en fonction du temps afin d'évaluer le niveau de pollution d'une zone donnée ou encore l'évolution de la température au cours du temps dans un pays afin d'étudier la problématique du réchauffement climatique.

Dans ce sujet, on va s'intéresser à la modélisation des niveaux annuels du lac Huron exprimés en mètres entre 1875 et 1972 représentés dans la figure 1.

![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-02.jpg?height=691&width=1142&top_left_y=765&top_left_x=431)

Figure 1 - Niveaux annuels du lac Huron exprimés en mètres.


## PARTIE 1

Dans cette partie, on note $n$ un entier naturel supérieur ou égal à 3 . Pour tout entier $i$ dans $\{1,2, \ldots, n\}$, on note $y_{i}$ le niveau du lac Huron de la $i$ ème observation à l'instant $t_{i}$. Dans la figure 1 , les points de coordonnées $\left(t_{i}, y_{i}\right)$ sont représentés par des points $\left({ }^{\prime} \bullet\right)$. Dans la suite, les $t_{i}$ sont supposés distincts. On s'intéresse à l'ajustement d'un polynôme de degré 2 au nuage de points $\left(\left(t_{1}, y_{1}\right),\left(t_{2}, y_{2}\right), \ldots,\left(t_{n}, y_{n}\right)\right)$. Un tel ajustement est représenté dans la figure 2 . Pour réaliser cet ajustement on utilise le critère des moindres carrés défini ci-dessous.

1. On  définit la  fonction $F: \mathbb{R}^{3}  \longrightarrow \mathbb{R}$
   par :
   
    $$
    \forall(a, b, c) \in \mathbb{R}^{3}, \quad F(a, b,c)=\sum_{i=1}^{n}\left(y_{i}-a-b t_{i}-c t_{i}^{2}\right)^{2}
    $$
   
    1. Expliquer en quelques lignes ou à l'aide d'un dessin ce que l'on
       cherche à faire lorsque l'on minimise la fonction $F$ ci-dessus par
       rapport à $a, b$ et $c$.
   
    2. Justifier que les dérivées partielles de $F$ par rapport à $a, b$ et
       $c$ existent. Calculer les dérivées partielles de $F$ par rapport à
       $a, b$ et $c$ que l'on notera dans la suite : $\frac{\partial
       F}{\partial a}(a, b, c)$, $\frac{\partial F}{\partial b}(a, b, c)$ et
       $\frac{\partial F}{\partial c}(a, b, c)$, respectivement. 

   ( *les questions qui suivaient faisaient appel au programme de 2e année* )

<!--
2. On note $\langle u ; v\rangle$ le produit scalaire dans $\mathbb{R}^{n}$ de deux vecteurs $u=\left(\begin{array}{c}u_{1} \\ u_{2} \\ \vdots \\ u_{n}\end{array}\right)$ et $v=\left(\begin{array}{c}v_{1} \\ v_{2} \\ \vdots \\ v_{n}\end{array}\right)$ de $\mathbb{R}^{n}$, défini par :

$$
\langle u ; v\rangle=\sum_{i=1}^{n} u_{i} v_{i}
$$

On note également :

$$
T=\left(\begin{array}{ccc}
1 & t_{1} & t_{1}^{2} \\
1 & t_{2} & t_{2}^{2} \\
\vdots & \vdots & \vdots \\
1 & t_{n} & t_{n}^{2}
\end{array}\right), Y=\left(\begin{array}{c}
y_{1} \\
y_{2} \\
\vdots \\
y_{n}
\end{array}\right) \text { et } \beta=\left(\begin{array}{c}
a \\
b \\
c
\end{array}\right)
$$

a) Montrer que :

$$
Y-T \beta=\left(\begin{array}{c}
y_{1}-a-b t_{1}-c t_{1}^{2} \\
y_{2}-a-b t_{2}-c t_{2}^{2} \\
\vdots \\
y_{n}-a-b t_{n}-c t_{n}^{2}
\end{array}\right)
$$

En déduire que :

$$
\frac{\partial F}{\partial a}(a, b, c)=-2\left\langle Y-T \beta ;\left(\begin{array}{c}
1 \\
1 \\
\vdots \\
1
\end{array}\right)\right\rangle
$$

b) Montrer que :

$$
\frac{\partial F}{\partial b}(a, b, c)=-2\left\langle Y-T \beta ;\left(\begin{array}{c}
t_{1} \\
t_{2} \\
\vdots \\
t_{n}
\end{array}\right)\right\rangle
$$

De la même façon, on pourrait montrer que :

$$
\frac{\partial F}{\partial c}(a, b, c)=-2\left\langle Y-T \beta ;\left(\begin{array}{c}
t_{1}^{2} \\
t_{2}^{2} \\
\vdots \\
t_{n}^{2}
\end{array}\right)\right\rangle
$$

3. Montrer à l'aide des questions précédentes que si $\widehat{\beta}=\left(\begin{array}{l}\widehat{a} \\ \widehat{b} \\ \widehat{c}\end{array}\right)$ est solution du système suivant :

$$
\left\{\begin{aligned}
\frac{\partial F}{\partial a}(a, b, c) & =0 \\
\frac{\partial F}{\partial b}(a, b, c) & =0 \\
\frac{\partial F}{\partial c}(a, b, c) & =0
\end{aligned}\right.
$$

alors

$$
\left({ }^{t} T T\right) \widehat{\beta}={ }^{t} T Y
$$

où ${ }^{t} T$ désigne la transposée de $T$. On admet que $\widehat{\beta}$ ainsi défini minimise la fonction $F$ définie dans la question 1.

4. Soit $A$ une matrice réelle ayant $n$ lignes et $p$ colonnes. On note

$$
\operatorname{Ker}(A)=\left\{u \in \mathbb{R}^{p}, A u=0\right\}
$$

a) Montrer que $\operatorname{Ker}(A)=\operatorname{Ker}\left({ }^{t} A A\right)$.

b) Que vaut $\operatorname{dim}\left(\operatorname{Ker}\left({ }^{t} A A\right)\right)+\operatorname{rang}\left({ }^{t} A A\right)$ ? Justifier.

c) Que vaut $\operatorname{dim}(\operatorname{Ker}(A))+\operatorname{rang}(A)$ ? Justifier.

d) En déduire une condition suffisante sur $A$ pour laquelle ${ }^{t} A A$ est inversible. Cette condition est-elle nécessaire?

5. Dans le cas où ${ }^{t} T T$ est inversible, donner l'expression de $\widehat{\beta}$.

![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-04.jpg?height=599&width=1037&top_left_y=1705&top_left_x=478)

Figure 2 - Niveaux annuels du lac Huron et ajustement d'un polynôme de degré 2.

L'ajustement  $T  \widehat{\beta}$  obtenu aux  questions  précédentes  est
représenté sur  la figure 2  ainsi que les  données. On remarque  que cette
modélisation peut être améliorée, ce sera le but des parties suivantes.

-->

##  PARTIE 2

Des éléments de syntaxe `Python`, et  en particulier l'usage du module numpy,
sont donnés en annexe  à la fin de la partie 2. Dans  tout ce qui suit, les
variables $n, p, A, M, i, j$  et $c$ vérifient les conditions suivantes qui
ne seront pas rappelées à chaque question : 

- $n$ et $p$ sont des entiers naturels tels que $p \geq n \geq 2$;
- $A$ est une matrice carrée à $n$ lignes inversible;
- $M$ est une matrice à $n$ lignes et $p$ colonnes telle que la sous-matrice carrée constituée des $n$ premières colonnes de $M$ est inversible;
- $i$ et $j$ sont des entiers tels que $0 \leq i \leq n-1$ et $0 \leq j \leq n-1$;
- $c$ est un réel non nul.

On note $L_{i} \leftarrow L_{i}+c L_{j}$ l'opération qui ajoute à la ligne $i$ d'une matrice la ligne $j$ multipliée par c.

1. Soit la fonction `initialisation` :

    ```python
    def initialisation(A):
       n = np.shape(A)[0]
       mat = np.zeros((n,2*n))
       for i in range(0, n):
           for j in range(0, n):
               mat[i,j] = A[i,j]
       return(mat)
    ```
	Pour chacune des affirmations suivantes, indiquer si elle est vraie ou
    fausse, en justifiant. L'appel `initialisation(A)` renvoie :

    1. une matrice rectangulaire à $n$ lignes et $2 n$ colonnes remplie de zéros;
    2.  une matrice de même taille que $A$;
    3. une erreur au niveau d'un range;
    4. une matrice rectangulaire telle que les $n$ premières colonnes correspondent aux $n$ colonnes de $A$, et les autres colonnes sont nulles.

2. Les trois fonctions `multip`, `ajout` et `permut` suivantes ne renvoient rien : elles *modifient* les matrices auxquelles elles s'appliquent.

    1. Que réalise la fonction `multip`?

        ```python
         
		 def multip(M, i, c):
            p = np.shape(M)[1]
            for k in range(0, p):
                 M[i,k] = c*M[i,k]
        ```

    2. Compléter la fonction `ajout`, afin qu'elle effectue l'opération $L_{i} \leftarrow L_{i}+c L_{j}$.

        ```python
         def ajout(M, i, j, c):
             p = np.shape(M)[1]
             for k in range(0, p):
                 _____ ligne(s) à compléter _____
        ```

    3. Écrire une fonction `permut` prenant pour argument $M, i$ et $j$, et qui modifie $M$ en échangeant les valeurs des lignes $i$ et $j$.

       Dans la suite du sujet, l'expression « opération élémentaire sur les lignes » fera référence à l'utilisation de `permut`, `multip` ou `ajout`.

3. Soit la colonne numéro $j$ dans la matrice $M$. On cherche le numéro $r$
   d'une ligne où  est situé le plus grand coefficient  (en valeur absolue)
   de  cette colonne  parmi  les lignes  $j$ à  $n-1$.  Autrement dit,  $r$
   vérifie : 

    $$
    |A[r, j]|=\max \{|A[i, j]| \text { pour } i \text { tel que } j \leq i \leq n-1\}
    $$

    Écrire une fonction `rang_pivot` prenant pour argument $M$ et $j$, et qui
    renvoie cette valeur de $r$. Lorsqu'il y a plusieurs réponses possibles
    pour $r$, dire (avec justification) si l'algorithme renvoie le plus petit
    $r$, le plus grand $r$ ou un autre choix. (*L'utilisation d'une commande
    `max` déjà programmée dans `Python` est bien sûr proscrite.*)

4. Soit la fonction `mystere` :

    ```python
     def mystere(M):
        n = np.shape(M)[0]
        for j in range(0, n):
            r = rang_pivot(M, j)
            permut(M, r, j)
            for k in range(j+1, n):
                ajout(M, k, j, -M[k,j]/M[j,j])
            print(M)
    ```


    1. On considère dans cette question l'algorithme `mystere` appliqué à la
        matrice $M_{1}=$ $\left(\begin{array}{ccc}3 & 2 & 2 \\ -6 & 0 & 12 \\ 1
        & 1 & -3\end{array}\right)$ : indiquer combien de fois la ligne
        `print(M)` est exécutée ainsi que les différentes valeurs qu'elle
        affiche. 

    2. De façon générale, que réalise cet algorithme?

5. Soit la fonction `reduire`, qui modifie $M$ :

    ```python
    def reduire(M):
        n = np.shape(M)[0]
        mystere(M)
        for i in range(0, n):
             multip(M, i, 1/M[i,i])
        #Les lignes suivantes sont à compléter :
        __________________________
    ```


    1. Compléter la fonction afin que la portion de code manquante effectue les opérations élémentaires suivantes sur les lignes :

        ```
        pour j prenant les valeurs n-1, n-2, ..., 1, faire :
            pour k prenant les valeurs j-1, j-2, ..., 0, faire :
                Lk reçoit Lk - M[k, j] Lj
        ```

    2. Indiquer ce que réalise cette fonction.

6. Inversion de $A$.

    1. Écrire une fonction `augmenter` prenant pour argument $A$ et qui renvoie la matrice de taille $(n, 2 n)$ définie ainsi :

        - dans la partie gauche (composée des $n$ lignes et $n$ premières colonnes), elle contient les coefficients de $A$;
        - dans la partie droite (composée des $n$ lignes et $n$ dernières colonnes), elle contient les coefficients de la matrice identité de taille $n$.

        Par exemple,

        $$
        \text { pour }\left(\begin{array}{cc}
        1 & 2 \\
        -1 & 3
        \end{array}\right) \text { la fonction renvoie }\left(\begin{array}{cccc}
        1 & 2 & 1 & 0 \\
        -1 & 3 & 0 & 1
        \end{array}\right)
        $$

    2. À l'aide des fonctions précédentes, proposer un raisonnement permettant d'inverser A.

    3. Écrire une fonction `inverser` prenant pour argument $A$ et qui renvoie la matrice inverse de $A$, en suivant le raisonnement décrit en question $6 \mathrm{~b}$.

    4. Quelle méthode connue venez-vous d'implémenter?

## 2. Annexe : Rappels Python pour la partie 2

On considère que le module `numpy`,  permettant de manipuler des tableaux à
deux dimensions, est importé via `import numpy as np`. Pour une matrice $M$
à $n$ lignes et $p$ colonnes, les indices vont de 0 à $n-1$ pour les lignes
et de 0 à $p-1$ pour les colonnes. 

| Python | Interprétation |
| :--- | :--- |
| `abs(x)` | Valeur absolue du nombre $x$ |
| `M[i, j]` | Coefficient d'indice $(i, j)$ de la matrice $M$ |
| `np.zeros(n, p)` | Matrice à $n$ lignes et $p$ colonnes remplie de zéros |
| `T = np.shape(M)` | Dimensions de la matrice $M$ |
| `T[0]` ou `np.shape(M)[0]` | Nombre de lignes |
| `T[1]` ou `np.shape(M)[1]` | Nombre de colonnes |
| `M[a:b, c:d]` | Matrice extraite de $M$ constituée des lignes $a$ à $b-1$ et des |
|  | colonnes $c$ à $d-1:$ |
|  | si $a$ (resp. $c)$ n'est pas précisé, l'extraction commence à la |
|  | première ligne (resp. colonne) |
|  | si $b$ (resp. $d)$ n'est pas précisé, l'extraction finit à la dernière |
|  | ligne (resp. colonne) incluse |

##  PARTIE 3

Nous allons modéliser ici, par une approche mécanistique, l'évolution dans le temps de la hauteur $x(t)$ de la surface d'un lac dont la géométrie est simplifiée comme sur la figure 3.

![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-08.jpg?height=483&width=1102&top_left_y=475&top_left_x=454)

Figure 3 - Modélisation d'un lac.

Si $S$, section horizontale du lac, est supposée constante par rapport à la hauteur, une simple équation de bilan sur les volumes nous permet d'écrire :

$$
S \dfrac{\mathrm{d} x}{\mathrm{~d} t}(t)=D_{e}(t)-D_{s}(t)
$$

où le débit entrant $D_{e}(t)$ est une fonction supposée connue. On peut modéliser le débit sortant $D_{s}(t)$ comme étant proportionnel à $x(t)$ soit $D_{s}(t)=\alpha x(t)$, où $\alpha$ est un réel strictement positif. On obtient donc :

$$
\dfrac{\mathrm{d} x}{\mathrm{~d} t}(t)=\dfrac{D_{e}(t)}{S}-\dfrac{\alpha}{S} x(t) .
$$

Par la suite le modèle d'évolution s'écrira :

$$
\dfrac{\mathrm{d} x}{\mathrm{~d} t}(t)=u(t)-k x(t) \qquad\qquad (1)
$$

où $k \in \mathbb{R}_{+}^{*}$ et $u(t)$ est une fonction réelle connue, appelée "entrée" du système et dans la suite, cette entrée prendra différentes formes. Dans la suite, on admettra l'existence de solution à l'équation (1).

Soit la fonction $h: \mathbb{R} \longrightarrow \mathbb{R}$ représentée dans la figure 4 et définie comme suit :

$$
\left\{\begin{array}{l}
h(t)=1, \quad \forall t \geq 0 \\
h(t)=0, \quad \forall t<0
\end{array}\right.\qquad\qquad (2)
$$

![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-08.jpg?height=308&width=803&top_left_y=2159&top_left_x=615)

Figure 4 - Entrée sous la forme d'une marche.

1. Calculer la solution de (1) pour $t \in[0,+\infty[$ avec $u(t)=h(t)$, où $h$ est définie dans (2) et pour la condition initiale $x(0)=0$.
2. 
    1. Montrer que  l'entrée $\delta(t)$ (Cf. figure 5)  peut s'écrire sous
       la  forme  $\delta(t)=h(t)-h(t-\Delta)$  où  $\Delta$  est  un  réel
       strictement positif fixé et $h$ est représentée dans la figure 4 . 
	   
	2.  On   admet  que  la   solution  de  (1)   pour  $t  \geq   0$  avec
	   $u(t)=\delta(t)$   et   pour   condition   initiale   $x(0)=0$   est
	   $x(t)=\dfrac{1}{k}\left(e^{-k(t-\Delta)}-e^{-k  t}\right)$  pour  $t
	   \geq \Delta$. Exprimer la solution pour $t \in$ $[0, \Delta]$. 

      ![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-09.jpg?height=317&width=794&top_left_y=687&top_left_x=628)

      Figure 5 - Entrée sous la forme d'un créneau.

      Dans la suite on notera $t_{i}=i \Delta$ où $i \in \mathbb{N}$.

3. Pour  $i \in  \mathbb{N}^{*}$, on considère  $u(t)$ la  fonction créneau
   définie   comme  suit   :   $u(t)=u_{i}$,  lorsque   $t$  appartient   à
   $\left[t_{i-1},   t_{i}[\right.$   et   $u(t)=0$   sinon   (Cf.   figure
   6$)$. Montrer que : 

    $$
     \forall t \geq t_{i}, \quad x(t)=u_{i} \dfrac{1-e^{-k \Delta}}{k} e^{-k\left(t-t_{i}\right)}
    $$

    si $x(0)=0$.

    ![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-09.jpg?height=317&width=805&top_left_y=1643&top_left_x=631)

    Figure 6 - Entrée sous forme d'un créneau entre $\left[t_{i-1}, t_{i}[\right.$.

4. Dans cette question on suppose que $u(t)$ est une fonction constante par
   morceaux définie comme suit :  $u(t)=u_{i}$, lorsque $t$ appartient à $t
   \in\left[t_{i-1},   t_{i}\left[\right.\right.$,   pour   tout   $i   \in
   \mathbb{N}^{*}$ (Cf. figure 7)). Montrer que : 
   
    $$
     x\left(t_{i}\right)=\sum_{j=1}^{i} u_{j} \dfrac{1-e^{-k \Delta}}{k} e^{-k\left(t_{i}-t_{j}\right)}
    $$

    si $x(0)=0$.

5. En  effectuant un changement d'indice  sur la sommation, montrer  que le
   résultat de la question précédente $x\left(t_{i}\right)$ peut s'écrire : 
   
    ![](https://cdn.mathpix.com/cropped/2024_01_02_d57535931fff12e083f2g-10.jpg?height=315&width=792&top_left_y=271&top_left_x=629)
   
    Figure 7 - Entrée sous forme d'une fonction en escalier.
   
    $$
     x\left(t_{i}\right)=\nu         \sum_{\ell=0}^{i-1}         u_{i-\ell}
     \Phi^{\ell}\qquad\qquad (3)
    $$

    où vous expliciterez $\nu$ et $\Phi$.

6. Que pouvez vous dire de la suite $\left(\Phi^{\ell}\right)_{\ell \in \mathbb{N}}$ ? Interpréter l'équation (3).

  
  
Dans la suite, il était proposé une modélisation probabiliste de l'évolution dans le temps de la hauteur d'un lac.

