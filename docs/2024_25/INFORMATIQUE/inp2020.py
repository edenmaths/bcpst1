#########################
#
#  I N P 2020
#
######################


# Q 1
#####

seq = 'ATCGTACGTACG'

assert seq[3] == 'G'
assert seq[2:6] == 'CGTA'

# Q 2
#####

from numpy.random import randint 

def generation(n: int) -> list[int]:
    gen = ""
    bases = "ACGT"
    for _ in range(n):
        gen = gen + bases[randint(0,4)]
    return gen

# Q 3
#####

def mystere(seq: str) -> list[float]:
    """
    Renvoie le pourcentage de présence des 4 bases dans un génôme
    seq rentré sous forme de chaîne de caractères

    >>>  g = generation(10)
    >>> g
    'AGTACTTTTG'
    >>> mystere(g)
    [20.0, 10.0, 20.0, 50.0]
    """
    compt = [0, 0, 0, 0]
    n = len(seq)
    i = n - 1
    bases = "ACGT"
    while i >= 0:
        for k in range(4):
            compt[k] = compt[k] + int(seq[i] == bases[k])
        i = i - 1
    return [cp*100/n for cp in compt]


## Q 4 : H P
############

# 4 comparaisons à chaque tour de boucle
# n tours : i est décrémenté de 1 à chaque tour 

# Q 5
#####

def recherche(M: str, S: str) -> int:
    n, m = len(S), len(M)
    for k in range(0, n - m + 1):
        if S[k: k + m] == M:
            return k
    return -1
    

# Q 6 : HP
##########

# dans le pire des cas (M non trouvée dans S), il
# faudra effecteur les (n - m + 1) itérations de
# chacune m comparaisons soit m.(n - m + 1) opérations
# Cela donne environ 150 milliards d'opérations donc
# cela durera environ 15 centièmes de secondes


# Q 7 : HP
###########

# On multiplie cela par les (3*10**9)/50 paquets
# ce qui donne environ 9 millions de secondes
# soit environ 104 jours



# Q 8
## OK

# Q 9
# 'AC' pour le 1er, 'ACA' pour le 2nd


def fonctionannexe(M: str) -> list[int]:
    F = [0]
    i = 1
    j = 0
    while i < m :
        if M[i] == M[j] :
            F.append(j+1)
            i = i+1
            j = j+1
        else:
            if j > 0 :
                j = F[j - 1]
            else:
                F.append(0)
                i = i + 1
    return F
# Q 18
def eval(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo naïf
    """
    res = 0
    n = len(P)
    for k in range(n):
        res = res + P[(n - 1) - k]*4**k
    return res
    
    
# Q 19
def horner_it(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo de Horner
    """
    ...
    
# Q 20
def horner_rec(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo de Horner écrirt récursivement
    """
    ...
    

# Q 20 bis : donner la fonction de hachage
def rang(base: int) -> int:
    """
    associe à chaque base son rang
    A -> 0, C -> 1, G -> 2, T -> 3
    """
    bases = "ACGT"
    for k in range(4):
        if base == bases[k]:
            return k

def hachage(genome: str) -> int:
    P = [rang(base) for base in genome] # le polynôme en base 4
    e = eval(P) # evaluer P(4) avec une des fonctions eval
    return e % 13 # renvoyer P(4) modulo 13
