def sequentielle(gens: list, suspect) -> bool:
    compt = 0
    for personne in gens:
        compt += 1
        if personne == suspect:
            print(f"{suspect} trouvé en {compt} comparaisons avec seq")
            return True
    print(f"{suspect} pas trouvé en {compt} comparaisons avec seq")
    return False

def recherche_dicho(gens: list, suspect) -> bool:
    i_deb = 0
    i_fin = len(gens) - 1
    compt = 0
    if i_fin == -1:
        print(f"{suspect} trouvé en {compt} comparaisons avec dicho")
        return False
    while i_fin - i_deb > 1:
        compt += 1
        i_mil = (i_deb + i_fin) // 2
        if gens[i_mil] == suspect:
            print(f"{suspect} trouvé en {compt} comparaisons avec dicho")
            return True
        elif gens[i_mil] > suspect:
            i_fin = i_mil
        else:
            i_deb = i_mil
    if gens[i_fin] == suspect:
        print(f"{suspect} trouvé en {compt + 1} comparaisons avec dicho")
        return True
    else:
        print(f"{suspect} pas trouvé en {compt + 1} comparaisons avec dicho")
        
# tests
assert recherche_dicho(range(100000), 9999)
assert not recherche_dicho(range(100000), 430000)
assert not recherche_dicho([], 42)

assert sequentielle(range(100000), 99999)
assert not sequentielle(range(100000), 430000)
assert not sequentielle([], 42)