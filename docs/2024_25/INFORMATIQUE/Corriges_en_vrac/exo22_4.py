from math import sqrt, pi

def exo(n):
    som = 0
    for k in range(1, n + 1):
        som += sqrt(n*n - k*k)
    return som/n**2