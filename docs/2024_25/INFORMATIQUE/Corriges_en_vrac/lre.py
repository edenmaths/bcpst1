import numpy as np
from scipy.linalg import null_space
import sympy as sy

A = sy.Matrix([[2,1,3,-1],[3,-1,2,0],[1,3,4,-2],[4,-3,1,1]])
K = A.nullspace()