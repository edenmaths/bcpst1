import matplotlib.pyplot as plt
import numpy as np
import math

x = np.linspace(-1,1,500)
y = (np.exp(x) + np.exp(-x))*0.5/x

plt.plot(x,y)

plt.show()