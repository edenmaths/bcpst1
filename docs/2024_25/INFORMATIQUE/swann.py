from urllib.request import urlretrieve
import re

# on récupère le texte et on l'enregistre sous le nom swann.txt
urlretrieve('https://www.gutenberg.org/files/2650/2650-0.txt', 'swann.txt')
# on ouvre le fichier en lecture seule
fic = open('swann.txt', 'r')
# et on le transforme en une longue chaîne de caractères
texte_brut = fic.read()
# on referme le fichier
fic.close()
# pour enlever tout ce qui n'est pas une lettre de l'alphabet:
texte = re.sub('[^A-ZÉÈÀÙÊÔÂÜÛÏÎa-zéèàùêôâüûïî]+', ' ', texte_brut)
# on tranmsforme cette longue chaîne en une liste des mots qu'on suppose séparés par des espaces
liste_Mots = texte.split() 
# pour tout mettre en minuscules :
liste_mots = {mot.lower() for mot in liste_Mots}
liste_ordo = sorted(liste_mots)

def recherche(elt, liste):
    a, b, cpt  = 0, len(liste), 0
    while b - a > 1 :
        cpt += 1
        c = (a + b) // 2
        if elt == liste[c]:
            print(f"Trouvé en {cpt} étapes")
            return True
        elif elt > liste[c]:
            a = c
        else:
            b = c
    print(f"Ce mot est absent - décidé après {cpt} étapes")
    return False
            
