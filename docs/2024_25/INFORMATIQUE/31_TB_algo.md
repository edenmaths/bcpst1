{{ chapitre(5, "Info aux concours - les 10 dernières minutes", "TP ", 0)}}


# Écrit Agro-Véto TB

Voici deux exercices du concours TB :

- le [premier](./SujetsTB/EnonceTB2016Info.pdf)


- le [second](./SujetsTB/EnonceTB2017Info.pdf)

À vous de jouer.


# Entraînement en autonomie ou presque 

Vous   trouverez   [ICI](https://e-nsi.gitlab.io/pratique/)   de   nombreux
exercices de niveaux très divers avec une console vous permettant de tester
si votre réponse est correcte.


# Oral G2E

Les [Exercices](./B2/ExosOralInformatique_2023.pdf) et les [sources TeX](./B2/ExosOralInformatique_2023.tex)


<!--

### Exercice 1


Un codon est  constitué de $3$ nucléotides,  et a pour base  azotée soit A,
soit T, soit G, soit C. 

1. Ecrire une fonction de vérification qui  prend en entrée une chaine de 3
	nucléotides, et qui renvoie `True` si c'est bien un codon et `False` sinon. 
   On rappelle quels sont les 3 codons 'stop' : `TAG`, `TAA`, `TGA`. 

2. Écrire  une fonction qui cherche  le codon stop dans  une séquence d'ADN
   (si il y en a plusieurs alors on s'arrête au premier) et renvoie 
   la position de ce codon dans la séquence.  Si il n'y a pas de codon stop
   alors la fonction renvoie $-1$. 
   On prend  en compte le  fait que  la lecture de  la séquence se  fait de
   codons en codons (c'est à dire tous les 3 nucléotides). 

3. Les nucléotides complémentaires sont : (A-T) et (C-G).

      1.  Écrire une  fonction qui  prend en  entrée un  nucléotide et  qui
         renvoie son complémentaire 

      2.  Écrire  une fonction qui prend  deux brins de même  taille et qui
          vérifie s'ils sont complémentaires.   Si oui, elle renvoie `True`,
          sinon elle renvoie `False`. 

4. On rappelle le phénomène de  croisement entre deux brins au niveau d'une
   position $k$ sur le brin. 
   Par exemple  si les brins  : `'ATTGTC'` et  `'CGCTGA'` se croisent  à la
   position 3, les deux nouveaux brins sont: `'ATTTGA'` et `'CGCGTC'`.

	Écrire une fonction  qui prend en paramètre deux brins  et une position
	$k$ et qui fait le croisement.  
	La fonction renvoie les deux nouveaux brins.


### Exercice 2

1. Créer une fonction `anagra` qui prend  un mot `M` en entrée et qui teste
   si `M` est un anagramme (par exemple `anagra('RIGHHGIR')` renvoie 
   `True`).  
   
2. Créer une  fonction `verlan` qui prend  un mot en entrée  et qui inverse
   les  deux  moitiés  du  mot (par  exemple  `verlan('DFRTGGZE')`  renvoie
   `'GGZEDFRT'`).
   
3. Créer  une fonction qui  prend en entrée une  phrase et qui  retourne le
   nombre de mots. 
   
   
### Exercice 3


1. On modélise ici une marche aléatoire dans $ℤ$. À l'instant $0$, l'objet
   est en  $0$, puis  se déplace de  $\pm$ $1$ à  chaque instant,  avec une
   probabilité $1/4$ de se déplacer vers la droite. 
   
    1. Créer une fonction qui modélise  cette marche et renvoie la position
       après $n$ déplacements. 
	2. Idem,  mais la fonction renvoie  l'instant du premier retour  en $0$
	   s'il existe.

2.  On modélise  ici une  marche aléatoire  dans une  grille $G$  de taille
   $(r,r)$,  dont le  départ se  fait en  $(0,0)$. Créer  une fonction  qui
   modélise les $n$  premiers déplacements, qui se  font aléatoirement dans
   l'une des quatre directions standard, tout en restant dans la grille.  
   
   Reprendre le  programme ci-dessus et  renvoyer, pour chaque  colonne, le
   nombre de passages par cette colonne.  



### Exercice 4



On a une liste contenant des nombres (qui peuvent se répéter).

1. Écrire une fonction `comptage(liste, x)` qui compte les occurrences d'un
   nombre.  Par exemple,  `comptage([8, 55,  7, 3,  7, 55,  9, 9,  9], 55)`
   renvoie `2` ; 
   `comptage([8, 55, 7, 3, 7, 55, 9, 9, 9], 4)` renvoie `0`.
   
1.  Écrire une fonction `supprime(liste, x)` qui supprime toutes les
   occurrences du nombre `x` à l'exception de la première.  
   Par exemple, `supprime([8, 55, 7, 3, 7, 55, 9, 9, 9], 55)` renvoie `[8, 55, 7,
   3, 7, 9, 9, 9]` ; 
   `supprime([8, 55, 7, 3, 7, 55, 9, 9, 9], 4)` renvoie `[8, 55, 7, 3, 7, 55, 9, 9, 9]`.

3. Écrire  une fonction qui, à  partir d'une liste donnée,  redonne la même
   liste mais sans répétition.  
   
   
   
-->
