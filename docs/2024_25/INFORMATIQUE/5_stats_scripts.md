# Statistiques bivariées


```python
import numpy as np
import matplotlib.pyplot as plt


def moyenne(xs: list) -> float:
    s = 0
    for x in xs:
        s += x
    return s/len(xs)

def variance(xs: list) -> float:
    xbar = moyenne(xs)
    return moyenne([(x - xbar)**2 for x in xs])

def covariance(xs: list, ys: list) -> float:
    """Avec la def"""
    nx, ny = len(xs), len(ys)
    assert nx == ny, "séries de longueurs différentes"
    xbar, ybar = moyenne(xs), moyenne(ys)
    return moyenne([(xs[k] - xbar)*(ys[k] - ybar) for k in range(nx)])

def covariance2(xs: list, ys: list) -> float:
    """Avec KH"""
    nx, ny = len(xs), len(ys)
    assert nx == ny, "séries de longueurs différentes"
    xbar, ybar = moyenne(xs), moyenne(ys)
    xybar = moyenne([xs[k]*ys[k] for k in range(nx)])
    return xybar - xbar*ybar

def coeff_corr(xs: list, ys: list) -> float:
    return covariance(xs, ys) / np.sqrt(variance(xs) * variance(ys))

def reg_lin(xs: np.ndarray, ys: np.ndarray):
    xbar, ybar = moyenne(xs), moyenne(ys)
    a = covariance(xs, ys) / variance(xs)
    b = ybar - a*xbar # la droite passe par le point moyen
    D = [a*xs[k] + b for k in range(len(xs))] # ou a*xs + b puisque xs est un np.array
    print(f'a = {a}, b = {b}, r = {coeff_corr(xs, ys)}')
    plt.scatter(xs, ys, marker='P', color='blue')
    plt.plot(xs, D, color='red')
    plt.show()
    return lambda x: a*x + b


### Radioactivity

X = np.array([2,4,5,8,9,11,12,13])
Y = np.array([8.9,10.2,10.5,12.2,12.3,13.2,13.8,14.9])

f = reg_lin(X, Y)

# en 2000
print(f'en 2000 : {f(10)}')

# en 2023
print(f'en 2023 : {f(33)}')
```



## Tableau des effectifs/ des fréquences

### Matrice bordante

```python
from copy import deepcopy

def bordante_lignes(T: list) -> list:
    B = deepcopy(T)
    nl, nc = len(T), len(T[0])
    for ligne in B:
        total = 0
        for nb in ligne:
            total += nb
        ligne.append(total)
    return B
    
def bordante_cols(T: list) -> list:
    B = deepcopy(T)
    nl, nc = len(T), len(T[0])
    B.append([0 for _ in range(nc)])
    for j in range(nc):
        for i in range(nl):
            B[nl][j] += B[i][j]
    return B

def bordante(T: list):
    return bordante_cols(bordante_lignes(T))


## Avec les outils de numpy

# np.sum(T, axis=0) renvoie la liste des sommes par colonne
# np.sum(T, axis=1) renvoie la liste des sommes par ligne
# np.column_stack((T, L)) ajoute à une matrice T une liste T en tant que colonne
# np.vstack((T, L)) ajoute à une matrice T une liste T en tant que ligne

def np_bordante(T: np.ndarray) -> np.ndarray:
    L = np.column_stack((T, np.sum(T, axis=1))) 
    return np.vstack((L, np.sum(L, axis=0)))

## Avec un dataframe de pandas

import pandas

def df_bordante(D: pandas.DataFrame) -> pandas.DataFrame:
    Dd = D.copy()
    Dd.loc['Total', :] = Dd.sum(axis=0)
    Dd.loc[:, 'Total'] = Dd.sum(axis=1)
    return Dd
```




# Fonctions du Poly

## barres / beignets

```python
def dico_eff(tab: list, total = False) -> dict:
    d = {}
    for el in tab:
        if el in d:
            d[el] += 1
        else:
            d[el] = 1
    if total:
        d['total'] = len(tab)
    return d

def dico_freq(tab: list) -> dict:
    dic_freq = {}
    dic_eff = dico_eff(tab, True)
    total = dic_eff['total']
    for cle in dic_eff:
        dic_freq[cle] = 100*dic_eff[cle] / total
    return dic_freq


def barre(d_eff: dict, name=None) -> None:
    plt.clf()
    plt.bar([k for k in d_eff], [d_eff[k] for k in d_eff], width=0.5)
    if name is not None:
        plt.savefig(f"./FIGURES/{name}_barre.pdf",bbox_inches='tight',pad_inches=0)
    else:
        plt.show()


def donut(d_eff: list, name=None) -> None:
    plt.clf()
    plt.pie([d_eff[k] for k in d_eff], labels=[k for k in d_eff], wedgeprops={'width':0.5})
    if name is not None:
        plt.savefig(f"./FIGURES/{name}_donut.pdf",bbox_inches='tight',pad_inches=0)
    else:
        plt.show()


sondage = {'Extrêmement': 50.9, 'Tout à fait': 34.3, 'Totalement': 10.9, 'Très': 3.9}
```

## Histogrammes

```python
ventes = [163,169,183,224,140,163,172,104,121,141,139,135,126,163,127,141,136,95,128,95,133,92,119,103,150,149,110,148,153,156,151,109,143,123,75,189,126,173,171,128,153,142,148,112,112,150,121,162,120] 


def histo(vals: list, nb_barres: int) -> None:
    m = min(vals)
    M = max(vals)
    pas = (M - m) / nb_barres
    plt.clf()
    plt.hist(vals, bins=[m + pas*k for k in range(nb_barres + 1)], edgecolor='black', linewidth=0.2) 
    plt.show()


def histo2(vals: list, lims: list) -> None:
    plt.clf()
    plt.hist(vals, bins=lims, edgecolor='black', linewidth=0.2, density=True) 
    plt.show()

femmes = [17.5]*202 + [22.5]*204 + [27.5]*359 + [37.5]*338 + [47.5]*304 + [70]*10
```

## Réchauffement climatique

```python
import pandas
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
from datetime import date

def clim(lieu="Monde", an=1970):
    # récupération de l'année courante
    auj = date.today().year
    # on prend le fichier de l'année courante s'il est disponible
    # (à partir de  mars) 
    an_deb = auj if date.today().month >= 3 else auj - 1
    # On laisse le choix du lieu
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
    # récupération du fichier choisi
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    urlretrieve(url, filename="./temp_anomalies.csv")
    df = pandas.read_csv("temp_anomalies.csv", header=4)

    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ
    def formate(d: int) -> date:
        s = str(d)
        return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01') 
    
    df['Year'] = df['Year'].map(formate)


    # Tracé
    plt.clf()
    # on récupère le dernier mois fourni
    dernier = df.iloc[-1,0] 
    titre = f"Évolution des anomalies de températures {tit} depuis {an}\nDernière mesure le 1er {dernier.strftime('%B %Y')} "
            
    ax = df.plot.scatter(x='Year', y='Anomaly', c='Anomaly', colormap="viridis")
    plt.title(titre)
    ax.grid(True)
    return ax
```




## banquise

```python
import pandas
from urllib.request import urlretrieve
import datetime
from matplotlib.dates import drange
import numpy as np


debut = datetime.datetime(1987,7,1)
fin = datetime.datetime(2022,7,1)
delta = datetime.timedelta(weeks=5*52)
dates = drange(debut, fin, delta)

invites = [6491, 5858, 5042, 5418, 4744, 4904, 3711, 4122]
#plt.clf()
#plt.plot_date(dates, invites, linestyle='dotted')
#plt.show()



url = "http://psc.apl.uw.edu/wordpress/wp-content/uploads/schweiger/ice_volume/PIOMAS.vol.daily.1979.2022.Current.v2.1.dat.gz"

site = "http://psc.apl.washington.edu/research/projects/arctic-sea-ice-volume-anomaly/"

urlretrieve(url, filename="./banquise.gz")

df = pandas.read_table("banquise.gz", compression='gzip', sep="\s+")

an = datetime.timedelta(days=365)
jours = datetime.timedelta(days=len(df))
debut_derniere_annee = datetime.datetime(1979 + (jours// an), 1, 1)

depart = datetime.datetime(1979, 1, 1)
arrivee = debut_derniere_annee + datetime.timedelta(days=(jours % an).days - 1)
delta = datetime.timedelta(hours = 24)
dates = drange(depart, arrivee, delta)
# enlever les 31 décembre des années bissextiles
nelles_dates = np.delete(dates, [364 + 365 + k*4*365 for k in range((jours//an)//4)], axis=0)

volumes = [v for v in df['Vol']] 
```

### moyennes mobiles sur une année avec les min/max

```python
vols_an = [np.mean([volumes[365*k:365*(k + 1)]]) for k in range(1 + jours//an)]
maxi = [np.max([volumes[365*k:365*(k + 1)]]) for k in range(1 + jours//an)]
mini = [np.min([volumes[365*k:365*(k + 1)]]) for k in range(1 + jours//an)]
std = [np.std([volumes[365*k:365*(k + 1)]]) for k in range(1 + jours//an)]
extrema = [mini, maxi]
annees = [1979 + k for k in range(1 + jours//an)]


def moyenne_mobile(absx: list, data: list, k: int) -> list:
    k2 = k // 2
    n = len(data)
    if k % 2 == 1:
        new_data = [np.mean(data[i - k2:i + k2 + 1]) for i in range(k2, n - k2)]
    else:
        extr = lambda i: [0.5*(data[i - k2] + data[i + k2])]
        new_data = [np.mean(data[i-k2+1:i+k2] + extr(i) ) for i in range(k2, n - k2)]
    return absx[k2:n - k2], new_data


plt.clf()
#plt.plot_date(nelles_dates, volumes, fmt=',')
plt.errorbar(annees, vols_an, yerr = std, fmt='-o', ecolor='red', capsize=4, linestyle='dotted')
plt.ylim(0, 1.1*max(volumes))
#X, Y = moyenne_mobile(annees, vols_an, 5)
#plt.plot(X, Y, color='red', marker='P')
#plt.plot(annees, vols_an, linestyle='dotted', marker='+')
plt.savefig(f"./FIGURES/banquise5.pdf",bbox_inches='tight',pad_inches=0)
plt.show()
```


# Médiane

```python
frontières = [k*5 for k in range(8)]
élèves = [300,420,500,330,250,160,40]

def class2val(cs: list, effs: list)-> list:
    res = []
    for k in range(len(cs) - 1):
        res += [0.5*(cs[k] + cs[k+1])]*effs[k]
    return res

def val2vals(vs: list, effs: list)-> list:
    res = []
    for k in range(len(vs)):
        res += [vs[k]]*effs[k]
    return res


def mediane(xs: list) -> float:
    ordre = sorted(xs)
    n = len(xs)
    if n % 2 == 1:
        return ordre[n//2]
    else:
        return 0.5*(ordre[n//2 - 1] + ordre[n // 2])

def quartiles(xs: list) -> list:
    ordre = sorted(xs)
    n = len(xs)
    M = mediane(xs)
    if n % 2 == 0:
        Q1 = mediane(xs[:n//2])
        Q3 = mediane(xs[n//2:])
    elif n % 4 == 1: 
        Q1 = mediane(xs[:n//2 + 1])
        Q3 = mediane(xs[n//2:])
    else:
        Q1 = mediane(xs[:n//2])
        Q3 = mediane(xs[n//2 + 1:])
    return [Q1, M, Q3]


notes = np.array(range(21))
A = [0,0,0,1,1,0,2,3,2,9,5,9,5,1,2,1,0,2,1,1,0]
B = [0,5,5,5,5,0,0,0,0,0,3,0,5,5,0,2,0,5,0,5,0]

plt.clf()
plt.boxplot([val2vals(notes, A), val2vals(notes, B)], labels=["classe A", "classe B"], showmeans=True, notch=True, patch_artist=True, boxprops=dict(facecolor='cyan', color='blue'),whiskerprops=dict(linestyle='dotted'))
print(quartiles(val2vals(notes, A)))
print(quartiles(val2vals(notes, B)))
plt.savefig(f"./FIGURES/moustache2.pdf",bbox_inches='tight',pad_inches=0)
plt.show()
```
