{{ chapitre(13, "Méthode d'Euler", "TP ", 0)}}



## Euler et les rectangles


Vous connaissez la méthode des rectangles ?  Alors vous connaissez la
méthode d'Euler explicite...

En effet, si  $f$ est une fonction  de classe $\scr C^0$  sur un intervalle
$[a,b]$, alors on considère que 

$$ \displaystyle \int_a^bf(x){\rm d}x\approx f(a)×(b-a) $$

Considérez  maintenant  une  fonction  $y$  de classe  $\scr  C^1$  sur  un
intervalle $[x_n,x_{n+1}]$ alors

$$  \displaystyle \int_{x_n}^{x_{n+1}}y'(x){\rm d}x\approx y'(x_n)×(x_{n+1}-x_n) $$


Alors,     si     on     considère     une     équation     différenetielle
$y'(x)=f\bigl(y(x),x\bigr)$ avec $y$ de classe $\scr C^1$ sur un intervalle
$[a,b]$ dont $(x_i)_{0≤i≤n}$ est une  subdivision régulière de largeur $h$,
alors

$$ \displaystyle \int_{x_n}^{x_{n+1}}y'(x){\rm d}x=y(x_{n+1})-y(x_n)\approx
h×f\bigl(y(x_n),x_n\bigr)$$

d'où 

$$
y(x_{n+1})\approx y(x_n)+h×f\bigl(y(x_n),x_n\bigr)
$$

ce qui nous  donne un algorithme de construction des  points de coordonnées
$\bigl(x_n,y(x_n)\bigr)$


1. Déterminer une fonction `ret(f, a,  b, n)` qui renvoie une approximation
   de $ \displaystyle \int_a^bf(x){\rm d}x$  par la méthode des rectangles,
   `n` étant le nombre de subdivisions régulières.

2. En déduire une fonction `euler_expl(f, a, b, y0, n)` qui renvoie le couple
   formé  des listes  des $(x_i)$  et des  $(y(x_i))$ définis  précédemment
   sachant  que l'on  souhaite avoir  une approximation  de la  solution de
   l'EDO $y'(x)=f\bigl(y(x),x\bigr)$ sur $[a,b]$ sachant que $y(a)=y_0$.
   
3. En déduire  une fonction `trace_euler_expl(f, a, b, y0,  n)` qui affiche
   le tracer associé à cette approximation.
   
4. Utiliser cette fonction avec `trace_euler_expl(lambda y, x : y, 0, 2, 1,
   2**5)`. Tracer  sur le même  graphique la représentation de  la solution
   exacte.
   

## Un exemple pathologique

On veut résoudre sur $[1,10]$ l'équation différentielle
$y'(x)=3\dfrac{y(x)}{x}-\dfrac{5}{x^3}$ sachant que $y(1)=1$

1. En  utilisant la méthode  de variation  de la constante,  résolvez cette
   EDO.
2. Tracez sur un même graphique  la représentation graphique de la solution
   exacte et celle de votre approximation. Commentaires ?
   
   
## CENTRALΕ 2015 (Extrait de l'épreuve d'informatique commune)

Le sujet complet est [ICI](Centrale2015.pdf).

Nous n'allons aborder que le début de  ce sujet qui permet de travailler la
méthode d'Euler.

### Partie A

Début de l'énoncé :

![A](centrale15_1.pdf)

#### Question II.A.1

 Sachant que $y''(t)=f(y(t))$, on introduit $z:I\to ℝ,\ z:t↦y'(t)$.  Démontrer que  l'équation  peut  se mettre  sous  la  forme d'un  système
    différentiel du premier ordre en $z(t)$ et $y(t)$ noté $(S)$.
	


#### Question II.A.2

$n > 1, \ J_n=[\![ 0, n-1 ]\!], \ h=\dfrac{t_{\max}-t_{\min}}{n-1}, \ t_i=t_{\min}+ih$

Démontrer que pour tout entier $i\in[\![ 0, n-2 ]\!]$


$$y(t_{i+1})=y(t_i)+\int_{t_i}^{t_{i+1}}z(t){\rm d t}\qquad
z(t_{i+1})=z(t_i)+\int_{t_i}^{t_{i+1}}f \bigl( y(t) \bigr){\rm d} t$$

**Aide**  : calculez  $\displaystyle\int_{t_i}^{t_{i+1}}z(t){\rm  d} t$  et
$\displaystyle\int_{t_i}^{t_{i+1}}f \bigl( y(t) \bigr){\rm d} t$

#### Question II.B.1

 Dans le schéma d’Euler explicite, chaque terme sous le signe intégrale est remplacé par sa valeur prise en la
borne inférieure.



Dans ce schéma, montrer que les équations précédentes permettent de définir deux
suites $(y_i)_{i\in J_n}$ et $(z_i)_{i\in J_n}$
où $y_i$ et $z_i$ sont des valeurs approchées de $y(t_i)$ et $z(t_i)$. 

Donner les relations de récurrence permettant de
déterminer les valeurs de $y_i$ et $z_i$ connaissant $y_0$ et $z_0$.


#### Question II.B.2


  Écrire une fonction `euler` qui reçoit en argument les paramètres qui vous semblent pertinents et
qui  renvoie deux  listes de  nombres  correspondant aux  valeurs associées  aux
suites $(y_i)_{i\in J_n}$ et $(z_i)_{i\in J_n}$. 

Vous
justifierez le choix des paramètres transmis à la fonction.


#### Question II.B.3

Pour illustrer cette méthode, on considère l’équation différentielle 

$$
y''(t)=-ω^2y(t)
$$ 

dans laquelle $ω$ est un nombre réel.


##### Question II.B.3.a

Démontrer qu’on peut définir une quantité  E, indépendante du temps, vérifiant une
équation de la forme:

$$
\frac{1}{2}y'(t)^2+g \bigl( y(t) \bigr) = E
$$


**Aide** :
$y''(t)=-ω^2y(t)$ alors $\frac{1}{2}2y'(t)y''(t)=-\frac{1}{2}ω^22y'(t)y(t)$...



##### Question II.B.3.b


On  note $E_i$  la valeur  approchée  de $E$  à  l’instant $t_i$  , $i\in  J_n$,
calculée en utilisant les valeurs approchées  de $y(t_i)$ et $z(t_i)$ obtenues à
la question II.B.1. Démontrer que $E_{i+1}-E_i=h^2ω^2E_i$

**Aide** On fait les calculs et ça marche avec $z_{i+1}=z_i-ω^2y_ih$



##### Question II.B.3.c


Qu’aurait donné un schéma numérique qui satisfait à la conservation de 𝐸 ?

**Aide** pas de piège...



##### Question II.B.3.d

 En portant les valeurs  de $y_i$ et $z_i$ sur l’axe des  abscisses et l’axe des
 ordonnées respectivement, quelle serait l’allure du graphe qui respecte la
 conservation de $E$ ?
 
 
**Aide**  : une  équation du  type $a^2y^2  + b^2z^2=c^2$  est celle  d'une
*ellipse* or $\frac{1}{2}y'(t)^2+\frac{ω^2}{2}y(t)^2=E$ donc...



##### Question II.B.3.e


La mise  en oeuvre de  la méthode d’Euler  explicite génère  le résultat
graphique donné figure 1 à gauche. Dans 
un  système d’unités  adapté,  les calculs  ont été  menés  en prenant  $y_0=3$,
$z_0=0$, $t_{\min} = 0$, $t_{\max} = 3$, $ω=2π$ et $n=100$

![fig1](figure1.pdf)

En quoi ce graphe confirme-t-il que le schéma numérique ne conserve pas 𝐸 ?
Pouvez-vous justifier son allure ? 

##### Question subsidiaire

Créer une fonction  `phase_euler(f, y0, z0, h, n)` qui  reproduit la figure
ci-dessus  donc   comme  celle   ci-dessous  avec   `phase_euler(lambda  x:
-(2*pi)**2*x, 3, 0, 3/100,100)`

Qu'observez-vous   avec   `phase_euler(lambda   x:  -(2*pi)**2*x,   3,   0,
3/2**13,2**13)` ?

![graph](Graph.pdf)


#### Schéma de Verlet

 Le  physicien  français Loup  Verlet  a  proposé  en 1967  un  schéma
  numérique d’intégration d’une équation de la 
forme (II.1)  dans lequel, en  notant $f_i=f(y_i)$ et  $f_{i+1}=f(y_{i+1})$, les
relations de récurrence s’écrivent

$$y_{i+1}=y_i+hz_i+\frac{h^2}{2}f_i\qquad z_{i+1}=z_i+\frac{h}{2}(f_i+f_{i+1})$$

 Écrire une fonction `verlet` qui reçoit en argument les paramètres qui vous semblent pertinents et qui
renvoie deux  listes de nombres  correspondant aux valeurs associées  aux suites
$(y_i)_{i\in J_n}$ et $(z_i)_{i\in J_n}$.  




## AGRO-VÉTO 2022 
Le  sujet  se  compose  de  5  parties  largement  indépendantes  et  de  2
annexes. La partie 5 est consacrée à l'informatique. Les candidats pourront
admettre  le  résultat   d'une  question  pour  répondre   à  une  question
postérieure à condition de le mentionner explicitement.

Dans le cadre de l'étude d'une  espèce de lièvres, on souhaite modéliser la
dynamique d'évolution  temporelle de cette population.  L'objectif du sujet
est de comparer différentes modélisations ainsi que leur simulation. 


### Partie 1 - Modélisation de la dynamique d'une population isolée

On  note $t$  le temps  et  $x(t)$ l'effectif  des lièvres  en fonction  du
temps.

1.  On suppose,  pour  commencer, que  la population  est  isolée dans  un
    environnement aux  ressources abondantes.   On propose de  modéliser la
    dynamique  de  population  de  lièvres  par  l'équation  différentielle
    suivante : 

    $$
    \frac{\mathrm{d} x}{\mathrm{~d} t}=r x \qquad (1)
    $$

    où $r$ est une constante strictement positive représentant le taux de
    reproduction intrinsèque des lièvres.
	
	1. Résoudre  l'équation  différentielle  (1)  de  condition  initiale
	     $x(0)=x_{0}>0$.
	2. Dessiner à la main l'allure de la solution de (1).
	3.  Que peut-on dire de l'évolution de la population de lièvres avec ce
	    modèle?  L'équation différentielle  (1)  est-elle une  modélisation
	    raisonnable? 
		
1.  On suppose à présent que les ressources du milieu sont limitées et on
    modélise la dynamique de population de lièvres par l'équation
    différentielle suivante : 

    $$
    \frac{\mathrm{d} x}{\mathrm{~d} t}=r x\left(1-\frac{x}{K}\right) \qquad (2)
    $$

    où $K$ est une constante strictement positive. 
    
	1. Intuitivement, quelle allure a une solution $x(\cdot)$ de (2) tant
      que l'effectif de lièvres $x(t)$ reste petit? 
    1. En considérant (2), établir le tableau de signes de $\frac{\mathrm{d}
      x}{\mathrm{~d} t}$ en fonction de $x$ pour $x \geq 0$. 
    1.  Soit $x(\cdot)$ une solution de (2) avec $x(0)=x_{0}>0$.  Est-ce
       possible qu'il existe un $t>0$ tel que $x(t)=0$ ?  
	   *La réponse  sera justifiée en  s'appuyant sur le tableau  de signes
	   obtenu à la question précédente.* 
    1.  Soit $x(\cdot)$ une solution de (2) avec $x(0)=x_{0}>0$. On admettra
       que pour tout $t \geq 0, x(t)>0$.  
       On   pose  $z(t)=\frac{1}{x(t)}$.   Démontrer  que   $\frac{\mathrm{d}
       z}{\mathrm{~d} t}=\frac{r}{K}-r z$.  
    1.  Résoudre l'équation différentielle $\frac{\mathrm{d} z}{\mathrm{~d}
       t}=\frac{r}{K}-r z $.   On exprimera la solution en  fonction de $t,
       r, K$ et $z_{0}=z(0)$. 
    1.  En déduire une expression de $x(t)$ en fonction de $t, r, K$ et
       $x_{0}$.
    1.  Quelle est la limite de $x(t)$ quand $t$ tend vers l'infini ?
    1.  Dessiner à la main l'allure des solutions obtenues pour une
       condition initiale petite $x_{0}>0$ d'une part et pour une condition
       initiale $x_{0}>K$ d'autre part. 
    1.  En s'appuyant sur les réponses aux questions précédentes, décrire
        les  différences  entre  les  modèles  (1) et  (2)  et  donner  une
        interprétation biologique de la constante $K$. 
		
		
### Partie 2 - Modèle proie-prédateur

Dans  cette partie,  on souhaite  modéliser l'évolution  conjointe de  deux
populations : la  population de lièvres, qui constituent des  proies et une
population  de lynx,  qui  sont  des prédateurs  des  lièvres. On  modélise
l'évolution des effectifs $x(t)$ des proies et $y(t)$ des prédateurs par le
système différentiel suivant : 

$$
\left\{\begin{array}{l}
\displaystyle{\frac{\mathrm{d} x}{\mathrm{~d} t}=r x-p x y} \\
\ \\
\displaystyle{\frac{\mathrm{d} y}{\mathrm{~d} t}=-m y+q x y}
\end{array}\right. \qquad (3)
$$

où  $r,   p,  m$  et   $q$  sont  des  constantes   positives  représentant
respectivement le taux  de reproduction intrinsèque des proies,  le taux de
mortalité des  proies due aux  prédateurs rencontrés, le taux  de mortalité
intrinsèque des  prédateurs et  le taux de  reproduction des  prédateurs en
fonction des proies rencontrées et mangées. 


1. Que devient le système (3) s'il n'y a pas de lynx, c'est-à-dire $y(t)=0$
   pour tout $t \geq 0$ ? Comment évolue alors la population de lièvres ? 
   
1. Que devient  le système (3) s'il  n'y a pas de  lièvres, c'est-à-dire si
   $x(t)=0$ pour  tout $t \geq 0$  ? Comment évolue alors  la population de
   lynx?
   
1.  On pose $s=r t, \bar{x}(s)=\frac{q}{r} x(t)$ et $\bar{y}(s)=\frac{p}{r}
    y(t)$. Démontrer que le système d'équations différentielles (3) peut se
    réécrire 

    $$
    \left\{\begin{array}{l}
    \displaystyle{\frac{\mathrm{d} \bar{x}}{\mathrm{ds}}=\bar{x}-\bar{x} \bar{y}} \\
    \ \\
    \displaystyle{\frac{\mathrm{d} \bar{y}}{\mathrm{~d} s}=-a \bar{y}+\bar{x} \bar{y}}
    \end{array},\right. \qquad (4)
    $$

    où $a$ est une constante à déterminer. 
    **Dans toute la suite, pour simplifier les notations, on omettra les
    barres et on notera donc $x$ pour $\bar{x}$ et $y$ pour $\bar{y}$. Ainsí
    le système (4) sera noté :** 

    $$
    \left\{\begin{array}{l}
    \displaystyle{\frac{\mathrm{d} x}{\mathrm{~d} s}=x-x y } \\
    \ \\
    \displaystyle{\frac{\mathrm{d} y}{\mathrm{~d} s}=-a y+x y}
    \end{array}\right. \qquad (5)
    $$
   
    On appelle *point d'équilibre* d'un système d'équations de la forme
   
    $$
    \left\{\begin{array}{l}
    \displaystyle{\frac{\mathrm{d} x}{\mathrm{ds}}=g_{1}(x, y) } \\
    \ \\
    \displaystyle{\frac{\mathrm{d} y}{\mathrm{~d} s}=g_{2}(x, y)}
    \end{array}\right. \qquad (6)
    $$
    
    un couple $(x, y) \in \mathbb{R}^{2}$ tel que les membres de droite des
    équations différentielles s'annulent simultanément pour ces
    valeurs. Ainsi, un point d'équilibre de (5) est une solution de 

    $$
	\left\{\begin{array}{rl}
	x-x y & =0 \\
	\ \\
	-a y+x y & =0
	\end{array} .\right. \qquad (7)
    $$
	
1.  
    1. Déterminer les points d'équilibre du système (5).
	1. En terme  de dynamique des populations, que  représentent ces points
	   d'équilibre ?
	1. Donner une interprétation biologique des points d'équilibre obtenus.
	
	
1.  Pour $c>0$ fixé, on définit $f_{c}(u)=u-c \ln u$ pour $u>0$.

    1. Démontrer que pour tout $u>0$, on a : $f_{c}(u) \geq c(1-\ln c)$.
    1.  Démontrer que $f_{c}(u)=c(1-\ln c)$ si et seulement si $u=c$.
    1. Soit $M \geq c(1-\ln c)$. Démontrer qu'il existe $b$ et $B$ avec $0<b
      \leq B$, tels que $f_{c}(u) \leq M$  si et seulement si $b \leq u \leq B$. 

1. On définit maintenant, pour $x>0$ et $y>0$, $ V(x, y)=x-a \ln x+y-\ln y \qquad (6)$
   Dans  toute  cette question,  on  considère  $(x(\cdot), y(\cdot))$  une
   solution de (5) avec des conditions initiales $x(0)=x_{0}>0$ et $y(0)=y_{0}>0$. 
   
    1. 
	    1. Démontrer que, pour tout $s \geq 0$, on a: $f_{a}(x(s)) \leq V(x(s),
           y(s))-1$ et $f_{1}(y(s)) \leq V(x(s), y(s))-a(1-\ln a) .$ 
        1.  Démontrer que $V(x(s), y(s))$ reste constante pour tout $s \geq 0$.
        1.  En déduire qu'il existe $b_{x}, B_{x}, b_{y}$ et $B_{y}$ avec
            $0<b_{x} \leq B_{x}$ et $0<b_{y} \leq B_{y}$ tels que, pour tout $s
            \geq 0$, on a : $b_{x} \leq x(s) \leq B_{x}$ et $b_{y} \leq y(s) \leq
            B_{y}$. 
    1. Démontrer que $V(x, y) \geq 1+a(1-\ln a)$, pour tous $x$ et $y$ dans
      $\mathbb{R}_{+}^{*}$ et  que, de plus, l'équation  $V(x, y)=1+a(1-\ln
      a)$ admet  une unique  solution $\left(x^{*}, y^{*}\right)$  que l'on
      explicitera.  
    1. En s'appuyant sur les questions précédentes, répondre en justifiant
      aux questions suivantes : 
	  
	    1.  Les populations de proies et de prédateurs peuvent-elles
	      s'éteindre ? Et peuvent-elles devenir arbitrairement grandes ? 
        1.  Les populations de proies et de prédateurs peuvent-elles
          s'arrêter d'évoluer ?
	    1.  Quels comportements peut-on envisager pour les populations de
	      proies et de prédateurs ? 
  

(...)

### Partie 4 - Résolution numérique du système différentiel

Dans cette partie, on met en  place une résolution numérique du système (4)
appelée la méthode d'Euler.

On se fixe un petit intervalle de temps $h>0$. A partir d'une condition initiale
 $\left(x_{0},      y_{0}\right)     \in      \mathbb{R}_{+}^{*}     \times
 \mathbb{R}_{+}^{*}$, on construit une suite de points de $\mathbb{R}^{2}$,
 notée $\left(x_{k}, y_{k}\right)_{k \in \mathbb{N}}$, définie par : 


$$
\left\{\begin{array}{l}
x_{k+1}=x_{k}+h\left(x_{k}-x_{k} y_{k}\right) \\
y_{k+1}=y_{k}+h\left(-a y_{k}+x_{k} y_{k}\right)
\end{array},\right. \qquad (8)
$$

telle que $\left(x_{k}, y_{k}\right)$ est une approximation d'une solution de (4) au temps $t_{k}=h k$.

On pose $v_{k}=V\left(x_{k}, y_{k}\right)$, où la fonction $V$ est définie par (6).


1. Exprimer $v_{k+1}-v_{k}$ en fonction de $h, a, K, x_{k}$ et $y_{k}$.
1.  Démontrer que $v_{k+1}-v_{k} \geq 0$.
1.   Démontrer  que   :  $v_{k+1}-v_{k}=0$  si  et  seulement   si  la  suite
    $\left(x_{k}, y_{k}\right)$ est constante. 
1.   Interpréter au  vu de  la partie  2. La  méthode d'Euler  est-elle ici
    satisfaisante ?
	
	
### Partie 5 - Étude informatique

Cette partie consiste  à mettre en place des  programmes informatiques pour
simuler les  différents modèles  considérés dans le  sujet. Bien  que cette
partie soit largement indépendante des parties précédentes, il est vivement
conseillé  de les  avoir lues.  Les programmes  sont à  rédiger en  langage
Python.  L'annexe  2   comporte  des  rappels  sur  les  commandes
utiles. Avant chaque algorithme, on écrira brièvement le raisonnement suivi
et la formule qu'il est censé calculer. 

1.  On considère le code

	```python
	r = 2
	
	def sol(t):
	    return m.exp(r*t)
		
	def liste(T, h):
	    t = 0
		L = [0]
		while t+h <= T:
		    t = t+h
			L.append(t)
		return L
		
	def mystere():
	Lt = liste(20, 10**-2)
	n = len(Lt)
	Ls = []
	for k in range(0, n):
	    Ls.append(sol(Lt[k]))
	plt.plot(Lt, Ls)
	plt.show()
	```
	
    1.  La fonction  `exp` est  présente  dans le  module `math`.  Indiquer
       comment  charger ce  module  pour que  l'utilisation  de `exp`  soit
       correcte dans `sol`.
	1.  Que renvoient  `liste(1, 0.2)`  et `liste(1,  0.3)` ?  Indiquer, de
	   façon générale ce que renvoie `liste(T, h)`.
	1.  Que contient  `Ls`  dans la  fonction `mystere`  ?  Que fait  cette
	   fonction ?

1.  
    1. On considère `L=[[3, 1], [7], [1, 9, 8, 0]]` :
	    
	    1. Que vaut `L[1]` ?
	    1. Que vaut `L[0][1]` ?
		1. Que vaut `len(L)` ?
		1. Que vaut `L` après avoir exécuté `L.append(9.75)`
		
	1. Écrire une fonction d'entête `lapin(x, y)` qui renvoie $x-x y$ et une fonction
       d'entête `lynx(x, y)` qui renvoie $-a y+x y$. La constante $a$
       est supposée être préalablement définie dans une variable globale. 
	1. On ajoute au code précédent la fonction suivante (où les arguments `x0, y0, T`
       et `h` définissent respectivement $x_{0}, y_{0}$, l'intervalle d'étude $[0, T]$ et le
       pas $h>0$ de la méthode d'Euler définie à la partie 4) : 
	   
	   ```python
	   def resol_1(x0, y0, T, h):
	       x, y = x0, y0
		   t = 0
		   Lx = [x]
		   Ly = [y]
		   Lt = [t]
		   while t + h <= T:
		   #### Ligne(s) à compléter ####
		   return [Lt, Lx, Ly]
	   ```
	   Compléter les lignes manquantes dans  la boucle, afin que les listes
	   `Lx`  et  `Ly` contiennent  respectivement  les  $x_{k}$ et  $y_{k}$
	   définis  par l'équation  (8)  et  que la  liste  `Lt` contienne  les
	   $t_{k}$.
	1. On ajoute au code précédent :
	   
	   ```python
	   x0 = 1
	   y0 = 0.5
	   T = 20
	   h = 10**-2
	   
	   def trace_pop_1():
	       L = resol_1(x0, y0, T, h)
		   plt.plot(L[0], L[1])
		   plt.plot(L[0], L[2])
		   plt.show()
	   ```
	   Expliquer  à  quoi  correspond  chaque ligne  de  la  fonction 
	   `trace_pop_1`,  en fonction  des $x_{k}$  et $y_{k}$  de la  méthode
	   d'Euler. 
	1.   L'exécution  de   `trace_pop_1`  donne   les  courbes   présentées
	   figure  1.   Identifier  (avec   explications),  en  lien   avec  la
	   modélisation effectuée, chacune des deux courbes. 
	1.  On ajoute au code précédent :
	   
	   ```python
	   def fonctionV(x, y) :
		   return x - a*m.log (x) + y - m.log (y)
	   ```
	   
	   Écrire une fonction d'entête `trace_V_1()` qui effectue la
	   représentation  graphique  des  valeurs  $v_{k}$  en  ordonnées,  en
	   fonction des valeurs $t_{k}$ en abscisses. 

1.  On cherche dorénavant à effectuer une résolution numérique avec une autre méthode, 
    appelée méthode de Heun, où la relation de récurrence définissant les
    $x_{k}$ et $y_{k}$ est :

    $$
    \left\{\begin{aligned}
    u_{k} &=x_{k}+h\left(x_{k}-x_{k} y_{k}\right) \\
    w_{k} &=y_{k}+h\left(-a y_{k}-x_{k} y_{k}\right) \\
    x_{k+1} &=x_{k}+\frac{h}{2}\left(x_{k}-x_{k} y_{k}+u_{k}-u_{k} w_{k}\right) \\
    y_{k+1} &=y_{k}+\frac{h}{2}\left(-a y_{k}+x_{k} y_{k}-a w_{k}+u_{k} w_{k}\right)
    \end{aligned}\right.
    $$

    1. Écrire une fonction d'entête `resol_2(x0, y0, T, h)`, analogue de
      `resol_1` utilisant la méthode de Heun. 
    1. Que suffit-il de modifier dans la fonction `trace_V_1` pour lui
       faire utiliser la méthode de Heun? 
       On appelle `trace_V_2` la fonction ainsi modifiée.
    1.  L'exécution de `trace_V_1` et `trace_V_2` donne les courbes présentées
       en figure 2 : justifier quelle méthode semble la plus satisfaisante. 
	  
1.  On définit, de manière analogue à la fonction `trace_pop_1`, la
    fonction `trace_pop_2` associée à la méthode de Heun. L'exécution
    de `trace_pop_2` donne les courbes présentées figure 3.  
    Comparer les figures 1 et 3. Quel problème numérique est soulevé par la méthode d'Euler ?
    Que permet d'améliorer la méthode de Heun ? 

1.  Interpréter l'évolution des dynamiques de population des lièvres et des
    lynx obtenues.
   
### Figures

#### Figure 1 - Tracé donné par `trace_pop_1` 

![heun1](heun1.jpg)

#### Figure 2 - Tracés donnés par `trace_V_1` et par `trace_V_2`

![heun1](heun2.jpg)

#### Figure 3 - Tracé donné par `trace_pop_2`

![heun1](heun3.jpg)

### Annexe 2 - Rappels de syntaxes Python pour la partie 5

On suppose que le module `matplotlib.pyplot`, qui permet de tracer des
graphiques, est importé avec l'alias `plt` ; 
 que le module  `math`, qui permet d'utiliser  des fonctions mathématiques,
 est importé avec l'alias `m`. 
  Les variables `Lx`  et `Ly` sont deux listes de  réels, de même longueur;
  la variable `x` est un réel. 

-  `plt.plot(Lx, Ly)` place les points dont les abscisses sont contenues dans
   `Lx` et les ordonnées dans `Ly`, et les relie entre eux par des segments.
    Si cette fonction n'est pas suivie de `plt.show()`, le graphique n'est pas affiché.

- `plt.show()` affiche le(s) tracé(s) précédemment créé(s) par `plt.plot`.

- `m.exp(x)` renvoie $\exp (x) .$

- `m.log(x)` renvoie $\ln (x)$.


