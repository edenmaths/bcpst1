import numpy.random as rd

#########################
## ex 2-3
########################

def simuY():
    X = 1
    tirage = rd.random()
    while tirage < 0.5:
        X += 1
        tirage = rd.random()
    Y = 0
    blanche = False
    while not blanche and Y < X:
        blanche = rd.random() < 1/3
        Y += 1
    while not blanche:
        blanche = rd.random() < 1/4
        Y += 1
    return Y

# ou en utilisant les lois de random

def simuY2():
    X = rd . geometric (1/2)
    Y = 0 
    blanche = False
    while not blanche and n < X :
        blanche = rd.binomial(1, 1/3)
        Y += 1
    while not blanche:
        blanche = rd.binomial(1, 1/4)
        Y += 1
    return Y

# LFGN

def espe(N):
    S = 0
    for _ in range(N):
        S += simuY()
    return S/N


############################
## exo 2-4
##############################


def jeu():
    X = 1
    six = rd.random() < 1/6
    while not six:
        X += 1 
        six = rd.random() < 1/6
    # ou plus tard X = rd.geometric(1/6)
    return 1 if X % 2 == 0 else 0

def pA(N):
    S = 0
    for _ in range(N):
        S += jeu()
    return S/N

def jeu_avec_gain():
    X = 1
    six = rd.random() < 1/6
    while not six:
        X += 1 
        six = rd.random() < 1/6
    # ou plus tard X = rd.geometric(1/6)
    return X if X % 2 == 0 else -X

def gA(N):
    S = 0
    for _ in range(N):
        S += jeu_avec_gain()
    return S/N



###################################################
# exo 2-5
################################################
import numpy as np
import matplotlib.pyplot as plt


def simulX(n, p):
    X = [0 for _ in range(n+1)]
    for i in range(1, n+1):
        X[i] = X[i - 1] + (1 if rd.random() < p else -1)
    return X

def trace_simulX():
    Ab = range(31)
    Or = simulX(30, 1/3)
    plt.scatter(Ab, Or)
    plt.show()
    

def etat_rev(n, p):
    X = simulX(n, p)
    etats_0 = 0 # 0 est éteint au départ
    for i in range(1, n+1): # on parcourt toutes les positions
        if X[i] == 0: # si on est revenu en 0
            etats_0 = 1 - etats_0 # on change l'état de 0
    return etats_0 # on renvoie le dernier état de 0


def pAn(N, n, p):
    S = 0
    for _ in range(N):
        S += etat_rev(n, p)
    return S/N
