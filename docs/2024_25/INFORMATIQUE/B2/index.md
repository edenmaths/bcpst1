---
hide:
  # - navigation # Hide navigation
  # - toc # Hide table of contents
---

1. [Sommes : AV 2017](./TD1_AV2017)
1. [Algo : AV 2021](./TD2_AV2021)
1. [Méthode de Newton : AV 2020](./TD3_AV2020)
1. [Motifs et Horner : INP 2020](./4_INP_2020)

1. [Algo pour l'oral : quelques rappels](../30_denomb)

1. [Info à l'oral : les 10 dernières minutes](../31_TB_algo)

1.  [Info à l'oral : les 30 premières minutes](./oral_part1)

