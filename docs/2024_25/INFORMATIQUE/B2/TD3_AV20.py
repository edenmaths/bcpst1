###########################
## TD3 - AV 2020 Partie 2
###########################

import numpy as np
import matplotlib.pyplot as plt

## Partie 1 - Recherche du minimum d'une fonction

# 1.1 
def f(t: float) -> float:
    return 72 - 32*np.exp(-t) + 32*np.exp(-10*t)

# 1.3
def minimum_f(N):
    Lt1 = np.linspace(0, 5, N)
    Ly1 = []
    for k in range(N):
        Ly1.append(Ly1[k])
    # Ly1 = f(Lt1) #plus malin
    m = Ly1[0] # le minimum est fixé au départ à la 1ère valeur de y...
    tm = Lt1[0] # ...prise en la 1ère valeur de t
    for k in range(1, N): # on compare avec les autres valeurs
        if Ly1[k] < m: # si on trouve un y plus petit
            m, tm = Ly1[k], Lt1[k] # on lie le mini à cette valeur de y et idem pour t
    return m, tm

def dessine(N):
    """
    On trace la courbe obtenue en reliant N points
    régulièrement espacés
    """
    global f
    Lt1 = np.linspace(0, 5, N)
    Ly1 = f(Lt1)
    plt.plot(Lt1, Ly1, marker = "X")
    
## Partie 2 : algorithme de Newton
    
def Newt(F, G, u0, e, nder):
    u = u0 # u va être relié aux valeurs successives de la suite
    n_iter = 0
    while abs(F(u)) >= e:
        u = u - F(u)/G(u)
        n_iter = n_iter + 1
        if n_iter > nder:
            return False
    return u

def g(t):
    return 32*np.exp(-t) - 320*np.exp(-10*t)
