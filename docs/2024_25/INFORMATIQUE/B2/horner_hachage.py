# Q 18
def eval(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo naïf
    """
    res = 0
    for k in range(len(P)):
        res = res + P[k]*4**k
    return res
    
    
# Q 19
def horner_it(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo de Horner
    """
    ...
    
# Q 20
def horner_rec(P: list[int]) -> int:
    """
    évalue P(4), le polynôme P
    étant comme d'habitude représenté
    par la liste de ses coefficients,
    avec l'algo de Horner écrirt récursivement
    """
    ...
    

# Q 20 bis : donner la fonction de hachage
def rang(base: int) -> int:
    """
    associe à chaque base son rang
    A -> 0, C -> 1, G -> 2, T -> 3
    """
    bases = "ACGT"
    for k in range(4):
        if base == bases[k]:
            return k

def hachage(genome: str) -> int:
    P = [rang(base) for base in genome] # le polynôme en base 4
    e = eval(P) # evaluer P(4) avec une des fonctions eval
    return e % 13 # renvoyer P(4) modulo 13
    
 
            
    