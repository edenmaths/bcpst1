{{ chapitre(2, "Probabilités discrètes", "TP ", 0)}}



En  guise  de révision  mathématique  et  informatique  pour les  BCPST  2,
quelques petits exercices sur les probas discrètes  où il
est  explicitement demandé  de fournir  une fonction  Python.

## Quelques exercices


### Avec `random` et `randint`

!!! {{ exercice()}}

    On  considère  une urne  contenant  deux  boules  :  une verte  et  une
    rouge. On  y ajoute des  boules successivement en suivant  le protocole
    ci-dessous :
	
	- À chaque étape $n≥1$, on tire une boule au hasard de l'urne.
	- Si elle  est verte, on la  remet dans l'urne et on  rajoute une boule
	  verte et une boule rouge.
	- Si elle est rouge, on la  remet dans l'urne et on rajoute deux boules
	  rouges.
	  
	On note $V_n$ la variable aléatoire  qui représente le nombre de boules
	vertes dans l'urne à l'issue de l'étape $n≥1$ (on a donc $V_0=1$).
	
	1.  Déterminer le  nombre  de boules  $b_n$ dans  l'urne  à l'issue  de
	   l'étape $n$ pour $n>=1$. 
	2. Déterminer $V_n(Ω)$.
	3. Écrire une fonction **Python** qui  prend en argument un entier $n≥1$ et
	   simule une réalisation de la variable aléatoire $V_n$ puis une autre
	   fonction qui prend en argument un entier $n≥1$ et estime l'éspérance
	   de $V_n$.
	   

!!! {{ exercice()}}
	
	Toutes les  variables aléatoires de  cet exercice sont définies  sur le
	même espace probabilisé $(Ω,\scr A, ℙ)$.
	
	Soit $n$ un entier supérieur ou égal à 2.
	
	Une souris  se trouve dans une  boîte. Sur les cloisons  de cette boîte
	sont dessinées $n-1$  fausses portes et cette  boîte comporte également
	une vraie porte lui permettant de sortir de la boîte. Lorsque la souris
	s'aperçoit qu'elle a choisi une fausse porte, elle revient au centre de
	la boîte pour un nouvel essai. On  note X la VAR représentant le nombre
	d'essais faits par notre souris pour trouver la porte qui lui permet de
	sortir.
	
	1. Dans cette question, on suppose que la souris se souvient des portes
	   qu'elle a  déjà ouvertes et qu'elle  ne retente jamais deux  fois la
	   même porte.
	   
	     1. Déterminer la loi de X et donner son espérance.
	     1. Écrire une fonction **Python** qui prend en argument un entier
	        $n≥2$ et qui renvoie une simulation de X.
	
	1. Dans  cette question,  on suppose  que la souris  ne possède  pas de
	   mémoire.
	   
	     1. Reconnaître la loi de X. Donner son espérance.
	     1. Écrire une fonction **Python** qui prend en argument un entier
	        $n≥2$ et qui renvoie une simulation de X.
	
	1. Dans  cette question, la souris  ne possède toujours pas  de mémoire
	   et,  à chaque  erreur,  on  dessine une  nouvelle  fausse porte.  Au
	   départ, la cage possède une fausse porte et la vraie porte.
	   
	     1. Déterminer dans ce cas la loi de X. Admet-elle une espérance ?
	     1. Écrire une fonction **Python** qui prend en argument un entier
	        $n≥2$ et qui renvoie une simulation de X.
	
	1.  Dans cette  question, on  suppose que  la souris  est sans  mémoire
	   pendant  les $\ell$  premiers essais  ($\ell∈ℕ^*$) puis  possède une
	   mémoire immédiate ensuite : à  partir du $\ell+1$-ème essai, et tant
	   qu'elle n'est pas sortie, elle évite alors la dernière porte essayée
	   pour l'essai suivant.
	   
	     1. Déterminer la loi de X et vérifier que
	        $\displaystile\sum_{k=1}^{+\infty} ℙ(X=k)=1$.
	     1. Écrire une fonction **Python** qui prend en arguments un entier
	        $n≥2$ et un entier $\ell≥1$ et qui renvoie une simulation de X.
	     1. Écrire une fonction **Python** `simulesperance` qui prend en
	        arguments trois entiers $N≥1$, 
	        $n≥2$ et $\ell≥1$ et qui calcule la moyenne d'un échantillon de
	        taille $N$ de réalisations de la VAR X.
		  

### Avec `geometric` et `binomial`

!!! {{ exercice()}}
	
	On dispose d'une pièce équilibrée et de deux urnes. L'urne U contient une
	boule blanche  et deux  boules rouges  tamdis que  l'urne V  contient 1
	boule blanche et 3 boules rouges.
	On suit le protocole suivant:
	
	- *Étape  1* : on effectue  des lancers successifs de  la pièce jusqu'à
	  obtention  du premier  Face.  On note  alors X  le  numéro du  lancer
	  amenant face pour la première fois.
	- *Étape 2* : si le premier  face est obtenu lors du $k$-ème lancre, on
	  effectue $k$  tirages d'une  boule avec remise  dans U,  suivis d'une
	  succession infinie de tirages d'une boule avec remise dans V.
	  
	On note Y le numéro du tirage  dans les urnes amenant une boule blanche
	pour la première fois.
	
	Ainsi, si  on a obtenu  la succession  *pile*, *pile*, *face*,  alors X
	vaut  3, on  effectue alors  3 tirages  dans U  puis une  succession de
	tirages  dans  V.  Si  on  obtient des  couleurs  de  boules  suivantes
	successivement  *rouge*,  *rouge*,  *rouge*, *rouge*  *blanche*,  alors
	Y vaut 5.
	
	1. Écrire une fonction **Python** permettant de simuler une réalisation
	   de Y.
	   
	1. À l'aide de cette fonction,  donner une estimation de l'espérance de
	   Y. Quel résultat du cours utilise-t-on?
	   

!!! {{ exercice()}}

    On effectue une série de  lancers d'un dé équilibré jusqu'à l'obtention
    du premier 6. On note X le rang du premier 6 obtenu. 
	Un jeu opposant  deux joueurs A et  Βutilise ces lancers :  le joueur A
	gagne si  le nombre  de lancers  ainsi effectué est  pair, le  joueur B
	gagne si celui-ci est impair.
	
	1. Déterminer la loi de X, son espérance et sa variance.
	1. Écrire un programme **Python** permettant de calculer une estimation
	   de la  probabilité que  A gagne.  Si vous aviez  à choisir,  de quel
	   joueur prendriez-vous le rôle ?
	1.  Calculez  la  probabilité  que  le  joueur  A  gagne  et  confirmez
	   l'hypothèse faite.
	1. Le joueur  perdant donne au joueur gagnant une  somme d'argent égale
	   au  nombre de  lancers de  dés. On  considère la  variable aléatoire
	   Y égale au gain du joueur A. On a donc Y = X si X est pair et $-X$
	   sinon, ce qui donne $Y=(-1)^XX.
	   
	      1. Modifiez le  programme précédent pour permettre  de donner une
	         estimation du gain moyen du joueur A. 
		  1. Démontrer que Y a une espérance et la calculer.
		  2. Quel joueur préférez-vous être ?
		  
		  
### Avec des graphiques


!!! {{ exercice()}}

	Soit $p$ un réel de $]0,1[$.
	
	Au XIXe siècle,  des personnes employées par les  villes se chargeaient
	d'allumer  un par  un les  réverbères de  la ville.  On suppose  que le
	parcours effectué  par un allumeur  de réverbères peut  être représenté
	par une droite  graduée, que l'allumeur part de la  position 0, que les
	réverbères se situent  en des points d'abscisses  entières positives ou
	négatives. Cependant à chaque instant  $n∈ℕ$, si l'allumeur se situe au
	réverbère d'abscisse  $x_n$, il  se positionnera  à l'instant  $n+1$ au
	réverbère d'abscisse $x_{n}+1$ avec une probabilité $p$ ou au réverbère
	d'abscisse  $x_n-1$ avec  une  probabilité $q=1-p$.  À  chaque fois  il
	change l'état du réverbère rencontré.
	À  l'instant  $n=0$,  tous  les  réverbères  sont  éteints  dont  celui
	d'abscisse 0. 
	On admet que  ce problème peut se modéliser dans  un espace probabilisé
	$(Ω\scr T, ℙ)$.
	Notons  la VAR  $X_n$  égale à  l'abscisse où  se  trouve l'allumeur  à
	l'instant $n$.
	
	1. 
	     1. Donner la loi de $X_0$ et la loi de $X_1$.
		 1.  Démontrer  que  $X_2(Ω) =\{-2,0,2\}$  et  que  $ℙ(X_2=0)=2pq$,
		    $ℙ(X_2=-2=q^2$ et $ℙ(X_2=2)=p^2$.
	1.  
	     1. Écrire une fonction `simulX` en **Python** prenant en entrée un
	        entier  $n∈ℕ$ et  un réel  $p∈]0,1[$  et qui  renvoie la  liste
	        $[X_0,X_1,...X_n]$ des positions de  l'allumeur aux instants 0,
	        1,...,$n$.
		 1. Utiliser la fonction précédente pour tracer en **Python** $X_k$
		    en fonction de $k$  pour $k∈{0,1,..30}$ et $p=\frac{1}{3}$. Sur
		    votre graphe, l'allumeur revient-il  à sa position initiale? En
		    quel(s) instant(s)?
		 1. À  partir de la  fonction obtenue à  la question a,  écrire une
		    fonction `EtatRev`  en **Python**  prenant en entrée  un entier
		    $n∈ℕ$ et  un réel $p∈]0,1[$  et qui  renvoie 1 si  le réverbère
		    initial (en position 0) est allumé à l'instant $n$ et 0 sinon.
		 1. On  note $A_n$  l'événement "*le réverbère  positionné en  0 est
		    allumé à l'instant* $n$".
		    Écrire en **Python** une fonction prenant en entrée deux
		    entiers naturels $N$  et $n$ et un réel  $p∈]0,1[$ et renvoyant
		    une valeur approchée de $ℙ(A_n)$ à partir de $N$ simulations de
		    l'expérience.   Préciser  le   théorème   utilisé  pour   cette
		    approcimation.
			
