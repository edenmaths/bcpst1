### Récursion
#############

#############################
## Somme d'entiers successifs
##############################

# On remarque que S_{n} = n + S_{n-1}

def som_ent(n: int) -> int:
    if n <= 0:
        return 0
    else:
        return n + som_ent(n - 1)
    
# test
assert som_ent(100) == 100*101/2

###################################
## Somme des éléments d'une liste
###################################

# On remarque que som([a1, a2,..., an]) = an + som([a1, a2,..., a_{n-1}])
# ou encore que som([a1, a2,..., an]) = a1 + som([a2, a3,..., a_{n}])
# et som([]) = 0 -- cas terminal

def som(xs: list[int]) -> int:
    if xs == []:
        return 0
    else:
        return xs[-1] + som(xs[:-1]) # ou dans l'autre sens xs[0] + xs[1:]
    
# on peut sinon fabriquer des fonction tête et queue
def tete(xs: list):
    return xs[0]

def queue(xs: list) -> list:
    return xs[1:]

def som2(xs: list[int]) -> int:
    if xs == []:
        return 0
    else:
        return tete(xs) + som2(queue(xs))
    
assert som2([1,2,3,4]) == 10

###########################################
# Longueur
############################################

def long(xs: list) -> int:
    if xs == []:
        return 0
    else:
        return 1 + long(queue(xs))
    
assert long([10,2,-3,44]) == 4


###########################################
# Compteur
############################################

def compt(lettre: str, mot: str) -> int:
    if mot == "":
        return 0
    else:
        nouveau = 1 if mot[0] == lettre else 0
        return nouveau + compt(lettre, mot[1:])

# ou par exemple

def compt2(lettre: str, mot: str) -> int:
    if mot == "":
        return 0
    elif mot[0] == lettre:
        return 1 + compt2(lettre, mot[1:])
    else:
        return compt2(lettre, mot[1:])
    
assert compt("a", "abricadabra") == 4

##################
# puissance entière
##################

def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    else:
        return a * pow(a, n - 1)
    
assert pow(2, 10) == 1024


##########
# tortue
##########

from turtle import *

def turec(n, fin=50):
    left(60)
    forward(fin + n)
    if n < fin :
        turec(n + 1, fin)
    else:
        hideturtle()
        write("FINI")
        exitonclick()
