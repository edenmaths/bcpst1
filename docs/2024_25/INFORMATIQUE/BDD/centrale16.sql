BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS vol (
	id_vol	TEXT PRIMARY KEY,
	depart	TEXT,
	arrivee       TEXT,
	jour          DATE,
	heure         TIME,
	niveau         INTEGER
);
INSERT INTO vol VALUES ('AF1204', 'CDG', 'FCO', '2016-05-02', '07:35', 300);
INSERT INTO vol VALUES ('AF1205', 'FCO', 'CDG', '2016-05-02', '10:25', 300);
INSERT INTO vol VALUES ('AF1504', 'CDG', 'FCO', '2016-05-02', '10:05', 310);
INSERT INTO vol VALUES ('AF1505', 'FCO', 'CDG', '2016-05-02', '13:00', 310);
INSERT INTO vol VALUES ('AF1507', 'MRS', 'CDG', '2016-05-02', '13:10', 330);
INSERT INTO vol VALUES ('AF1607', 'MRS', 'ORY', '2016-05-02', '13:50', 350);

CREATE TABLE IF NOT EXISTS aeroport (
	id_aero TEXT PRIMARY KEY,
	ville	TEXT,
	pays       TEXT
);
INSERT INTO aeroport VALUES ('CDG', 'Paris', 'France');
INSERT INTO aeroport VALUES ('ORY', 'Paris', 'France');
INSERT INTO aeroport VALUES ('MRS', 'Marseille', 'France');
INSERT INTO aeroport VALUES ('FCO', 'Rome', 'Italie');

COMMIT;

