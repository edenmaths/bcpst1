# Épreuves Agro-Véto TB

## Question 1

### 1.a.i

`ch[0]` est une chaîne de caractères qui renvoie le premier élément de la chaîne `"1234"` c'est-à-dire `"1"`



ch = "1234"
print(f"ch[0] est de type {type(ch[0])} et vaut {ch[0]}")

### 1.1.ii

`int(ch[0])` transforme la chaîne `"1"` en l'entier qu'elle contient c'est-à-dire `1`


```python
ch = "1234"
print(f"int(ch[0]) est de type {type(int(ch[0]))} et vaut {int(ch[0])}")
```

    int(ch[0]) est de type <class 'int'> et vaut 1


### 1.b


```python
def string2list(chaine: str) -> list[int]:
    #liste = []
    #for car in chaine:
    #    liste.append(int(car))
    #return liste
    return [int(car) for car in chaine]
```


```python
string2list("1234")
```




    [1, 2, 3, 4]



### 1.c

Le `+` est entre deux chaînes de caractère : c'est donc l'opérateur de concaténation des chaînes.

La chaîne obtenue est donc `"12"`


```python
"1" + str(2)
```




    '12'



### 1.d

Même idée que pour la 1.b mais en construisant une chaîne à partir de la chaîne vide


```python
def list2string(liste: list[int]) -> str:
    ch = ""
    for n in liste:
        ch += str(n)
    return ch
```


```python
list2string([1,2,3,4])
```




    '1234'



## Question 2

### 2.1.a


```python
from random import randint

def generateur_solution(n: int) -> list[int]:
    return [randint(0, 9) for _ in range(n)]
```


```python
generateur_solution(10)
```




    [6, 2, 1, 7, 1, 7, 9, 3, 8, 6]



### 2.1.b

cf TP précédent (comptage des décimales de pi)


```python
def distribution(ns: list[int]) -> list[int]:
    cpts = [0]*10
    for n in ns:
        cpts[n] += 1
    return cpts
```


```python
distribution([4, 7, 7, 1])
```




    [0, 1, 0, 0, 1, 0, 0, 2, 0, 0]



### 2.1.c

On choisit la 4e 



```python
def nb_entiers_communs(liste1: list[int], liste2: list[int]) -> int:
    s = 0
    distrib1 = distribution(liste1)
    distrib2 = distribution(liste2)
    for k in range(10):
        m = min(distrib1[k], distrib2[k])
        #print(f"{k} est présent {m} fois dans les 2 listes")
        s += m
    return s    
```


```python
nb_entiers_communs([4, 7, 7, 1], [4, 4, 7, 7])
```




    3



### 2.1.d

On regarde les deux listes en parallèle et on compte le nombre de coincidences


```python
def bien_places(proposition: list[int], solution: list[int]) -> int:
    cpt = 0
    for k in range(len(proposition)):
        if proposition[k] == solution[k]:
            cpt += 1
            #print(f"{solution[k]} bien placé en {k+1}e position")
    return cpt
```


```python
bien_places([4, 4, 1, 7, 1], [4, 7, 7, 1, 1])
```




    2



## Programme principal


```python
def jeu(nb_chiffres=4, nb_essais_max=12):
    nb_essais = 0
    nb_bien_places = 0
    solution = generateur_solution(nb_chiffres)
    print(f"Vous avez au plus {nb_essais_max} essais")
    while nb_essais < nb_essais_max and nb_bien_places < nb_chiffres:
        print(f"Nombre d'essais restant {nb_essais_max - nb_essais}")
        proposition = string2list(input("Entrez votre proposition"))
        nb_bien_places = bien_places(proposition, solution)
        nb_mal_places = nb_entiers_communs(proposition, solution) - nb_bien_places
        print(f"{nb_bien_places} bien placés")
        print(f"{nb_mal_places} mal placés")
        nb_essais += 1
    if nb_bien_places == nb_chiffres:
        print("GAGNÉ !")
    else:
        print("PERDU !")
```


```python
jeu()
```

    Vous avez au plus 12 essais
    Nombre d'essais restant 12


    Entrez votre proposition 1234


    1 bien placés
    1 mal placés
    Nombre d'essais restant 11


    Entrez votre proposition 1111


    0 bien placés
    0 mal placés
    Nombre d'essais restant 10


    Entrez votre proposition 2222


    1 bien placés
    0 mal placés
    Nombre d'essais restant 9


    Entrez votre proposition 3333


    1 bien placés
    0 mal placés
    Nombre d'essais restant 8


    Entrez votre proposition 3256


    0 bien placés
    2 mal placés
    Nombre d'essais restant 7


    Entrez votre proposition 2738


    2 bien placés
    0 mal placés
    Nombre d'essais restant 6


    Entrez votre proposition 2939


    3 bien placés
    0 mal placés
    Nombre d'essais restant 5


    Entrez votre proposition 2930


    4 bien placés
    0 mal placés
    GAGNÉ !



```python

```
