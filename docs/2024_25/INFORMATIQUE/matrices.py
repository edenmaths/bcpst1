def eye(n):
    return [[1 if i == j else 0 for j in range(n)] for i in range(n)]

def prod(A: list, B: list) -> list:
    ra, ca = len(A), len(A[0])
    rb, cb = len(B), len(B[0])
    assert ca == rb, "tailles incompatibles pour le produit"
    C = [[0 for j in range(cb)] for i in range(ra)]
    for i in range(ra):
        for j in range(cb):
            for k in range(ca):
                C[i][j] += A[i][k]*B[k][j]
    return C

def trans(A: list) -> list:
    ra, ca = len(A), len(A[0])
    return [[A[j][i] for j in range(ra)] for i in range(ca)]

def ps(r1: list, r2: list) -> float:
    n1, n2 = len(r1), len(r2)
    assert n1 == n2, "listes de tailles incompatibles"
    p = 0
    for i in range(n1):
        p += r1[i]*r2[i]
    return p

def prod2(A: list, B: list) -> list:
    ra, ca = len(A), len(A[0])
    rb, cb = len(B), len(B[0])
    assert ca == rb, "tailles incompatibles pour le produit"
    tB = trans(B)
    return [[ps(A[i], tB[j]) for j in range(cb)] for i in range(ra)]

