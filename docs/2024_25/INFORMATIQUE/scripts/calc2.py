age = int(input("Quel est ton âge ?"))

if age < 18:
   message = "Va te coucher !"
elif age < 60:
   message = "Tu peux regarder le film"
else :
   message = "Reveille-toi !"

print(f"Tu as {age} ans : {message.upper()}")
