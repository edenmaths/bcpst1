import folium # pour manipuler des cartes
import json # langage de stockage de données
import os  # lance des commandes shell 
from urllib.request import urlretrieve # pour récupérer des fichiers sur internet
import pandas as pd
import numpy as np
from zipfile import ZipFile



#pip install html5lib  openpyxl

url_pop = "https://www.insee.fr/fr/statistiques/6013867?sommaire=6011075"
tab = pd.read_html(url_pop)[0]
popd = tab["Population municipale"].to_dict()

url_bigzip = "https://www.insee.fr/fr/statistiques/fichier/6011070/ensemble.zip"
bigzip = urlretrieve(url_bigzip)[0]

with ZipFile(bigzip, 'r') as zip:
    zip.extract('donnees_departements.csv')
    
dfdep = pd.read_csv('donnees_departements.csv', sep=';')

popd = {}
for c in range(96):
    popd[dfdep['CODDEP'][c]] = dfdep['PMUN'][c]


url_euro = "https://www.data.gouv.fr/fr/datasets/r/742618ab-eb59-4a00-801d-c2c3ebac1efd"
df2 = pd.read_excel(url_euro)

df2.columns = df2.iloc[0] # 2e ligne comme en-tête de colonnes
df2 = df2[1:] # on prend comme valeurs celles à partir de la 3e ligne


def dic_t2(colonne: str) -> dict:
    dic1 = {}
    for c in range(96):
        dic1[df2['Code département'][c]] = df2[colonne][c]
    dic2 = {}
    for c in popd:
        dic2[c] = 100 * dic1[c] / popd[c]
    return dic2

url_geo_dep = 'https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/departements.geojson'
deps_geo = open(urlretrieve(url_geo_dep)[0]).read()
geo_json_data = json.loads(deps_geo)

def carte(stats, legende):
    mini, maxi = min(stats.values()), max(stats.values()) # pour l'échelle de couleur
    bins = np.linspace(int(mini), int(maxi)+1, 10) # on peut avoir 9 niveaux de couleurs au maximum 
    m = folium.Map([45.5, -2], tiles='stamentoner', zoom_start=6) # obtenir la carte du fond centrée sur la France
    folium.Choropleth(
        geo_data = geo_json_data, # les données des zones géographiques qui seront colorées
        name = 'choropleth', # le type de coloriage
        data = stats, # les données qui vont être traitées 
        key_on = 'feature.properties.code',
        fill_color = 'Reds', # la palette de couleur : d'autres ici -> https://colorbrewer2.org/
        fill_opacity = 0.8, # transparence du remplissage
        line_opacity = 0.2, # transparence des frontières
        legend_name = legende,
        bins = bins,
        reset = True
    ).add_to(m)
    folium.LayerControl().add_to(m) # on ajoute le coloriage au fond de carte
    m.save(os.path.join('./', 'Colormap_{}.html'.format(legende))) # on enregistre la carte au format html pour la lire dans un navigateur
    return m


# 4 : LFI 5 : RN

carte(dic_t2('Voix 5'), 'RN sur pop du département')
