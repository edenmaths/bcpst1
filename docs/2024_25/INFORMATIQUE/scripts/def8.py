def lesDonneesDe(prenom: str, nom: str, domaine: str = "mafirme.fr") -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees
