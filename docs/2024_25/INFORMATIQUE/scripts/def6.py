def tasDeDonnees(donnees: list[tuple[str, str, str]]) -> str:
    """
    donnees est une liste de triplets de la forme (prenom, nom, domaine)
    renvoie une chaîne sous la forme :
    prenom
    nom
    pnom@domaine

    >>> data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk")]
    >>> tasDeDonnees(data)
    'James\nHaskell\njhaskell@wasps.uk\nJames\nBond\njbond@secret.uk\n'
    """
    sortie = ""
    for triplet in donnees:
        prenom, nom, domaine = triplet
        sortie += lesDonneesDe(prenom, nom, domaine)
    return sortie

print(tasDeDonnees(data))
