import matplotlib.pyplot as plt


## Tableau des effectifs/ des fréquences

S = 'mfmmmfffmfmmmmfffmmffffmffmmmmmfmffmffffmmmfffmmffffffffffmmmfmmfmfmmmfmmmf'
T = [c for c in S]

from copy import deepcopy

def bordante_lignes(T: list) -> list:
    B = deepcopy(T)
    nl, nc = len(T), len(T[0])
    for ligne in B:
        total = 0
        for nb in ligne:
            total += nb
        ligne.append(total)
    return B
    
def bordante_cols(T: list) -> list:
    B = deepcopy(T)
    nl, nc = len(T), len(T[0])
    B.append([0 for _ in range(nc)])
    for j in range(nc):
        for i in range(nl):
            B[nl][j] += B[i][j]
    return B

def bordante(T: list):
    return bordante_cols(bordante_lignes(T))



def dico_eff(tab: list, total = False) -> dict:
    d = {}
    for el in tab:
        if el in d:
            d[el] += 1
        else:
            d[el] = 1
    if total:
        d['total'] = len(tab)
    return d

def dico_freq(tab: list) -> dict:
    dic_freq = {}
    dic_eff = dico_eff(tab, True)
    total = dic_eff['total']
    for cle in dic_eff:
        dic_freq[cle] = 100*dic_eff[cle] / total
    return dic_freq


def barre(d_eff: dict, name=None) -> None:
    plt.clf()
    plt.bar([k for k in d_eff], [d_eff[k] for k in d_eff], width=0.5)
    if name is not None:
        plt.savefig(f"./FIGURES/{name}_barre.pdf",bbox_inches='tight',pad_inches=0)
    else:
        plt.show()


def donut(d_eff: list, name=None) -> None:
    plt.clf()
    plt.pie([d_eff[k] for k in d_eff], labels=[k for k in d_eff], wedgeprops={'width':0.5})
    if name is not None:
        plt.savefig(f"./FIGURES/{name}_donut.pdf",bbox_inches='tight',pad_inches=0)
    else:
        plt.show()


sondage = {'Extrêmement': 50.9, 'Tout à fait': 34.3, 'Totalement': 10.9, 'Très': 3.9}


#### Histogrammes

ventes = [163,169,183,224,140,163,172,104,121,141,139,135,126,163,127,141,136,95,128,95,133,92,119,103,150,149,110,148,153,156,151,109,143,123,75,189,126,173,171,128,153,142,148,112,112,150,121,162,120] 


def histo(vals: list, nb_barres: int) -> None:
    m = min(vals)
    M = max(vals)
    pas = (M - m) / nb_barres
    plt.clf()
    plt.hist(vals, bins=[m + pas*k for k in range(nb_barres + 1)], edgecolor='black', linewidth=0.2) 
    plt.show()


def histo2(vals: list, lims: list) -> None:
    plt.clf()
    plt.hist(vals, bins=lims, edgecolor='black', linewidth=0.2, density=True) 
    plt.show()

femmes = [17.5]*202 + [22.5]*204 + [27.5]*359 + [37.5]*338 + [47.5]*304 + [70]*10



### Moyennes mobiles 

import pandas
from urllib.request import urlretrieve
from datetime import date 
from matplotlib.dates import drange
import numpy as np
import matplotlib.pyplot as plt


# On récupère l'année actuelle
actuel = date.today().year

# On récupère les données en ligne qui sont compressées au format gzip sous forme d'un dataframe de pandas
url = f"http://psc.apl.uw.edu/wordpress/wp-content/uploads/schweiger/ice_volume/PIOMAS.vol.daily.1979.{actuel}.Current.v2.1.dat.gz"
urlretrieve(url, filename="./banquise.gz")
df = pandas.read_table("banquise.gz", compression='gzip', sep="\s+")

# Le dataframe a une colonne Year, une colonne #Day et une colonne Vol
# On crée des listes à partir des données du dataframe 
noannees = df['Year'].to_list()
nojours = df['#day'].to_list()
volumes = df['Vol'].to_list()

# La liste des dates enregistrées au format date : année, mois, jour
dates = [date.fromordinal(date(noannees[i], 1, 1).toordinal() + nojours[i] - 1) for i in range(len(noannees))]

# les années étudiées
les_ans = range(noannees[0], noannees[-1]+1)

# moyennes, maxi, mini, écart-type par an
vols_an = [df.loc[df['Year']==y]['Vol'].mean() for y in les_ans]
maxi = [df.loc[df['Year']==y]['Vol'].max() for y in les_ans]
mini = [df.loc[df['Year']==y]['Vol'].min() for y in les_ans]
std = [df.loc[df['Year']==y]['Vol'].std() for y in les_ans]
extrema = [mini, maxi]



def moyenne_mobile(absx: list, data: list, k: int) -> list:
    k2 = k // 2
    n = len(data)
    if k % 2 == 1:
        new_data = [np.mean(data[i - k2:i + k2 + 1]) for i in range(k2, n - k2)]
    else:
        extr = lambda i: [0.5*(data[i - k2] + data[i + k2])]
        new_data = [np.mean(data[i-k2+1:i+k2] + extr(i) ) for i in range(k2, n - k2)]
    return absx[k2:n - k2], new_data



plt.clf()
#plt.plot_date(dates, volumes, fmt=',')
plt.errorbar(les_ans, vols_an, yerr = std, fmt='-o', ecolor='red', capsize=4, linestyle='dotted')
plt.ylim(0, 1.1*max(volumes))
X, Y = moyenne_mobile(les_ans, vols_an, 5)
#plt.plot(X, Y, color='red', marker='P')
#plt.plot(les_ans, vols_an, linestyle='dotted', marker='+')
#plt.savefig(f"./banquise5.pdf",bbox_inches='tight',pad_inches=0)
plt.show()


##### Médiane

frontières = [k*5 for k in range(8)]
élèves = [300,420,500,330,250,160,40]

def class2val(cs: list, effs: list)-> list:
    res = []
    for k in range(len(cs) - 1):
        res += [0.5*(cs[k] + cs[k+1])]*effs[k]
    return res

def val2vals(vs: list, effs: list)-> list:
    res = []
    for k in range(len(vs)):
        res += [vs[k]]*effs[k]
    return res


def mediane(xs: list) -> float:
    ordre = sorted(xs)
    n = len(xs)
    if n % 2 == 1:
        return ordre[n//2]
    else:
        return 0.5*(ordre[n//2 - 1] + ordre[n // 2])

def quartiles(xs: list) -> list:
    ordre = sorted(xs)
    n = len(xs)
    M = mediane(xs)
    if n % 2 == 0:
        Q1 = mediane(xs[:n//2])
        Q3 = mediane(xs[n//2:])
    elif n % 4 == 1: 
        Q1 = mediane(xs[:n//2 + 1])
        Q3 = mediane(xs[n//2:])
    else:
        Q1 = mediane(xs[:n//2])
        Q3 = mediane(xs[n//2 + 1:])
    return [Q1, M, Q3]


notes = np.array(range(21))
A = [0,0,0,1,1,0,2,3,2,9,5,9,5,1,2,1,0,2,1,1,0]
B = [0,5,5,5,5,0,0,0,0,0,3,0,5,5,0,2,0,5,0,5,0]

plt.clf()
plt.boxplot([val2vals(notes, A), val2vals(notes, B)], labels=["classe A", "classe B"], showmeans=True, notch=True, patch_artist=True, boxprops=dict(facecolor='cyan', color='blue'),whiskerprops=dict(linestyle='dotted'))
print(quartiles(val2vals(notes, A)))
print(quartiles(val2vals(notes, B)))
plt.savefig(f"./FIGURES/moustache2.pdf",bbox_inches='tight',pad_inches=0)
plt.show()

### Climat 1


import pandas
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
from datetime import date

def clim(lieu="Monde", an=1970):
    # récupération de l'année courante
    auj = date.today().year
    # on prend le fichier de l'année courante s'il est disponible
    # (à partir de  mars) 
    an_deb = auj if date.today().month >= 3 else auj - 1
    # On laisse le choix du lieu
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
    # récupération du fichier choisi
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    urlretrieve(url, filename="./temp_anomalies.csv")
    df = pandas.read_csv("temp_anomalies.csv", header=4)

    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ
    def formate(d: int) -> date:
        s = str(d)
        return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01') 
    
    df['Year'] = df['Year'].map(formate)
    df.columns = ["Année", "Anomalie"]

    # Tracé
    plt.clf()
    # on récupère le dernier mois fourni
    dernier = df.iloc[-1,0] 
    titre = f"Évolution des températures {tit} depuis {an}\nDernière mesure le 1er {dernier.strftime('%B %Y')} "
            
    ax = df.plot.scatter(x='Année', y='Anomalie', s=1, c='Anomalie', colormap="viridis")
    plt.title(titre)
    ax.grid(True)
    return ax


# Clim 2


import pandas
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
from datetime import date
#from scipy import interpolate
import numpy as np
#import localreg as lr
#import locale



def clim(lieu="Monde", an=1970):
    auj = date.today().year
    an_deb = auj if date.today().month >= 3 else auj - 1
    
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
    
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    # import du csv et transformation en dataframe
    urlretrieve(url, filename="./temp_anomalies.csv")
    df = pandas.read_csv("temp_anomalies.csv", header=4)

    def formate(d: int) -> date:
        s = str(d)
        return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01') 
    
    df['Year'] = df['Year'].map(formate)

    # Transformation de la date aaaamm au format date de datetime
    #dates =  [date(d//100, d%100, 1) for d in df["Year"]]
    # dataframe par mois avec les dates au bon format
    #data_mois = pandas.DataFrame({"Date":dates, "Anomalie":df['Anomaly']})
    #data_mois["Date"] = data_mois["Date"].astype("datetime64[ns]")
    # lissage par année 
    #dates_mois_np = df["Year"].to_numpy()
    #anos_mois_np = df["Anomaly"].to_numpy()
    #anos_an = np.array([np.mean(anos_mois_np[12*k:12*(k+1)]) for k in range(1+len(dates_mois_np)//12)])
    #dates_an = np.arange(an, dates_mois_np[-1]//100 + 1)
    #dates_an_dates = np.array([date(i,12,31) for i in dates_an])
    #data_an =  pandas.DataFrame({"Date":dates_an_dates, "Anomalie":anos_an})
    # Pour les prévisions
    #futur_i = [202000 + (k//12)*100 + k%12 + 1 for k in range(1, 240)]
    #futur = [date(d//100, d%100, 1) for d in futur_i]
    #futur_i_an = np.arange(futur_i[0]//100, futur_i[-1]//100 + 1)
    #futur_an = np.array([date(i,12,31) for i in futur_i_an])

    # Tracé
    plt.clf()
    #fig, ax = plt.subplots()
    dernier = df.iloc[-1,0] 
    titre = f"Évolution des anomalies de températures {tit} depuis {an}\nDernière mesure le 1er {dernier.strftime('%B %Y')} "
            
    #ax.scatter(data_an['Date'],data_an['Anomalie'],
    #           label="Températures mesurées",
    #           c = data_an["Anomalie"],
    #           cmap="viridis"
    #)
    ax = df.plot.scatter(x='Year', y='Anomaly', c='Anomaly', colormap="viridis")
    
    #interpol = lr.localreg(dates_an, anos_an, degree=2, kernel=lr.rbf.epanechnikov, frac=0.7)
    #extrapol = lr.localreg(dates_an, anos_an, x0=futur_i_an, degree=2, kernel=lr.rbf.epanechnikov, frac=0.7)

    #ax.plot(futur_an, extrapol, label="Prévision")
    #ax.plot(dates_an_dates, interpol, label="interpolation")

    plt.title(titre)
    ax.grid(True)

    plt.show()
