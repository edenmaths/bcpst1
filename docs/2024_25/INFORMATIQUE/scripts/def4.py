def lesDonneesDe(prenom, nom, domaine):
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees

print(lesDonneesDe('James', 'Haskell', 'wasps.uk'))
