import pandas
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
from datetime import date

def clim(lieu="Monde", an=1970):
    # récupération de l'année courante
    auj = date.today().year
    # on prend le fichier de l'année courante s'il est disponible
    # (à partir de  mars) 
    an_deb = auj if date.today().month >= 3 else auj - 1
    # On laisse le choix du lieu
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
    # récupération du fichier choisi
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    urlretrieve(url, filename="./temp_anomalies.csv")
    df = pandas.read_csv("temp_anomalies.csv", header=4)

    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ
    def formate(d: int) -> date:
        s = str(d)
        return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01') 
    
    df['Date'] = df['Date'].map(formate)
    df.columns = ["Année", "Anomalie"]

    # Tracé
    plt.clf()
    # on récupère le dernier mois fourni
    dernier = df.iloc[-1,0] 
    titre = f"Évolution des températures {tit} depuis {an}\nDernière mesure le 1er {dernier.strftime('%B %Y')} "
            
    ax = df.plot.scatter(x='Année', y='Anomalie', s=1, c='Anomalie', colormap="viridis")
    plt.title(titre)
    ax.grid(True)
    return ax
