def vitesse_moyenne1(dist: float, tps: float) -> float:
    print(dist/tps)


def vitesse_moyenne2(dist: float, tps: float) -> float:
    return dist/tps

def moyenne(xs: list) -> float:
    m = 0
    for x in xs:
        m = m + x
    return m / (len(xs))

def moy_des_vitesses_moy(couples: list) -> float:
    vs = [vitesse_moyenne2(c[0], c[1]) for c in couples]
    return moyenne(vs)


cpls = [(12, 5), (11, 4), (9, 12)]
