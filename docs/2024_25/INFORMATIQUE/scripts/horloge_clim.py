#!/usr/bin/env python
# coding: utf-8


from dash import Dash, dcc, html, Input, Output, callback
import pandas
from urllib.request import urlretrieve
import datetime
from datetime import date
from matplotlib.dates import drange 
import plotly.express as px
from base64 import b64encode
import io
from scipy import interpolate
import numpy as np
import localreg as lr
from plotly.offline import plot
import locale

app = Dash(__name__, routes_pathname_prefix="/horloge/")
server = app.server
buffer = io.StringIO()
html_bytes = buffer.getvalue().encode()
encoded = b64encode(html_bytes).decode()
locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
# récupération de l'année courante
auj = datetime.date.today().year
# on prend le fichier de l'année courante s'il est disponible
# (à partir de  mars)
an_deb = auj if datetime.date.today().month >= 3 else auj - 1

app.layout = html.Div([
    html.Title("Réchauffement climatique"),
    html.P("Année de départ :"),
    dcc.Slider(1900, an_deb,5,value=1990, marks=None, id="annee",tooltip={"placement": "bottom", "always_visible": True}),
    html.P("Lieu étudié :"),
    dcc.RadioItems(id="lieu",
                   inline=True,
                   options=["Externat", "Arctique", "Europe", "Monde"],
                   value="Monde"),
    dcc.Graph(id='graphe'),

    html.P("Page HTML :"),
    
    html.A(
        html.Button("Téléchargez en HTML"), 
        id="download",
        href="data:text/html;base64," + encoded,
        download="horloge_num.html"
    ),

    html.P("Code Python :"),

     html.A(
        html.Button("Téléchargez le code Python de la page :"), 
        id="downloadpy",
        #href="data:text/html;base64," + encoded,
        href="https://edenmaths.gitlab.io/bcpst1/2023_24/INFORMATIQUE/scripts/horloge_clim.py"
    )
    
])

@callback(
    Output('graphe', 'figure'),
    Input('annee', 'value'),
    Input("lieu", "value"))
def update_figure(an, lieu):
    # Récupérations des données mensuelles depuis an_deb
    if lieu == "Externat":
        gp = "47.218015,-1.573113"
        tit = "à l'Externat"
        lo = "land_ocean"
    elif lieu == "Monde":
        gp = "globe"
        tit = "dans le monde"
        lo = "land_ocean"
    elif lieu == "Europe":
        gp = "europe"
        tit = "en Europe"
        lo = "land"
    elif lieu == "Arctique":
        gp = "arctic"
        tit = "au Pôle Nord"
        lo = "land_ocean"
    
    url = f"https://www.ncei.noaa.gov/access/monitoring/climate-at-a-glance/global/time-series/{gp}/{lo}/all/10/{an}-{an_deb}/data.csv"
    # import du csv et transformation en dataframe
    urlretrieve(url, filename="./temp_anomalies.csv")
    df = pandas.read_csv("temp_anomalies.csv", header=4)


    # les dates sont au format YYYYMM
    # On les transforme au format standard YYYY-MM-JJ    
    def formate(d: int) -> date:
        s = str(d)
        return date.fromisoformat(s[0:4] + '-' + s[4:] + '-01') 
    
    df['Date'] = df['Date'].map(formate)
    df.columns = ["Année", "Anomalie"]
    data_an = df
    dernier = df.iloc[-1,0] 

    ## lissage par année
    data_mois = data_an
    data_mois["Année"] = data_mois["Année"].astype("datetime64[ns]")
    dates_mois_np = data_mois["Année"].to_numpy()
    anos_mois_np = data_mois["Anomalie"].to_numpy()
    anos_an = np.array([np.mean(anos_mois_np[12*k:12*(k+1)]) for k in range(len(dates_mois_np)//12 + 1) ])
    dates_an = np.arange(an, int(str(dernier)[:4]) + 1)
    dates_an_dates = np.array([datetime.date(i,12,31) for i in dates_an])
 

    ## Pour les prévisions
    futur_i = [202000 + (k//12)*100 + k%12 + 1 for k in range(1, 240)]
    futur = [datetime.date(d//100, d%100, 1) for d in futur_i]
    futur_i_an = np.arange(futur_i[0]//100, futur_i[-1]//100 + 1)
    futur_an = np.array([datetime.date(i,12,31) for i in futur_i_an])

    # Tracé
    
    titre = f"Évolution des anomalies de températures {tit} depuis {an}<br>Dernière mesure le 1er {dernier.strftime('%B %Y')} "
            
    fig = px.scatter(data_an,
                     x="Année",
                     y="Anomalie",
                     color="Anomalie",
                     color_continuous_scale=px.colors.sequential.Inferno
                    )

    
    interpol = lr.localreg(dates_an, anos_an, degree=2, kernel=lr.rbf.epanechnikov, frac=0.7)
    extrapol = lr.localreg(dates_an, anos_an, x0=futur_i_an, degree=2, kernel=lr.rbf.epanechnikov, frac=0.7)

    fig.add_scatter(x=futur_an, y=extrapol, name="Prévision")
    fig.add_scatter(x=dates_an_dates, y=interpol, name="interpolation")
    

    fig.update_yaxes(showline=False, linewidth=2, linecolor='black', gridcolor='red',zerolinecolor='black')
    fig.update_xaxes(showline=False, linewidth=0.1, linecolor='black', gridcolor='red')
    
    fig.update_layout(title_text=titre, coloraxis_showscale=False, plot_bgcolor="white")
    
    
    fig.write_html(f"horloge_clim_{lieu}_{an}.html")

    plot(fig, include_plotlyjs=False, output_type='div')

    return fig



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')  



