{{ chapitre(7, "Calcul matriciel", "TP ", 0)}}


# Les algos de base

## Produit matriciel

### Matrices particulières

2.  Créer une  fonction `zeros(n:  int,  p: int)  -> list`  qui renvoie  la
   matrice nulle de
   taille $n×p$ sous forme de liste de listes.

1. Créer une fonction `eye(n: int) -> list` qui renvoie la matrice unité de
   taille $n$ sous forme de liste de listes.
   
   On peut imaginer au moins trois méthodes :
   
   ```python
   # méthode 0 : on remplit petit à petit avec append
   def eye0(n: int) -> list:
       M = []
       for i in range(n):
           ligne = ...
           for j in range(n):
               if ...:
                   ....append(...)
               else:
                   ....append(...)
           M.append(...)
       return M

   # méthode 1 : avec zeros

   def eye1(n: int) -> list:
       M = zeros(...)
       for i in range(n):
           for j in range(n):
               if i == j:
                   ...
       return M
	   
   # méthode 2 : avec zeros et une seule boucle !

   def eye1(n: int) -> list:
       M = zeros(...)
       for i in range(n):
           ...
       return M

   # méthode 3 : listes par compréhension
   def eye(n:int) -> list:
       return [[... if ... else ... for ...] for ...]
   ```


### Le classique

Rappelez la définition du produit  matriciel et déterminer une fonction calculant
le produit de deux matrices données sous forme de liste de listes.

```python
def prod(A: list, B: list) -> list:
    ra, ca = ..., ... # nb de lignes et colonnes de A
    rb, cb = ..., ... # idem pour B
    assert ..., "tailles incompatibles pour le produit"
    C = zeros(...)
    for i in range(...):
        for j in range(...):
            for k in range(...):
                C[i][j] = C[i][j] + ...
    return C
```

### Avec le produit scalaire

1. Déterminer une fonction Python qui calcule la transposée d'une matrice.
   ```python
   def trans(A: list) -> list:
	   ra, ca = ...
	   return [[A[j][i] for j in range(...)] for i in range(...)]
   ```


 

2. Déterminer une fonction qui calcule le produit scalaire de deux
    listes, i.e. la somme du produit terme à terme de leurs composantes.

	```python
	def ps(r1: list, r2: list) -> float:
		n1, n2 = len(r1), len(r2)
		assert ..., "listes de tailles incompatibles"
		p = ...
		for i in range(...):
			p = p ...
		return p
	```

3.  En déduire une fonction Python qui calcule le produit de deux matrices 
   données  sous la  forme  de listes  de  listes à  l'aide  des deux  fonctions
   précédentes. 
   
   ```python
   def prod2(A: list, B: list) -> list:
	   ra, ca = len(A), len(A[0])
	   rb, cb = len(B), len(B[0])
	   assert ca == rb, "tailles incompatibles pour le produit"
	   tB = trans(B)
       return [[... for j in range(cb)] for i in range(ra)]
   ```


# Poly

Parcourir le Poly  et répondre à tous les problèmes  posés sur la représentation
des  matrices  en  Python  :  trace, tests  divers  (une  matrice  est-elle
symétrique ? triangulaire ? scalaire ? diagonale ?...)


# Voir des matrices

Dans  cette partie  nous  allons  représenter les  matrices  sous forme  de
`array` de la bibliothèque `numpy`.

## Préliminaire

Construisez  une fonction  `mat_pow(M: np.ndarray,  n: int)  -> np.ndarray`  qui
renvoie la puissance n-ième de la matrice M en utilisant la fonction de `numpy` qui calcule
le produit matriciel. (NB : cette fonction existe déjà dans
`numpy` et s'appelle `np.linalg.matrix_power`)


## Coloration

Explorer le code suivant :

```python
import matplotlib.pyplot as plt
import numpy as np

def nilpo(taille: int) -> np.ndarray: 
    return np.array([[1 if i < j else 0 for j in range(taille)] for i in range(taille)])

def montre(mat: np.ndarray):
	plt.clf()
	plt.imshow(mat)
	plt.show()

# Que se passe-t-il ici ?
def montre_nilpo(taille: int):
	for i in range(1, taille):
		montre(mat_pow(nilpo(taille), i))
```

Testez-le avec d'autres  matrices de l'exercice sur les  matrices nilpotentes ou
sur les calculs de puissances.

<!--
Créer une fonction `affiche_mat` prenant une matrice en argument et qui l'affiche en couleurs
puis une autre fonction  qui prend une matrice et un entier  `n` en arguments et
qui affiche ses `n` premières puissances.
-->

## Art

Afficher des croix, losanges, damiers etc. à l'aide des fonctions précédentes.

## Photoshop

### Exploration

Installer la bibliothèque `pooch` puis :

```python
from scipy import datasets
mystere = datasets.face()
```

Qu'est-ce que `mystere` ? 

### Manipulation

Afficher un raton-laveur:

- la tête en bas
- la patte vers la gauche
- juste sa tête
- en négatif
- en niveaux de gris



<!--
Des éléments de réponses sont [ICI](./image_mat.md)
-->
