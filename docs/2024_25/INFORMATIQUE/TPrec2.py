############
#
# Palindrome
#
#############

def pal(mot: str) -> bool:
    # CAS TERMINAL
    # un mot vide ou d'une lettre est un palindrome
    if len(mot) <= 1:
        return True
    # DESCENTE
    else:
        # si les extrêmes diffèrent ce n'en est pas un
        if mot[0] != mot[-1]:
            return False
        # si les extrêmes sont les mêmes, on regarde l'intérieur
        else:
            return pal(mot[1:-1])
        
assert pal("kayak")

assert not pal("tralapart")

###########################
#
#  Max
#
#############################


# c'est le plus grand entre le 1er et le plus grand du reste de la liste

def maxi(xs: list):
    # on s'assure que la liste n'est pas vide
    assert len(xs) > 0, 'La liste est vide !'
    tête, queue = xs[0], xs[1:]
    # s'il n'y a qu'un élément, c'est le plus grand
    if len(xs) == 1: 
        return tête
    # sinon on compare le max de la queue avec la tête
    else: 
        m_q = maxi(queue) # c'est l'appel récursif sur une liste réduite
        # on renvoie le + grd entre la tête et la plus grd de la queue
        return tête if tête > m_q else m_q
    
assert maxi([1,2,3,2,1]) == 3
assert maxi([1]) == 1
