import numpy as np
import matplotlib.pyplot as plt
import numpy.random as rd

def ber(p):
    return 1 if rd.random() < p else 0

def bin(n, p):
    X = 0
    for k in range(n):
        X = X + ber(p)
    return X

def prob_bin(n, p, N=100000):
    dis = [0 for _ in range(n + 1)]
    for k in range(N):
        dis[bin(n, p)] += 1
    return dis



    
