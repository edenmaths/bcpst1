{{ chapitre(2, "Algorithmes élémentaires à maîtriser", "TP ", 0)}}




# Exercices SANS MACHINE

## Le danger des affectations multiples

!!! {{ exercice()}}

    Que vaut `a` à la fin de chacun de ces quatre séquences d'instructions ?

	1. 
	   ```python
	   >>> a = 1
	   >>> a = a + a
	   >>> a = a + a
	   >>> a = a + a
	   ```

	2. 
	   ```python
	   >>> a = False
	   >>> a = not a
	   >>> a = not a
	   >>> a = not a
	   ```

	3. 
	   ```python
	   >>> a = 2
	   >>> a = a * a
	   >>> a = a * a
	   >>> a = a * a
	   ```

	4. 
	   ```python
	   >>> a = 2
	   >>> a = a**a
	   >>> a = a**a
	   >>> a = a**a
       ```


## Vocabulaire

!!! {{ exercice()}}

	 ```python
	 def fonction(mot: str, lettre: str) -> int:
		compteur = 0
		for caractere in mot:
		   if caractere == lettre:
			   compteur = compteur + 1
		return compteur

	 n = fonction('Trololo', 'o')
	 ```

	 Dans le script ci dessus :

	 1. Quel est le nom et la signature de la fonction ?
	 2. Quels sont les paramètres de la fonction ?
	 3. Quels sont les arguments de ce code ?
	 4. Quelles sont les variables locales à la fonction ?
	 5. Quel est le type de `n`
	 6. Quelle sera la valeur de `n` après l'exécution de ce script ?
	 7. Construisez  des phrases  décrivant la dernière  ligne en  utilisant *appel*,
		*appelant*, *appelé*, *argument*, *passe*,...

## Double


!!! {{ exercice()}}


	 Considérons la fonction

	 ```python
	 def double(s):
		 return s + s
	 ```

	 À quelles valeurs sont liées `s` et `t` à la fin de l'exécution du programme suivant ?

	 ```python
	 s = "Salut"
	 s = double(s)
	 t = "Bye"
	 t = double(double(double(t)))
	 ```
	 


## Boucles


!!! {{ exercice()}}

	 Que vaut `j` à la fin de l'exécution de ces fragments de code:

	 ```python
	 j = 0
	 for ind in range(10):
		 j += 1
	 ```


	 ```python
	 j = 0
	 for ind in range(2, 10):
		 j += 1
	 ```


	 ```python
	 j = 0
	 for ind in range(j, 10):
		 j += 1
	 ```


	 ```python
	 j = 0
	 for ind in range(10):
		 j += j
	 ```


	 ```python
	 j = 0
	 for ind in range(j):
		 j += 1
	 ```

	 Réfléchissez d'abord, frappez ensuite et enfin expliquez.


# Exercices AVEC MACHINE


## Calculs de sommes et de produits

!!! {{ exercice()}}

    === "Énoncé"

         Déterminer une fonction  qui prend en paramètre une  liste d'entiers et
         qui renvoie leur somme :
		 
		 {{ IDE("scripts/tp2_1") }}

	    Par exemple :
	
		```python
		In [2]: somme([1, 2, 3])
		Out[2]: 6
		```
		
		Que  pourrait-on   prévoir  dans  le  cas   où  la  liste  est   vide  ?
		Renseignez-vous sur la commande `assert`.
		
		```python
		In [5]: somme([])
        ---------------------------------------------------------------------------
        AssertionError Traceback (most recent call last)
        <ipython-input-5-2199740c4a2d> in <module>
        ----> 1 somme([])
		...
		AssertionError: Liste vide 
		```

		
		Que pourrait-on prévoir dans le cas  où la liste contient un élément non
		entier ?
		Renseignez-vous sur la commande `type`.
		
		```python
		In [8]: somme([1,2.5])
		---------------------------------------------------------------------------
		AssertionError                            Traceback (most recent call last)
		<ipython-input-8-0a4cf3f40bdf> in <module>
		----> 1 somme([1,2.5])
		...
		AssertionError: Les éléments de la liste doivent être entiers
		```
		
		Déterminez ensuite une fonction qui calcule la **factorielle** d'un entier naturel.
		
	=== "Indications"
	
	

	
## Compteurs

!!! {{ exercice()}}

    === "Énoncé"

	    On dispose d'une chaîne de bases azotées A, C, G, Τ sous la forme d'une
	    *chaîne de caractères*. Par exemple `"GATACCA"`.
		
		Déterminez  une fonction  qui prend  en  paramètre une  telle chaîne  et
		compte combien il y a d'adénine.
		
		{{ IDE("scripts/tp2_2") }}
		
		Par exemple :
		
		```python
		In [14]: adenine('GATACCA')
		Out[14]: 3
		```
		
		NB: *la solution de cet exercice se trouve quelque part sur cette page*.
		
		
		Déterminez  ensuite une  fonction  qui compte  le  nombre de  caractères
		correspondant à la base donnée en
		deuxième paramètre dans la chaîne.
		
		```python
		def compte_base(chaine: str, base: str) -> int :
		```
		
		Par exemple :
		
		```python
		In [16]: compte_base('GATACCA', 'C')
		Out[16]: 2
		```
		
		Déterminez ensuite  un moyen de  renvoyer le  nombre de bases  de chaque
		type,  par exemple  sous  la  forme d'une  liste  de  4 nombres  entiers
		correspondant aux compteurs des bases classées dans l'ordre alphabétique:
		
		```python
		In [17]: compte_bases('GATACCA')
		Out[17]: [3, 2, 1, 1]
		```
		
		
		Combien de fois avez-vous parcouru la chaîne ? Pouvez-vous faire mieux
		en ne la parcourant qu'une seule fois ?
		
		Combien de `if` avez-vous utilisé?
		
		Vous  pourrez   faire  mieux  en   regardant  l'aide  sur   la  fonction
		`string.index` ou en créant vous-même  une fonction qui prend une chaîne
		et  un caractère  en argument  et qui  renvoie l'indice  de la  première
		occurence du caractère dans la chaîne et une erreur sinon (avec 
		`raise ValueError("caractère non présent")` en consultant éventuellement
		l'aide sur `raise`).
		
		```python
		In [7]: indice('a', "ocatarinabellatchitchix")
		Out[7]: 2

        In [8]: indice('z', "ocatarinabellatchitchix")
		---------------------------------------------------------------------------

	    ValueError: caractère non présent
		```
		
		On pourra en particulier réfléchir  à la présence d'un `return` **dans**
		un bloc de boucle `for`.

	=== "Indications"

         
	         
	
	

## Recherche

!!! {{ exercice("Test")}}

    === "Énoncé"

        Déterminez  une fonction  qui  renvoie  vrai si  un  caractère donné  en
        paramètre est présent dans une chaîne donnée également en paramètre  et
        faux sinon. Vous donnerez une version avec une boucle `for` et une autre
        avec `while`.
		
		{{ IDE() }}
		
		Modifiez-la pour  qu'elle renvoie  vrai si le  caractère est  présent au
		moins deux fois puis modifiez-la encore  pour qu'elle renvoie vrai si le
		caractère apparaît exactement deux fois.

	=== "Indications"

         
		 
!!! {{ exercice("Max d'une liste")}}

    === "Énoncé"

        Déterminez une  fonction qui  renvoie le  maximum d'une  liste d'entiers
        donnée en argument et une autre qui renvoie le deuxième maximum.
		
		
		{{ IDE() }}

		```python
		>>> max1([-1, -2, -9, -3, -2, -9, -7])
		-1
		>>> max2([-1, -2, -9, -3, -2, -9, -7])
		-2
		```

		Déterminez ensuite une fonction qui renvoie l'indice de la première occurence du maximum et sa valeur:

		```python
		>>> max3([1, 2, 9, 3, 2, 9, 7])
		(2, 9)
		```

		Déterminez enfin une fonction qui renvoie la liste des indices des occurences du maximum et ce maximum:

		```python
		>>> max4([1, 2, 9, 3, 2, 9, 7])
		([2,5], 9)
		```
	=== "Indications"

        

!!! {{ exercice("Facture")}}

	On  dispose d'une  liste de  produits,  d'une liste  des prix  unitaires de  ces
	produits  et d'une  liste  des quantités  achetées de  ces  produits. Créez  une
	fonction qui calcule le prix total de ces achats.

	Créez un programme interactif qui propose des produits avec leurs prix unitaires
	et demande  quelle quantité on  souhaite acheter et renvoie  le prix à  payer et
	affiche le détail de la facture.

	Créez  un programme  interactif  qui demande  des noms  de  produits, leur  prix
	unitaire, la quantité achetée  et renvoie  le prix à  payer et
	affiche le détail de la facture.
	
	{{ IDE() }}


	Proposez des variantes : remises en cas d'achats nombreux, TVA,...



!!! {{ exercice("Histogramme")}}

    Créez une fonction qui prend un tableau `tab` d'entiers en paramètre et qui renvoie un
    nouveau tableau dont la `i`-ème cellule contient le nombre de fois que l'entier
    `i` apparaît dans le tableau `tab`. 
	
	{{ IDE() }}


!!! {{ exercice("Dessine-moi un carré")}}

    À l'aide d'une boucle dessinez un carré rempli d'étoiles:

	```
	>>> print(carre)

	**********
	**********
	**********
	**********
	**********
	**********
	**********
	**********
	**********
	**********
	```
	
	{{ IDE() }}


	Imaginez des options : taille, symbole, forme,...


!!! {{ exercice("Zip")}}

	Créez un `zipper` qui prend deux listes et renvoie une liste de longueur la plus
	petite des deux et contenant des couples formés de l'élément `i` de la liste 1 et
	l'élément i de la liste 2. Par exemple :

	```python
	Entrez les éléments de la liste 1 séparés par des espaces : 12 32 1 4 67
	Entrez les éléments de la liste 2 séparés par des espaces : a b papa c
	[('12', 'a'), ('32', 'b'), ('1', 'papa'), ('4', 'c')]
	``` 
	
	{{ IDE() }}


!!! {{ exercice("Upside Down")}}

	Demandez un mot et renvoyez-le écrit à l'envers :

	```console
	Entrez votre mot : Bonjour
	À l'endroit : Bonjour
	À l'envers  : ruojnoB

	Entrez votre mot : kayak
	À l'endroit : kayak
	À l'envers : kayak
	C'est un palindrome !
	```    
	
	{{ IDE() }}

!!! {{ exercice("Poker")}}

	 Créez un programme qui  demande un entier `n` et qui affiche  `n` mains de Poker
		 (5 cartes) d'un jeu mélangé. On pourra regarder l'aide de `random.sample`.


	 ```console
	 Combien de mains (moins de 10) voulez-vous afficher ? : 10
	 D♦ 6♦ 5♠ 2♦ 10♥

	 10♦ 8♣ 7♠ V♥ 2♠

	 8♥ D♠ 4♥ V♦ 3♥

	 6♥ V♣ 7♦ 1♠ 9♠

	 1♦ R♥ 8♦ V♠ 3♦

	 R♦ 9♦ 9♥ 8♠ 2♣

	 2♥ 6♠ 6♣ 1♣ D♣

	 R♣ 3♠ D♥ 1♥ R♠

	 5♣ 4♣ 9♣ 7♥ 3♣

	 10♠ 5♦ 10♣ 4♦ 4♠
	 ```
	 
	 {{ IDE() }}

	 Défi : quelle est la main gagnante ?    


!!! {{ exercice("π-statistiques")}}

    

	 Une bibliothèque de  Python nous donne autant de chiffre  de l'écriture décimale
	 de $\pi$ que l'on souhaite.

	 ```python
	 >>> from mpmath import mp

	 >>> mp.dps = 200

	 >>> print(str(mp.pi))
	 3.141592653589793238462643383279502884197169399375105820974944592307816
	 40628620899862803482534211706798214808651328230664709384460955058223172
	 5359408128481117450284102701938521105559644622948954930382

	 >>> len(str(mp.pi)) 
	 200
	 ```

	 On voudrait  obtenir une liste des  fréquences d'apparition des chiffres  0, 1,
	 2,..., 9  dans les 100  000 premières décimales  de $\pi$:
	 
	 ```python
	 Out[1]: [9.999, 10.137, 9.908, 10.026, 9.970, 10.027, 10.028, 10.025, 9.978, 9.902]

	 ```
	 
	 {{ IDE() }}


!!! {{ exercice("Boucles imbriquées (Nested loops)")}}

    

	 Expliquez :

	 ```python
	 In [32]: s = ''                                                          

	 In [33]: for i in range(5,10): 
		 ...:     for j in range(10): 
		 ...:         s += f"{i*j}  " 
		 ...:     s += "\n" 
		 ...:                                                                 

	 In [34]: print(s)                                                        
	 0  5  10  15  20  25  30  35  40  45  
	 0  6  12  18  24  30  36  42  48  54  
	 0  7  14  21  28  35  42  49  56  63  
	 0  8  16  24  32  40  48  56  64  72  
	 0  9  18  27  36  45  54  63  72  81  
	 ```
	 
	 
	 {{ IDE("scripts/tp2_3") }}


!!! {{ exercice("Krampouz-dog")}}



	 Une krampouz-dog contient  ou non une saucisse (pour  les végétariens), contient
	 ou non une  krampouz (pour les Vendéens),  contient ou non de  la moutarde (pour
	 les enfants), contient ou non du ketchup (pour les parents), contient ou non des
	 oignons.

	 On peut voir la conception d'une  krampouz-dog ainsi (vous terminerez l'arbre en
	 faisant figurer le Ketchup et les oignons...):

	 ```mermaid
	 graph TD;
	 C[Choix] --Avec-->S[Saucisse]
	 C[Choix] --Sans-->PS[No Saucisse]

	 S[Saucisse] --Avec-->K1[Krampouz]
	 S[Saucisse] --Sans-->PK1[No Krampouz]
	 PS[No Saucisse] --Avec-->K2[Krampouz]
	 PS[No Saucisse] --Sans-->PK2[No Krampouz]
	 K1[Krampouz] --Avec-->M1[Moutarde]
	 K1[Krampouz] --Sans-->PM1[No Moutarde]
	 PK1[No Krampouz] --Avec-->M2[Moutarde]
	 PK1[No Krampouz] --Sans-->PM2[No Moutarde]
	 K2[Krampouz] --Avec-->M3[Moutarde]
	 K2[Krampouz] --Sans-->PM3[No Moutarde]
	 PK2[No Krampouz] --Avec-->M4[Moutarde]
	 PK2[No Krampouz] --Sans-->PM4[No Moutarde]
	 ```


	 Chaque ingrédient correspond à un certain nombre de calories. Un(e) geek étant très
	 à  cheval sur  la  diététique,  on donne  les  calories  correspondant à  chaque
	 ingrédient:

	 * saucisse : 140
	 * krampouz : 120
	 * moutarde : 20
	 * ketchup : 80
	 * oignon : 40


	 Faites figurer chaque combinaison possible avec les calories qui correspondent.

	 Vous pourrez obtenir par exemple quelque chose comme ça :

	 ```console
		 Sa	Kz	Mo	Ke	Og	Calories
	 1	0	0	0	0	0	0
	 2	0	0	0	0	1	40
	 3	0	0	0	1	0	80
	 4	0	0	0	1	1	120
	 5	0	0	1	0	0	20
	 6	0	0	1	0	1	60
	 7	0	0	1	1	0	100
	 8	0	0	1	1	1	140
	 9	0	1	0	0	0	120
	 10	0	1	0	0	1	160
	 11	0	1	0	1	0	200
	 12	0	1	0	1	1	240
	 13	0	1	1	0	0	140
	 14	0	1	1	0	1	180
	 15	0	1	1	1	0	220
	 16	0	1	1	1	1	260
	 17	1	0	0	0	0	140
	 18	1	0	0	0	1	180
	 19	1	0	0	1	0	220
	 20	1	0	0	1	1	260
	 21	1	0	1	0	0	160
	 22	1	0	1	0	1	200
	 23	1	0	1	1	0	240
	 24	1	0	1	1	1	280
	 25	1	1	0	0	0	260
	 26	1	1	0	0	1	300
	 27	1	1	0	1	0	340
	 28	1	1	0	1	1	380
	 29	1	1	1	0	0	280
	 30	1	1	1	0	1	320
	 31	1	1	1	1	0	360
	 32	1	1	1	1	1	400
	 ```

	 On peut utiliser au moins 5 boucles imbriquées...ou moins.




# Oral G2E



### Exercice 1


Un codon est  constitué de $3$ nucléotides,  et a pour base  azotée soit A,
soit T, soit G, soit C. 

1. Ecrire une fonction de vérification qui  prend en entrée une chaine de 3
	nucléotides, et qui renvoie `True` si c'est bien un codon et `False` sinon. 
   On rappelle quels sont les 3 codons 'stop' : `TAG`, `TAA`, `TGA`. 

2. Écrire  une fonction qui cherche  le codon stop dans  une séquence d'ADN
   (si il y en a plusieurs alors on s'arrête au premier) et renvoie 
   la position de ce codon dans la séquence.  Si il n'y a pas de codon stop
   alors la fonction renvoie $-1$. 
   On prend  en compte le  fait que  la lecture de  la séquence se  fait de
   codons en codons (c'est à dire tous les 3 nucléotides). 

3. Les nucléotides complémentaires sont : (A-T) et (C-G).

      1.  Écrire une  fonction qui  prend en  entrée un  nucléotide et  qui
         renvoie son complémentaire 

      2.  Écrire  une fonction qui prend  deux brins de même  taille et qui
          vérifie s'ils sont complémentaires.   Si oui, elle renvoie `True`,
          sinon elle renvoie `False`. 

4. On rappelle le phénomène de  croisement entre deux brins au niveau d'une
   position $k$ sur le brin. 
   Par exemple  si les brins  : `'ATTGTC'` et  `'CGCTGA'` se croisent  à la
   position 3, les deux nouveaux brins sont: `'ATTTGA'` et `'CGCGTC'`.

	Écrire une fonction  qui prend en paramètre deux brins  et une position
	$k$ et qui fait le croisement.  
	La fonction renvoie les deux nouveaux brins.


### Exercice 2

1. Créer une fonction `anagra` qui prend  un mot `M` en entrée et qui teste
   si `M` est un anagramme (par exemple `anagra('RIGHHGIR')` renvoie 
   `True`).  
   
2. Créer une  fonction `verlan` qui prend  un mot en entrée  et qui inverse
   les  deux  moitiés  du  mot (par  exemple  `verlan('DFRTGGZE')`  renvoie
   `'GGZEDFRT'`).
   
3. Créer  une fonction qui  prend en entrée une  phrase et qui  retourne le
   nombre de mots. 
   


### Exercice 3



On a une liste contenant des nombres (qui peuvent se répéter).

1. Écrire une fonction `comptage(liste, x)` qui compte les occurrences d'un
   nombre.  Par exemple,  `comptage([8, 55,  7, 3,  7, 55,  9, 9,  9], 55)`
   renvoie `2` ; 
   `comptage([8, 55, 7, 3, 7, 55, 9, 9, 9], 4)` renvoie `0`.
   
1.  Écrire une fonction `supprime(liste, x)` qui supprime toutes les
   occurrences du nombre `x` à l'exception de la première.  
   Par exemple, `supprime([8, 55, 7, 3, 7, 55, 9, 9, 9], 55)` renvoie `[8, 55, 7,
   3, 7, 9, 9, 9]` ; 
   `supprime([8, 55, 7, 3, 7, 55, 9, 9, 9], 4)` renvoie `[8, 55, 7, 3, 7, 55, 9, 9, 9]`.

3. Écrire  une fonction qui, à  partir d'une liste donnée,  redonne la même
   liste mais sans répétition.  
   


