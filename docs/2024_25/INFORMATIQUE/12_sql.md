{{ chapitre(14, "SQL : une initiation", "TP", 0)}}



## Généralités et vocabulaire


> Nous avons vu au [chapitre précédent](./10_MIB/) des exemples de représentations de données sous forme 
> d'une ou plusieurs tables.
> Nous allons développer ici ce point de vue en montrant quelques limites de la représentation
> d'informations complexes dans des structures de données plates et en introduisant le
> vocabulaire des schémas relationnels.


### Table, relation





Tout  a commencé  avec cet  homme  qui reçut  le prix Turing en  1981,
notamment     suite     à    son     travail     initié     par    un     [court
article](http://www.seas.upenn.edu/~zives/03f/cis550/codd.pdf) publié en 1970.


![photo de Edgar Codd](./IMG/codd.webp)



Une relation (une table) peut être représentée  par un tableau à deux entrées où
les lignes sont des $n$-uplets et les colonnes des attributs.

Considérons une table `Table_aliens` constituée des attributs `NomAlien`, `Sexe`, `Planète`, `NoCabine`.


Par exemple:



|  NoCabine |  NomAlien | Planete| Sexe|
|-----|---|---|---|
| 1 | Zorglub | Trantor | M|
| 2 | Blorx | Euterpe | M |
| 3 | Urxiz | Aurora | M |
| 4 | Zbleurdite | Trantor | F|
| 5 | Darneurane | Trantor | M|
| 6 | Mulzo | Helicon | M |
| 7 | Zzzzzz | Aurora | F |
| 8 | Arghh | Nexon | M |
| 9 | Joranum | Euterpe | F |


La relation `Table_aliens` est de **degré** 4 et de **cardinalité** 9. Il y a 

|Maths|BDD|
|-----|---|
|Relation|Table|
|$n$-uplet|Ligne|
|Attribut|Colonne|



### Clés primaires/étrangères

> Les bases de données actuelles gèrent de très grosses quantités d'informations (de l'ordre du pétaoctet).
> Dans ce cadre, identifier rapidement des informations est d'une importance capitale.
> La notion de clé primaire va pemettre de formaliser la caractérisation d'enregistrements dans une relation.
> La notion de clé étrangère va permettre de se référer à des données d'une table dans une autre table de 
> manière efficace.


On dit qu'un attribut (ou ensemble d'attributs) K est une **clé primaire** d'une
table (relation) T si la connaissance  de K permet d'identifier une unique ligne
de la table.

Par exemple `Planète`  n'est pas une clé primaire  (pourquoi?) Que pourriez-vous
choisir comme clé primaire?

!!! {{ exercice()}}

     Proposer une clé primaire pour la table suivante:
	 
    | id_eleve | devoir | note |
    |----------|------------|------|
    | 1 | Maths DS1 | 14.5 |
    | 1 | Maths DS2 | 16.5 |
    | 2 | Maths DS1 | 17 |
    | 2 | Projet Info | 18 |
    | 3 | Projet Info | 11 |


	


Si un attribut d'une table est la clé primaire d'une autre table, alors c'est
une **clé étrangère**.



Considèrons par exemple la table `Table_villes` suivante :

| Planete | Ville |
  |---------|:---------|
  | Trantor | Terminus |
  | Euterpe | Arcturus |
  | Helicon | Kalgan |
  | Aurora | Hesperos |
  | Gaia | Siwenna |
  | Bcpst | Terminus|


On peut  prendre comme clé  primaire `Planete` qui  est alors une  clé étrangère
dans la table `Table_aliens`.

### Contraintes d'intégrité


Une contrainte d'intégrité est une règle qui définit la cohérence d'une donnée ou d'un ensemble de données d'une  base de données.


Tout  d'abord,  le type  de  données  que l'on  cherche  à  stocker définit  une
contrainte de domaine. Cela est intégré dans la conception de la BDD.

Ensuite, chaque ligne d'une table doit pouvoir être identifiée par une clé primaire, unique et non nulle. On parle dans ce cas de contrainte de relation.

Enfin, lorsque des tables sont liées, il est indispensable que les trois règles suivantes soient respectées :

1. Une clé étrangère ne peut être une valeur qui n'est pas clé primaire de la table à laquelle on se réfère.

2. Une ligne de la table primaire ne peut être effacée si elle possède des lignes liées.

3. La clé primaire ne peut être changée dans la table primaire si cette ligne possède des lignes liées.

Ces trois règles définissent la notion de contrainte d'intégrité réferentielle d'une base de données.

Cet ensemble  de règles est au  cœur même de la  base de données et  confère le
caractère relationnel au  modèle étudié. Cependant, ces contraintes  ne sont pas
au programme de BCPST. Il faut pourtant être conscient de leur existence car
elles confèrent une partie de l'efficacité et de la sécurité des BDD.








### SGBD 
Un Système de Gestion de Bases de Données (SGBD ou DBMS en
anglais) regroupe des outils logiciels  permettant de gérer, manipuler, stocker,
sécuriser des informations organisées dans une base de données (cf chap 7).

Sous l'impulsion  d'informaticiens comme Charles  Bachman, des SGBD sont  mis au
point avant  même l'apparition du  modèle relationnel de  E.F. Codd en  1970. La
plupart suivent  ce modèle (MySQL,  PostgreSQL, MariaDB,...) mais il  existe des
alternatives (MongoDB, Cassandra,... de type NoSQL *Not Only SQL*).

La gestion d'une BDD est complexe du fait de sa taille, du nombre de ses
utilisateurs, de la nécessité d'assurer la cohérence, la sécurité des
informations.

Un SGBD doit être indépendant des BDD qu'il gère.


### SQL 

Le  langage   le  plus  communément   utilisé  pour  dialoguer   (effectuer  des
**requêtes**) avec le SGBD est le  SQL (*Structured Query Language* : langage de
requête structurée). C'est celui que nous allons utiliser pour créer des tables,
ajouter,  supprimer, mettre  à  jour des  données.  Il  a été  créé  en 1979  et
normalisé en  1986.Il colle le  plus possible à  de l'anglais ordinaire.  Il est
en ce sens *déclaratif*.




## SQL

C'est le cœur des exercices de concours sur le sujet.

### Hors programme : création d'une table.

C'est hors  programme mais  il est  cependant instructif  de savoir  comment est
créée une table.



Par exemple, voici comment créer la table `Table_aliens`:



```sql
CREATE TABLE IF NOT EXISTS Table_aliens (
	NoCabine INT PRIMARY KEY,
	NomAlien	TEXT NOT NULL,
	Planete	TEXT NOT NULL,
	Sexe	TEXT NOT NULL
);
INSERT INTO Table_aliens VALUES (1,'Zorglub','Trantor','M');
INSERT INTO Table_aliens VALUES (2,'Blorx','Euterpe','M');
INSERT INTO Table_aliens VALUES (3,'Urxiz','Aurora','M');
INSERT INTO Table_aliens VALUES (4,'Zbleurdite','Trantor','F');
INSERT INTO Table_aliens VALUES (5,'Darneurane','Trantor','M');
INSERT INTO Table_aliens VALUES (6,'Mulzo','Helicon','M');
INSERT INTO Table_aliens VALUES (7,'Zzzzzz','Aurora','F');
INSERT INTO Table_aliens VALUES (8,'Arghh','Nexon','M');
INSERT INTO Table_aliens VALUES (9,'Joranum','Euterpe','F');
INSERT INTO Table_aliens VALUES (10,'AnnE','Avonlea','F');
```




```mermaid
classDiagram
	Table_aliens <|-- Table_agents
	Table_aliens <|-- Table_cabines
	Table_responsables <|-- Table_cabines
	Table_miams <|-- Table_aliens
	Table_villes <|-- Table_aliens
	Table_gardiens <|-- Table_responsables
	Table_gardiens <|-- Table_agents
	
	Table_aliens : +int NoCabine
	Table_aliens : text NomAlien
	Table_aliens : text Planete
	Table_aliens : text Sexe
	
	class Table_agents{
		+int  NoCabine
		text  NomAgent
	}
	
	class Table_cabines{
		+int  NoCabine
		int  NoAllee
	}
	
	class Table_responsables{
		+int  NoAllee
		text  NomAgent
	}
	
	class Table_miams{
		text Aliment
		+text NomAlien
	}
	
	class Table_villes{
		+text Planete
		text Ville
	}
	
	class Table_gardiens{
		+text NomAgent
		text VilleAgent
	}
	
	
	
	
```






### SELECT


La  commande   `SELECT`  permet,   comme  son  nom   l'indique  en   partie,  de
SÉLECTionner des colonnes d'une ou de plusieurs tables données en
paramètres. La syntaxe est 

```sql
SELECT colonne(s) 
FROM nom table(s)
```

Comme souvent, l'étoile `*` indique que l'on sélectionne tout. 

Par  exemple,   pour  avoir  toutes   les  lignes   et  colonnes  de   la  table
`Table_aliens`, on tapera :

```sql
SELECT * 
FROM Table_aliens
```

Testez :

{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" init="2022_23/INFORMATIQUE/BDD/les_Aliens0.sql"}!}




On peut ne désirer que certaines colonnes :

```sql
SELECT NomAlien, Planete 
FROM Table_aliens
```


{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" init="2022_23/INFORMATIQUE/BDD/les_Aliens.sql" espace="aliens"}!}


### WHERE


`WHERE` permet de  spécifier des critères de sélection. Par  exemple, pour avoir
les noms des Aliens de sexe féminin :


```sql
SELECT NomAlien AS AliensFéminins
FROM Table_aliens
WHERE Sexe = "F"
```

Les critères peuvent s'exprimer en utilisant:

- des opérateurs de comparaison: `<, >, <=, >=, =, <>`{:.sql}
- des opérateurs logiques: `AND, NOT, OR`{:.sql}
-   des   prédicats    définissant   des   ensembles: `IN, BETWEEN ... AND ..., LIKE`{:.sql}
- des  caractères génériques: `_` qui  remplace un caractère
  et `%` qui remplace une chaîne de caractères.

!!! attention
	Le  caractère `%`  est aussi  celui qui  calcule le  reste dans  la division
	euclidienne... 

{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" espace="aliens"}!}

!!! {{ exercice()}}

    Déterminez  la liste  des aliens  venant de  Trantor puis  celle des  aliens
    féminins  venant de  Trantor  puis celle  des aliens  venant  de Trantor  ou
    d'Euterpe (deux solutions) puis celle des aliens dont les numéros de cabines
    sont pairs.
	
	

	
### Les fonctions de groupes (ou d'agrégation)



Les fonctions de groupe permettent d'obtenir des informations sur un ensemble de
$n$-uplets en travaillant  sur les **colonnes** et non pas  sur les lignes comme
	avec `WHERE`{:.sql}.

Les fonctions disponibles sont:

- `AVG`{:.sql} : calcule la moyenne d'une colonne
- `SUM`{:.sql} : calcule la somme d'une colonne
- `MIN`{:.sql} : calcule le minimum d'un colonne
- `MAX`{:.sql} : le max
- `VARIANCE`{:.sql} : ...
- `STDDEV`{:.sql} : l'écart-type
- `COUNT`{:.sql} : nombre de lignes de la colonne
- `EVERY`{:.sql} : vrai si toutes les occurrences de la colonne sont
  vraies
- `ANY | SOME`{:.sql} : vrai si au moins une occurrence de la colonne est
  vraie



!!! {{ exercice()}}

    === "Énoncé"
        Combien  y a-t-il  d'aliens  venant de  Trantor ? 

        Combien  y a-t-il  de planètes d'où sont originaires les aliens dont le numéro de cabine est inférieur à 5 ? 

    === "Indications"

        ```sql
		  SELECT COUNT(*) AS Nb_Aliens_Trantor
		  FROM Table_aliens
		  WHERE Planete = 'Trantor'
	    ```
        Regardez ensuite l'aide sur [`DISTINCT`](https://sql.sh/fonctions/agregation/count)

### GROUP BY

Attention: c'est une clause souvent utilisée aux concours mais dangereuse...

Elle permet de subdiviser une table en groupes mais les valeurs des attributs de
regroupement  peuvent ne  plus être  atomiques  ce qui  limitera les  opérations
réalisables. De plus, l'affichage est incomplet.

Avec 

```sql
SELECT Planete,  NomAlien
FROM Table_aliens
GROUP BY Planete
```

On devrait obtenir

|Planete|NomAlien|
|-------|--------|
|Aurora |Urxiz|
||Zzzzz|
|Avonlea|AnnE|
|Euterpe|Blorx|
||Joranum|
|Helicon|Mulzo|
|Nexon|Arggh|
|Trantor|Zorglub|
||Zbleurdite|
||Darneurane|

Et pourtant :


{!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" espace="aliens"}!}


Mais si on les compte, que se passe-t-il ?

```sql
SELECT Planete, COUNT(*) AS Nb_aliens
FROM Table_aliens
GROUP BY Planete
```





### Tri : ORDER BY



La  clause  `ORDER`  permet  de préciser  dans  quel  ordre  les
$n$-uplets sélectionnés seront donnés. 

Cette clause  ne modifie pas l'ensemble  des données retournées par  la requête,
mais simplement leur 
ordre   de   présentation.   Les   expressions  sont   généralement   des   noms
d’attributs. On peut trier par 
ordre croissant par  défaut, ou par ordre décroissant, en  utilisant dans ce cas
le mot réservé `DESC`. 


Par exemple, ajouter `ORDER BY Nb_aliens`{:.sql} à la requête précédente.


### Les jointures



Une  **jointure** consiste  à  construire une  nouvelle  relation dont  les
$n$-uplets sont la concaténation d'un $n$-uplet d'une première relation et d'un 
$n$-uplet d'une seconde relation vérifiant une condition de jointure.
 
Les deux relations doivent avoir au moins une colonne commune *sémantiquement*. 

En fait,  une jointure  est un  produit cartésien suivi  d'une sélection  sur la
condition de jointure. 



On   peut   utiliser   la    clause  `WHERE`   ou   l'opérateur
`JOIN...ON`.


Par exemple,  on voudrait  obtenir le  responsable de  l'allée pour  chacune des
cabines.

On peut tenter 

```sql
SELECT *
FROM Table_cabines as C, Table_responsables AS R
WHERE C.NoAllee = R.NoAllee
```

ou bien 

```sql
SELECT *
FROM Table_cabines as C JOIN Table_responsables AS R ON C.NoAllee = R.NoAllee
```

On voit la redondance de l'attribut de jointure. On évitera donc l'étoile.

```sql
SELECT NoCabine, NomAgent AS Responsable_allée
FROM Table_cabines as C JOIN Table_responsables AS R ON C.NoAllee = R.NoAllee
```




## Petit Mémo

### Principales commandes SQL et leur syntaxe

```sql
SELECT colonne1 [AS alias1],
       colonne2 [AS alias2]
FROM table1 [AS t11], table2 [AS t2]
WHERE [critères de jointure et sélection : ]
GROUP BY colonnei
ORDER BY colonnej [ASC|DESC]
```

### Jointure :

```sql
SELECT colonne1 [AS c1],
       colonne2 [AS c2]
FROM table1 AS t1 JOIN table2 AS t2 ON t1.clea = t2.clea [AND t1.cleb = t2.cleb]
```


### Listes :

```sql
DEPT IN (44, 35, 22, 56, 29)
DEPT BETWEEN 03 AND 06
```

### Filtres :

```sql
nom LIKE 'AN%' -- qui commence par AN
nom LIKE 'ANN_' -- qui commence par ANN suivi par un seul caractère
nom LIKE '%E' -- qui finit par un E
nom LIKE '%CHOC%' -- qui contient la chaîne de caractères CHOC
```








!!! {{ exercice()}}

    Reprendre les questions du chapitre 13 :
	
	

     {!{ sqlide titre="Tape ta requête SQL ici jeune Padawan :" espace="aliens"}!}

	
	2. Comment obtenir l'ensemble des gardiens?
	
	3. Comment obtenir l'ensemble des villes d'où sont originaires les gardiens?
	
	4.  Comment  obtenir  l'ensemble  des triplets  (numéro  de  cabine,  alien,
	   gardien) pour chaque cabine?
	
	5. Comment obtenir l'ensemble de tous les aliens de l'allée 2?
	
	6. Comment obtenir la liste des aliens  dont les gardiens sont originaires de la
		planète Trantor?

	7. Comment obtenir l'ensemble des gardiens des aliens féminins qui mangent du bortsch?
   
   
    8. Existe-t-il un aliment qui commencent par la même lettre que le nom du
	  gardien qui surveille l'alien qui le mange?
   
    9. Est-ce que tous les aliens qui ont un 'x' dans leur nom ont un gardien qui
	  vient de Terminus?  
  
  
    1. Quels sont les agents venant de Terminus ?

    2. Déterminer une requête donnant le nom des agents classés selon l'ordre alphabétique.

    3. Déterminer les numéros des cabines associées à la ville et le nom de l'agent
    qui les surveille.


    5. Déterminer  le nom et la  ville des agents  ne venant pas de  Terminus et
       dont le numéro de cabine est impair. 



## [SQL Murder Mystery](https://github.com/NUKnightLab/sql-mysteries)


Un meurtre  a été commis.  Comme vous  ne prenez jamais  de notes, vous  ne vous
souvenez plus du nom du meurtrier. Vous  vous souvenez juste que le meurtre a eu
lieu le 15 janvier 2018 dans la ville de SQL City.


![e g robinson](./IMG/egr.webp)

Vous disposez ensuite de la base de données schématisée ici :

![bdd de sql murder mystery](./IMG/sqlmm.png)

À vous de jouer :


{!{ sqlide titre="Entrez ici vos requêtes pour mener l'enquête:" base="2022_23/INFORMATIQUE/BDD/sql-murder-mystery.db" espace="bddmm"}!}


Vous rentrerez ensuite votre solution ici :



{!{ sqlide titre="Votre solution:" sql="2022_23/INFORMATIQUE/BDD/sqlmm-sol.sql" espace="bddmm"}!}



## Centrale 2018


La théorie cinétique des gaz vise à expliquer le comportement macroscopique
d'un gaz à partir des mouvements des particules qui le composent.  
Depuis la naissance de l'informatique, de nombreuses simulations numériques
ont permis de  retrouver les lois de comportement de  différents modèles de
gaz comme celui du gaz parfait. 

Ce sujet s'intéresse à un  gaz parfait monoatomique. Nous considérerons que
le  gaz  étudié   est  constitué  de  $N$   particules  sphériques,  toutes
identiques,  de masse  $m$ et  de rayon  $R$, confinées  dans un  récipient
rigide.
Les  simulations seront  réalisées  dans un  espace à  une,  deux ou  trois
dimensions; le récipient contenant le gaz sera, suivant le cas, un segment
de longueur $L$, un carré de côté $L$ ou un cube d'arête $L$.  

Dans le  modèle du gaz  parfait, les  particules ne subissent  aucune force
(leur poids est négligé) ni aucune autre action à distance.  
Elles  n'interagissent que  par  l'intermédiaire de  chocs,  avec une  autre
particule ou avec la paroi du récipient.
Ces chocs  sont toujours  élastiques, c'est-à-dire que  l'énergie cinétique
totale est conservée. 


On dispose d'une fonction de simulation pour laquelle toutes les particules
ne  sont  plus nécessairement  identiques.  Cette  fonction enregistre  ses
résultats dans une base de données dont la structure est donnée:


```mermaid
classDiagram

	class SIMULATION{
		integer  SI_NUM
		datetime SI_DEB
		float SI_DUR
		integer SI_DIM
		float SI_L
	}
	
	class REBOND{
		integer SI_NUM
		integer RE_NUM
		integer PA_NUM
		float RE_T
		integer RE_DIR
		float RE_VIT
		float RE_P
		}
		
	class PARTICULE{
		integer PA_NUM
		varchar PA_NOM
		float PA_M
		float PA_R
	}
```


Cette base comporte les trois tables suivantes :

-   la table SIMULATION donne les caractéristiques de chaque simulation effectuée. Elle contient les colonnes

	-   SI_NUM numéro d'ordre de la simulation (clef primaire)
	-   SI_DEB date et heure du lancement du programme de simulation
	-   SI_DUR durée (en secondes) de la simulation (il ne s'agit pas du temps d'exécution du programme, mais du temps simulé)
	-   SI_DIM nombre de dimensions de l'espace de simulation
	-   SI_N nombre de particules pour cette simulation
	-   SI_L (en mètres) taille du récipient utilisé pour la simulation

-   la table PARTICULE des types de particules considérées. Elle contient les colonnes
	-   PA_NUM numéro (entier) identifiant le type de particule (clef primaire)
	-   PA_NOM nom de ce type de particule
	-   PA_M masse de la particule (en grammes)
	- PA_R rayon (en mètres) de la particule

-   la table REBOND, de clef primaire (SI_NUM, RE_NUM), liste les chocs des particules avec les parois du récipient. Elle contient les colonnes

	-  SI_NUM numéro d'ordre de la simulation ayant généré ce rebond
	-  RE_NUM numéro d'ordre du rebond au sein de cette simulation
	-  PA_NUM numéro du type de particule concernée par ce rebond
	-  RE_T temps de simulation (en secondes) auquel ce rebond est arrivé
	-  RE_DIR paroi concernée : entier non nul de l'intervalle [-SI_DIM, SI_DIM] donnant la direction de la normale à la paroi. Ainsi $-2$ désigne la paroi située en $y=0$ alors que 1 désigne la paroi située en $x=L$
	-  RE_VIT norme de la vitesse de la particule qui rebondit (en $\textrm{m}\cdot \textrm{s}^{-1}$)
	-  RE_VP valeur absolue de la composante de la vitesse normale à la paroi (en m$\cdot$s )


Questions :

1.  Écrire une requête SQL qui donne le nombre de simulations effectuées pour chaque nombre de dimensions de l'espace de simulation.

2. Écrire une requête SQL qui donne, pour chaque simulation, le nombre de rebonds enregistrés et la vitesse moyenne des particules qui frappent une paroi.

3. Écrire une requête SQL qui, pour une simulation $n$ donnée, calcule, pour chaque paroi, la variation
de quantité de mouvement due aux chocs des particules sur cette paroi tout au long de la simulation. 
On se rappellera que lors du rebond d'une particule sur une paroi la composante de sa vitesse normale à la paroi est inversée, ce qui correspond à une variation de quantité de mouvement de $2m|v_\bot|$ où $m$ désigne la masse de la particule et $v_\bot$ la composante de sa vitesse normale à la paroi.


## Centrale 2016


Ce  problème  s'intéresse  à  différents aspects  relatifs  à  la
sécurité  aérienne et  plus  précisément au  risque de  collision
entre deux appareils. Dans cet  exercice nous abordons l'enregistrement des
plans de vol des différentes compagnies aériennes.



Afin  d'éviter  les  collisions  entre  avions, les  altitudes  de  vol  en
croisière sont normalisées.  Dans la majorité des pays, les avions volent à
une altitude multiple de 1000 pieds (un pied vaut 30,48 cm) au-dessus de la
surface  isobare  à 1013,25  hPa.  L'espace  aérien  est ainsi  découpé  en
tranches horizontales appelées niveaux de  vol et désignées par les lettres
"FL" (*flight level*) suivies de l'altitude en centaines de pieds :
"FL310" désigne  une altitude de croisière  de 31000 pieds au-dessus  de la
surface isobare de référence. 


Eurocontrol  est   l'organisation  européenne  chargée  de   la  navigation
aérienne, elle gère  plusieurs dizaines de milliers de vols  par jour. Toute
compagnie qui souhaite faire traverser le  ciel européen à un de ses avions
doit soumettre à cet organisme un  plan de vol comprenant un certain nombre
d'informations : trajet, heure de départ, niveau de vol souhaité, etc. Muni
de ces informations, Eurocontrol peut prévoir les secteurs aériens qui vont
être  surchargés   et  prendre   des  mesures   en  conséquence   pour  les
désengorger : retard au décollage, modification de la route à suivre, etc. 


Nous modélisons  (de manière très  simplifiée) les  plans de vol  gérés par
Eurocontrol sous  la forme d'une  base de données  comportant deux
tables : 


- la table `vol` qui répertorie les plans de vol déposés par les compagnies
  aériennes ; elle contient les colonnes
  
     - `id_vol` : numéro du vol (chaine de caractères) ;
     - `depart` : code de l'aéroport de départ (chaine de caractères) ;
     - `arrivee` : code de l'aéroport d'arrivée (chaine de caractères) ; 
     - `jour` : jour du vol (de type date, affiché au format `aaaa-mm-jj`) ;
     - `heure` : heure de décollage souhaitée (de type time, affiché au format `hh:mi`) ;
     - `niveau` : niveau de vol souhaité (entier).

|`id_vol`|`depart`     |`arrivee`     |  `jour`          |`heure`       |`niveau`     |
|--------|-----|-----|------------|-------|-----|
| AF1204 | CDG | FCO | 2016-05-02 | 07:35 | 300 |
| AF1205 | FCO | CDG | 2016-05-02 | 10:25 | 300 |
| AF1504 | CDG | FCO | 2016-05-02 | 10:05 | 310 |
| AF1505 | FCO | CDG | 2016-05-02 | 13:00 | 310 |


- la table `aeroport` qui répertorie les aéroports européens ; elle contient les colonnes
  
     - `id_aero` : code de l'aéroport (chaine de caractères) ;
     - `ville` : principale ville desservie (chaine de caractères) ;
     - `pays` : pays dans lequel se situe l'aéroport
  (chaine de caractères).
  
  
| `id_aero` | `ville`   | `pays` |
|-----------|-----------|--------|
| CDG       | Paris     | France |
| ORY       | Paris     | France |
| MRS       | Marseille | France |
| FCO       | Rome      | Italie |


Les types  SQL `date` et  `time` permettent de mémoriser  respectivement un
jour du  calendrier grégorien et  une heure du  jour. Deux valeurs  de type
`date`  ou  de type  `time`  peuvent  être  comparées avec  les  opérateurs
habituels(`=`, `<`, `<=`, etc.).  La comparaison s'effectue suivant l'ordre
chronologique. Ces valeurs peuvent également être comparées à une chaine de
caractères correspondant  à leur représentation externe  (`'aaaa-mm-jj'` ou
`'hh:mi'`). 


{!{ sqlide titre="Tape ta requête SQL ici jeune aiguilleur du ciel:" init="2022_23/INFORMATIQUE/BDD/centrale16.sql"}!}



1.  Écrire une requête SQL qui fournit le nombre de vols qui doivent
   décoller dans la journée du 2 mai 2016 avant midi. 

1. Écrire  une requête  SQL qui  fournit la  liste des  numéros de  vols au
   départ d'un aéroport desservant Paris le 2 mai 2016. 


1. Que fait la requête suivante ?

	```sql
	SELECT id_vol
	FROM vol
		JOIN aeroport AS d ON d.id_aero = depart
		JOIN aeroport AS a ON a.id_aero = arrivee
	WHERE
		d.pays = 'France' AND
		a.pays = 'France' AND
		jour = '2016-05-02'
	```

1.  Certains vols  peuvent engendrer  des conflits  potentiels :  c'est par
   exemple  le cas  lorsque deux  avions suivent  un même  trajet, en  sens
   inverse, le même  jour et à un  même niveau. Écrire une  requête SQL qui
   fournit la liste des couples (Id$_1$ , Id$_2$) des identifiants des vols
   dans cette situation. 



## Agro-Veto 2042


Les  réseaux   sociaux  essaient   d'automatiser  la  traque   de  messages
indésirables. Voici  les propositions de  la startup  *Ouaf* qui a  créé un
réseau social de microblogage et voudrait éviter les messages illicites. 


Une  première  dans  la  lutte  contre  les  contenus  indésirables  consiste  à
répertorier  des messages  dans  une base  de données  modélisée  par le  schéma
ci-dessous (les clés primaires sont précédées d'un astérisque `*` )

```mermaid
classDiagram
Message <-- Contient_IP_Noire
Message <-- Contient_Mot_Noir
Message <-- Contient_Dom_Noir
Message --> Membre
Liste_Noire_IP <-- Contient_IP_Noire
Liste_Noire_Mots <-- Contient_Mot_Noir
Liste_Noire_Domaines <-- Contient_Dom_Noir
Pays --> Langue
Message --> Langue
Membre --> Pays


class Membre{
Int  : id*
Text : email
Text : pseudo
Int  : nb_messages
Int  : nb_amis
Int  : id_pays
}


class Message{
Int       : *id
Timestamp : date
Text      : message
Int       : id_lang
Int       : id_membre
}

class Contient_IP_Noire{
Int : *id_mess
Int : *id_ip
}


class Contient_Dom_Noir{
Int : *id_dom
Int : *id_mess
}


class Liste_Noire_Domaines{
Int  : *id
Text : domaine
Int  : id_pays
}


class Contient_Mot_Noir{
Int : *id_mess
Int : *id_mot
}


class Liste_Noire_Mots{
Int  : *id
Text : mot
Int  : id_lang
}

class Liste_Noire_IP{
Int  : *id
Int  : ip
Int  : id_pays
}


class Pays{
Int : *id
Text : nom
Text : id_langue
}

class Langue{
Int  : *id
Text : nom
}
```

1. Écrire une requête SQL donnant tous les identifiants  des messages écrits en mars 2020.

1. Écrire une requête SQL donnant les identifiants des messages écrits dans
   la langue du pays d'origine de leur auteur. 

1. Écrire une requête SQL donnant  les identifiants et le nombre d'amis des
   membres ayant envoyé  des messages contenant des  mots indésirables avec
   une IP de la liste noire. 

1. Écrire une  requête SQL donnant les pseudos des  membres ayant envoyé au
   moins quarante-deux  messages entraînant  leur classement dans  au moins
   une des trois listes noires. 

