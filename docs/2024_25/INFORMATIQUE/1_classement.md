{{ chapitre(1, "Un aperçu de ce que nous allons maîtriser cette année", "TP ")}}




## Classement des Prépas selon l'origine de leurs bacheliers

Le recrutement des  Prépas est très divers : certains  lycées ne retiennent
que des élèves ayant  eu les félicitations du jury au  Bac, d'autres ont au
maximum quelques détenteurs de la mention bien...

Et  pourtant, deux  ou trois  ans p[lus  tard, les  choses peuvent  parfois
beaucoup changer (heureusement pour nous comme nous allons le constater).



Depuis  l'apparition de  *Parcoursup*, tout  le monde  a accès  à certaines
données sur  les heureux  utilisateurs de ce  merveilleux outil  comme vous
pouvez            le             constater            sur            [cette
page](https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-parcoursup/information/?timezone=Europe%2FBerlin&sort=tri)


On se  retrouve avec un fichier  contenant un tableau de  13644 lignes (une
pour  chaque  établissement  d'enseignement   supérieur)  et  114  colonnes
correspondant  à autant  de critères  : nom  de l'établissement,  nombre de
candidats boursiers, etc.

Nous  allons voir  comment  quelques petites  instructions  en Python  vont
pouvoir  nous permettre  de  classer les  différents  lycées proposant  une
classe BCPST selon une approximation du  résultat moyen des élèves admis au
Baccalauréat.


On va obtenir quelque chose comme ça pour 2022 :

```python
 [[13.78, 27, 'Lycée Albert Schweitzer'],
  [13.98, 34, 'Lycée général privé Externat Chavagnes'],
  [14.08, 44, "Lycée Emmanuel d'Alzon"],
  [14.12, 39, 'Lycée Pierre-Gilles de Gennes - ENCPB'],
  [14.12, 40, 'Lycée Jacques Prevert'],
  [14.17, 31, 'Lycée agricole Le Chesnoy - Les Barres'],
  [14.26, 95, 'Lycée Louis Thuillier'],
  [14.4, 35, 'Lycée Baimbridge'],
  [14.44, 88, 'Lycée Chaptal'],
  [14.46, 71, 'Lycée Camille Guérin'],
  [14.54, 23, 'Lycée Centre Sud'],
  [14.60, 46, 'Lycée Robespierre'],
  [14.68, 44, 'Lycée Claude Fauriel'],
  [14.86, 45, 'Lycée Jean-Baptiste Say'],
  [14.92, 39, 'Lycée Blaise Pascal'],
  [14.92, 45, 'Lycée Louis Barthou'],
  [14.93, 43, 'Lycée Georges De La Tour'],
  [14.96, 41, 'Lycée Assomption Bellevue'],
  [15.06, 42, 'Lycée Leonard Limosin'],
  [15.14, 127, 'Lycée Marcelin Berthelot'],
  [15.20, 137, 'Lycée Thiers'],
  [15.40, 44, 'Lycée Francois Ier'],
  [15.48, 94, 'Lycée Malherbe'],
  [15.60, 47, 'Lycée La Martiniere Monplaisir'],
  [15.62, 93, 'Lycée Montaigne'],
  [15.72, 40, 'Lycée Pothier'],
  [15.82, 89, 'Lycée Lakanal'],
  [15.92, 45, 'Lycée Victor Hugo'],
  [15.93, 44, 'Lycée Albert Chatelet'],
  [16.00, 40, 'Lycée Roland Garros'],
  [16.06, 35, 'LEGTPA Louis Pasteur (Clermont-Marmilhat)'],
  [16.08, 39, 'Lycée agricole Angers le Fresne'],
  [16.14, 89, 'Lycée Henri Poincaré'],
  [16.20, 141, 'Lycée Francois René De Chateaubriand'],
  [16.24, 49, 'Lycée Ozenne'],
  [16.30, 47, 'Lycée Fénelon'],
  [16.55, 82, 'Lycée Faidherbe'],
  [16.58, 94, 'Lycée général Clemenceau'],
  [16.62, 95, 'Lycée Champollion'],
  [16.65, 96, 'Lycée Janson De Sailly'],
  [16.65, 48, 'Lycée Pierre Corneille'],
  [16.66, 48, 'Lycée CLEMENCEAU'],
  [16.67, 86, 'Lycée Hoche'],
  [16.74, 44, 'Lycée Masséna'],
  [16.84, 47, 'Lycée Jean Rostand'],
  [16.86, 96, 'Lycée Pierre De Fermat'],
  [16.94, 33, 'Lycée Carnot'],
  [16.96, 48, 'Lycée Descartes'],
  [17.2, 144, 'Lycée Saint-Louis (6ème)'],
  [17.28, 35, 'L.E.G.T.A. Toulouse Auzeville'],
  [17.36, 133, 'Lycée du Parc'],
  [17.42, 47, 'Lycée Joffre'],
  [17.76, 62, 'Lycée Henri IV'],
  [18.02, 41, 'Lycée Sainte Geneviève']])
```

Par exemple la 2e ligne nous indique que dans notre beau lycée, en 2022, il
y avait 34 admis et en comptant les sans mention à 11, les AB à 13, les B à
15, les TB à 17 et les félicitations du jury à 19 on arrivait à une moyenne
de 13,98 soit presque le score le plus bas des 54 BCPST de France...

Nous calculerons également la moyenne des moyennes, aisni que la médiane et
également la note telle qu'un tiers  des effectifs lui soit inférieure : en
effet, seulement  2/3 des candidats sont  admis dans une école  après leurs
deux années de prépa...

On obtiendra par exemple que 2/3 des élèves ont un score au Bac supérieur à
15,2 ce qui nous laisserait peu d'espoir...et pourtant !


## Récupérations de données sur un serveur distant

Nous allons dans un premier temps voir comment récupérer des données ce qui
est une tâche très commune pour un-e scientifique des années 2020.


Nous               utiliserons                la               bibliothèque
[`pandas`](https://python-simple.com/python-pandas/panda-intro.php) qui est
l'outil de base en analyse de données. Il faut donc l'importer et au besoin
l'installer sur votre environnement Python.

```python
import pandas
```

On observe alors ce que nous fournit le site du ministère :

![psup](./IMG/psup.png)

et             en             particulier            le             fichier
[CSV](https://fr.wikipedia.org/wiki/Comma-separated_values)   qui  est   le
format  classique  pour  représenter  des données  tabulaires  simples  (on
utilise à  présent beaucoup plus  souvent le format JSON  (*djéssonne*) que
nous  découvrirons  après  avoir  étuddié  les  dictionnaires  Python).  On
obtient alors un immense fichier texte de  11 Mo tous sauf lisible. On peut
l'ouvrir dans un logiciel du type libreoffice  ou excel mais on aura u npeu
de mal avec ses 13644 lignes.

Nous  allons donc  plutôt  l'importer, l'enregistrer  localement sur  notre
propre  machine   et  le  tramsformer   dans  un  format   exploitable  par
Python-Pandas.


On copie le  lien vers le fichier CSV  (le clic droit est votre  ami) et on
l'associe à un nom de variable:

```python
url = f"https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B"
```

On choisit d'enregistrer ce fichier sous un nom plus commode, par exemple :

```python
nom_fichier = "./parcoursup_2022.csv"
```


Ensuite,  on  va  utiliser  la  fonction  `urlretrieve`  (*to  retrieve*  =
récupérer) de la bibliothèque `urllib.request`


```python
from urllib.request import urlretrieve

urlretrieve(url, filename = nom_fichier)
```

Bon,  le fichier  CSV est  dans  notre répertoire  de travail  sous un  nom
connu.  On va  maintenant  le  transformer en  un  *DataFrame*  qui est  la
structure de données de base de la bibliothèque `pandas`:

```python
ps = pandas.read_csv(nom_fichier, delimiter=';')
```

Mais nous  avons ici 13644  lignes et autant  d'écoles or seules  les prépas
BCPST nous intéressent :


```python
>>> ps

Session Statut de l’établissement de la filière de formation (public, privé…)  ... etablissement_id_paysage composante_id_paysage
0         2022                   Privé sous contrat d'association                     ...                      NaN                   NaN
1         2022                       Privé enseignement supérieur                     ...                    gFP1V                   NaN
2         2022                       Privé enseignement supérieur                     ...                    8LnxR                   NaN
3         2022                       Privé enseignement supérieur                     ...                    nhwFO                 kL2gS
4         2022                       Privé enseignement supérieur                     ...                    8LnxR                   NaN
...        ...                                                ...                     ...                      ...                   ...
13639     2022                                             Public                     ...                      NaN                   NaN
13640     2022                                             Public                     ...                    g6rwB                   NaN
13641     2022                                             Public                     ...                    g6rwB                   NaN
13642     2022                                             Public                     ...                    g6rwB                   NaN
13643     2022                                             Public                     ...                    g6rwB                   NaN

[13644 rows x 118 columns]
```


## Filtrage et formatage des données, 

Nous allons donc filtrer ce dataframe et ne retenir
que les 54 prépas BCPST :

```python
bcpst = ps[ps['Filière de formation détaillée'].str.contains('BCPST')].set_index('Établissement')
```

La dernière  instruction permet  de choisir  la colonne  donnant le  nom de
l'établissement comme repère des lignes.


```python
>>> bcpst

                                           Session Statut de l’établissement de la filière de formation (public, privé…)  ... etablissement_id_paysage composante_id_paysage
Établissement                                                                                                             ...                                               
Lycée Masséna                                 2022                                             Public                     ...                      NaN                   NaN
Lycée Leonard Limosin                         2022                                             Public                     ...                      NaN                   NaN
Lycée Henri Poincaré                          2022                                             Public                     ...                      NaN                   NaN
Lycée du Parc                                 2022                                             Public                     ...                      NaN                   NaN
Lycée Malherbe                                2022                                             Public                     ...                      NaN                   NaN
Lycée Pierre De Fermat                        2022                                             Public                     ...                      NaN                   NaN
Lycée Montaigne                               2022                                             Public                     ...                    A3mQB                   NaN
Lycée Claude Fauriel                          2022                                             Public                     ...                      NaN                   NaN
Lycée Louis Barthou                           2022                                             Public                     ...                      NaN                   NaN
Lycée Assomption Bellevue                     2022                   Privé sous contrat d'association                     ...                      NaN                   NaN
Lycée Hoche                                   2022                                             Public                     ...                      NaN                   NaN
Lycée Champollion                             2022                                             Public                     ...                      NaN                   NaN
Lycée général privé Externat Chavagnes        2022                   Privé sous contrat d'association                     ...                      NaN                   NaN
Lycée CLEMENCEAU                              2022                                             Public                     ...                    DWgD5                   NaN
Lycée Georges De La Tour                      2022                                             Public                     ...                      NaN                   NaN
Lycée Faidherbe                               2022                                             Public                     ...                      NaN                   NaN
Lycée Blaise Pascal                           2022                                             Public                     ...                      NaN                   NaN
LEGTPA Louis Pasteur (Clermont-Marmilhat)     2022                                             Public                     ...                      NaN                   NaN
Lycée Henri IV                                2022                                             Public                     ...                    s7GjA                   NaN
Lycée Janson De Sailly                        2022                                             Public                     ...                    vnpwW                   NaN
Lycée Jean-Baptiste Say                       2022                                             Public                     ...                      NaN                   NaN
Lycée Francois Ier                            2022                                             Public                     ...                      NaN                   NaN
Lycée Victor Hugo                             2022                                             Public                     ...                      NaN                   NaN
Lycée Ozenne                                  2022                                             Public                     ...                      NaN                   NaN
Lycée Albert Schweitzer                       2022                                             Public                     ...                      NaN                   NaN
Lycée Marcelin Berthelot                      2022                                             Public                     ...                      NaN                   NaN
Lycée Roland Garros                           2022                                             Public                     ...                      NaN                   NaN
Lycée Thiers                                  2022                                             Public                     ...                      NaN                   NaN
Lycée Carnot                                  2022                                             Public                     ...                      NaN                   NaN
L.E.G.T.A. Toulouse Auzeville                 2022                                             Public                     ...                      NaN                   NaN
Lycée Joffre                                  2022                                             Public                     ...                      NaN                   NaN
Lycée général Clemenceau                      2022                                             Public                     ...                      NaN                   NaN
Lycée Pothier                                 2022                                             Public                     ...                    dk854                   NaN
Lycée agricole Le Chesnoy - Les Barres        2022                                             Public                     ...                      NaN                   NaN
Lycée agricole Angers le Fresne               2022                                             Public                     ...                      NaN                   NaN
Lycée Francois René De Chateaubriand          2022                                             Public                     ...                    7W67W                   NaN
Lycée Albert Chatelet                         2022                                             Public                     ...                      NaN                   NaN
Lycée Robespierre                             2022                                             Public                     ...                      NaN                   NaN
Lycée Fénelon                                 2022                                             Public                     ...                      NaN                   NaN
Lycée Pierre-Gilles de Gennes - ENCPB         2022                                             Public                     ...                    i2Smw                   NaN
Lycée Sainte Geneviève                        2022                   Privé sous contrat d'association                     ...                      NaN                   NaN
Lycée Louis Thuillier                         2022                                             Public                     ...                    OUqnD                   NaN
Lycée Camille Guérin                          2022                                             Public                     ...                      NaN                   NaN
Lycée Jacques Prevert                         2022                                             Public                     ...                      NaN                   NaN
Lycée Lakanal                                 2022                                             Public                     ...                    07AuS                   NaN
Lycée Baimbridge                              2022                                             Public                     ...                      NaN                   NaN
Lycée Emmanuel d'Alzon                        2022                   Privé sous contrat d'association                     ...                      NaN                   NaN
Lycée Centre Sud                              2022                                             Public                     ...                    Ail2e                   NaN
Lycée Descartes                               2022                                             Public                     ...                      NaN                   NaN
Lycée Jean Rostand                            2022                                             Public                     ...                    x16cz                   NaN
Lycée La Martiniere Monplaisir                2022                                             Public                     ...                    jegRO                   NaN
Lycée Saint-Louis (6ème)                      2022                                             Public                     ...                    FADNw                   NaN
Lycée Chaptal                                 2022                                             Public                     ...                      NaN                   NaN
Lycée Pierre Corneille                        2022                                             Public                     ...                      NaN                   NaN

[54 rows x 117 columns]
```

Plus que 54 lignes mais encore  117 colonnes. Nous voudrions repèrer celles
qui nous donnent les pourcentages de mentions :

```python
>>> mots_cles = list(bcpst.columns.values)

>>> mots_cles

['Session',
 'Statut de l’établissement de la filière de formation (public, privé…)',
 "Code UAI de l'établissement",
 'Code départemental de l’établissement',
 'Département de l’établissement',
 'Région de l’établissement',
 'Académie de l’établissement',
 'Commune de l’établissement',
 'Filière de formation',
 'Sélectivité',
 'Filière de formation très agrégée',
 'Filière de formation détaillée',
 'Filière de formation.1',
 'Filière de formation détaillée bis',
 'Filière de formation très détaillée',
 'Coordonnées GPS de la formation',
 'Capacité de l’établissement par formation',
 'Effectif total des candidats pour une formation',
 'Dont effectif des candidates pour une formation',
 'Effectif total des candidats en phase principale',
 'Dont effectif des candidats ayant postulé en internat',
 'Effectif des candidats néo bacheliers généraux en phase principale',
 'Dont effectif des candidats boursiers néo bacheliers généraux en phase principale',
 'Effectif des candidats néo bacheliers technologiques en phase principale',
 'Dont effectif des candidats boursiers néo bacheliers technologiques en phase principale',
 'Effectif des candidats néo bacheliers professionnels en phase principale',
 'Dont effectif des candidats boursiers néo bacheliers professionnels en phase principale',
 'Effectif des autres candidats en phase principale',
 'Effectif total des candidats en phase complémentaire',
 'Effectif des candidats néo bacheliers généraux en phase complémentaire',
 'Effectif des candidats néo bacheliers technologique en phase complémentaire',
 'Effectif des candidats néo bacheliers professionnels en phase complémentaire',
 'Effectifs des autres candidats en phase complémentaire',
 'Effectif total des candidats classés par l’établissement en phase principale',
 'Effectif des candidats classés par l’établissement en phase complémentaire',
 'Effectif des candidats classés par l’établissement en internat (CPGE)',
 'Effectif des candidats classés par l’établissement hors internat (CPGE)',
 'Effectif des candidats néo bacheliers généraux classés par l’établissement',
 'Dont effectif des candidats boursiers néo bacheliers généraux classés par l’établissement',
 'Effectif des candidats néo bacheliers technologiques classés par l’établissement',
 'Dont effectif des candidats boursiers néo bacheliers technologiques classés par l’établissement',
 'Effectif des candidats néo bacheliers professionnels classés par l’établissement',
 'Dont effectif des candidats boursiers néo bacheliers professionnels classés par l’établissement',
 'Effectif des autres candidats classés par l’établissement',
 'Effectif total des candidats ayant reçu une proposition d’admission de la part de l’établissement',
 'Effectif total des candidats ayant accepté la proposition de l’établissement (admis)',
 'Dont effectif des candidates admises',
 'Effectif des admis en phase principale',
 'Effectif des admis en phase complémentaire',
 "Dont effectif des admis ayant reçu leur proposition d’admission à l'ouverture de la procédure principale",
 'Dont effectif des admis ayant reçu leur proposition d’admission avant le baccalauréat',
 'Dont effectif des admis ayant reçu leur proposition d’admission avant la fin de la procédure principale',
 'Dont effectif des admis en internat',
 'Dont effectif des admis boursiers néo bacheliers',
 'Effectif des admis néo bacheliers',
 'Effectif des admis néo bacheliers généraux',
 'Effectif des admis néo bacheliers technologiques',
 'Effectif des admis néo bacheliers professionnels',
 'Effectif des autres candidats admis',
 'Dont effectif des admis néo bacheliers sans information sur la mention au bac',
 'Dont effectif des admis néo bacheliers sans mention au bac',
 'Dont effectif des admis néo bacheliers avec mention Assez Bien au bac',
 'Dont effectif des admis néo bacheliers avec mention Bien au bac',
 'Dont effectif des admis néo bacheliers avec mention Très Bien au bac',
 'Dont effectif des admis néo bacheliers avec mention Très Bien avec félicitations au bac',
 'Effectif des admis néo bacheliers généraux ayant eu une mention au bac',
 'Effectif des admis néo bacheliers technologiques ayant eu une mention au bac',
 'Effectif des admis néo bacheliers professionnels ayant eu une mention au bac',
 'Dont effectif des admis issus du même établissement (BTS/CPGE)',
 'Dont effectif des admises issues du même établissement (BTS/CPGE)',
 'Dont effectif des admis issus de la même académie',
 'Dont effectif des admis issus de la même académie (Paris/Créteil/Versailles réunies)',
 "% d’admis ayant reçu leur proposition d’admission à l'ouverture de la procédure principale",
 '% d’admis ayant reçu leur proposition d’admission avant le baccalauréat',
 '% d’admis ayant reçu leur proposition d’admission avant la fin de la procédure principale',
 '% d’admis dont filles',
 '% d’admis néo bacheliers issus de la même académie',
 '% d’admis néo bacheliers issus de la même académie (Paris/Créteil/Versailles réunies)',
 '% d’admis néo bacheliers issus du même établissement (BTS/CPGE)',
 '% d’admis néo bacheliers boursiers',
 '% d’admis néo bacheliers',
 '% d’admis néo bacheliers sans information sur la mention au bac',
 '% d’admis néo bacheliers sans mention au bac',
 '% d’admis néo bacheliers avec mention Assez Bien au bac',
 '% d’admis néo bacheliers avec mention Bien au bac',
 '% d’admis néo bacheliers avec mention Très Bien au bac',
 '% d’admis néo bacheliers avec mention Très Bien avec félicitations au bac',
 '% d’admis néo bacheliers généraux',
 'Dont % d’admis avec mention (BG)',
 '% d’admis néo bacheliers technologiques',
 'Dont % d’admis avec mention (BT)',
 '% d’admis néo bacheliers professionnels',
 'Dont % d’admis avec mention (BP)',
 'Effectif des candidats en terminale générale ayant reçu une proposition d’admission de la part de l’établissement',
 'Dont effectif des candidats boursiers en terminale générale ayant reçu une proposition d’admission de la part de l’établissement',
 'Effectif des candidats en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement',
 'Dont effectif des candidats boursiers en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement',
 'Effectif des candidats en terminale professionnelle ayant reçu une proposition d’admission de la part de l’établissement',
 'Dont effectif des candidats boursiers en terminale générale professionnelle ayant reçu une proposition d’admission de la part de l’établissement',
 'Effectif des autres candidats ayant reçu une proposition d’admission de la part de l’établissement',
 'Regroupement 1 effectué par les formations pour les classements',
 'Rang du dernier appelé du groupe 1',
 'Regroupement 2 effectué par les formations pour les classements',
 'Rang du dernier appelé du groupe 2',
 'Regroupement 3 effectué par les formations pour les classements',
 'Rang du dernier appelé du groupe 3',
 'list_com',
 'tri',
 'cod_aff_form',
 "Concours communs et banque d'épreuves",
 'Lien de la formation sur la plateforme Parcoursup',
 'Taux d’accès',
 'Part des terminales générales qui étaient en position de recevoir une proposition en phase principale',
 'Part des terminales technologiques qui étaient en position de recevoir une proposition en phase principale',
 'Part des terminales professionnelles qui étaient en position de recevoir une proposition en phase principale',
 'etablissement_id_paysage',
 'composante_id_paysage']

```

Les intitulés sont un peu long ! Nous allons plutôt les numéroter :

```
>>> reperage = [(i,mots_cles[i]) for i in range(len(mots_cles))]

>>> reperage

[(0, 'Session'),
 (1, 'Statut de l’établissement de la filière de formation (public, privé…)'),
 (2, "Code UAI de l'établissement"),
 (3, 'Code départemental de l’établissement'),
 (4, 'Département de l’établissement'),
 (5, 'Région de l’établissement'),
 (6, 'Académie de l’établissement'),
 (7, 'Commune de l’établissement'),
 (8, 'Filière de formation'),
 (9, 'Sélectivité'),
 (10, 'Filière de formation très agrégée'),
 (11, 'Filière de formation détaillée'),
 (12, 'Filière de formation.1'),
 (13, 'Filière de formation détaillée bis'),
 (14, 'Filière de formation très détaillée'),
 (15, 'Coordonnées GPS de la formation'),
 (16, 'Capacité de l’établissement par formation'),
 (17, 'Effectif total des candidats pour une formation'),
 (18, 'Dont effectif des candidates pour une formation'),
 (19, 'Effectif total des candidats en phase principale'),
 (20, 'Dont effectif des candidats ayant postulé en internat'),
 (21, 'Effectif des candidats néo bacheliers généraux en phase principale'),
 (22,
  'Dont effectif des candidats boursiers néo bacheliers généraux en phase principale'),
 (23,
  'Effectif des candidats néo bacheliers technologiques en phase principale'),
 (24,
  'Dont effectif des candidats boursiers néo bacheliers technologiques en phase principale'),
 (25,
  'Effectif des candidats néo bacheliers professionnels en phase principale'),
 (26,
  'Dont effectif des candidats boursiers néo bacheliers professionnels en phase principale'),
 (27, 'Effectif des autres candidats en phase principale'),
 (28, 'Effectif total des candidats en phase complémentaire'),
 (29,
  'Effectif des candidats néo bacheliers généraux en phase complémentaire'),
 (30,
  'Effectif des candidats néo bacheliers technologique en phase complémentaire'),
 (31,
  'Effectif des candidats néo bacheliers professionnels en phase complémentaire'),
 (32, 'Effectifs des autres candidats en phase complémentaire'),
 (33,
  'Effectif total des candidats classés par l’établissement en phase principale'),
 (34,
  'Effectif des candidats classés par l’établissement en phase complémentaire'),
 (35, 'Effectif des candidats classés par l’établissement en internat (CPGE)'),
 (36,
  'Effectif des candidats classés par l’établissement hors internat (CPGE)'),
 (37,
  'Effectif des candidats néo bacheliers généraux classés par l’établissement'),
 (38,
  'Dont effectif des candidats boursiers néo bacheliers généraux classés par l’établissement'),
 (39,
  'Effectif des candidats néo bacheliers technologiques classés par l’établissement'),
 (40,
  'Dont effectif des candidats boursiers néo bacheliers technologiques classés par l’établissement'),
 (41,
  'Effectif des candidats néo bacheliers professionnels classés par l’établissement'),
 (42,
  'Dont effectif des candidats boursiers néo bacheliers professionnels classés par l’établissement'),
 (43, 'Effectif des autres candidats classés par l’établissement'),
 (44,
  'Effectif total des candidats ayant reçu une proposition d’admission de la part de l’établissement'),
 (45,
  'Effectif total des candidats ayant accepté la proposition de l’établissement (admis)'),
 (46, 'Dont effectif des candidates admises'),
 (47, 'Effectif des admis en phase principale'),
 (48, 'Effectif des admis en phase complémentaire'),
 (49,
  "Dont effectif des admis ayant reçu leur proposition d’admission à l'ouverture de la procédure principale"),
 (50,
  'Dont effectif des admis ayant reçu leur proposition d’admission avant le baccalauréat'),
 (51,
  'Dont effectif des admis ayant reçu leur proposition d’admission avant la fin de la procédure principale'),
 (52, 'Dont effectif des admis en internat'),
 (53, 'Dont effectif des admis boursiers néo bacheliers'),
 (54, 'Effectif des admis néo bacheliers'),
 (55, 'Effectif des admis néo bacheliers généraux'),
 (56, 'Effectif des admis néo bacheliers technologiques'),
 (57, 'Effectif des admis néo bacheliers professionnels'),
 (58, 'Effectif des autres candidats admis'),
 (59,
  'Dont effectif des admis néo bacheliers sans information sur la mention au bac'),
 (60, 'Dont effectif des admis néo bacheliers sans mention au bac'),
 (61, 'Dont effectif des admis néo bacheliers avec mention Assez Bien au bac'),
 (62, 'Dont effectif des admis néo bacheliers avec mention Bien au bac'),
 (63, 'Dont effectif des admis néo bacheliers avec mention Très Bien au bac'),
 (64,
  'Dont effectif des admis néo bacheliers avec mention Très Bien avec félicitations au bac'),
 (65,
  'Effectif des admis néo bacheliers généraux ayant eu une mention au bac'),
 (66,
  'Effectif des admis néo bacheliers technologiques ayant eu une mention au bac'),
 (67,
  'Effectif des admis néo bacheliers professionnels ayant eu une mention au bac'),
 (68, 'Dont effectif des admis issus du même établissement (BTS/CPGE)'),
 (69, 'Dont effectif des admises issues du même établissement (BTS/CPGE)'),
 (70, 'Dont effectif des admis issus de la même académie'),
 (71,
  'Dont effectif des admis issus de la même académie (Paris/Créteil/Versailles réunies)'),
 (72,
  "% d’admis ayant reçu leur proposition d’admission à l'ouverture de la procédure principale"),
 (73,
  '% d’admis ayant reçu leur proposition d’admission avant le baccalauréat'),
 (74,
  '% d’admis ayant reçu leur proposition d’admission avant la fin de la procédure principale'),
 (75, '% d’admis dont filles'),
 (76, '% d’admis néo bacheliers issus de la même académie'),
 (77,
  '% d’admis néo bacheliers issus de la même académie (Paris/Créteil/Versailles réunies)'),
 (78, '% d’admis néo bacheliers issus du même établissement (BTS/CPGE)'),
 (79, '% d’admis néo bacheliers boursiers'),
 (80, '% d’admis néo bacheliers'),
 (81, '% d’admis néo bacheliers sans information sur la mention au bac'),
 (82, '% d’admis néo bacheliers sans mention au bac'),
 (83, '% d’admis néo bacheliers avec mention Assez Bien au bac'),
 (84, '% d’admis néo bacheliers avec mention Bien au bac'),
 (85, '% d’admis néo bacheliers avec mention Très Bien au bac'),
 (86,
  '% d’admis néo bacheliers avec mention Très Bien avec félicitations au bac'),
 (87, '% d’admis néo bacheliers généraux'),
 (88, 'Dont % d’admis avec mention (BG)'),
 (89, '% d’admis néo bacheliers technologiques'),
 (90, 'Dont % d’admis avec mention (BT)'),
 (91, '% d’admis néo bacheliers professionnels'),
 (92, 'Dont % d’admis avec mention (BP)'),
 (93,
  'Effectif des candidats en terminale générale ayant reçu une proposition d’admission de la part de l’établissement'),
 (94,
  'Dont effectif des candidats boursiers en terminale générale ayant reçu une proposition d’admission de la part de l’établissement'),
 (95,
  'Effectif des candidats en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement'),
 (96,
  'Dont effectif des candidats boursiers en terminale technologique ayant reçu une proposition d’admission de la part de l’établissement'),
 (97,
  'Effectif des candidats en terminale professionnelle ayant reçu une proposition d’admission de la part de l’établissement'),
 (98,
  'Dont effectif des candidats boursiers en terminale générale professionnelle ayant reçu une proposition d’admission de la part de l’établissement'),
 (99,
  'Effectif des autres candidats ayant reçu une proposition d’admission de la part de l’établissement'),
 (100, 'Regroupement 1 effectué par les formations pour les classements'),
 (101, 'Rang du dernier appelé du groupe 1'),
 (102, 'Regroupement 2 effectué par les formations pour les classements'),
 (103, 'Rang du dernier appelé du groupe 2'),
 (104, 'Regroupement 3 effectué par les formations pour les classements'),
 (105, 'Rang du dernier appelé du groupe 3'),
 (106, 'list_com'),
 (107, 'tri'),
 (108, 'cod_aff_form'),
 (109, "Concours communs et banque d'épreuves"),
 (110, 'Lien de la formation sur la plateforme Parcoursup'),
 (111, 'Taux d’accès'),
 (112,
  'Part des terminales générales qui étaient en position de recevoir une proposition en phase principale'),
 (113,
  'Part des terminales technologiques qui étaient en position de recevoir une proposition en phase principale'),
 (114,
  'Part des terminales professionnelles qui étaient en position de recevoir une proposition en phase principale'),
 (115, 'etablissement_id_paysage'),
 (116, 'composante_id_paysage')]
 
 ```
 
Les numéros  82 à  86 nous  intéressent. Nous  allons utiliser  la fonction
`iloc`  (LOCalisation par  Indice) en  prenant toutes  les lignes  (`:`) et
seulement les colonnes de 82 à 86 :


```python
>>> utile = bcpst.iloc[:, 82:87]

>>> utile

                                           % d’admis néo bacheliers sans mention au bac  ...  % d’admis néo bacheliers avec mention Très Bien avec félicitations au bac
Établissement                                                                            ...                                                                           
Lycée Masséna                                                                       0.0  ...                                               12.0                        
Lycée Leonard Limosin                                                               2.0  ...                                                0.0                        
Lycée Henri Poincaré                                                                0.0  ...                                                6.0                        
Lycée du Parc                                                                       0.0  ...                                               25.0                        
Lycée Malherbe                                                                      0.0  ...                                                3.0                        
Lycée Pierre De Fermat                                                              1.0  ...                                               17.0                        
Lycée Montaigne                                                                     0.0  ...                                                2.0                        
Lycée Claude Fauriel                                                                0.0  ...                                                0.0                        
Lycée Louis Barthou                                                                 0.0  ...                                                0.0                        
Lycée Assomption Bellevue                                                           5.0  ...                                                0.0                        
Lycée Hoche                                                                         0.0  ...                                               10.0                        
Lycée Champollion                                                                   0.0  ...                                                9.0                        
Lycée général privé Externat Chavagnes                                              3.0  ...                                                0.0                        
Lycée CLEMENCEAU                                                                    0.0  ...                                                6.0                        
Lycée Georges De La Tour                                                            0.0  ...                                                2.0                        
Lycée Faidherbe                                                                     0.0  ...                                               12.0                        
Lycée Blaise Pascal                                                                 3.0  ...                                                0.0                        
LEGTPA Louis Pasteur (Clermont-Marmilhat)                                           0.0  ...                                               12.0                        
Lycée Henri IV                                                                      0.0  ...                                               40.0                        
Lycée Janson De Sailly                                                              0.0  ...                                                7.0                        
Lycée Jean-Baptiste Say                                                             0.0  ...                                                0.0                        
Lycée Francois Ier                                                                  0.0  ...                                                2.0                        
Lycée Victor Hugo                                                                   0.0  ...                                                4.0                        
Lycée Ozenne                                                                        0.0  ...                                                0.0                        
Lycée Albert Schweitzer                                                            19.0  ...                                                0.0                        
Lycée Marcelin Berthelot                                                            1.0  ...                                                3.0                        
Lycée Roland Garros                                                                 0.0  ...                                                8.0                        
Lycée Thiers                                                                        2.0  ...                                                1.0                        
Lycée Carnot                                                                        0.0  ...                                               16.0                        
L.E.G.T.A. Toulouse Auzeville                                                       0.0  ...                                               26.0                        
Lycée Joffre                                                                        0.0  ...                                               32.0                        
Lycée général Clemenceau                                                            0.0  ...                                               11.0                        
Lycée Pothier                                                                       0.0  ...                                                0.0                        
Lycée agricole Le Chesnoy - Les Barres                                              0.0  ...                                                0.0                        
Lycée agricole Angers le Fresne                                                     0.0  ...                                                8.0                        
Lycée Francois René De Chateaubriand                                                0.0  ...                                                4.0                        
Lycée Albert Chatelet                                                               0.0  ...                                                5.0                        
Lycée Robespierre                                                                   3.0  ...                                                3.0                        
Lycée Fénelon                                                                       0.0  ...                                                5.0                        
Lycée Pierre-Gilles de Gennes - ENCPB                                               6.0  ...                                                0.0                        
Lycée Sainte Geneviève                                                              0.0  ...                                               51.0                        
Lycée Louis Thuillier                                                               7.0  ...                                                1.0                        
Lycée Camille Guérin                                                                3.0  ...                                                0.0                        
Lycée Jacques Prevert                                                               8.0  ...                                                0.0                        
Lycée Lakanal                                                                       0.0  ...                                                6.0                        
Lycée Baimbridge                                                                   10.0  ...                                                3.0                        
Lycée Emmanuel d'Alzon                                                              8.0  ...                                                0.0                        
Lycée Centre Sud                                                                    5.0  ...                                                5.0                        
Lycée Descartes                                                                     0.0  ...                                               15.0                        
Lycée Jean Rostand                                                                  0.0  ...                                               16.0                        
Lycée La Martiniere Monplaisir                                                      2.0  ...                                                0.0                        
Lycée Saint-Louis (6ème)                                                            0.0  ...                                               19.0                        
Lycée Chaptal                                                                      11.0  ...                                                0.0                        
Lycée Pierre Corneille                                                              0.0  ...                                               12.0                        

[54 rows x 5 columns]

```

## Fonctions utiles

 
On avance...Maintenant, on voudrait une petite représentation graphique des
répartitions  des mentions  par lycée.  Comme  on ne  connaît pas  toujours
comment sont répertoriés les lycées, on  peut demander la ligne du lycée en
fonction d'une partie du nom seulement (par exemple "Ext" pour nous).

Ensuite   on    utilisera   la   fonction   `pie`    de   la   bibliothèque
`matplotlib.pyplot`.  On  va alors  fabriquer  une  **fonction** ayant  une
partie du nom du lycée comme argument. On commencera par 


```python
import matplotlib.pyplot as plt

def gateau(lyc):
    for lycee in utile.index:
        if lyc in lycee:
            utile.loc[lycee].plot.pie()
            plt.show()
    raise ValueError("Lycée non trouvée")
```


Alors par exemple :

```python
>>> gateau("Ext")
```

donne :

![mentions](./IMG/eden_mentions.png)



!!! {{ exercice()}}

    === "Énoncé"

        Créer une fonction `mentions(lyc)`  qui affiche les pourcentages de
        mentions d'un lycée dont le bout  du nom est donné en argument. Par
        exemple :
		
		```python
		>>> mentions("Ext")

	    % d’admis néo bacheliers sans mention au bac                                  3.0
		% d’admis néo bacheliers avec mention Assez Bien au bac                      52.0
		% d’admis néo bacheliers avec mention Bien au bac                            38.0
		% d’admis néo bacheliers avec mention Très Bien au bac                        7.0
		% d’admis néo bacheliers avec mention Très Bien avec félicitations au bac     0.0
		Name: Lycée général privé Externat Chavagnes, dtype: float64
		```

	=== "Indications"
	
	    ???




## Un peu de stats


!!! {{ exercice()}}

    === "Énoncé"

	    Expliquez ce que fait cette fonction :
		
		```python
		def classement_ps():
			notes = [11 + 2*pts for pts in range(0, 5)]
			adm = 45
			c = []
			e = 0
			for lycee in utile.index:
				xs = list(utile.loc[lycee])
				s, t = 0, 0
				for i in range(len(xs)):
					t = t + xs[i]
					s = s + xs[i]*notes[i]
				e = e + bcpst.loc[lycee].iloc[adm]
				c.append([s/t, bcpst.loc[lycee].iloc[adm],  lycee])
			return sorted(c)
	    ```

	=== "Indications"

         ???
		 
		 
Il  s'agirait ensuite  de calculer  la moyenne  des scores  des lycées,  la
médiane, la  valeur correspondant au tiers,  et toute autre idée  qui vous
viendrait en tête...

