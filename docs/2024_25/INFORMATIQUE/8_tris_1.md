{{ chapitre(8, "Tris : la découverte", "TP", 0)}}


# Les tris

## Tri à bulle

Complétez  les fonctions  suivantes après  avoir étudié  les animations  et
testé sur un tas de crayons...

![tribulle](./IMG/bubble_sort.gif "Tri à bulle")

```python
def tri_bulle(xs: list) -> list:
    for i_stop in range(..., ..., ...): # de droite à gauche je place les maxis
        for i in range(...): # le numéro du caillou actif qui se compare à son suivant
            if ...: # s'il est plus grand que son suivant
                ... = ... # on les échange
    return xs
```

ou dans l'autre sens :


![tribulle2](./IMG/bubble_sort2.gif "Tri à bulle")


```python
def tri_bulle_2(xs: list) -> list:
    for i_debut in range(..., ...): # de gauche à droite je place les minis
        for i in range(..., ..., ...): # le numéro du caillou actif qui se compare à son suivant
            if ...: # s'il est plus grand que son suivant
                ... = ... # on les échange
    return xs
```


Que se passe-t-il si la suite est déjà triée ou « presque » triée ? Modifiez vos
fonctions pour en tenir compte.


## Tri par insertion

![triinser](./IMG/insertion_sort.gif "Tri par insertion\")

Expliquez ce qui se passe ici :


```python
def insere(elmt, cs: list) -> list:
    cs.append(elmt) # ...
    n = len(cs)
    i = n - 1 # ... 
    while cs[i] < cs[i - 1] and i > 0: # ...
        cs[i - 1], cs[i] = cs[i], cs[i - 1] # ...
        i -= 1 # ...
    return cs

def tri_insere(Pioche: list) -> list:
    Main = [] # ...
    for carte in Pioche:
        Main = insere(carte, Main) # ...
    return Main

def tri_insere_2(jeu: list) -> list:
    for i in range(len(jeu)):
        jeu[:i + 1] = insere(jeu[i], jeu[:i]) # ...
    return jeu
```

Étudiez en particulier la différence entre les deux versions du tri.


## Tri par selection

![triselec](./IMG/selection-sort.gif "Tri par sélection\")

Complétez les fonctions suivantes


```python
def ind_mini(xs: list, debut: int) -> int:
    """
    Renvoie l'indice de 1ère apparition du mini
    d'une tranche de la liste xs qui commence à l'indice debut
    """
    mini = xs[debut] # on commence à lire à l'indice debut
    i_min = debut
    for i in range(...):
        if ... :
            ...
            ...
    return ...
    
def tri_selec(ma_liste: list) -> list:
    xs = ma_liste[:] # copie indépendante
    for debut in range(...) : # les débuts montent
        i_min = ... # l'indice du  mini de la tranche
        ..., ... = ... # on le met au début de la tranche
    return xs
    
# TODO tout faire dans une seule fonction
```


# La visualisation


Expliquez ce qui est fait ici et utilisez ces fonctions

```python
import matplotlib.pyplot as plt
import numpy as np
from np.random import choice
from time import perf_counter


def test(tri):
    """ teste 20 fois le tri donné en argument sur des listes de taille 1000"""
    for _ in range(20):
        t = liste(1000)
        assert tri(t) == sorted(t)
    print(f"{tri} est OK après 20 tests")
    
    
def liste(n):
    """Liste aléatoire de n entiers entre 0 et 10*n"""
    return list(choice(range(10*n), n))

def temps(tri, n):
    p = liste(n)
    debut = perf_counter()
    tri(p)
    return perf_counter() - debut

def liste_temps(tri):
    n = 10
    xs, ts = [], []
    while n < 4000:
        t = temps(tri, n)
        ts.append(t)
        xs.append(n)
        n = int(n*1.2)
    return xs, ts

def trace(liste_tris):
    plt.clf()
    for tri in liste_tris:
        x, y = liste_temps(tri)
        plt.plot(x, y, label = str(tri)[9:-19])
    plt.legend()
    plt.show()
```


# Tri  par comptage

## Principe

Le tri par comptage sert à trier des tableaux de nombres entiers. Le principe
est simple :

- On  dispose d'un tableau  `tab` d'entiers naturels  que l'on veut  trier, avec
  d'éventuelles répétitions, par exemple :
  
  ```python
  tab = [5, 4, 1, 5, 2, 12, 2, 4, 0, 5, 5, 2]
  ```
  
- On  crée un tableau  représentant l'*histogramme*  de cette série  de nombres,
  comme nous l'avons déjà vu dans notre premier TP.
  
  Par exemple, ici, cela donnera :
  
  ```python
  histo = [1, 1, 3, 0, 2, 4, 0, 0, 0, 0, 0, 0, 1]
  ```
  
-  On crée  une liste  vide  `liste-triée` que  l'on  remplit petit  à petit  en
  parcourant `histo`.  On va par  exemple y rajouter un  `0`, puis un  `1`, puis
  trois `2`,  puis aucun  `3` puis  deux `4` puis  quatre `5`  et enfin  un `12`
  c'est-à-dire:
  
  ```python
  liste_triée = [0, 1, 2, 2, 2, 4, 4, 5, 5, 5, 5, 12]
  ```

## Programmation

À  vous de  mettre  en œuvre  une  fonction qui  prendra  un tableau  d'entiers
naturels en paramètre  et renverra ce tableau trié dans  l'ordre croissant selon
ce principe.

```python
def tri_compte(entiers: list) -> list:
    m = max(...)
    compteurs = [0 for _ in range(...)] # Au départ, on met les compteurs à zéro
    # compteurs est l'histogramme de xs
    for entier in entiers:
        compteurs[...] += 1
    entiers_tries = [] # on va ajouter chaque entier autant de fois qu'il est présent
    for i in range(...):
        for occ in range(...):
            entiers_tries.append(...)
    return ...
```

## Complexité

Est-ce que ce tri est plus efficace que les tris précédemment étudiés ?

## Médiane

Comment adapter  ce tri à  la recherche  des quartiles d'une  série statistique
d'entiers ?




# Récupération, lecture et écriture de fichiers de données

## Tailles des joueurs de 2nde ligne de l'ASM depuis 1900.


On dispose  d'un [tableau](http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer) donnant  la taille et la  masse des joueurs  ayant joué
2nde ligne à l'A.S. Montferrand depuis le début du XXe siècle.

![merle](./IMG/merle.webp "merle")

On peut récupérer ces données en utilisant la bibliothèque `pandas` qui est au
programme de BCPST et que nous utiliserons selon nos besoins ces prochains mois.

Il faudra commencer par l'installer !

```shell
pip install pandas
```

Puis nous allons l'utiliser pour extraire des tableaux mis en ligne. On commence par récupérer l'adresse de la page où se situent les données :

```python
import pandas 

url = "http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer"
```

**URL** signifie  *Uniform Resource  Locator* (ou,  en français,  « localisateur
uniforme de ressource »). Une URL est simplement l'adresse d'une ressource en ligne.



On utilise ensuite la fonction `read_html` qui va extraire les tableaux présents
sur une page donnée. Comme ici il y  en a plusieurs, on va extraire le numéro
`1`. Ce sont des rugbymen qui ont fait ce site donc il y a quelques problèmes de
nommage des colonnes...On va se servir de la ligne `2` comme en-tête : c'est celle contenant les
titres des colonnes.

```python
tab = pandas.read_html(url)[1]
tab.columns = tab.iloc[1] # 2e ligne comme en-tête de colonnes
tab = tab[2:] # on prend comme valeurs celles à partir de la 3e ligne
tab = tab.astype({"Taille": int, "Poids": int}) # pour convertir les éléments des colonnes Taille et Poids en entiers
```

et on obtient:

```
             Nom Prénom                                     Poste       Né le  Taille  Poids
0           PUECH Louis                   2ème ligne, 3ème ligne,  04/12/1894     181     83
1          COURTEZ René           Pilier, 2ème ligne, 3ème ligne,  00/00/1898     178     86
2       MARMAYOU Joseph  Pilier, 2ème ligne, 3ème ligne, Arrière,  02/10/1898     170     78
3           REMUS André                   2ème ligne, 3ème ligne,  00/00/1899     188     85
4        DUPOUY Etienne           Pilier, 2ème ligne, 3ème ligne,  05/09/1907     182     87
..                  ...                                       ...         ...     ...    ...
116     ASTIER Corentin                               2ème ligne,  12/01/1995     198    117
117  VAN TONDER Jacobus                   2ème ligne, 3ème ligne,  03/03/1998     197    118
118       LANEN Thibaud                   2ème ligne, 3ème ligne,  01/04/1999     196    102
119    ANNANDALE Edward                               2ème ligne,  19/01/2001     200    116
120     AMATOSERO Miles                               2ème ligne,  15/06/2002     201    125

[121 rows x 5 columns]
```

On va par exemple extraire la colonne des tailles :

```python
tailles = tab["Taille"]
```

et en  faire une liste  puisque nous avons construit  une fonction qui  trie les
listes avec le tri par comptage.

```python
ts = [taille for taille in tailles]
```

`ts` est une liste contenant 121  entiers naturels correspondant aux tailles des
121 joueurs ayant été 2nd ligne au cours de l'histoire de l'ASM.


##### Pouvez-vous trier les joueurs dans l'ordre croissant de leurs tailles ?

Il faudrait par exemple obtenir :

```python
In [86]: tri_joueurs(t_n)
Out[86]: 
[('MARMAYOU Joseph', 170, '1898'),
 ('BONNET Jean-Claude', 176, '1937'),
 ('COURTIAL Roger', 177, '1922'),
 ('COURTEZ René', 178, '1898'),
 ('VIRLOGEUX Roger', 178, '1921'),
 ('ALOUACHE Fouad', 178, '1983'),
 ('NATRIASHVILI Irakli', 178, '1984'),
 ('ARMILHON Chistian', 179, '1932'),
 ('CLANET Jean', 179, '1933'),
 ('JULIEN Jean-Baptiste', 180, '1908'),
 ('FRADET Raoul', 180, '1909'),
 ('CORPORON Elie', 180, '1911'),
 ('FARON Lucien', 180, '1922'),
 ('LENIAUD Paul', 180, '1931'),
 ('ROUGERIE Jacques', 180, '1945'),
 ('DETREZ Pierre-Edouard', 180, '1956'),
 ('CARRIAS Lilian', 180, '1969'),
 ('PUECH Louis', 181, '1894'),
 ('LAURANT Jacques', 181, '1932'),
 ('MANIERE Yves', 181, '1946'),
 ('DUPOUY Etienne', 182, '1907'),
 ('LOMBARTEIX René', 182, '1913'),
 ('PUJOL Marcel', 182, '1915'),
 ('BOURLET Jean-Claude', 182, '1936'),
 ('PUJOL Guy', 182, '1938'),
 ('GARRET René', 182, '1941'),
 ('BRAVO Joseph', 182, '1954'),
 ('MONNET Michel', 183, '1909'),
 ('CHARTON Pierre', 183, '1913'),
 ('HERICE Daniel', 183, '1921'),
 ('BUISSON Maurice', 183, '1922'),
 ('BARCARO Claude', 184, '1946'),
 ('VIARS Sébastien', 184, '1971'),
 ('COGNET Lucien', 185, '1909'),
 ('SAURET Joseph', 185, '1932'),
 ('COUDEYRE Jean-Louis', 185, '1952'),
 ('MENIEU Emmanuel', 185, '1969'),
 ('GOURDON Kevin', 185, '1990'),
 ('SZYMEZACK Marius', 186, '1931'),
 ('LASSERRE Yvan', 186, '1945'),
 ('CRISTINA Jacques', 186, '1948'),
 ('BRANDELY Jacques', 186, '1950'),
 ('DUCHENE Christophe', 186, '1969'),
 ('CAPDEVIELLE Pierre', 186, '1974'),
 ('CHEVALLIER Bernard', 187, '1925'),
 ('CHABAUD Frédéric', 187, '1961'),
 ('MARION Hervé', 187, '1963'),
 ('REMUS André', 188, '1899'),
 ('CHASSAING Bernard', 188, '1940'),
 ('CARRIER Jean-Pierre', 189, '1957'),
 ('BOISSON Robert', 190, '1941'),
 ('COMBEUIL Bernard', 190, '1944'),
 ('CAZALBOU Bernard', 190, '1949'),
 ('BRUGIROUX Jacques', 190, '1955'),
 ('VALLEIX Jean', 191, '1921'),
 ('PELLOUX Jean-Paul', 191, '1958'),
 ('JUILLET Christophe', 191, '1969'),
 ('SARRAUTE Christophe', 191, '1969'),
 ('BOIZE Stéphane', 192, '1934'),
 ('BOURDILLON Henri', 192, '1940'),
 ('VERDIER Jean-Claude', 192, '1947'),
 ('RODRIGUEZ Laurent', 192, '1960'),
 ('LECOMTE Eric', 192, '1968'),
 ('HAWRO Serge', 192, '1969'),
 ('DONGIEU Christophe', 192, '1972'),
 ('CAMISULI Lionel', 192, '1980'),
 ('PICARD Thierry', 193, '1956'),
 ('DENIS Luc', 193, '1960'),
 ('RICOU Serge', 193, '1963'),
 ('BIDAUD Richard', 193, '1969'),
 ('WEBER Fred', 194, '1952'),
 ('GABY Pierre', 194, '1963'),
 ('GOUJON Loann', 194, '1989'),
 ('OCCHINI Patrick', 195, '1958'),
 ('TITTELBACH Christophe', 195, '1971'),
 ('MACHACEK Jan', 195, '1972'),
 ('BARRIER David', 195, '1975'),
 ('WHITE Jason', 195, '1978'),
 ('HEZARD Baptiste', 195, '1989'),
 ('VAITANAKI Lyonel', 196, '1970'),
 ('JAQUES Troy', 196, '1972'),
 ('CUDMORE Jamie', 196, '1978'),
 ('YATO Peceli', 196, '1993'),
 ('LANEN Thibaud', 196, '1999'),
 ('SENTENAC Frédéric', 197, '1967'),
 ('GREC Fabrice', 197, '1967'),
 ('VERSAILLES Jean-Philippe', 197, '1969'),
 ('PIERRE Julien', 197, '1981'),
 ('DECHARTRES Benjamin', 197, '1987'),
 ('GRANOUILLET Maxime', 197, '1992'),
 ('VAN TONDER Jacobus', 197, '1998'),
 ('GASPAROTTO Guy', 198, '1948'),
 ('MERLE Olivier', 198, '1965'),
 ('BOOME Selborne', 198, '1975'),
 ('PEARCE Ed', 198, '1975'),
 ('MARTIN Philippe', 198, '1981'),
 ('VIGOUROUX Pierre', 198, '1983'),
 ('SAMSON Christophe', 198, '1984'),
 ('JACQUET Loïc', 198, '1985'),
 ('VAN DER MERWE Phillip', 198, '1985'),
 ('ITURRIA Arthur', 198, '1994'),
 ('ASTIER Corentin', 198, '1995'),
 ('FERGUSON Scott', 199, '1958'),
 ('GABY Dominique', 199, '1959'),
 ('LOUW Hottie', 199, '1976'),
 ('THION Jérôme', 199, '1977'),
 ('PRIVAT Thibaut', 199, '1979'),
 ('CHAMELOT Raphaël', 200, '1969'),
 ('JEDRASIAK Paul', 200, '1993'),
 ('ANNANDALE Edward', 200, '2001'),
 ('HINES Nathan', 201, '1976'),
 ('MERRICK George', 201, '1992'),
 ('LAVANINI Tomàs', 201, '1993'),
 ('AMATOSERO Miles', 201, '2002'),
 ('BROUZET Olivier', 202, '1972'),
 ('PREGERMAIN Frédéric', 203, '1966'),
 ('TIMANI Sitaleki', 203, '1986'),
 ('EVERSON Joël', 203, '1990'),
 ('VAHAAMAHINA Sébastien', 203, '1991'),
 ('MICHAUD Ludovic', 204, '1982'),
 ('GUNTERT Laurent', 208, '1975')]
```

##### Médianes et autres quartiles 

Il ne reste plus qu'à trouver la taille médiane et les autres quartiles à l'aide
du tri par comptage  puis de faire la même chose avec  les poids puis d'explorer
les autres postes.


##### Corrélation taille/poids

Tracer le  nuage de points  des tailles  et poids de  ces joueurs, la  droite de
régression et donner le coefficient de corrélation.

![reglin](./IMG/reg.png "Poids vs Tailles")

```python
a = 1.2550975911810356, b = -136.33318797858038, r = 0.7883381585309878
```



#### Sélection de lignes



On peut sélectionner des lignes selon des critères.

- Avec un "masque":

```python
masque = tab["Taille"] >= 200
tab[masque]
```

ou une fonction:

```python
tab[lambda df: df["Taille"] >= 200]
```

Par exemple, on veut tous les joueurs nés en 1969 :

```python
tab[lambda df: df['Né le'].str.contains('1969')]
```

ou qui avait Louis comme prénom:


```python
tab[lambda df: df['Nom Prénom'].str.contains('Louis')]
```




## Tailles humaines

Qu'est-ce qu'un fichier `csv` ?

Que faire de [ces données](average-height-by-year-of-birth.csv) ?


On pourra par exemple utiliser `read_csv`.:

```python
import pandas

df = pandas.read_csv('votre fichier csv')
```

On prendra soin de  renommer les colonnes de manière plus  pratique et on pourra
faire des sélections :

```python
import numpy as np
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
import pandas

url = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/average-height-by-year-of-birth.csv"

urlretrieve(url, filename="./tailles_terriens.csv")
terre = pandas.read_csv("tailles_terriens.csv")

terre = terre.rename(columns = {'Mean male height (cm)': "Hommes", "Mean female height (cm)": "Femmes"})

France = terre.loc[lambda df: df["Code"] == 'FRA']
France.plot(x = 'Year', y = ['Hommes', 'Femmes'])
plt.show()
```

Que fait la fonction `taux` ci-dessous ?

```python
def pays(code):
    return  terre.loc[lambda df: df["Code"] == code]

def taux(code, sexe):
    p = pays(code)
    t0 = p.iloc[0][sexe]
    t1 = p.iloc[-1][sexe]
    return (t1 - t0) / t0
```



## Génôme du SARS COVID 2

### Récupération du génôme


Vous pouvez retrouver le génôme du *Severe acute respiratory syndrome coronavirus
2          isolate          Wuhan-Hu-1*          en          suivant          [ce
lien](https://www.ncbi.nlm.nih.gov/projects/sviewer/?id=NC_045512&tracks=%5Bkey:sequence_track,name:Sequence,display_name:Sequence,id:STD649220238,annots:Sequence,ShowLabel:false,ColorGaps:false,shown:true,order:1%5D%5Bkey:gene_model_track,name:Genes,display_name:Genes,id:STD3194982005,annots:Unnamed,Options:ShowAllButGenes,CDSProductFeats:true,NtRuler:true,AaRuler:true,HighlightMode:2,ShowLabel:true,shown:true,order:9%5D&v=1:29903&c=null&select=null&slim=0).


Nous n'allons travailler que la suite des G,  A, C et T au format `txt` à l'aide
de Python.

Elle commence ainsi:

```
CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGT
```

Nous allons commencer par récupérer ce fichier qui est en ligne :

```python
from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
with open("./genome.txt", "r") as gen:
	covid = gen.read().rstrip()
```


La méthode  `read`  va  permettre  de   transformer  ce  texte  en  chaîne  de
caractères.  La  méthode  `rstrip`  permet  d'effacer  les  caractères  spéciaux
indésirables : passage à la ligne, tabulation, espaces à la fin, etc.

```
>>> covid

'CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGTCCAAGACATGTGATCTGCACCTCTGAAGACATGCTTAACCCTA...'
```

Nous voudrions classer les bases azotées selon leur fréquence à l'aide du tri
par comptage. Le problème est que cette fois les données ne sont pas des entiers
mais une des lettres `A`, `C`, `G` ou `T`.



### Découverte des dictionnaires Python


Nous  avions jusqu'à  maintenant  manipulé des  données à  l'aide  de listes  qui
étaient indexées par des  entiers : on y accède aux éléments  par leur rang dans
la liste.

Il  existe une  autre structure  de donnée  qui permet  de choisir  le type  des
indices qui peuvent notamment être des chaînes de caractères. Par exemple :

```python
In [1]: chaines = {'A': 5, 'C': 2, 'G': 24, 'T': 12}

In [2]: chaines['A']
Out[2]: 5

In [3]: bases = [cles for cles in chaines]

In [4]: bases
Out[4]: ['A', 'C', 'G', 'T']

In [5]: freqs = [chaines[cle] for cle in chaines]

In [6]: freqs
Out[6]: [5, 2, 24, 12]

In [7]: chaines['A'] += 10

In [8]: chaines
Out[8]: {'A': 15, 'C': 2, 'G': 24, 'T': 12}

In [9]: {base: 0 for base in 'ACGT'}
Out[9]: {'A': 0, 'C': 0, 'G': 0, 'T': 0}
```

Un dictionnaire  est donc une  structure mutable  associant des *valeurs*  à des
*clés*.  On  utilise des  accolades  comme  pour  les  ensembles car  c'est  une
structure par essence non ordonnée.


Déterminer une fonction qui va compter les bases azotées
dans un génôme et renvoyer un dictionnaire `base: fréquence`:

```python
In [1]: compte_bases(gs)
Out[1]: {'A': 2953, 'C': 1817, 'G': 1985, 'T': 3245}
```

Comment trier les bases selon leur fréquence ?

Déterminer un  dictionnaire des  fréquences des n-uplets  de bases.  Par exemple
pour les doublets:

```python
compte_nuplets(gs, 2)

{'CT': 694,
 'TG': 935,
 'GC': 396,
 'TT': 1020,
 'GT': 712,
 'TC': 432,
 'CA': 714,
 'AT': 819,
 'CG': 124,
 'AA': 866,
 'AG': 564,
 'GG': 362,
 'GA': 515,
 'AC': 703,
 'TA': 858,
 'CC': 285}
```


Créez  alors une  fonction  qui  tri les  clés  des  dictionnaires selon  leurs
valeurs:

```python
>>> tri_dic(compte_nuplets(gs, 1))

[('C', 1817), ('G', 1985), ('A', 2953), ('T', 3245)]
```




On peut aussi ne s'occuper que des répétitions d'une même base:

```python
compte_repets(gs, 2)

{'TT': 1020, 'AA': 866, 'GG': 362, 'CC': 285}
```

```python
compte_repets(gs, 4)

{'TTTT': 95, 'AAAA': 76, 'GGGG': 7}
```




## Jouons avec les noms et les prénoms


* Voici un fichier de 10 Mo contenant 
[800000 noms de familles](https://www.data.gouv.fr/fr/datasets/r/9ae80de2-a41e-4282-b9f8-61e6850ef449) recensés en France
en 2018.
* En voici un autre contenant 
[200000 prénoms](https://www.data.gouv.fr/fr/datasets/r/4b13bbf2-4185-4143-92d3-8ed5d990b0fa) 
recensés en France en 2018.
* Voici un fichier zippé contenant le [recensement les prénoms donnés en France depuis
  1900](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip)
  classés par départements.  Il contient 3,5 millions de lignes  !! **Il ne faut
  surtout pas l'ouvrir dans un tableur !!**
  
**On  voudrait utiliser  ces fichiers  pour faire  certaines recherches,  certains
classements.  Pour cela,  il faut  bien  avoir maîtrisé  tout  ce qui  a été  vu
auparavant dans le cours !**

**Vous présenterez votre  enquête dans un notebook interactif au  format `ipynb` que
vous pourrez  ensuite mettre  sur votre  compte gitlab et  vous me  fournirez le
lien.**


### Informations sur le dernier fichier 



Le fichier départemental comporte 3 624 994 enregistrements et cinq variables décrites ci-après.

Ce fichier est trié selon les variables `SEXE`, `PREUSUEL`, `ANNAIS`, `DPT`.

* Nom : `SEXE` - intitulé : `sexe` - Type : `caractère` - Longueur : 1 - Modalité : `1` pour masculin, `2` pour féminin
* Nom : `PREUSUEL` - intitulé : `premier prénom` - Type : `caractère` - Longueur : `25`
* Nom : `ANNAIS` - intitulé : `année de naissance` - Type : `caractère` - Longueur : `4` - Modalité : 1900 à 2018, `XXXX`
* Nom : `DPT` - intitulé : `département de naissance` - Type : `caractère` - Longueur : `3` - Modalité : liste des départements, `XX`
* Nom : `NOMBRE` - intitulé : `fréquence` - Type : `numérique` - Longueur : `8`




La refonte  du processus électoral a  entrainé un nombre de  corrections dans la
base des  prénoms plus important  que les  années précédentes. En  effet, chaque
électeur  est  maintenant  inscrit  au  répertoire  électoral  unique  avec  son
état-civil officiel (celui du Répertoire national d'identification des personnes
physiques / RNIPP), des anomalies ont donc été corigées.

Le fichier des prénoms est établi à  partir des seuls bulletins de naissance des
personnes   nées  en   France  (métropole   et  départements   d’outre-mer  hors
Mayotte).  En  conséquence,  l’exhaustivité  n’est pas  garantie  sur  toute  la
période, notamment pour les années antérieures à 1946. Les utilisateurs pourront
donc  constater des  écarts  avec le  nombre annuel  des  naissances évalué  par
l'Insee. Ces écarts, importants en début de période, vont en s’amenuisant. Après
1946, ils sont peu significatifs. 

Les  informations contenues  dans le  fichier des  prénoms sont  basées sur  les
bulletins d'état-civil  transmis à  l’Insee par les  officiers d’état  civil des
communes. Ces  bulletins sont  eux-mêmes établis à  partir des  déclarations des
parents.  L'Insee ne  peut  garantir  que le  fichier  des  prénoms soit  exempt
d'omissions ou d'erreurs.

### Pour comprendre

Pour chaque prénom, il est indiqué pour chaque année de naissance (de 1900 à 2018) et chaque sexe, le nombre de personnes inscrites à l'état civil sous ce prénom. Pour le fichier « DPT2018 », la précision est apportée pour chaque département.

#### Les personnes prises en compte

Le champ couvre l'ensemble des personnes nées en France hors Mayotte et enregistrées à l'état civil sur les bulletins de naissance. Les personnes nées à l'étranger sont exclues.

#### Le champ des prénoms retenus

Dans les fichiers de l’état civil, en l'occurrence les bulletins de naissance, les différents prénoms sont séparés par une espace (ou blanc). Ainsi deux prénoms séparés par un tiret constituent un seul prénom composé (exemple : Anne-Laure). Le premier prénom simple ou composé figure en début de liste, et c'est celui qui sera retenu après le traitement de la protection de l'anonymat.

#### Conditions portant sur les prénoms retenus

1. Sur la période allant de 1900 à 1945, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
2. Sur la période allant de 1946 à 2018, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
3. Pour une année de naissance donnée, le prénom a été attribué au moins 3 fois à des personnes de sexe féminin ou de sexe masculin

Les effectifs des prénoms ne remplissant pas les conditions 1 et 2 sont regroupés (pour chaque sexe et chaque année de naissance) dans un enregistrement dont le champ prénom (PREUSUEL) prend la valeur «_PRENOMS_RARES_». Les effectifs des prénoms remplissant la condition 2 mais pas la condition 3 sont regroupés (pour chaque sexe et chaque prénom) dans un enregistrement dont le champ année de naissance (ANNAIS) prend la valeur «XXXX».

#### Précision pour le département de naissance

Sur toute la période, le département de naissance (variable DPT) est celui existant au moment de la naissance. Ainsi une personne née à Issy-les-Moulineaux en 1949 sera codée en 75 (Seine), et une personne née en 1971 à Issy-les-Moulineaux sera codée en 92 (Hauts-de-Seine).

En effet, de par la loi n° 64-707 du 10/07/1964, il y a eu création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise), à partir des anciens départements 75 (Seine) et 78 (Seine-et-Oise), avec une date d'effet dans le fichier au 1er janvier 1968.

*Deux regroupements de codes ont été faits pour la variable DPT* :

* le code DPT=20 est utilisé pour les naissances ayant eu lieu dans les deux départements 2A (Corse-du-Sud) et 2B (Haute-Corse) ;
* le code DPT=97 est utilisé pour les naissances ayant eu lieu en Guadeloupe, en MArtinique, en Guyane et à La Réunion.

#### Décrets et lois relatifs aux départements métropolitains

* Décret du 18/01/1955 : la Seine-Inférieure devient Seine-Maritime (76)

* Décret du 09/03/1957 : la Loire-Inférieure devient Loire-Atlantique (44)

* Loi n° 64-707 du 10/07/1964 : création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise)

* Décret du 10/10/1969 : les Basses-Pyrénées deviennent Pyrénées-Atlantiques (64)

* Décret du 13/04/1970 : les Basses-Alpes deviennent Alpes-de-Haute-Provence (04)

* Loi n° 75-356 du 15/05/1975 : création des départements 2A (Corse-du-Sud) et 2B (Haute-Corse)

* Décret du 27/02/1990 : les Côtes-du-Nord deviennent Côtes-d'Armor (22)

#### Éléments de contexte sur le choix des prénoms

Il n'existe pas  de liste de prénoms autorisés. Les  prénoms connus étrangers ou
certains diminutifs peuvent ainsi être  choisis. En conséquence, on peut trouver
par exemple des prénoms à une seule  lettre dans le fichier des prénoms. Ce sont
effectivement  les  parents qui  choisissent  librement  le  ou les  prénoms  de
l’enfant  lors de  la  déclaration  de naissance.  En  particulier depuis  1993,
l’officier  d’état  civil  ne  peut  plus  refuser  le  prénom  choisi  par  les
parents. Il peut toutefois avertir le procureur de la République s’il estime que
le prénom nuit à l’intérêt de l’enfant (exemple : prénom ridicule ou grossier) ;
ou  que le  prénom méconnaît  le droit  d’un tiers  à voir  protéger son  nom de
famille (exemple  : un  parent ne peut  choisir comme prénom  le nom  de famille
d’une autre  personne dont l’usage  constituerait une usurpation).  Le procureur
peut  ensuite saisir  le  juge  aux affaires  familiales  qui  peut demander  la
suppression du prénom sur  les registres de l’état civil. Dans  ce cas le prénom
serait également supprimé du fichier des prénoms. 

L'alphabet  utilisé pour  l’écriture  des prénoms  doit être  celui  qui sert  à
l'écriture du  français. Les caractères  alphabétiques qui ne sont  pas utilisés
dans la langue française ne sont donc pas  autorisés (par exemple le « ñ » ou le
« ș »  ). Ainsi, par exemple,  le prénom d'origine roumaine  "Rareș" apparaît en
tant que "RARES" dans le fichier des prénoms. 


