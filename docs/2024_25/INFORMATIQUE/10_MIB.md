{{ chapitre(14, "Dictionnaires, données en tables", "TP", 0)}}


# Les MIB


## Travail à faire

Vous  rendrez un  compte-rendu, papier  ou  électronique, à  l'issue de  ce
TP. Qui sait ? Je pourrais alors  décider de faire passer votre niveau ECTS
de F à E pour ceux qui voudraient nous quitter...j'rigole...quoique...

Attention  ! Il  ne  s'agit pas  d'un compte-rendu  de  CM2. Les  questions
proposées  ne  sont  que  des   pistes.  Il  faut  commenter,  documenter,
expliquer, analyser, apporter des éléments  nouveaux et non pas bêtement se
contenter de donner  un bout de code. Votre numérotation  ne suivra pas les
numéros  des  questions.  Il  faut   un  vrai  compte-rendu  d'étudiant  ou
d'étudiante  qui  doit constituer  la  base  d'une documentation  pour  vos
collègues scientifiques.

## Contexte

Vous avez trouvé un stage de vetéralien pour prendre soin des extra-terrestres
capturés et retenus dans le centre de détention inter-galactique des MIB.

![aliens](./IMG/aliens.png)



## Gestion de fichiers 

 En arrivant, un extraterrestre est confiné dans
une cellule et surveillé par un gardien. 
Pour vous aider à vous organiser, les MIB ont mis à votre disposition cinq tables de données résumant
les informations essentielles sur les aliens et les gardiens:


* La table `BaseAliens` qui donne  des renseignements sur les extraterrestres et
  ressemble un peu à ça :


|  NoCabine |  NomAlien | Planete| Sexe|
|-----|---|---|---|
| 1 | Zorglub | Trantor | M|
| 2 | Blorx | Euterpe | M |
| 3 | Urxiz | Aurora | M |
| 4 | Zbleurdite | Trantor | F|
| 5 | Darneurane | Trantor | M|
| 6 | Mulzo | Helicon | M |
| 7 | Zzzzzz | Aurora | F |
| 8 | Arghh | Nexon | M |
| 9 | Joranum | Euterpe | F |

* La  table `BaseAgents` qui donne  le nom et  la ville d'origine des  agents et
  qui ressemble un peu à ça :


|  NomAgent| VilleAgent|
|---|---|
|Branno |  Terminus|
|    Darell |  Terminus|
|  Demerzel |  Arcturus|
|    Seldon|   Terminus|
|   Dornick|     Kalgan|
|    Hardin|   Terminus|
|   Trevize |  Hesperos|
|   Pelorat |    Kalgan|
|     Riose|   Terminus|
|    Palver|    Siwenna|
|    Amaryl   |Arcturus|


*  La  table `BaseGardiens`  qui  affecte  à chaque  cabine  un  gardien et  qui
  ressemble un peu à ça :


|  NoCabine|  NomAgent|
|---|---|
|1|    Branno|
|         2|    Darell|
|         3|  Demerzel|
|         4|    Seldon|
|         5|   Dornick|
|         6|    Hardin|
|         7|   Trevize|
|         8|   Pelorat|
|         9|     Riose|

*  La  table  `BaseMiams` qui  donne  l'aliment  servi  à  chaque alien  et  qui
  ressemble un peu à ça :


|       Aliment|    NomAlien|
|:---|---|
|      Bortsch  |   Zorglub|
|       Bortsch |      Blorx|
|       Zoumise |      Urxiz|
|       Bortsch | Zbleurdite|
|  Schwanstucke | Darneurane|
|       Kashpir |      Mulzo|
|       Kashpir |     Zzzzzz|
|       Zoumise |      Arghh|
|       Bortsch |    Joranum|



* La table  `BaseCabines` qui précise dans quelle allée  se trouve chaque cabine
  et qui ressemble un peu à ça :


|  NoAllee|  NoCabine|
|---|---|
|        1 |        1|
|        1 |        2|
|        2 |        3|
|        1 |        4|
|        2 |        5|
|        2 |        6|
|        2 |        7|
|        1 |        8|
|        1 |        9|


* La  table `BaseResponsables` qui  précise l'agent responsable de  chaque allée
  et qui ressemble un peu à ça :


|  NoAllee| NomAgent|
|---|---|
|        1|   Seldon|
|        2|  Pelorat|





Elles  se  trouvent  sous  forme  de fichiers  `csv`  regroupés  dans  l'archive
[`MIB_Files.zip](./MIB_Files.zip).




- Décompressez ce fichier et récupérez ces tables sous forme de dictionnaires
   qui ressembleront à ça :
   
   
   ```python
   BaseAliens = 
   [{'NomAlien': 'Zorglub', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 1},
    {'NomAlien': 'Blorx', 'Sexe': 'M', 'Planete': 'Euterpe', 'NoCabine': 2},
    {'NomAlien': 'Urxiz', 'Sexe': 'M', 'Planete': 'Aurora', 'NoCabine': 3},
    {'NomAlien': 'Zbleurdite', 'Sexe': 'F', 'Planete': 'Trantor', 'NoCabine': 4},
    {'NomAlien': 'Darneurane', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 5},
    {'NomAlien': 'Mulzo', 'Sexe': 'M', 'Planete': 'Helicon', 'NoCabine': 6},
    {'NomAlien': 'Zzzzzz', 'Sexe': 'F', 'Planete': 'Aurora', 'NoCabine': 7},
    {'NomAlien': 'Arghh', 'Sexe': 'M', 'Planete': 'Nexon', 'NoCabine': 8},
    {'NomAlien': 'Joranum', 'Sexe': 'F', 'Planete': 'Euterpe', 'NoCabine': 9}]
   ```


- On a oublié une table, `BaseVilles` qui précise la planète sur laquelle se trouve chaque ville
  
| Planete | Ville |
  |---------|:---------|
  | Trantor | Terminus |
  | Euterpe | Arcturus |
  | Helicon | Kalgan |
  | Euterpe | Hesperos |
  | Gaia | Siwenna |

  Créez le fichier `csv` puis le dictionnaire correspondant.
  

## Extraction de données


- Comment obtenir la liste des gardiens?


- Comment obtenir la liste des villes d'où sont originaires les gardiens?

- Comment obtenir la liste des triplets (numéro de cabine, alien, gardien) pour chaque cabine?

- Comment obtenir la liste de tous les aliens de l'allée 2?

- Comment obtenir la liste des aliens  dont les gardiens sont originaires de la
   planète Trantor?

- Comment obtenir la liste des gardiens des aliens féminins qui mangent du bortsch?
   
   
## Tests

- Existe-t-il un aliment dont le nom commence par la même lettre que le nom du
   gardien qui surveille l'alien qui le mange?
   
- Est-ce que  tous les aliens qui ont  un 'x' dans leur nom ont  un gardien qui
   vient de Terminus?  
  
  
## Créations de tables 

- Renvoyer `BaseTerminus`, une table extraite de `BaseAgents` ne contenant que les lignes dont l'attribut `VilleAgent` vaut `Terminus`.

- Renvoyer `BaseAlpha`, une table dérivée de `BaseAgents` triée selon l'ordre alphabétique du nom des agents.

- Renvoyer `BaseComplete`, la table contenant le numéro de cabine, la ville et le nom de l'agent.

- Renvoyer `BaseVille`, la table contenant le numéro de cabine et la ville des agents.

- Renvoyer `BaseImpair`, la table contenant le nom et la ville des agents ne venant pas de Terminus et dont le numéro de cabine est impair.


### À toutes fins utiles

Commentez  et  expliquez les  différentes  étapes  de  ce code.  Vous  vous
renseignerez  bien sûr  sur  les différentes  bibliothèques employées  afin
d'expliquer à  un ou une collègue  de travail comment les  utiliser pour un
recherche similaire. Vous pourrez partir des questions posées mais bien sûr
ne pas vous en contenter.



```py linenums="1"
from zipfile import ZipFile
from urllib.request import urlretrieve
import pandas as pd

url_mib = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/MIB_Files.zip"
mib_zip_fic = urlretrieve(url_mib)[0]

with ZipFile(mib_zip_fic, 'r') as mib_zip:
    fics = mib_zip.namelist() 
    mib_zip.extractall()
```

- Pourquoi le `[0]` de la ligne 6 ?
- À quoi est lié `fics` ?
- Que se passe-t-il après la ligne 10 ?


```python linenums="1"
les_bases = {}
for fic in fics[1:]:
    nom = fic[10:-4]
    df = pd.read_csv(fic)
    les_lignes = df.values.tolist()
    les_cols = df.columns.tolist()
    n = len(les_lignes)
    les_bases[nom] = []
    for k in range(n):
        les_bases[nom].append({les_cols[i]:les_lignes[k][i] for i in range(len(les_cols))})
```

- À quoi est lié `fic` ?
- Et `nom` ? Pourquoi ce `[10:-4]` ?
- Expliquez ce qui se passe à chaque ligne.


```python linenums="1"
BaseAgents = les_bases['BaseAgents']
les_agents = [ligne['Nom'] for ligne in BaseAgents]
les_villes = [ligne['Ville'] for ligne in BaseAgents]
BaseGardiens = les_bases['BaseGardiens']
BaseAliens = les_bases['BaseAliens']

triplets = [
    (ligneg['NoCabine'], lignea['Nom'],ligneg['Nom'])
    for ligneg in BaseGardiens
    for lignea in BaseAliens
    if lignea['NoCabine'] == ligneg['NoCabine']
]
```

- À quoi sont liés les références créées ci-dessus ? 
- `BaseAliens` contient une erreur : laquelle ? Comment la corriger ?

!!! warning "Attention !"

	Comme dans la vraie vie, certaines tables sont « corrompues » : des données sont mal rentrées ou ont
	changé par rapport aux tableaux affichés plus haut. 
