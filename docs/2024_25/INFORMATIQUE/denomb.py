from sympy import N, pi

def stats_pi(n: int) -> dict[str, int]:
    res = {str(i): 0 for i in range(10)}
    pipi = str(N(pi, n))
    pipi = '3' + pipi[2:]
    for k in pipi:
        res[k] += 1
    return res



def cherche(motif: str, n: int) -> int:
    pipi = str(N(pi, n))
    pipi = '3' + pipi[2:]
    taille = len(motif)
    for i in range(n - taille):
        if pipi[i:i+taille] == motif:
            return i
    raise ValueError(f"{motif} n'a pas été trouvé dans les {n} premières décimales de π") 


### covid


from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2021_22/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
with open("./genome.txt", "r") as gen:
	covid = gen.read().rstrip()



def parse(chaine: str, n: int) -> dict[str, list[int]]:
    dic = {}
    lc = len(chaine)
    for i in range(lc - n):
        motif = chaine[i:i+n]
        if motif not in dic:
            dic[motif] = [i]
        else:
            dic[motif].append(i)
    return dic


##### Denomb

def combi1(n,K):
    k = min(K, n - K)
    if k == 0:
        return 1
    elif k == 1:
        # print("eval de C(n,1)")
        return n
    # print("Appel de C(" + str((n,k)) + ")")
    return combi1(n - 1, k - 1) + combi1(n - 1, k)

#In [49]: %timeit combi1(100,90)
# trop long : calcul interrompu au bout de 5 min


def facto(n):
    p = 1
    for k in range(1,n + 1):
        p *= k
    return p
    

def combi2(n,K):
    k = min(K, n - K)
    return facto(n)//(facto(k)*facto(n-k))

#In [63]: %timeit combi2(100,90)
#100000 loops, best of 3: 15.8 µs per loop


# mémoïsation
def enrobe(f):
    global cache
    cache = {}
    def combi_cache(n,k):
        if (n,k) in cache:
            #print("rappel via le cache de C(" + str((n,k)) + ")")
            return cache[(n,k)]
        else:
            cache[(n,k)] = f(n,k)
            #print("calcul via la définition de C(" + str((n,k)) + ")")
            return cache[(n,k)]
    return combi_cache


#from functools import lru_cache

#@lru_cache(maxsize = None)
@enrobe
def combi3(n,K):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    elif k == 0:
        return 1
    elif k == 1:
        return n
    else:
        return combi3(n - 1, k - 1) + combi3(n - 1, k)

#In [64]: %timeit combi3(100,90)
#1000000 loops, best of 3: 359 ns per loop



# avec un tableau 

def combi4(n,K):
    k = min(K,  n - K)
    c = [[0 for j in range(k + 1)] for i in range(n + 1)]
    c[0][0] = 1
    for i in range(1,n + 1):
        c[i][0] = 1
        for j in range(1,min(k + 1, i + 1)):
            c[i][j] = c[i-1][j-1] + c[i-1][j]
    return c[n][k]

#In [65]: %timeit combi4(100,90)
#1000 loops, best of 3: 406 µs per loop

# on utilise c(n,k) = (n - k + 1)/k * c(n, k - 1)

def combi5(n,K):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    num, den = 1,1
    for i in range(1, k + 1):
        num *= n - i + 1
        den *= i
    return num // den

#In [66]: %timeit combi5(100,90)
#100000 loops, best of 3: 2.25 µs per loop

def combi6(n,K):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    elif k == 0:
        return 1
    elif k == 1:
        return n
    else:
        return ((n - k + 1)*combi6(n, k - 1)) // k

#In [67]: %timeit combi6(100,90)
#100000 loops, best of 3: 5.88 µs per loop

from functools import reduce

def combi7(n,K):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    return reduce(lambda acc, i: (acc * (n - i + 1)) // i, range(1,k + 1), 1)

#In [68]: %timeit combi7(100,90)
#100000 loops, best of 3: 3.31 µs per loop

# curiosité

def triangle(n, symbole='*'):
    for i in range(n + 1):
        ligne = ''
        for j in range(i + 1):
            if combi4(i,j) % 2 == 0:
                ligne += ' '
            else:
                ligne += symbole
        print(ligne)

import turtle as t
def sier(n,length):
    if n == 0:
        return
    for i in range(3):
        sier(n - 1, length / 2)
        t.fd(length)
        t.rt(120)

def combi_cache(n, K, cache = {}):
    k = min(K, n - K)
    if (n < k) or (n < 0) or (k < 0):
        raise ValueError("Arguments incohérents")
    elif k == 0:
        return  1
    elif k == 1:
        return n
    if (n, k) in cache:
        #print("rappel via le cache de C(" + str((n, k)) + ")")
        return cache[(n, k)]
    else:
        cache[(n, k)] = combi_cache(n-1, k-1) + combi_cache(n-1, k) 
        #print("calcul via la définition de C(" + str((n,k)) + ")")
        return cache[(n,k)]


import timeit

def temps(commande: str) -> float:
    res =  timeit.timeit(stmt=commande, globals=globals(), number=10)
    print(f'{commande} a été exécuté en {res*10**6:.2f} μs')

temps('combi_cache(100,90)')
temps('combi5(100,90)')
