## Exercice 12.1


```python
import numpy as np
```


```python
def douzun(n: int, p: int) -> list:
    M = []
    for i in range(1, n + 1):
        ligne = []
        for j in range(1, p + 1):
            ligne.append(i + 3**j)
        M.append(ligne)
    return M
```


```python
def nulle(n: int, p: int) -> list:
    Z = [[0 for _ in range(p)] for _ in range(n)]
    #for i in range(n):
        #ligne = []
        #for j in range(p):
            #ligne.append(0)
        #Z.append(ligne)
    return Z
```


```python
def douzun_2(n: int, p: int) -> list:
    M = nulle(n, p)
    for i in range(n):
        for j in range(p):
            M[i][j] = (i+1) + 3**(j+1)
    return M
```


```python
def douzun_3(n: int, p: int) -> list:
    return [[i + 3**j for j in range(1, p+1)] for i in range(1, n+1)]
```


```python
def douzun_np(n: int, p: int) -> np.ndarray:
    M = np.zeros((n, p))
    for i in range(n):
        for j in range(p):
            M[i, j] = (i+1) + 3**(j+1)
    return M
```


```python
def douzun_np_2(n: int, p: int) -> list:
    M =[[i + 3**j for j in range(1, p+1)] for i in range(1, n+1)]
    return np.array(M)
```


```python
douzun_2(2, 3)
```




    [[4, 10, 28], [5, 11, 29]]



## Exercice 12.2

### Version liste de listes


```python
def est_carree(M: list) -> bool:
    return len(M) == len(M[0])
```


```python
def trace(M: list) -> float:
    assert est_carree(M), "La matrice doit être carrée"
    nb_lignes = len(M)
    s = 0
    for i in range(nb_lignes):
        s += M[i][i]
    return s
```


```python
def trace_2(M: list) -> float:
    assert est_carree(M), "Matrice non carrée"
    return sum([M[i][i] for i in range(len(M))])
```


```python
trace_2([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
```




    15



### Version NumPy


```python
def est_carree(M: np.ndarray) -> bool:
    return M.shape[0] == M.shape[1]
```


```python
def trace(M: np.ndarray) -> float:
    assert est_carree(M), "La matrice doit être carrée"
    nb_lignes = M.shape[0]
    s = 0
    for i in range(nb_lignes):
        s += M[i, i]
    return s
```


```python
trace(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
```




    15



## Ex 12.4


```python
def oeil(n: int) -> list:
    return [[0 if i != j else 1 for j in range(n)] for i in range(n)]
```


```python
oeil(5)
```




    [[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 1]]



## Ex 12.8


```python
def est_triang_sup(M: np.ndarray) -> bool:
    nb_lignes, nb_cols = M.shape
    assert nb_lignes == nb_cols, "La matrice n'est pas carrée"
    for j in range(nb_cols - 1):
        for i in range(j + 1, nb_lignes):
            if M[i, j] != 0:
                return False
    return True
```


```python
def est_triang_sup_2(M: np.ndarray) -> bool:
    nb_lignes, nb_cols = M.shape
    assert nb_lignes == nb_cols, "La matrice n'est pas carrée"
    for i in range(1, nb_lignes):
        for j in range(i - 1):
            if M[i, j] != 0:
                return False
    return True
```


```python
# test 
m1 = np.array([[1,2,3],[0,4,5],[0,0,7]])
m1
est_triang_sup_2(m1)
```




    True




```python
m2 = np.array([[1,2,3],[0,4,5],[1,0,7]])
m2
est_triang_sup_2(m2)
```




    False



## Ex 12.11


```python
def est_sym(M: np.ndarray) -> bool:
    nb_lignes, nb_cols = M.shape
    for j in range(nb_cols - 1):
        for i in range(j + 1, nb_lignes):
            if M[i, j] != M[j, i]:
                return False
    return True
```


```python
est_sym(np.eye(5))
```




    True




```python
est_sym(np.array([[1,2,3],[2,10,4],[4,5,6]]))
```




    False



## Ex 12.14


```python
def transpose(M: list) -> list:
    nb_lignes, nb_cols = len(M), len(M[0])
    T = nulle(nb_cols, nb_lignes)
    for i in range(nb_cols):
        for j in range(nb_lignes):
            T[i][j] = M[j][i]
    return T
    
```


```python
transpose([[1,2,3],[4,5,6]])
```




    [[1, 4], [2, 5], [3, 6]]



## Ex 12.19


```python
def produit(A: list, B: list) -> list:
    nb_la, nb_ca = len(A), len(A[0])
    nb_lb, nb_cb = len(B), len(B[0])
    assert nb_ca == nb_lb, "tailles non compatibles pour le produit"
    C = nulle(nb_la, nb_cb)
    for i in range(nb_la):
        for j in range(nb_cb):
            for k in range(nb_ca):
                C[i][j] += A[i][k] * B[k][j] 
    return C
    
```


```python
produit([[1,2,3],[4,5,6]], [[1,2,3,4],[5,6,7,8],[9,8,7,6]])
```




    [[38, 38, 38, 38], [83, 86, 89, 92]]




```python
def idn(n: int) -> list:
    """
    Renvoie la matrice identité de taille n
    """
    return [[1 if i == j else 0 for j in range(n)] for i in range(n)]

def pow(M: list, n:int) -> list:
    """
    Renvoie M à la puissance n
    Version récursive
    """
    assert n >= 0, "La puissance doit être positive"
    
```


```python
# appliquer à l'exercice 21
```


```python
# exos 47,48,49,55 -> fin
```
