{{ chapitre(3, "Récursion", "TP ", 0)}}



*Un grand merci à Charles Poulmaire et Vincent-Xavier Jumel*



# Recursion is everywhere


!!! {{ exercice()}}

    Comment comprenez-vous cette phrase :

	« Je crois que le chat que le chien que le vétérinaire a soigné a
	chassé a mangé »

	


	

![<img src="./IMG/inception.jpg" width="400"/>](./IMG/inception.jpg)

![<img src="./IMG/umaguma.png" width="600"/>](./IMG/umaguma.jpg)

![vache](./IMG/vache.gif)

![k2000](./IMG/k2000.jpeg)

![k2000](./IMG/k2000.gif)

# Récursion / Récursivité


![fixing_problems](./IMG/fixing_problems.webp)


Il ne faut pas confondre récurrence et récursion. 


- **Récurrence** 
    - En anatomie, une artère est récurrente si son trajet la ramène vers son
      point d'origine. On parle également de la récurrence des nerfs laryngés inférieurs.
  
    - En termes de biologie générale, on parle de récurrence pour désigner le retour d’une espèce ou d’un organe à un type antérieur. 
  
    - En musique la récurrence d'une ligne mélodique est sa forme inversée dans
      le sens du temps, c'est-à-dire lue de la dernière note à la première
      comme dans le *Krebskanon* de J.S. Bach: 
  
      ![krebs](./IMG/krebs.jpeg)
 

- **Récursion** 
    - Le mot « récursion » est apparu en francais suite à un emprunt dans les années 1920 au mot anglais *recursion* qui tirait lui-même
      son origine du latin *recursum* lui-même supin du verbe
      *recurrere* qui signifie « courir de nouveau » puis « revenir (en
      arrière...) ».

    - Noam Chomski a mis en avant dans les années 1950 que les langages
      naturels sont récursifs en ce sens que , par exemple, une phrase est constituée d'un
      sujet, d'un verbe et éventuellement d'une autre phrase.  On a longtemps
      cru que la récursion était propre au langage humain ce qui le distingait
      ainsi de la communication animale.  Cependant, [des recherches
   récentes](https://www.sciencedirect.com/science/article/abs/pii/S0010027711002903?via%3Dihub)
	   tendent  à  démontrer que  des  babouins  peuvent appréhender  des
	   structures  récursives  et  donc  que  notre  capacité  à  raisonner
	   récursivement  aurait une  origine beaucoup  plus ancienne  et qu'il
	   faut chercher ailleurs les différences entre les langages humains et
	   animaux. 
	   
	   
	   

## Exemple basique 

```python
def factorial_recursive(n: int) -> int:
    assert n > 0, "n doit être strictement positif"
    return 1 if n == 1 else n * factorial_recursive(n - 1)
```

Voilà comment cela se passe:

```
factorial_recursive(5)
5 * factorial_recursive(4)
5 * (4 * factorial_recursive(3))
5 * (4 * (3 * factorial_recursive(2)))
5 * (4 * (3 * (2 * factorial_recursive(1))))
5 * (4 * (3 * (2 * 1)))
5 * (4 * (3 * 2))
5 * (4 * 6)
5 * 24
120
```


![stack](./IMG/stack.webp)


<iframe
src="https://www.recursionvisualizer.com/?function_definition=def%20f1%28n%3A%20int%29%20-%3E%20int%3A%0A%20%20%20%20assert%20n%20%3E%200%2C%20%22n%20doit%20%C3%AAtre%20positif%22%0A%20%20%20%20return%201%20if%20n%20%3D%3D%201%20else%20n*f1%28n%20-%201%29&function_call=f1%285%29"
width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>



# Quelques exercices

- Bing : Bing is not Google
- PIP : PIP Installs Packages
- PNG : PNG's Not GIF
- FARM : Farm Animal Rights Movement
- LIFE : Life Is For Ever
- GRUNGE : Guitar Rock Utilizing Nihilist Grunge Energy

![dilbert](./IMG/dilbert.gif)


## Quizz

1. On considère la fonction suivante :
   ~~~python
   def f(n):
       if n == 0:
	       return 1
       else:
	       return 2 * f(n - 1)
   ~~~
   
    - Comment est évalué `f(8)` ?

       1. Ce calcul provoque une erreur.
       2. Cette expression vaut 14.
       3. Cette expression vaut 256.
       4. Cette expression vaut 2.
	
    - Comment est évalué `f(-1)`?


    
3. On considère la fonction suivante :
   ~~~python
   def inverse(lst):
       return inverse(lst[1:]) + lst[0]
   ~~~
   Cette fonction :

    1. renvoie l'image miroir d'une liste.
    2. calcule la somme des éléments de la liste
    3. compte le nombre d'éléments de la liste
    4. renvoie une erreur  (et dans ce cas la corriger  pour obtenir la fonction
       de la proposition a. ou celle de la proposition b.).

4. Le programme suivant renvoie une erreur.
   ~~~python
   def liste_entier_decr(n):
       if n == 0:
	       return 0
       return [n] + liste_entier_decr(n - 1)
   ~~~

    1. Vrai (et dans ce cas la corriger en tenant compte du nom de la fonction...)
    2. Faux



### MORALITÉ

On veillera à avoir une fonction de cette allure :

```
def fonc_rec(paramètres) -> type_sortie:
   éventuellement un assert pour ne pas s'éloigner du cas terminal
   if cas terminal:
      renvoyer un truc de type type_sortie
   else:
      fonc_rec(paramètres   qui  se   rapprochent  du   cas  terminal)   et
      éventuellent des trucs en faisant attention au type 
```

## Somme épisode 1

Déterminer une  fonction récursive qui  calcule la somme des  entiers de 1  à un
certain entier naturel `n` donné en argument.  Pour cela, si on note $S_n$ cette
somme, comment définir $S_n$ récursivement?

## Somme épisode 2

Déterminer une fonction récursive qui calcule  la somme des éléments d'une liste
d'entiers quelconques donnée en argument. 

## Longueur

Déterminer une fonction récursive qui renvoie la longueur d'une liste.

## Compteur

Déterminez  une  fonction  récursive  qui compte  le  nombre  d'occurences  d'un
caractère dans une chaîne, tous deux donnés en paramètre.


## Puissance entière 

Déterminez une fonction récursive qui calcule $a^n$ avec $n∈ℕ$


## Générer récursivement une liste

Écrire une fonction récursive d'argument `n` qui génère la liste

```Python
[n, n-1, ... , 1, 0]
```



## Palindrome

Déterminez une fonction récursive qui vérifie si un mot est un palindrome. 

## Max

Déterminer une fonction  récursive qui détermine le maximum  d'une liste de
nombres.  On n'utilisera bien sûr pas le `max` de Python.

*le plus grand c'est le plus grand entre le 1er et le plus grand du reste...*


```python
def maxi(xs: list):
    assert len(xs)..., "La liste est vide !"
    tête, queue = xs[0], xs[1:]
    if len(xs) == ...:
        return ...
    else:
        m_q = maxi(queue)
        return tête if ... else ...
```
	    


## Suite

Soit à présent la suite numérique $(u_n)$ définie sur l'ensemble des
entiers naturels  par : $u_0  = 12$  (le premier terme  de la suite)  et la
relation de  récurrence $u_{n+1} =  3u_n + 1$  (ici pour calculer  le terme
suivant il faut multiplier le précédent par 3 et lui ajouter 1). 

1. Compléter la  fonction suivante permettant de calculer le  terme de rang
   $n$ de la suite $(u_n)$ de manière itérative :
   
```python
def calcul_terme_itératif(n):
    u = ...
    for i in range(...):
        u = ...
    return u
```

Écrire la fonction `calcul_terme_récursif` suivante pour qu'elle renvoie la
valeur du terme  de rang $n$ de  la suite $(u_n)$ de  manière récursive. On
déplacera les blocs par glisser-déposer vers la droite. 

<iframe src="https://www.codepuzzle.io/IPACPZ", width="100%", height="600"></iframe>



## Dessin

Menez l'enquête et faites de plus beaux dessins...

```python
from turtle import *

def turec(n):
    left(60)
    forward(50 + n)
    if n < 50 :
        turec(n + 1)
    else:
        hideturtle()
        write("   FINI")
        exitonclick()
```

Essayez en particulier de dessiner un flocon de Von Koch...

![vonkoch](./IMG/vonkoch.png)


## Dénombrement


1. Déterminer une relation entre $A_n^p$ et $A_{n}^{p-1}$ et en déduire une fonction récursive qui calcule $A_n^p$.
1.  Déterminer plusieurs  fonctions  récursives qui  calculent $\binom{n}{p}$  :
   essayer à chaque  fois de  d'améliorer l'efficacité  de votre fonction
   en terme de calculs effectués par la machine.

## PGCD

Le plus grand commun diviseur de deux nombres, notamment utile pour écrire une fraction sous forme irréductible, peut être  déterminé efficacement à l'aide de l'algorithme d'Euclide.

Celui-ci affirme que le pgcd de `a` et de `b`, `b` étant un entier non nul, est le même que le pgcd de `b` et de `c`, avec `c` le reste de la division euclidienne de `a` par `b`. Le pgcd de `a` et de 0 étant `a`. 

1. Écrire une fonction récursive calculant le pgcd de `a` et de `b`.

2. En utilisant la fonction précédente écrire une fonction qui prend en entrée deux nombres entiers naturels non nuls `a` et b représentant une fraction $\frac{a}{b}$ et qui renvoie un couple d'entiers c et d tels que $\frac{c}{d}$ est l'écriture sous forme irréductible de $\frac{a}{b}$.


## Divide et Impera


![suntzu](./IMG/suntzu.jpeg)


Une méthode pour déterminer le maximum dans un tableau de nombres non vide est la suivante :

- si le tableau ne contient qu'un seul élément : c'est le maximum ;

- si le tableau contient au moins deux éléments, on le partage en deux
  sous-tableaux  contenant  le  même  nombre  (éventuellement  à  un  près)
  d'éléments  et le  maximum du  tableau initial  est alors  le maximum  du
  maximum de ces deux sous-tableaux.
  
On implémente cet algorithme de la façon suivante en Python (les tableaux sont ici assimilés aux données de type `list`) :
```python
1 def maximum(liste):
2     if len(liste) == 1:
3         return liste[0]
4     indice_milieu = len(liste)//2
5     max_gauche = maximum(liste[:indice_milieu])
6     max_droite = maximum(liste[indice_milieu:])
7     if max_gauche > max_droite:
8         return max_gauche
9     return max_droite
```

On rappelle que si `L` est une liste et `m` un entier compris entre `1` et `len(L)-1`, alors
 - l'instruction `L[:m]` renvoie une liste contenant les éléments de `L` d'indices allant de `0` à `m-1` ;
 - l'instruction `L[m:]` renvoie une liste contenant les éléments de `L` d'indices allant de `m` à `len(L)-1`.

Par exemple, si `L = [1, 5, 6, 3]`, alors `L[:2]` est la sous-liste `[1, 5]` et `L[2:]` est la sous-liste `[6, 3]`.


Indiquer  les  numéros   de  lignes  correspondant  aux   trois  étapes  de
l'algorithme :  cas de  base, découpage en  sous-listes et  combinaison des
résultats.

Expliquer ce qui se passe ci-dessous:

<iframe src="https://www.recursionvisualizer.com/?function_definition=def%20maximum%28liste%29%3A%0A%20%20%20%20if%20len%28liste%29%20%3D%3D%201%3A%0A%20%20%20%20%20%20%20%20return%20liste%5B0%5D%0A%20%20%20%20indice_milieu%20%3D%20len%28liste%29%2F%2F2%0A%20%20%20%20max_gauche%20%3D%20maximum%28liste%5B%3Aindice_milieu%5D%29%0A%20%20%20%20max_droite%20%3D%20maximum%28liste%5Bindice_milieu%3A%5D%29%0A%20%20%20%20if%20max_gauche%20%3E%20max_droite%3A%0A%20%20%20%20%20%20%20%20return%20max_gauche%0A%20%20%20%20return%20max_droite%0A%0A%0A%0A&function_call=maximum%28%5B2%2C5%2C6%2C4%2C9%2C8%2C1%5D%29" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe>

## Quelques solutions


```python
## utilitaires

def tete(xs):
    return xs[0]

def queue(xs):
    return [xs[i] for i in range(1, len(xs))]

## SOMME1

def somme1(n: int) -> int:
    """
    Calcule la somme des entiers de 1 à n
    """
    assert n >= 1, "n doit être supérieur à 1"
    if n == 1:
        return 1
    else:
        return n + somme1(n - 1)

def somme1(n: int) -> int:
    assert n >= 1, "n doit être supérieur à 1"
    return 1 if n == 1 else n + somme1(n - 1)

def somme1(n: int, s = 1) -> int:
    assert n >= 1, "n doit être supérieur à 1"
    return s if n == 1 else somme1(n - 1, s + n)

def test_somme1(n: int) -> bool:
    return somme1(n) == n*(n + 1)/2
```

<!--


```
## SOMME2

def somme2(xs: list):
    if len(xs) == 0:
        return 0
    else:
        return xs[0] + somme2([xs[i] for i in range(1, len(xs))])

def somme2(xs: list):
    if len(xs) == 0:
        return 0
    else:
        return tete(xs) + somme2(queue(xs))

    

## LONGUEUR

def long(xs: list) -> int:
    return 0 if xs == [] else 1 + long(queue(xs))


## Compteur de lettres
        
def compte_car(mot: str, lettre: str) -> int:
    if mot == "":
        return 0
    elif lettre == tete(mot):
        return 1 + compte_car(queue(mot), lettre)
    else:
        return compte_car(queue(mot), lettre)
		
def compte_car(mot: str, lettre: str) -> int:
    if mot == "":
        return 0
    else:
        return compte_car(queue(mot), lettre) + int(lettre == tete(mot))
```
-->
