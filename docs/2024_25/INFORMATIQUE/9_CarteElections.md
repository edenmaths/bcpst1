# Créer des cartes à partir de données

## Les bibliothèques utiles


```python
import folium # pour manipuler des cartes
import json # langage de stockage de données
import os  # lance des commandes shell 
from urllib.request import urlretrieve # pour récupérer des fichiers sur internet
import pandas as pd
import numpy as np
```


```python
#pip install html5lib  openpyxl
```

## Récupérer les données

Voici par exemple l'adresse de la page où l'on trouve les populations des départements sur le site de l'INSEE: 


```python
url_pop = "https://www.insee.fr/fr/statistiques/6013867?sommaire=6011075"
```
<!--
![tableau populations insee](./insee.png)
-->
On a vu précédemment quelle méthode de la bibliothèque `pandas` nous permet de récupérer un tableau d'une page web sous forme d'un `dataframe`, la structure de données de base de `pandas` :


```python
tab = pd.read_html(url_pop)[0]
tab
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Code département</th>
      <th>Nom du département</th>
      <th>Population municipale</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>01</td>
      <td>Ain</td>
      <td>652 432</td>
    </tr>
    <tr>
      <th>1</th>
      <td>02</td>
      <td>Aisne</td>
      <td>531 345</td>
    </tr>
    <tr>
      <th>2</th>
      <td>03</td>
      <td>Allier</td>
      <td>335 975</td>
    </tr>
    <tr>
      <th>3</th>
      <td>04</td>
      <td>Alpes-de-Haute-Provence</td>
      <td>164 308</td>
    </tr>
    <tr>
      <th>4</th>
      <td>05</td>
      <td>Hautes-Alpes</td>
      <td>141 220</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>95</th>
      <td>95</td>
      <td>Val-d'Oise</td>
      <td>1 249 674</td>
    </tr>
    <tr>
      <th>96</th>
      <td>971</td>
      <td>Guadeloupe</td>
      <td>384 239</td>
    </tr>
    <tr>
      <th>97</th>
      <td>972</td>
      <td>Martinique</td>
      <td>364 508</td>
    </tr>
    <tr>
      <th>98</th>
      <td>973</td>
      <td>Guyane</td>
      <td>281 678</td>
    </tr>
    <tr>
      <th>99</th>
      <td>974</td>
      <td>La Réunion</td>
      <td>861 210</td>
    </tr>
  </tbody>
</table>
<p>100 rows × 3 columns</p>
</div>



On transforme ensuite ces données en dictionnaire Python avec la méthode `to_dict` qui transforme un *dataframe* en *dictionnaire* :


```python
popd = tab["Population municipale"].to_dict()
popd
```




    {0: '652 432',
     1: '531 345',
     2: '335 975',
     3: '164 308',
     4: '141 220',
     5: '1 094 283',
     6: '328 278',
     7: '270 582',
     8: '153 287',
     9: '310 242',
     10: '374 070',
     11: '279 595',
     12: '2 043 110',
     13: '694 905',
     14: '144 692',
     15: '352 015',
     16: '651 358',
     17: '302 306',
     18: '240 073',
     19: '158 507',
     20: '181 933',
     21: '534 124',
     22: '600 582',
     23: '116 617',
     24: '413 223',
     25: '543 974',
     26: '516 762',
     27: '599 507',
     28: '431 575',
     29: '915 090',
     30: '748 437',
     31: '1 400 039',
     32: '191 377',
     33: '1 623 749',
     34: '1 175 623',
     35: '1 079 498',
     36: '219 316',
     37: '610 079',
     38: '1 271 166',
     39: '259 199',
     40: '413 690',
     41: '329 470',
     42: '765 634',
     43: '227 570',
     44: '1 429 272',
     45: '680 434',
     46: '174 094',
     47: '331 271',
     48: '76 604',
     49: '818 273',
     50: '495 045',
     51: '566 855',
     52: '172 512',
     53: '307 062',
     54: '733 760',
     55: '184 083',
     56: '759 684',
     57: '1 046 543',
     58: '204 452',
     59: '2 608 346',
     60: '829 419',
     61: '279 942',
     62: '1 465 278',
     63: '662 152',
     64: '682 621',
     65: '229 567',
     66: '479 979',
     67: '1 140 057',
     68: '767 086',
     69: '1 875 747',
     70: '235 313',
     71: '551 493',
     72: '566 412',
     73: '436 434',
     74: '826 094',
     75: '2 165 423',
     76: '1 255 633',
     77: '1 421 197',
     78: '1 448 207',
     79: '374 878',
     80: '570 559',
     81: '389 844',
     82: '260 669',
     83: '1 076 711',
     84: '561 469',
     85: '685 442',
     86: '438 435',
     87: '372 359',
     88: '364 499',
     89: '335 707',
     90: '141 318',
     91: '1 301 659',
     92: '1 624 357',
     93: '1 644 903',
     94: '1 407 124',
     95: '1 249 674',
     96: '384 239',
     97: '364 508',
     98: '281 678',
     99: '861 210'}



Le site de l'Insee propose aussi ces données sous forme de fichier `csv` (qu'est-ce ?) téléchargeable sur [cette page](https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075#consulter). Il y a une petite icône vers un fichier `csv` mais en fait c'est une archive (un fichier compressé) au format `zip`. Il existe bien sûr une bibliothèque Python pour ça...


```python
from zipfile import ZipFile

url_bigzip = "https://www.insee.fr/fr/statistiques/fichier/6011070/ensemble.zip"
bigzip = urlretrieve(url_bigzip)[0]

with ZipFile(bigzip, 'r') as zip:
    zip.extract('donnees_departements.csv')
    
dfdep = pd.read_csv('donnees_departements.csv', sep=';')
```

En extrayant la colonne *PMUN* de ce *dataframe*, on obtient le même dictionnaire que précédemment...Enfin presque, car la Corse pose des problèmes. On crée donc un dictionnaire plus pratique :


```python
popd = {}
for c in range(96):
    popd[dfdep['CODDEP'][c]] = dfdep['PMUN'][c]
```

Sur              le               site              du              gouvernement
[`data.gouv.fr`](https://www.data.gouv.fr/fr/pages/donnees-des-elections/),   on
trouve de nombreuses données dont par exemple [les résultats des élections européennes](https://www.data.gouv.fr/fr/datasets/resultats-provisoires-des-elections-europeennes-du-9-juin-2024/) sous forme cette fois d'un fichier *excel* `xlsx` et d'un fichier `csv` bizarrement dénommé `txt`.


```python
url_euro = "https://www.data.gouv.fr/fr/datasets/r/742618ab-eb59-4a00-801d-c2c3ebac1efd"
df2 = pd.read_excel(url_euro)
```
<!--
Attention, il y a une colonne de titre. Il faut donc indiquer que la colonne des
titres des colonnes est la première ligne des valeurs

```python
df2.columns = df2.iloc[0] # 1ere ligne comme en-tête de colonnes
df2 = df2[1:] # on prend comme valeurs celles à partir de la 2e ligne
```
-->





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Code département</th>
      <th>Libellé département</th>
      <th>Inscrits</th>
      <th>Votants</th>
      <th>% Votants</th>
      <th>Abstentions</th>
      <th>% Abstentions</th>
      <th>Exprimés</th>
      <th>% Exprimés/inscrits</th>
      <th>% Exprimés/votants</th>
      <th>...</th>
      <th>% Voix/exprimés 29</th>
      <th>Sièges 29</th>
      <th>Numéro de panneau 30</th>
      <th>Nuance liste 30</th>
      <th>Libellé abrégé de liste 30</th>
      <th>Libellé de liste 30</th>
      <th>Voix 30</th>
      <th>% Voix/inscrits 30</th>
      <th>% Voix/exprimés 30</th>
      <th>Sièges 30</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>01</td>
      <td>Ain</td>
      <td>449217</td>
      <td>244260</td>
      <td>54,37%</td>
      <td>204957</td>
      <td>45,63%</td>
      <td>237523</td>
      <td>52,87%</td>
      <td>97,24%</td>
      <td>...</td>
      <td>2,12%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>40</td>
      <td>0,01%</td>
      <td>0,02%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>02</td>
      <td>Aisne</td>
      <td>373728</td>
      <td>189750</td>
      <td>50,77%</td>
      <td>183978</td>
      <td>49,23%</td>
      <td>183689</td>
      <td>49,15%</td>
      <td>96,81%</td>
      <td>...</td>
      <td>2,19%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>29</td>
      <td>0,01%</td>
      <td>0,02%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>03</td>
      <td>Allier</td>
      <td>249428</td>
      <td>138833</td>
      <td>55,66%</td>
      <td>110595</td>
      <td>44,34%</td>
      <td>131770</td>
      <td>52,83%</td>
      <td>94,91%</td>
      <td>...</td>
      <td>3,91%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>35</td>
      <td>0,01%</td>
      <td>0,03%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>04</td>
      <td>Alpes-de-Haute-Provence</td>
      <td>129172</td>
      <td>74445</td>
      <td>57,63%</td>
      <td>54727</td>
      <td>42,37%</td>
      <td>72337</td>
      <td>56,00%</td>
      <td>97,17%</td>
      <td>...</td>
      <td>3,64%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>18</td>
      <td>0,01%</td>
      <td>0,02%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>05</td>
      <td>Hautes-Alpes</td>
      <td>115059</td>
      <td>66931</td>
      <td>58,17%</td>
      <td>48128</td>
      <td>41,83%</td>
      <td>64928</td>
      <td>56,43%</td>
      <td>97,01%</td>
      <td>...</td>
      <td>3,33%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>18</td>
      <td>0,02%</td>
      <td>0,03%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>103</th>
      <td>986</td>
      <td>Wallis et Futuna</td>
      <td>9018</td>
      <td>2837</td>
      <td>31,46%</td>
      <td>6181</td>
      <td>68,54%</td>
      <td>2786</td>
      <td>30,89%</td>
      <td>98,20%</td>
      <td>...</td>
      <td>2,01%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>0</td>
      <td>0,00%</td>
      <td>0,00%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>104</th>
      <td>987</td>
      <td>Polynésie française</td>
      <td>212084</td>
      <td>28818</td>
      <td>13,59%</td>
      <td>183266</td>
      <td>86,41%</td>
      <td>27717</td>
      <td>13,07%</td>
      <td>96,18%</td>
      <td>...</td>
      <td>1,13%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>10</td>
      <td>0,00%</td>
      <td>0,04%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>105</th>
      <td>988</td>
      <td>Nouvelle-Calédonie</td>
      <td>222683</td>
      <td>29234</td>
      <td>13,13%</td>
      <td>193449</td>
      <td>86,87%</td>
      <td>27910</td>
      <td>12,53%</td>
      <td>95,47%</td>
      <td>...</td>
      <td>0,91%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>9</td>
      <td>0,00%</td>
      <td>0,03%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>106</th>
      <td>ZX</td>
      <td>Saint-Martin/Saint-Barthélemy</td>
      <td>25276</td>
      <td>4234</td>
      <td>16,75%</td>
      <td>21042</td>
      <td>83,25%</td>
      <td>4067</td>
      <td>16,09%</td>
      <td>96,06%</td>
      <td>...</td>
      <td>1,25%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>2</td>
      <td>0,01%</td>
      <td>0,05%</td>
      <td>0</td>
    </tr>
    <tr>
      <th>107</th>
      <td>ZZ</td>
      <td>Français établis hors de France</td>
      <td>1507606</td>
      <td>261506</td>
      <td>17,35%</td>
      <td>1246100</td>
      <td>82,65%</td>
      <td>258820</td>
      <td>17,17%</td>
      <td>98,97%</td>
      <td>...</td>
      <td>0,57%</td>
      <td>0</td>
      <td>30</td>
      <td>LDIV</td>
      <td>FRANCE LIBRE</td>
      <td>FRANCE LIBRE</td>
      <td>40</td>
      <td>0,00%</td>
      <td>0,02%</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>107 rows × 256 columns</p>
</div>



Il reste maintenant à créer un dictionnaire dont les clés seront les numéros des départements et les valeur le pourcentage de votes exprimés par rapport à la population de chaque département. Expliquer ce qui se passe ici :


```python
def dic_t2(colonne: str) -> dict:
    dic1 = {}
    for c in range(0, 96):
        dic1[df2['Code département'][c]] = df2[colonne][c]
    dic2 = {}
    for c in popd:
        dic2[c] = 100 * dic1[c] / popd[c]
    return dic2
```

## Créer la carte

On récupère les données géographiques pour dessiner chaque département sous la forme d'un fichier au format `json`


```python
url_geo_dep = 'https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/departements.geojson'
deps_geo = open(urlretrieve(url_geo_dep)[0]).read()
geo_json_data = json.loads(deps_geo)
```

Il ne reste plus qu'à utiliser `folium` pour rajouter des zones colorées correspondant aux données géographiques des départements, la couleur dépendant d'une échelle correspondant aux pourcentages calculés précédemment :


```python
def carte(stats, legende):
    mini, maxi = min(stats.values()), max(stats.values()) # pour l'échelle de couleur
    bins = np.linspace(int(mini), int(maxi)+1, 10) # on peut avoir 9 niveaux de couleurs au maximum 
    m = folium.Map([45.5, -2], tiles='stamentoner', zoom_start=6) # obtenir la carte du fond centrée sur la France
    folium.Choropleth(
        geo_data = geo_json_data, # les données des zones géographiques qui seront colorées
        name = 'choropleth', # le type de coloriage
        data = stats, # les données qui vont être traitées 
        key_on = 'feature.properties.code',
        fill_color = 'Reds', # la palette de couleur : d'autres ici -> https://colorbrewer2.org/
        fill_opacity = 0.8, # transparence du remplissage
        line_opacity = 0.2, # transparence des frontières
        legend_name = legende,
        bins = bins,
        reset = True
    ).add_to(m)
    folium.LayerControl().add_to(m) # on ajoute le coloriage au fond de carte
    m.save(os.path.join('./', 'Colormap_{}.html'.format(legende))) # on enregistre la carte au format html pour la lire dans un navigateur
    return m
```

avec cette appel

```python
carte(dic_t2('Voix 5'), 'RN sur pop du département')
```

on obtient [**la carte des votes RN**](./scripts/Colormap_RN sur pop du département.html)

et avec celui-ci:


```python
carte(dic_t2('Voix 4'), 'LFI sur pop du département')
```


et celle [**la carte des votes LFI**](./scripts/Colormap_LFI sur pop du département.html)

**Quelques autres:**

-  [La carte des votes LR](./scripts/Colormap_LR sur pop du département.html)

-  [La carte des votes Zemmour](./scripts/Colormap_REC sur pop du département.html)

-  [La carte des votes PS](./scripts/Colormap_PS sur pop du département.html)

-  [La carte des votes Ensemble](./scripts/Colormap_ENS sur pop du département.html)

-  [La carte des votes ELLV](./scripts/Colormap_ECOLO sur pop du département.html)






Pour avoir tous les libellés associés à leur numéro :


```python
df2.filter(regex='Libellé abrégé de liste')
```

ou sinon vous allez sur [la page Wikipedia](https://fr.wikipedia.org/wiki/Liste_des_listes_aux_%C3%A9lections_europ%C3%A9ennes_de_2024_en_France) les donnant
