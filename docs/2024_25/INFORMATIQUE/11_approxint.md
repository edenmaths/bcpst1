{{ chapitre(11, "Calcul approché d'intégrales", "TP ", 0)}}



# Approximation de $\mathbf{\pi}$ et calcul approché d'intégrale



On    s'intéresse    à    l'intégrale     sur    $[0,1]$    de    la    fonction
$f~:~t\mapsto\sqrt{1-t^2}$ 
car bien sûr $4 \displaystyle \displaystyle\int_{0}^{1} f(t) \;{\rm d} t = ????$.




Un  problème  demeure:  comment  calculer  une  approximation  de  cette
intégrale?


Nous avons évoqué plusieurs méthodes cette année et allons en voir de nouvelles.


## Méthode des rectangles



C'est la plus grossière.  Petit
rappel:

$$\displaystyle\int_{a}^{b}f(x)\;\text{d}x=\displaystyle\sum_{k=0}^{n-1}\frac{b-a}{n}f
\left(a+k \frac{b-a}{n}\right)+E_n$$

avec $E_n$ l'erreur commise.

Faites un dessin.

```python
def int_rec_droite(f, a: float, b: float, N: int) -> float:
    S = 0
    dt = (b - a) / N
	t = a
    while t < b:
        S = S + f(t)*dt
        t = t + dt
    return S
```


Écrivez  la fonction  jumelle `int_rec_gauche(f,a,b,N)`  puis des  fonctions qui
calculent les  mêmes choses en  utilisant une  boucle `for`au lieu  d'une boucle
`while`. 


Il *semble* falloir $2^{15}$ itérations pour obtenir six bonnes décimales de
$\pi$...

```python
In [5]: for k in range(15) : print(pi - 4*int_rec_gauche(f, 0, 1, 2**k))
3.141592653589793
1.409541846020916
0.6458835854873524
0.3017735092326199
0.14333961576205168
0.06899023071266264
0.033545740810107194
0.016436861193178665
0.008099570632563058
0.004007750870138604
0.0019890120507732867
0.0009892506783506505
0.0004927672320342857
0.0002457266651032519
0.00012263106339993612
```

## Méthode des trapèzes



Comment interpréter ce nom ? Faites un dessin. C'est déjà un peu plus précis, à vue d'œil.  Créez une fonction
`int_trap(f,a,b,N)` et faites les mêmes tests.


```python
In [7]: for k in range(15) : print(pi - 4*int_trap(f, 0, 1, 2**k))
1.1415926535897931
0.4095418460209159
0.1458835854873528
0.05177350923261992
0.01833961576205212
0.006490230712662637
0.0022957408101085264
0.0008118611931777764
0.00028707063256350196
0.0001015008701394926
3.58870507768394e-05
1.26881783466537e-05
4.485982035618008e-06
1.5860401174627725e-06
5.60750892830697e-07
```


Pour plus de précision, on pourrait  être tenté de prendre un plus petit
arc  de  cercle, disons  entre  $\frac{\pi}{3}$  et $\frac{\pi}{2}$,  en
utilisant encore $f$. En effet,  la fonction n’étant pas dérivable en 1,
on a un peu peur que  cela crée des perturbations.  Et intuitivement, on
se  dit qu’en  limitant l’intervalle  d’intégration, on  devrait limiter
l’ampleur de l’approximation.


```python
In [9]: for k in range(8): print(pi/12-(int_trap(f,0,1/2,10**k)-sqrt(3)/8))
-0.005817179530668071
-6.0117319471197916e-05
-6.014041918356305e-07
-6.014064968251631e-09
-6.013917142055902e-11
-6.002975894148221e-13
-8.43769498715119e-15
-4.418687638008123e-14
```



Cela semble mieux fonctionner. Bizarre tout de même ce dernier résultat....

On a utilisé des segments  de droite horizontaux, des segments de droite
obliques...Et   si   l’on  utilisait   des   segments   de  parabole   :
intuitivement, cela colle plus à la courbe, cela devrait être plus précis.

## Méthode de Simpson



On interpole la courbe par un arc de parabole. On veut que cet arc passe
par les points extrêmes de la courbe et le point d’abscisse le milieu de
l’intervalle. Pour cela, on va déterminer les $c_i$ tels que:

\[ 
\int_a^bf(x) \mathrm{d}x=c_0f(a)+c_1f \left(\frac{a+b}{2}\right)+c_2f(b)
\]

soit exacte pour $f(x)$ successivement égale à $1$, $x$ et $x^2$.

Posons $h=b-a$ et ramenons-nous au cas $a=0$.
On obtient le système suivant~:


\[
\left\{\begin{array}{l}
c_0+c_1+c_2=h\\
c_1+2c_2=h\\
c_1+4c_2=\frac{4}{3}h
\end{array}\right.
\]

alors $c_0=c_2=\frac{h}{6}$ et $c_1=\frac{4}{6}h$

\[\int_a^bf(x) \mathrm{d}x=\frac{b-a}{6}\left(f(a)+4f \left(\frac{a+b}{2}\right)+f(b)\right)\]


Subdivisez l'intervalle  d'intégration et  utilisez  la  méthode de
Simpson sur chaque subdivision dans un fonction Python:


```python
def int_simps(f, a: float, b: float, N: int) -> float:
    S = 0
    t = a
    dt = (b - a)/N
    while t + dt <= b:
        S += ...
        t += ...
    return S
```

Et on obtient :

```python
In [13]: for k in range(15) :
        print(pi - 12*(int_simps(f,0,1/2,2**k) - sqrt(3)/8))
   ....:    ....:
0.0006601149512537319
4.72214266773463e-05
3.0824728671774437e-06
1.9496909775540416e-07
1.222303502856903e-08
7.645306610015723e-10
4.779110440722434e-11
2.9887203822909214e-12
1.8474111129762605e-13
7.993605777301127e-15
-4.440892098500626e-16
-1.3766765505351941e-14
-8.881784197001252e-15
-4.440892098500626e-16
-1.7763568394002505e-15
```

Allons plus loin :

```python
In [17]: for k in range(8): print(pi/12 - (int_simps(f, 0, 1/2, 10**k) - sqrt(3)/8))
5.500957927112582e-05
6.664729046423901e-09
6.684097719755755e-13
3.885780586188048e-16
-1.7208456881689926e-15
-4.907185768843192e-14
4.030109579389318e-14
-6.844913524872709e-12
```



Pourquoi pas. Et si nous prenions notre petit arc de cercle :


```python
for k in range(8): print(pi - 12*(int_simps(f, 0, 1/2, 10**k) - sqrt(3)/8))
... 
0.0006601149512537319
7.997674877913141e-08
0.0520477428322228
0.005197018063311631
-2.042810365310288e-14
-5.88862292261183e-13
5.1961537730349505e-06
-8.213874025386758e-11
```



On s’aperçoit qu’on est un peu comme un spécialiste de la tectonique des plaques prenant des photos d’une plage tous les mois à la même heure. Il peut tout à fait se produire que le montage des photos mette en évidence que le niveau de la mer descend et il pourra dire à la radio que le réchauffement de la planète et la fonte des glaces, c’est n’importe quoi.

Ou bien un autre scientifique (ou le même...) peut jeter un caillou dans l’eau la nuit et prendre des photos au flash toutes les T secondes avec T la période de l’onde créée par le caillou et en conclure que jeter un caillou dans l’eau ne produit aucun effet sur la surface de l’eau si le hasard fait bien les choses.

Ici aussi, on observe sans garde-fou théorique une situation et on peut faire et
en conclure  absolument n’importe quoi  pour peu que le  hasard du choix  de nos
mesures  ne  nous  confronte  pas   aux  problèmes,  jusqu’à  ce  qu’enfin,  une
catastrophe survienne....



Nous  allons nous  contenter dans  un premier  temps  d'étudier l'erreur
commise par la méthode des trapèzes. Nous avons juste besoin du théorème
de Rolle qui peut être étudié en Terminale à titre d'exercice.



On considère une fonction $f$ de classe $\mathcal C^2$ sur un intervalle
$[a,b]$ à valeurs réelles. On considère une subdivision régulière de $[a,b]$ et on pose:

$$x_j=a+  j\frac{b-a}{N} $$

où $N$ désigne le nombre d'intervalles. On cherche une fonction polynomiale
P de degré au plus 1 telle que $P(a)=f(a)$ et $P(b)=f(b)$.

L'intégrale de $P$ sur $[a,b]$ vaut $J=(b-a) \frac{f(a)+f(b)}{2} $.

Soit $x\in]a,b[$. On introduit la fonction $g_x$ définie sur $[a,b]$ par:

$$g_x(t)=f(t)-P(t)+k_x(t-a)(b-t)$$

où le réel $k_x$ est choisi tel que $g_x(x)=0$.

La fonction $g_x$  est de classe $\mathcal C^2$ et  s'annule en $a$, $x$
et $b$ donc on peut  appliquer le théorème de \textsc{Rolle} sur $]a,x[$
et  $]x,b[$ et $g'_x$  s'annule donc  sur chacun  de ces  intervalle. On
applique alors une nouvelle fois  le théorème de \textsc{Rolle} à $g'_x$
sur $]a,b[$ et $g''_x$ s'annule donc au moins une fois en un réel $c_x$ de
$]a,b[$.

On pose $k_x= \dfrac{f''(c_x)}{2}$. On obtient finalement
que pour tout $x\in]a,b[$, il existe $c_x\in]a,b[$ tel que:

$$f(x)-P(x)=-(x-a)(b-x) \frac{f''(c_x)}{2} $$

On peut évidemment inclure les bornes de l'intervalle.

Soit  $M_2$ un majorant  de $\left|f''\right|$  sur $[a,b]$.  On obtient
donc, par intégration de l'égalité précédente:

$$\left|\displaystyle\int_{a}^{b}f(x)\;\mathrm{d}x-J\right|\leqslant  \frac{M_2}{12} (b-a)^3$$


puis,  par   application  de  cette  inégalité   sur  chaque  intervalle
$[x_j,x_{j+1}]$:



$$\left|\displaystyle\int_{a}^{b}f(x)\;\mathrm{d}x-J_N\right|\leqslant
\frac{M_2}{12} \frac{(b-a)^3}{N^2}$$


avec
$J_N=\frac{b-a}{N}\left(\frac{f(a)}{2}+\displaystyle\sum_{j=1}^{N-1}f(x_j)+\frac{f(b)}{2}\right)$.

On obtient donc que quand $N$ est multiplié par 2, le majorant de l'erreur
est divisé par 4.


## Alors que s'est-il passé?

 Il faut avoir à l’esprit que le processeur  compte en base 2 et nous en base 10
 et que son « zéro  » vaut environ $2,2\times 10^{-16}$. En fait, ce n’est  pas zéro mais une valeur
 nommée communément (pas seulement sur Python) epsilon. 

Comme c’est l’ordinateur qui va compter, il faudrait plutôt chercher à le ménager et en tenir compte.

Il nous faut donc quand même regarder sous le capot pour comprendre la panne.

Le  problème,  c’est que  nous  ne  travaillons  pas  en précision  infinie.  En
chargeant le module sys, on a accès à la commande `float_info.epsilon` qui donne
la différence entre 1.0 et le flottant suivant le plus proche :


```python
>>> from sys import*
>>> float_info.epsilon
2.220446049250313e-16
```



Eh oui, la mémoire de l’ordinateur n’est pas infinie. Ses « réels » admettent des successeurs.

Mais attention, entre 0 et 1, les choses sont différentes :

```python
>>> from sys import*
>>> e=float_info.epsilon
>>> e
2.220446049250313e-16
>>> e/2
1.1102230246251565e-16
>>> 1+e/2
1.0
>>> 1+e/2==1
True
>>> 0+e/2==0
False
>>> e*1e16
2.220446049250313
>>> -1-e/2
-1.0
>>> 1-e/2
0.9999999999999999
```

Cet `epsilon` n’est pas zéro, rappelons-le, mais détermine à partir de quand deux flottants seront considérés comme égaux par le système.

On peut définir une fonction qui illustrera ce phénomène :


```python
def egal_float(a, b):
    return abs(a - b) <= (float_info.epsilon * min(abs(a), abs(b)))
```

Si on n’y prête pas attention, on peut arriver à des résultats surprenants :

```python
>>> egal_float(0.1 + 0.1 + 0.1 , 0.3)
True
>>> 0.1 + 0.1 + 0.1 == 0.3
False
>>> 3 * 0.1 == 0.3
False
>>> 4 * 0.1 == 0.4
True
>>> 10+float_info.epsilon-10
0.0
>>> 1+float_info.epsilon-1
2.220446049250313e-16
>>> 10+10*float_info.epsilon-10
1.7763568394002505e-15
>>> x=0.1
>>> 3*x-0.3
5.551115123125783e-17
>>> 4*x-0.4
0.0
```




Rappelons également que le processeur est plus à l’aise avec les puissances de 2 car une multiplication par 2 ou une de ses puissances revient à un décalage dans son écriture binaire.


Effectuons la division de 1 par 10 en base 2:

```
1       | 1010
        ------
 10     | 0,00011
 100    |
 1000   |
 10000  |
- 1010  |
------  |
   1100 |
-  1010 |
------- |
     10 |
```

Continuez....

Reprenons l’affinement successif de notre subdivision mais avec des nombres de subdivisions égaux à des puissances de 2 :


```python
>>> for k in range(13): print(pi-12*(int_simps(f,0,1/2,2**k)-sqrt(3)/8))
... 
0.0006601149512537319
4.72214266773463e-05
3.0824728671774437e-06
1.9496909775540416e-07
1.222303502856903e-08
7.645306610015723e-10
4.779110440722434e-11
2.9887203822909214e-12
1.8474111129762605e-13
7.993605777301127e-15
-4.440892098500626e-16
-1.3766765505351941e-14
-8.881784197001252e-15
```



Tout a l’air de bien fonctionner jusqu’à 1024 mais ensuite, on arrive aux alentours de epsilon et ça commence à se détraquer informatiquement comme nous l’évoquerons dans l’appendice.

En affinant grossièrement à coups de puissances de 10, nous étions passés à côté du problème.

Comme quoi, agir intuitivement en mathématiques ou en informatique (ici : « plus on subdivise petit, meilleure sera la précision » ) peut entraîner de graves erreurs...

La fonction est aussi à prendre en considération, puisqu’elle demande de soustraire à 1 un tout petit nombre :

```python
>>> f(1e-8)
1.0
```



De plus, cela explique aussi les différences entre la « méthode while » et la « méthode for » .

Le test while  `t + dt <= b` peut s’arrêter pour de mauvaises raisons. Par exemple :


```python
>>> 1 + 0.1 + 0.1 + 0.1 - 0.3 <= 1
False
```



La boucle peut ainsi s’arrêter inopinément, alors qu’on est loin de la précision demandée.

Précédemment, avec la méthode « gros sabots » , nous n’avions pas vu de différence entre la méthode des trapèzes et la méthode de SIMPSON ce qui contredisait notre intuition. Observons à pas plus feutrés ce qui se passe et incluons les rectangles : 


```python
>>> for k in range(11):
    print(pi-12*(int_rec_gauche(f,0,1/2,2**k)-sqrt(3)/8),"\t",\
              pi-12*(int_trap(f,0,1/2,2**k)-sqrt(3)/8),"\t",\
              pi-12*(int_simps(f,0,1/2,2**k)-sqrt(3)/8))

0.5435164422364771  	 0.14159265358979312 	 0.0006601149512537319
0.23685514393423013 	 0.035893249610888134 	 4.72214266773463e-05
0.10948967563440082 	 0.009008728472729821 	 3.0824728671774437e-06
0.052494967553668115 	 0.0022544939728321722 	 1.9496909775540416e-07
0.025684006510449997 	 0.0005637697200326919 	 1.222303502856903e-08
0.012701069992491654 	 0.00014095159728411133  7.645306610015723e-10
0.006315297670323794 	 3.5238472716692115e-05  4.779110440722434e-11
0.00314883925282583 	 8.8096540218352e-06 	 2.9887203822909214e-12
0.0015722172151479974 	 2.2024157457778415e-06  1.8474111129762605e-13
0.0007855580037752219 	 5.506040738900708e-07 	 7.993605777301127e-15
0.00039264135087524465 	 1.3765102657714579e-07  -4.440892098500626e-16
```

