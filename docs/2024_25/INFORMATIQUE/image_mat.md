```python
import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
import matplotlib.cm as cm
import matplotlib.colors as colors
from urllib.request import urlopen
```

### Puissance

#### Version récursive


```python
def mat_pow(m: np.ndarray, n: int) -> np.ndarray:
    nl, nc = m.shape
    if nl != nc:
        raise ValueError("La matrice n'est pas carrée")
    if n == 0:
        return np.eye(nl)
    else:
        return m.dot(mat_pow(m, n - 1))
        
```


```python
m = np.array([[1,2],[3,4]])
mat_pow(m, 5)
```




    array([[1069., 1558.],
           [2337., 3406.]])



#### Version non récursive (impérative)


```python
def mat_pow2(m: np.ndarray, n: int) -> np.ndarray:
    nl, nc = m.shape
    if nl != nc:
        raise ValueError("La matrice n'est pas carrée")
    p = np.eye(nl)
    for _ in range(1, n + 1) :
        p = p.dot(m)
    return p
```


```python
mat_pow2(m, 5)
```




    array([[1069., 1558.],
           [2337., 3406.]])



### Coloration


```python

taille = 16 # taille de la matrice

Z = np.array([[1 if i < j else 0 for j in range(taille)] for i in range(taille)])

nf = 10
mats = [mat_pow(Z,i + 1) for i in range(nf)]
n = int(np.max(mats) + 2)

# Fabrication d'une barre de couleurs adaptée
cols = []
for i in range(n):
    cols.append(tuple([np.random.rand() for _ in range(4)]))
mes_couleurs = colors.ListedColormap(cols) 
ma_norme = colors.BoundaryNorm(range(len(cols)), mes_couleurs.N, clip=True)
# Fin de la fabrication de la colormap

for i in range(nf):
    plt.clf()
    im = plt.imshow(mat_pow(Z,i + 1), cmap=mes_couleurs, norm=ma_norme)
    cb = plt.colorbar()
    plt.show()
```


    
![png](output_8_0.png)
    



    
![png](output_8_1.png)
    



    
![png](output_8_2.png)
    



    
![png](output_8_3.png)
    



    
![png](output_8_4.png)
    



    
![png](output_8_5.png)
    



    
![png](output_8_6.png)
    



    
![png](output_8_7.png)
    



    
![png](output_8_8.png)
    



    
![png](output_8_9.png)
    



```python
cb.remove() # pour supprimer la colormap précédente et retrouver de belles couleurs
mystere = misc.face()
plt.imshow(mystere)
```




    <matplotlib.image.AxesImage at 0x7fcd1481dfd0>




    
![png](output_9_1.png)
    



```python
def neg1(photo: np.ndarray) -> np.ndarray:
    nl, nc, _ = photo.shape
    photo_neg = np.zeros((nl,nc,3), dtype='uint8')
    for i in range(nl):
        for j in range(nc):
            photo_neg[i,j] = 255 - photo[i, j]
    return photo_neg
```


```python
plt.imshow(neg1(mystere))
```




    <matplotlib.image.AxesImage at 0x7fcd14a2a2b0>




    
![png](output_11_1.png)
    



```python
def neg2(photo: np.ndarray) -> np.ndarray:
    return np.array([[255 - pixel for pixel in ligne] for ligne in photo])
```


```python
plt.imshow(neg1(mystere))
```




    <matplotlib.image.AxesImage at 0x7fcd14d29fd0>




    
![png](output_13_1.png)
    


##### Fonction d'ordre supérieur

On crée une fonction qui prend une photo et une transformation en paramètres et affiche la photo transformée. On n'aura plus qu'à crééer la fonction aui transforme un pixel pour afficher le résultat sans avoir à répéter tout le mécanisme commun de création


```python
def transforme_par_px(photo, change):
    new = np.array([[change(pixel) for pixel in ligne] for ligne in photo])
    plt.imshow(new)
    plt.show()
```


```python
def neg_pix(pixel):
    return 255 - pixel
```


```python
def grise_pix(pixel):
    g = np.sum(pixel) // 3
    return np.array([g, g, g], dtype='uint8')
```


```python
transforme_par_px(mystere, grise_pix)
```


    
![png](output_18_0.png)
    



```python
from urllib.request import urlretrieve
url = "http://4everstatic.com/images/850xX/drole/dessins/kermit-the-frog,-grenouille,-sourire-136324.jpg"
urlretrieve(url, filename="./kermit.jpg")
kermit = plt.imread("./kermit.jpg")
```


```python
plt.imshow(kermit)
```




    <matplotlib.image.AxesImage at 0x7fcd0a8a8b50>




    
![png](output_21_1.png)
    



```python
transforme_par_px(kermit, grise_pix)
```


    
![png](output_22_0.png)
    



```python
transforme_par_px(kermit, neg_pix)
```


    
![png](output_23_0.png)
    



```python
def retourne_h(im:np.ndarray) -> np.ndarray:
    nl, nc, _ = im.shape
    return np.array([[im[nl - 1 - i, j] for j in range(nc)] for i in range(nl)])
```


```python
plt.imshow(retourne_h(kermit))
```




    <matplotlib.image.AxesImage at 0x7fcd14c10550>




    
![png](output_25_1.png)
    



```python

```
