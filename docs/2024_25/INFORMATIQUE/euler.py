import matplotlib.pyplot as plt
import numpy as np

#### Euler explicite
def euler_expl(f, a, b, y0, n):
    """
    Approximation de la solution de l'EDO y'=f(y, x) sur [a, b]
    de condition initiale y(a) = y0 avec une subdivision de [a, b] en
    n segments de meme longueur
    """
    h = (b - a)/n
    X = [a + k*h for k in range(n + 1)]
    Y = [y0 for k in range(n + 1)]
    for k in range(n):
        Y[k + 1] = Y[k] + h*f(Y[k], X[k])
    return X, Y


def trace_euler_expl(f, a, b, y0, n):
    X, Y = euler_expl(f, a, b, y0, n)
    plt.scatter(X, Y)


### Exemple : y' = y sur [0, 2] avec y(0) = 1

trace_euler_expl(lambda y, x : y, 0, 2, 1, 2**5)

# On conna^it la solution : exp

Xe = np.linspace(0,2)
Ye = np.exp(Xe)

plt.plot(Xe, Ye)

plt.show()
