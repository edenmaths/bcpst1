{{ chapitre(10, "Les suites (suite)", "TP ", 0)}}



Quelques petits exercices sur les suites donnés au concours Agro-Véto où il
est  explicitement demandé  de fournir  une fonction  Python suivis  d'une
première  approche  de la  dynamique  des  populations  avec le  modèle  de
Verhulst illustré en Python. 


## Quelques questions posées aux concours


!!! {{ exercice()}}

    Écrire en Python une fonction  `moyennesuite` qui prend en argument une
	fonction `f`  et un  entier naturel  `n` et  qui renvoie  `s` égal  à $
	\dfrac{f(0)+\cdots+f(n)}{n+1}$.  Tester  avec les  fonctions  $x\mapsto
	\exp(1/(x+1))$ et $x\mapsto (-1)^x$. Que constate-t-on?
	
	
  
  


!!! {{ exercice()}}
    
	On  considère  une  suite  définie par  $r_{n+1}=  f(r_n)=\dfrac{r_n  +
	\frac{2}{r_n}}{2}$ et $r_0=1$.
	
	
	Observez ce que cela donne avec Python.
	
	Démontrer par récurrence que:
	
	1. pour tout entier naturel non nul $n$, on a $r_n≥\sqrt{2}$.
    2. la suite est décroissante
    3. $\sqrt{2}$ est l'unique point fixe positif de f.
	
	
    
!!! {{ exercice()}}

    Soit $g$ la fonction définie par : 
	
	$$
	g(t)=
	\begin{cases}
	1 \text{ si } t = 0\\
	\dfrac{\ln(1+t)}{t} \text{ si } t>0
	\end{cases}
	$$
	
	On considère la suite définie par $u_n=\dfrac{\ln(n+1)-\ln(n)}{n}$ pour
	tout entier naturel non nul $n$ et on pose 
	
	$$
	S_n=\displaystyle\sum_{k=1}^nu_k \text{ et } T_n=S_n+\dfrac{1}{n} 
	$$

	Représenter graphiquement, à l'aide d'un script Python, les 50 premiers
	termes des suites $(S_n)$ et $(T_n)$. Que peut-on conjecturer ? 

!!! {{ exercice()}}

    Soit $a$ et $b$ deux réels  strictement positifs. On définit les suites
    $(u_n)_{n∈ℕ}$ et $(v_n)_{n∈ℕ}$ par la relation de récurrence:
	
	$$
	u_0=a,\   v_0=b\  \text{   et  pour   tout  entier   naturel  }   n,  \
	u_{n+1}=\dfrac{u_n+v_n}{2} \text{ et } v_{n+1} = \sqrt{u_n\cdot v_n}
	$$
	
	On admet que les suites sont bien définies et sont à termes positifs.
	
	 Écrire une fonction  Python qui prend en argument  deux réels positifs
	 $a$  et  $b$  et  un  entier  naturel $n$  et  qui  renvoie  $u_n$  et
	 $v_n$.   Faire  tourner   cette   fonction   pour  certaines   valeurs
	 particulières. Que peut-on conjecturer?

!!! {{ exercice()}}

    Soit   $f$  la   fonction  définie   sur  $[0,1]$   par  $∀x∈[0,1],   \
    f(x)=\dfrac{1}{3}\left(-e^{-x}+\ln(1+x^2)\right)$.
	Soit  $(u_n)_{n∈ℕ}$  la suite  des  réels  définie par  $u_0∈]0,1[$  et
	$∀n∈ℕ,\ u_{n+1}=u_n-\dfrac{f(u_n)}{f'(u_n)}$. On  admet qu'il existe un
	unique réel  $\ell∈]0,1[$ tel que  $f(\ell)=0$ et que  si $u_0\neq\ell$
	alors $∀n∈ℕ, \ u_n\neq \ell$.
	
	Écrire une fonction Python, prenant en  entrée un réel $a$ et un entier
	$n$  et renvoyant  la liste  des valeurs  de $(u_0,u_1,...,u_n)$  quand
	$u_0=a$.



!!! {{ exercice()}}

	On  définit pour  tous entiers  $k$  et $m$  tels que  $0  ≤ k  ≤ m,  \
	I_{k,m}=\displaystyle\int_0^1x^k(1-x)^{m-k}dx$.
	
	1. Déterminer $I_{0,m}$
	2. Soit un entier  $k$ vérifiant $0 ≤ k ≤  m-1$. Déterminer ue relation
	   de récurrence entre $I_{k+1,m}$ et $I_k,m$.
	3. Écrire une  fonction Python notée `I` qui reçoit  en argument $k$ et
	   $m$ et qui permet de calculer $I_{k,m}$.



## Suites logistiques : étude de populations (1er épisode)


### Malthus

En 1798, le londonien Thomas Robert MALTHUS (1766-1834) propose une modélisation de la
population  humaine  suite à  une  loi  passée  par  le prime  minister  de
l'époque, William  Pitt, qui aidait financièrement  les familles nombreuses
pauvres.  Malthus  affirme   que  la  population  croit   selon  une  suite
géométrique alors que la progression des ressources serait arithmétique. Il
affirme donc  que la  loi va  entraîner le pays  à sa  perte et  qu'il faut
contrôler les naissances.

Il  est assez  aisé de  voir  de nos  jours  que le  modèle malthusien  est
absurde. Cependant,  il a pu  être vérifié  au XIXe  siècle en
Australie.  27 lapins  y ont  été introduits  en 1859  pour satisfaire  les
pulsions de chasseur  d'un certain Austin. 6 ans plus  tard, on estimait la
population à  22 millions de  lapins, ceux-ci  progressant à la  vitesse de
100km par an...(cf [cet
article](https://www.theguardian.com/environment/2022/aug/23/australias-rabbit-invasion-traced-back-to-single-importation-of-24-animals-in-1859-study-finds)).
De  nos  jours,  la  lutte  continue  avec  l'introduction  plus  ou  moins
hasardeuse de virus. Il reste 200 millions de lapins issus de ces 5 mères de
1859.

![rabbit](./IMG/rabbit.jpg)


### Présentation du problème 

Le mathématicien Belge Pierre-François Verhulst (28
  octobre 1804 - 15 février  1849) proposa en 1838 un modèle d'évolution
  des  populations  animales  qui  porte  son  nom  et  qui  rompt  avec
  l'habituelle  croissance exponentielle.  On suppose  qu'une population
  vaut  $p_n$  à   un  certain  instant  et  qu'il   existe  une  valeur
  d'équilibre $e$ telle  que la population tend à  y revenir avec autant
  de force  qu'elle s'en écarte.  Il existe donc un  coefficient positif
  $k$ tel que: 
  
$$ 
\dfrac{p_{n+1}-p_n}{p_n} = -k(p_n - e)
$$


  
!!! {{ exercice()}}

    Déduisez-en qu'il existe une constante $R \geqslant 1$ telle que: 
	
	$$
    p_{n+1}=Rp_n(1-\frac{k}{R}p_n)
	$$ 
	
	puis qu'il existe une valeur maximum de la population $p_{\text{ max}}$
	(en utilisant le fait que la population doit rester positive...)
	
	En déduire que :
	
	$$
	u_{n+1}=R u_n(1-u_n)
	$$ 


	en notant $u_n$ le rapport $\frac{p_n}{p_{\text{max}}}$.
	
	

Depuis Verhulst  on désigne par  *suite logistique*  ce type de  suite.  Il
faut bien sûr considérer la plus ancienne des définition du mot:

**LOGISTIQUE** n.f.  **1.** (1611) Anc. nom  de la partie de  l'algèbre qui
traite des quatre règles. 


car nous  n'utiliserons que les quatre opérations  arithmétiques de base
mais n'étudierons pas les problème de ravitaillement des armées.


!!! {{ exercice()}}

    Dans  quel   intervalle  varie   R  ?  On   connaît  déjà   une  valeur
    minimale.  Pour la  valeur  maximale, déterminer  quelle  peut être  la
    valeur maximale prise par $u_n$. 
	Si on note $f_R$ la fonction $x\mapsto Rx(1-x)$, étudiez cette fonction
	et  étudier son  éventuel  maximum. Avec  `pyplot`, tracer  différentes
	représentations graphiques de $f_R$ et conclure.
	

Étudier la  suite $(u_n)$,  c'est étudier le *système dynamique*
défini   par   la   fonction    $f$.   L'ensemble   des   $x$,   $f(x)$,
$f(f(x))$,...,$f^n(x)$,... est appelé l'*orbite* de $x$.


### Exploration graphique



Peut-on  se contenter  d'observer le  comportement d'une  suite? L'outil
informatique permet-il de se passer d'une exploration théorique?

Commençons donc  par explorer  au petit bonheur  le comportement  de ces
suites à l'aide de Python.


!!! {{ exercice()}}

    Que fait `escargot`?  Comment interpréter les résultats suivants? 
	
	```python
	import matplotlib.pyplot as plt
	import numpy as np

	def escargot(R, u0, n) :
		f = lambda x : R*x*(1 - x)
		X = np.linspace(0, 1)
		plt.plot(X, X)
		plt.plot(X, f(X))
		Ordo = []
		u = u0
		for k in range(n):
			Ordo.extend( [u,f(u)] )
			u = f(u)
		plt.plot([u0] + Ordo, Ordo + [u])
		
	def viz(R):
		escargot(R, 0.2, 200)
		plt.show()
	```
	Réécrire `escargot` pour visualiser certains des exercices
	proposés un peu plus haut ou en TD.


En version animée:

```python

from matplotlib import animation

fig = plt.figure()
ax = plt.axes()
ligne, = plt.plot([],[])
axe, = plt.plot([],[])
courbe, = plt.plot([],[])
plt.xlim(0,1)
plt.ylim(0,1)
texte = ax.text(0.02, 0.95, '')

def init():
    X = np.linspace(0, 1)
    axe.set_data(X, X)
    courbe.set_data([], [])
    ligne.set_data([], [])
    texte.set_text('')
    return ligne,courbe,axe,texte
    
def anim_escargot(R, u0, n) :
    f = lambda x : R*x*(1 - x)
    X = np.linspace(0, 1)
    courbe.set_data(X, f(X))
    texte.set_text('R = %.2f' % R)
    Ordo = []
    u = u0
    for k in range(n):
        Ordo += [u, f(u)]
        u = f(u)
    ligne.set_data([u0] + Ordo, Ordo + [u])
    return ligne, courbe, axe, texte

anim = animation.FuncAnimation(fig, lambda r : anim_escargot(1+r*0.01, 0.2, 50),init_func=init, blit=True, frames=300, interval=120)
plt.show()
```





### Diagramme de Feigenbaum

Mystère...comment interpréter ce code et ses résultats ?...

```python
def ucent(r, u0, nb) :
    f = lambda x : r*x*(1 - x)
    tmp = u0
    for k in range(100) :
        tmp = f(tmp)
    res = []
    for k in range(nb) :
        tmp = f(tmp)
        res.append(tmp)
    return ([r for _ in range(nb)], res)

def feig(r1, r2, pas, u0, nb) :
    for r in (r1 + i*pas for i in range(int((r2 - r1)/pas))) :
        X, Y = ucent(r, u0, nb)
        plt.scatter(X, Y, marker='.', s=1)
    plt.show()
```


## Suites récurrentes linéaires d'ordre 2

Reprenez   des  exemples   vus  en   classe,  par   exemple  la   suite  de
Fibonacci. Tracer  la représentation  graphique des  premiers termes  de la
suite  en  utilisant  la  relation de  récurrence  (version  impérative  et
récursive) puis en utilisant l'expression  explicite d'un terme de la suite
grâce aux formules du cours.

## Un exercice d'écrit d'Agro-Véto TB

Considérons la suite $(u_n)$ définie par 

$$
\begin{cases}
u_0=e-1\\
u_{n+1}=(n+1)u_n-1
\end{cases}
$$

1. Créer  une fonction Python  qui prend en argument  un entier $n$  et qui
   renvoie une valeur approchée de $u_n$.
   Représenter graphiquement la suite selon la méthode de votre choix.
   
1.  On  note  $I_n=\displaystyle\int_0^1(1-t)^ne^t{\rm  d}t$. 
	En utilisant la méthode du point milieu, déterminer une fonction Python
	qui calcule une valeur approchée de $I_n$.
	
	**Remarque**: *Il  existe un moyen  de calculer directement  une valeur
	approchée  d'une  intégrale  grâce  à*  `scipy.integrate.quad(fonction,
	debut, fin)`. *Utilisez-la pour vérifier vos calculs*.

1.  Établir  une
   relation de récurrence entre $I_{n+1}$ et $I_n$ puis calculer $I_0$.
   
1. Tracer  sur un même  graphique des termes  des suites de  terme généraux
   $u_n$, $\dfrac{1}{n+1}$ et  $\dfrac{e}{n+1}$

1. Démontrer     que    pour     tout    entier     naturel    $n$,
   $\dfrac{1}{n+1}≤u_n≤\dfrac{e}{n+1}$. Conclusion?

1. On      introduit     les     suites     de      termes     généraux
   $S_n=\displaystyle\sum_{k=0}^n\dfrac{1}{k!}$                           et
   $S'_n=S_n+\dfrac{1}{n×n!}$.
   Déterminer  des fonctions  Python qui  renvoient des  approximations des
   termes de ces deux suites.
   
1. Démontrer que ces suites sont adjacentes. 

1. Démontrer que $u_n=n!(e-S_n)$.

1. Retrouver ainsi un résultat de cours sur la suite $(S_n)$.

1. Tracer des termes de la suite $n×u_n$.

1. Démontrer que $\dfrac{1}{(n+1)!}≤e-S_n≤\dfrac{1}{n×n!}$ pour tout entier
   naturel non nul $n$. Conclure. 


## Un exercice d'écrit d'Agro-Véto BCPST (Modélisation mathématique et Informatique)



Les programmes sont à rédiger en langage Python. 
Avant chaque algorithme,  on écrira brièvement le raisonnement  suivi et la
formule qu'il est censé calculer. 

### I- Recherche du minimum d'une fonction

Soit $f:t\mapsto 72-32e^{-t}+32e^{-10t}$.

1. Écrire une fonction d'en-tête `f(t)` prenant en entrée $t$ (un réel) et qui renvoie la valeur de $f(t)$.


2. On  définit la fonction  d'en-tête `minimum_f(N)` prenant en  entrée $N$
   (un entier) par le code (incomplet) suivant: 
   
    ```python
	   def minimum_f(N):
		   Lt1 = np.linspace(0, 5, N)
		   Ly1 = []
		   for k in range(0, N):
			   Ly1.append(f(Lt1[k]))
		   ...
		   ...
		   return ...
    ```
    Décrire en quelques mots ce que contiennent les variables `Lt1` et `Ly1` à l'issue des lignes 2 à 5.

3.  Compléter sur la copie (avec autant de lignes que nécessaire) la
    fonction `minimum_f` pour qu'elle renvoie une liste composée des deux éléments suivants :
	
	- une valeur approchée $m$ du minimum de $f$ sur $[0;5]$ ;
	- le temps $t_m$ en lequel cette valeur approchée est atteinte (c'est-à-dire $m=f(t_m)$).

    *On veillera à n'écrire qu'une seule boucle après celle des lignes 4 et 5 et à ne pas utiliser la commande `min`*

4.  On teste la fonction `minimum_f` sur différentes valeurs de $N$.
	
	Voici les résultats obtenus (tronqués à 4 chiffres après la virgule) :
	
	- pour $N_1=20$, la fonction donne $m_1=49.7070$ et $t_{m_1}=0.2631$ ;
	- pour $N_2=20\ 000$, la fonction donne $m_2=49.7012$ et $t_{m_2}=0.2557$.
   
    Il y a une différence entre $t_{m_1}$ et $t_{m_2}$.

    - Cette différence était-elle prévisible et à quoi est-elle due ?
	- Est-ce $t_{m_1}$ ou $t_{m_2}$ qui devrait être le plus proche de la valeur exacte cherchée ?

5.  On teste  maintenant sur  une troisième  valeur située  entre $N_1$  et
   $N_2$   :   pour   $N_3=50$,   la  fonction   donne   $m_3=49.9370$   et
   $t_{m_2}=0.3061$. Commenter ce résultat.


### II- Recherche du point d'annulation d'une fonction

   Pour une fonction $F:I\rightarrow\mathbb{R}$ donnée, avec $I$ un intervalle de $\mathbb{R}$, on s'intéresse à la résolution approchée de l'équation $F(x)=0$.
   On suppose que $F$ s'annule en un point appelé $x^*\in I$ et que $F$ soit suffisamment régulière pour que les calculs suivants soient bien définis.
   L'objectif est d'obtenir une valeur approchée de $x^*$.

   On utilise la méthode de Newton, qui consiste à définir la suite $\left(u_n\right)_{n\in \mathbb{N}}$ par :

   $ 
     \left\{ \begin{array}{ll}
 	  u_0 \text{ à choisir } & \\ u_{n+1}=u_n-\frac{F(u_n)}{F'(u_n)} & \text{ pour } n\geqslant 0
      \end{array}\right. .
   $

   On admet que, sous des hypothèses non précisées ici, cette suite
   converge vers $x^*$ et que, s'il y a deux solutions à l'équation $F(x) =
   0$, alors selon le choix de $u_0$ la suite va converger vers une solution
   ou vers l'autre.


6. Soit $e > 0$. À l'aide d'une boucle `while`, écrire une fonction d'en-tête `Newt(F, G, u0, e)` qui calcule les termes de
   la suite en s'arrêtant au premier terme $u_{n_0}$ vérifiant 
   $|F(u_{n_0})|<e$ (en supposant qu'un tel $n_0$ existe).  
    Les arguments d'entrée sont: $F$ une fonction, $G$ une fonction qui correspond à $F'$, $u_0$ le premier terme de la suite et $e$.
    La fonction doit renvoyer $u_{n_0}$.

7.	Par rapport à l'objectif de résoudre de façon approchée l'équation $F(x) = 0$, expliquer à quoi correspond la quantité $e$ utilisée dans l'écriture de `Newt`. 
	Parmi les choix suivants pour $e$, indiquer (avec justification
	succincte) celui qui  semble le plus pertinent  pour obtenir la
	meilleure précision sur la valeur approchée : $e_1 = 100$, $e_2
	= 1$, $e_3 = 10^{-8}$. 

8.	Toujours  par  rapport  à   l'objectif  de  résoudre  de  façon
	approchée l'équation  $F(x) = 0$, justifier  s'il est pertinent
	ou  pas d'effectuer  la modification  suivante sur  la fonction
	`Newt`  :  renvoyer  le premier  terme  $u_n$  vérifiant
	$F(u_{n}) = 0$. 

9.	À la question 1., on a supposé l'existence d'un $n_0$ vérifiant $|F(u_{n_0})|<e$. 
	Que  se passe-t-il  dans la  fonction `Newt`  si un  tel
	$n_0$ n'existe pas ? 

10.	On  souhaite modifier  la fonction  `Newt` de  manière à
	fixer   un  nombre   maximal   d'itérations   pour  la   boucle
	`while`.
	Écrire une nouvelle fonction d'en-tête `NewtS(F, G, uO, e, nder)`, basée sur la fonction `Newt`, telle que : 
	-		s'il existe un terme $u_{n_0}$ tel que $|F(u_{n_0})|<e$
			avec $n_0\leqslant n_{\text{der}}$, la fonction renvoie
			$u_{n_0}$ ; 
	-   si  le   nombre  d'itérations   dépasse  (strictement)   la  valeur
	  $n_{\text{der}}$, la boucle est stoppée et la fonction renvoie le booléen `False`. 

11. On définit  les fonctions suivantes (où  $f$ a été définie  à la partie
    I) : 
	
	```python
	def F1(t):
	     return f(t)-70
    def G1(t) :
	    return 32*np.exp(-t)-320*np.exp(-10*t) 
    ```
	puis on exécute pour `e = 10**-14` et `nder = 9` les deux appels suivants : 
		
	- `NewtS(F1, G1, 2, e, nder)` qui renvoie $2.7725887222252261$ ; 
	- `NewtS(F1, G1, 0.1, e, nder)` qui renvoie $0.0072246696343453579$.
	
	Interpréter ces résultats en lien avec la partie 1.1.  


## Encore des planches d'oral AV


### Dichotomie


On considère $n$ un entier naturel supérieur ou égal à 2.
On introduit la fonction $f_n$ définie par :

$$
f_n(x)=x^n+x-n
$$

1. Justifier que  pour tout $n≥2$, $2^n≥n$ et en  déduire qu'il existe
    un unique $ε_n∈ℝ^*_+$ tel que $f_n(ε_n)=0$ et que $1≤ε_n≤2$.
1. On souhaite étudier la suite $(ε_n)_{n∈ℕ^*}$.
   
   1. Démontrer que $1≤ε_n≤n^{1/n}$.
   1.  En déduire que la suite $(ε_n)_{n∈ℕ^*}$ est convergente et sa
      limite $\ell$.
	  
1. En utilisant l'algorithme de dichotomie, écrire une fonction
   `dichotomie` en Python qui prend en  entrée un entier $n$ et qui
    renvoie un réel $x$ tel que $0≤x-ε_n≤\dfrac{1}{2n}$.
	
	
### Suites récurrentes doubles


  On pose  $f:x\mapsto e^{-x^2/2}$. La  fonction $f$ étant de  classe $\scr
  C^\infty$     sur      $ℝ$,     on     pose     pour      tout     $n∈ℕ$,
  $H_n(x)=(-1)^ne^{-x^2/2}f^{(n)}(x)$ où  $f^{(n)}$ est la  dérivée $n$-ème
  de $f$.

1. Calculer $f^{(n)}(x)$ pour tout $x∈ℝ$, pour
      $n∈\bigl\{0,1,2\bigr\}$.
1.  En déduire $H_j(x)$ pour chaque $j$ dans $\bigl\{0,1,2\bigr\}$.

1. Soient $h$ et $g$ deux fonctions de classe $\scr C^\infty$ sur
        $ℝ$.  Rappeler   la  formule   qui  permet  de   calculer  $(h\cdot
        g)^{(n)}$  en   fonction  des   dérivées  successives  de   $h$  et
        $g$. Rappeler le nom de cette formule.
		
1. Démontrer que
        $f^{(n+1)}(x)=-xf^{(n)}(x)-\binom{n}{1}f^{(n-1)}(x)$ pour  tout $x$
        in $ℝ$ puis que $H_{n+1}(x)=xH_n(x)-nH_{n-1}(x)$ pour tout $x∈ℝ$.
		
1. Démontrer par récurrence forte que $H_n$ est une fonction
      polynomiale de degré $n$ dont le coefficient dominant est égal à 1 et
      dont le coefficient de degré $n-1$ est nul.

1. Compléter le programme suivant pour que la fonction
    `hermite`  de  paramètre  `N`  renvoie  en  sortie  les
      coefficients  du polynôme  $H_N$ triés  dans une  liste dans  l'ordre
      croissant  de leur  degré  (Exemple: si  $P(X)=3X^3-4X^2+7$ alors  la
      liste qui représente $P$ est `[7, 0, -4, 3]`.
    
```python
def hermite(N):
    H_precedent = [1]
    H_p = H_precedent
    H_courant = [0, 1]
    H_c = H_courant
    if N == 0:
        return H_p
    elif N == 1:
        return ...
    else:
        for n in range(2, N+1):
            H_suivant = [0 for k in range(n+1)]
            H_s = H_suivant
            H_s[0] = -(n - 1)*...
            for k in range(1, n - 1):
                H_s[k] = H_c[...] - (n - 1)*H_p[...]
            H_s[n - 1] = ...
            H_s[n] = ...
            H_p = ...
            H_c = ...
    return H_c
```
