---
summary: Mathématiques et Informatique en BCPST 1 à l'Externat - Nantes - Tests et devoirs - Sources TeX et PDF
---




# Tests et devoirs 2024/25


- [Quelques conseils](conseilsDS) pour préparer les épreuves écrites.


## Interrogations de cours

- Test de rentrée [PDF](./INT_1_24.pdf) et [TEX](INT_1_24.tex)



## Devoirs surveillés



- DS1 [PDF](./DS1_24.pdf) et [TEX](DS1_24.tex) et [Corrigé](./DS1_24_corrige.pdf)

- DS2 [PDF](./DS2_24.pdf) et [TEX](DS2_24.tex)  et [Corrigé](./DS2_24_corrige.pdf)

- DS3 [PDF](./DS3_24.pdf) et [TEX](DS3_24.tex) et [Corrigé](./DS3_24_corrige.pdf)


- DS4 [PDF](./DS4_24.pdf) et [TEX](DS4_24.tex) et [Corrigé](./DS4_24_corrige.pdf)
- DS5 [PDF](./DS5_24.pdf) et [TEX](DS5_24.tex) <!--et [Corrigé](./DS5_23_corrige.pdf)-->

- DS6 [PDF](./DS6_24.pdf) et [TEX](DS6_24.tex) <!-- et [Corrigé](./DS6_23_corrige.pdf)-->

- DS7 [PDF](./DS7_24.pdf) et [TEX](DS7_24.tex) 


- DS8 [PDF](./DS8_24.pdf) et [TEX](DS8_24.tex) 


- DS9 [PDF](./DS9_24.pdf)  et [TEX](DS9_24.tex) 










# Tests et devoirs 2023/24


## Interrogations de cours

- Test de rentrée [PDF](./Test_1_23.pdf) et [TEX](Test_1_23.tex)
- Test semaine 38 [PDF](./Test_2_23.pdf) et [TEX](Test_2_23.tex)
- Test semaine 45 [PDF](./Test_3_23.pdf) et [TEX](Test_3_23.tex)

## Devoirs surveillés



- DS1 [PDF](./DS1_23.pdf) et [TEX](DS1_23.tex) et [Corrigé](./DS1_23_corrige.pdf)

- DS2 [PDF](./DS2_23.pdf) et [TEX](DS2_23.tex)  et [Corrigé](./DS2_23_corrige.pdf)

- DS3 [PDF](./DS3_23.pdf) et [TEX](DS3_23.tex) et [Corrigé](./DS3_23_corrige.pdf)


- DS4 [PDF](./DS4_23.pdf) et [TEX](DS4_23.tex) et [Corrigé](./DS4_23_corrige.pdf)
- DS5 [PDF](./DS5_23.pdf) et [TEX](DS5_23.tex) <!--et [Corrigé](./DS5_23_corrige.pdf)-->

- DS6 [PDF](./DS6_23.pdf) et [TEX](DS6_23.tex) <!-- et [Corrigé](./DS6_23_corrige.pdf)-->

- DS7 [PDF](./DS7_23.pdf) et [TEX](DS7_23.tex) 


- DS8 [PDF](./DS8_23.pdf) et [TEX](DS8_23.tex) 


- DS9 [PDF](./DS9_23.pdf)  et [TEX](DS9_23.tex) 

