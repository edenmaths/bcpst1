# Conseils pour aborder les épreuves écrites de mathématiques 



Les conseils  qui suivent sont  en grande  partie extraits des  rapports de
jury  des  dernières années,  que  je  vous  invite  par ailleurs  à  aller
consulter à l'occasion. 

## 1. Commencer par lire le sujet et établir une stratégie

Une lecture  préalable et attentive de  la totalité du sujet  (y compris le
préambule et  les sous-titres s'il y  en a) est nécessaire  pour comprendre
son articulation, assimiler les notations et hiérarchiser les difficultés. 

Cette  lecture (qui  peut  durer une  dizaine de  minutes  pour de  longues
épreuves) est d'autant plus 
nécessaire qu'un sujet de concours est toujours « trop long » : il est
impossible de tout faire. Pour avoir une bonne note, il faut privilégier la
qualité  à  la   quantité  et  sélectionner  avec  soin   les  questions  à
aborder. Dans cette optique, voici quelques conseils stratégiques : 

- Commencez par l'exercice  dont le thème vous plaît le  plus, c'est un bon
  moyen d'assurer un minimum de points  et de vous mettre en confiance pour
  la suite. 
- Si vous bloquez  plus de dix minutes sur une  question, mettez-là de côté
  et passez  à la suite.  Vous pourrez toujours  y revenir plus  tard, s'il
  vous reste du temps. 
-  Ne  négligez  pas  les questions  d'informatique,  elles  sont  toujours
  largement valorisées dans le barème. 
-  Il  arrive  que les  questions  de  la  fin  d'un exercice  donnent  des
  indications  sur les  résultats à  obtenir au  début :  utilisez-les pour
  vérifier la cohérence de vos réponses. 
-  Une heure  avant la  fin de  l'épreuve, faites  une liste  des questions
  faciles  qui n'ont  pas encore  été traitées  (une bonne  connaissance du
  cours est indispensable pour les identifier) : c'est le moment de vous en
  occuper, elles constituent  le meilleur rapport « temps  passé sur nombre
  de points ». 


## 2. Soigner la présentation

- Il est important d'écrire lisiblement,  dans une langue correcte, de bien
  séparer les questions (notamment en les numérotant), de clairement mettre
  en  évidence  les   résultats  obtenus  en  les  soulignant   ou  en  les
  encadrant.  Il  est dangereux  d'égarer  le  correcteur en  traitant  les
  questions dans le désordre. La copie  rendue doit être structurée, et ses
  pages numérotées.

- Évitez l'usage  abusif d'effaceur (qui traverse parfois la  copie et rend
  le verso  difficilement lisible)  et méfiez-vous des  stylos «  frixion »
  (leur  système d'effacement  est basé  sur la  chaleur et  il est  facile
  d'effacer  plusieurs épaisseurs  de page  sans y  prendre garde).  En cas
  d'erreur, la  rature nette  à la  règle est souvent  la solution  la plus
  propre. 

## 3. Faire preuve de rigueur dans sa rédaction

- Une bonne connaissance des notions et résultats du cours est un prérequis
  indispensable à la résolution correcte de nombreuses questions. Lorsqu'un
  théorème est invoqué, il faut notamment lister ses hypothèses et vérifier
  qu'elles sont toutes  satisfaites. Les calculs doivent  figurer au propre
  sur la copie  et faire l'objet de justifications. Il  ne faut pas laisser
  de résultat incomplet ou non simplifié. 

- N'hésitez pas à  faire usage de votre brouillon pour  mettre vos idées au
  clair avant  de vous lancer dans  la rédaction d'une réponse  et pensez à
  vous relire  après-coup, pour vérifier  la cohérence et  la vraisemblance
  des résultats obtenus. 

-  S'obstiner  à  soutenir  des conclusions  manifestement  absurdes  coûte
  cher.  Si vous  trouvez une  incohérence sans  identifier l'erreur  qui y
  mène, vous devez au minimum le signaler dans votre copie. 

## 4. Rester honnête

- Renoncez  à la  tentation du  bluff :  les correcteurs  sont suffisamment
  entraînés et vigilants  pour détecter les élèves qui  trichent dans leurs
  calculs pour obtenir le «bon résultat  ». Ils deviennent ensuite très peu
  indulgents dans la correction du reste de la copie. 

- Si vous ne savez  pas traiter une question, indiquez nettement
  que  vous en  admettez le  résultat pour  la suite.  Si l'omission  d'une
  question  ou  un  résultat  admis   n'apporte  pas  de  point,  cet  acte
  d'honnêteté est très apprécié. 

- Par  ailleurs, si vous  repérez des  ambiguïtés ou oublis  dans l'énoncé,
  signalez-le. Cela sera toujours valorisé. 

## 5. Reprendre sa copie après-coup

Pour progresser,  le plus important  est d'apprendre de ses  erreurs. Comme
pour les exercices du cours, la lecture du corrigé ne
suffit pas. Le travail de reprise doit mettre l'accent sur la compréhension
de ce qui a posé problème.  Posez-vous la question suivante : « Qu'estce qui
m'a fait  perdre le plus de  points? ». Plusieurs réponses  sont possibles,
par exemple : 

- le stress de l'épreuve,
- les erreurs de calcul,
- la méconnaissance du cours,
- des confusions d'objets (par exemple, manipuler une suite comme si c'était une fonction),
- une rédaction trop peu rigoureuse,
- une mauvaise compréhension du sujet,
- trop de temps perdu sur une question donnée,
- ...

Une  fois vos  faiblesses identifiées,  vous  pourrez les  corriger en  les
ciblant spécifiquement.  N'hésitez pas  à solliciter votre  professeur pour
voir s'il partage  votre analyse de la situation. Il  pourra également vous
proposer des stratégies individualisées pour progresser. 

## 6. Quelques conseils d'anciens

- « Ayez confiance en vous. »


- « Il  faut être ambitieux, toujours vouloir faire  mieux notamment gagner
  en rigueur, mais toujours dans un  souci de communauté, de la compétition
  pour créer une émulation intellectuelle et collective. » 

-  «   Apprenez  rigoureusement  toutes   les  hypothèses  de   toutes  les
  propositions/théorèmes du  cours, ce sont des  points à ne pas  perdre en
  DS. 

- « Bien reprendre les DS en essayant de refaire les exercices, pas
  seulement en lisant la correction et  soigner sa rédaction, dans tous les
  exercices pour prendre de bons réflexes. » 

- « Il ne faut surtout pas négliger Python. »

- « En devoir surveillé, privilégier la qualité que la quantité et ne pas
  oublier les exercices Python. » 

