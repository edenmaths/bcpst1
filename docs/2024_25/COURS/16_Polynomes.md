{{ chapitre(16, "Polynomes")}}


Cours au format 


- [PDF](../LATEX/Poly_poly_23.pdf) (version longue) et [version courte](../LATEX/Poly_poly_24.pdf)

- ses sources [TEX](../LATEX/polynomes_23.tex) (version longue) et [version
  courte](../LATEX/polynomes_24_short.tex) 

- et les [fonctions Python](../polynomes_py/) associées.

