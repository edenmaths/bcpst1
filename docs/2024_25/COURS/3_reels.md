{{ chapitre(3, "Nombres réels")}}



Cours au format 

- [PDF](../LATEX/Poly_reels_24.pdf) 

- et ses sources [TEX](../LATEX/reels24.tex)



!!! note "" 
	![Placeholder](./IMG/reel.jpeg){ align=right width=400}
	Si le *Reel* n'a  plus de secret pour les Écossais, les  nombres du même nom
	en réservent encore de nombreux aux élèves sortant du lycée.
	
	Par exemple,  quel est le  nombre réel qui  suit 2 ?  Est-ce vrai qu'il  y a
	autant de nombres  réels entre 0 et 1  que dans $ℝ$ tout entier  ? Les Grecs
	croyaient   que  tout   nombre  pouvait   s'écrire  sous   la  forme   d'une
	fraction  mais  nous  avons  depuis  démontré  que  $\sqrt{2}$  n'était  pas
	rationnel.  Mais $\sqrt{2}$  appartient  à une  famille  de nombres  appelés
	algébriques      car     solutions      d'une      équation     du      type
	$a_0+a_1x+a_2x^2+\cdots+a_nx^n$  avec  les  $a_i$  appartenant  à  $ℕ$.  Les
	nombres  qui  ne sont  pas  algébriques  sont appelés  *transcendants*  sont
	beaucoup plus nombreux et 
	pourtant on en  connaît beaucoup moins. On  a démontré  à la  fin du XIXe
	que $π$ et e étaient transcendants mais  par exemple on ne sait toujours pas si
	${\rm e} + π$ est transcendant ou $π^π$...


# Ordre dans $ℝ$

Au tableau


# Carrés, puissances, racines, etc.

Au tableau



# Aparté historique : naissance de l'algèbre

![Placeholder](./IMG/al.jpg){ align=left width=150}

Voici  notre héros  أبو عبد  الله محمد  بن موسى  الخوارزمي ou  si vous  préférez
*Abdallah Muhammad ibn Musa al-Khwarizmi*, né dans l'actuel Ouzbékistan vers 780. 

Son livre le plus célèbre, qu'il  a écrit entre 813 et 833 alors qu'il
  travaillait à *la maison de la sagesse*  de Bagdad, se nomme الكتاب المختصر في
  حساب  الجبر  والمقابلة  (*Al-Kitab  al-mukhtasar fi  hisab  AL-JABR*)  ce  qui
  signifie *l'Abrégé  du calcul par la  restauration et la comparaison*.  On n'y
  trouve ni notations ni nombres! Tous les nombres et calculs sont décrits par
    des phrases à l'aide des termes :

- nombres (*dirham*)

- racines (*jidhr* : ce qui est caché)

-  choses (*shay*  transposé en  espagnol  par *xay*  qui a  donné notre  fameux
  $x$...)
  
- biens (*mâl*...) qui désigne le carré d'une racine.

-  *Al-jabr* signifie  *réduction* au  sens  de *réduction  d'une fracture*.  Un
  *Algebrista* en espagnol désigne même un *rebouteux*.
  



 L'al-jabr
consiste  à  réduire  l'équation  en  éliminant  les  soustractions  par
addition de termes dans les deux membres.

En effet, à  l'époque d'Al-Khwarizmi, on ne travaillait qu'avec des entiers
positifs.


Par exemple:

$$
x^2 = 40x - 4x^2
$$ 

 est transformé, par al-jabr, en
 
$$
x^2 + 4x^2 = 40x
$$

puis

$$
5x^2= 40x
$$


En  effet, Al-Khawarizmi  nomme les  termes soustraits  (comme  $4x^2$ dans
l'exemple précédent) : nâqis (*terme enlevé*).  Le même mot est employé
pour désigner le membre manquant d'un amputé.

  

 **Al-jabr consiste donc à restaurer ce qui est manquant dans une équation.**

*Al-muqabala* signifie *confrontation*.



$$
x^2 + 5 = 40x + 4x^2
$$

contient des carrés dans les deux membres, chaque
membre est pourtant une somme.


Al-muqabala consiste donc à soustraire une quantité afin que des quantités de
même type  (dirham, racine ou  carré) ne puissent  se trouver à  la fois
dans les deux membres de l'équation. 
Dans 

$$
x^2 + 5 = 40x + 4x^2
$$

 on soustrait $x^2$ pour obtenir 

$$
5 = 40x + 3x^2
$$


On  retrouve  alors un  des  six  types  d'équations qu'Al-Khwarizmi  a
étudié. Une méthode de résolution générale de chaque type était proposée
ainsi qu'une démonstration, souvent géométrique.

Voici un problème proposé par Al-Khwarizmi:


!!! quote "Problème proposé par Al-Khwarizmi"

	*Un bien et dix de ses racines égale trente-neuf dirhams.*

En voici une solution illustrée géométriquement (faites un dessin) :

!!! quote "Solution"

	*Il y  a une autre  figure qui mène  également à cela  et c'est :  la surface
	(AB), étant  le bien, nous  voulons lui ajouter  l'équivalent de dix  de ses
	racines. Nous  divisons par deux  les dix,  elles deviennent cinq,  nous les
	transformons en deux surfaces sur les flancs  de la surface (AB), et ce sont
	les deux  surfaces (J), (N).  La longueur de  chacune des deux  surfaces est
	cinq coudées -et c'est la moitié des dix racines- et sa largeur est comme le
	côté de  la surface (AB). Il  nous reste alors  un carré dans l'angle  de la
	surface (AB), et c'est cinq par cinq, et c’est la moitié des dix racines que
	nous avons ajoutées  sur les flancs de la première  surface.  Or nous savons
	que la première  surface est le bien  et que les deux surfaces  qui sont sur
	ses deux  flancs sont  dix racines. Tout  cela est donc  trente neuf,  et il
	reste pour compléter la  surface la plus grande, un carré  de cinq par cinq,
	et c'est vingt-cinq.  Nous l'ajoutons à trente neuf afin  que se complète la
	surface  la  plus  grande,  et  c'est la  surface  (DE).  Tout  cela  vaudra
	soixante-quatre.  Nous prenons  sa racine, et c'est huit, et  c'est l'un des
	côtés de la surface la plus grande.  Si nous lui retranchons l'équivalent de
	ce que  nous lui avons ajouté  -et c'est cinq-  il reste trois, et  c'est le
	côté de la  surface ( AB ) qui est  le bien, et c'est sa racine;  et le bien
	est neuf.* 
	
	
!!! {{ exercice()}}

    Faites un joli dessin et réfléchissez à la méthode proposée par Al.
	
	Essayez ensuite avec  $x^2+6x=40$ puis avec $x^2+6x=-10$...

Nous  venons  de voir  qu'Al-Khwarizmi  n'utilisait  pas de  notations
  spéciales pour désigner les équations qu'il résolvait.


Il a fallu des siècles pour arriver au stade actuel.



- Diophante, IIIe siècle : $\Delta^{\Upsilon}\delta\ \zeta\gamma\ \epsilon\sigma\tau\iota \ \iota$
	
- Nicolas Chuquet, fin XVe siècle: $4^2\ \text{p}\ 3^1\text{ egault }10^0$

- Tartaglia, début XVIe: $4\text{q}\text{ p } 3\text{R} \text{ equale } 10\text{N}$

- Simon Stévin, fin XVIe: 4② $+3$① egales 10⓪
  
- François Viète, vers 1600: 4 in A quad $+3$ in A æquatur 10

- René Descartes, vers 1640: $4xx+3x\propto 10$

- Guillaume Connan, vers 2021: $4x^2+3x=10$



 Vous remarquez donc que  les notations sont relativement récentes mais
  on  sait  résoudre des  équations  depuis  des  millénaires:  il ne  faut  pas
  confondre mathématiques et notations ni litttérature et alphabet...







# Valeur absolue


Dans l'étude des suites,  tout va dépendre de la façon  de mesurer l'écart entre
les réels. Nous  allons les étudier en choisissant la  classique valeur absolue,
mais  il  existe  des  univers  étranges  qui  existent  dans  les  esprits  des
mathématiciens et parfois dans l'Univers où ce n'est pas le cas.

L'illustre [CAUCHY](https://bibmath.net/bios/index.php?action=affiche&quoi=cauchy) commence d'ailleurs  son cours en définissant  la valeur absolue
(qu'il nomme  *valeur numérique* et  définie comme la *base* d'un
nombre sans son signe,  un peu comme Vader et Obiwan sont de  même force mais de
signe opposés). 


!!! {{ definition("Valeur absolue") }}
    
    Soit $x∈ℝ$. On appelle **valeur absolue** de $x$ et on note $|x|$ le
    plus grand réel entre $x$ et $-x$:
  
    $$
    |x|={\rm max}(x,-x)
    $$
  
  

On en déduit les petits résultats suivants :

!!! {{ theoreme("Propriétés de la valeur absolue")}}

	1.  $|x| \geqslant 0$
	1.  $|x|^2=x^2$
	1.  $|x|=\sqrt{x^2}$
	1.  $|x|=0 ⟺x=0$
	1.  $|x\times y|=|x|\times|y|$


```python
def val(x: float) -> float:
	return x if x >= -x else -x
```



Il n'est peut-être pas inutile de faire le rappel suivant:


!!! {{ theoreme("Inégalité triangulaire")}}

    $$
	(∀(x,y)∈ℝ^2)(|x+y| \leqslant |x| + |y|)
	$$
	
	
# Partie entière

Le théorème suivant sera à moitié admis :

!!! {{ theoreme("Existence et unicité de la partie entière")}}

    Pour tout réel $x$, il existe un *unique* entier $ n \in ℤ $ tel que 
	
	$$ 
	n \leqslant x < n + 1 
	$$ 

	L'entier $n$ est appelé la *partie entière* de $x$, que l'on note $ \lfloor x \rfloor $.




Pour  s'habituer  à  manipuler  les   parties  entières,  voici  un  théorème  à
démontrer :

!!! {{ theoreme("Pseudo-linéarité")}}

    $$
	(∀x∈ℝ)(∀k∈ℤ)(\lfloor x+k\rfloor = k + \lfloor x \rfloor)
	$$




!!! {{ exercice()}}



	Est-ce que cela signifie que pour tout couple de réels $(x,y)$, on a $\lfloor
	x + y\rfloor = \lfloor x \rfloor + \lfloor y \rfloor$ ?




!!! {{ exercice()}}

    Le théorème 3-3 nous donne un encadrement de tout réel $x$ par deux
	entiers  successifs. Déterminez  de même  un encadrement  minimum de  $\lfloor
	x\rfloor$ en fonction de $x$.



# Théorème de la borne supérieure


!!! {{ definition("Partie minirée/majorée") }}
    
	On dit que la partie $A$ de $ℝ$ est **majorée** par $u\in ℝ$ si, et
	seulement si, on a : 
	
	$$
	\forall x(x\in A ⟹x\leqslant u)
	$$

	On dit que la partie $A$ est **minorée** par $v\in ℝ$ si, et
	seulement si, on a : 
	
	$$
	\forall x(x\in A⟹ v\leqslant x)
	$$


!!! {{ theoreme("Maximum (plus grand élément - PGE)")}}

    On dit que $g$ est le **plus grand élément**
	(ou élément **maximum**) de $A$ si, et seulement si, 
	
	$$
	\forall x((g\in A) \land (x\in A⟹ x\leqslant g))
	$$

	Si $g$ existe, il est unique.  On le note $\operatorname{max}(A)$.

	C'est le plus petit majorant de $A$.





!!! {{ theoreme("Minimum (plus petit élément - PPE)")}}

    On dit que $g$ est le **plus petit élément**
	(ou élément **minimum**) de $A$ si, et seulement si, 
	
	$$
	\forall x((g\in A) \land (x\in A⟹ x\geqslant g))
	$$

	Si $g$ existe, il est unique.  On le note $\operatorname{min}(A)$.

	C'est le plus grand minorant de $A$.



!!! warning "Danger !"

	 Il faudra bien distinguer les notions de majorant et de maximum : qu'est-ce
	 qui les différencie?


!!! {{ exercice()}}

    Pouvez-vous donner des majorants/maxima de $ℝ$? $ℕ$? $[0,1[$? $[0,1]$?


!!! {{ definition("Borne supérieure") }}
    Considérons $\mathscr{G}$ l'ensemble des majorants de $A.$ Si $
	\mathscr{G}$ admet un plus petit élément $s$, c'est la **borne supérieure**
	de $A.$ 
	
	On la note $\operatorname{sup}(A)$.


!!! {{ definition("Borne inférieure") }}
    Considérons $\mathscr{P}$ l'ensemble des minorants de $A.$ Si $
	\mathscr{P}$ admet un plus grand élément $i$, c'est la **borne inférieure**
	de $A.$ 
	
	
	
	On la note $\operatorname{inf}(A)$.



!!! {{ theoreme("Théorème (admis) de la borne supérieure/inférieure")}}

     Toute partie $A$ de $ℝ$ **non  vide et majorée** (resp. minorée) admet une
	borne supérieure (inférieure). 


!!! note "À retenir !"

	  Considérons une partie A de $ℝ$ non vide et majorée.
	  
	1.  ${\rm sup} A$ existe mais n'appartient pas forcément à A;
	1.  ${\rm max} A$ n'existe pas forcément. S'il existe, il appartient à A;
	1.  SI ${\rm max } A$ existe ALORS ${\rm max} A = ....$;
	1.  La borne supérieure est unique mais le nombre de majorants est infini.

On  déduit de  la définition  les propositions  suivantes qui  seront utiles  en
exercice :


!!! note "Caractérisation de la borne sup(inf)érieure"

	- Le réel $\alpha$ est la borne supérieure d'une partie $A$ de $ℝ$ si, et
	  seulement si:

    $$
       \begin{cases}
	   (∀ x ∈ A)(x \leqslant α)\\
	   (∀ ε >0)(∃ x ∈ A)(α-ε < x )
	   \end{cases}
    $$
	
	- Le réel $\alpha$ est la borne inférieure d'une partie $A$ de $ℝ$ si, et
	  seulement si :
	  
	$$
	  \begin{cases}
	  (∀ x ∈ A)(x \quad α)\\
	  (∀ ε >0)(∃ x ∈ A)(α ..... x )
	  \end{cases}
	$$
	  
	- Cas  plus simple: si  vous trouvez qu'un majorant  de A appartient  à A,
      c'est le maximum et en même temps la borne supérieure.


!!! {{ exercice()}}

    Quelle est la borne supérieure de $[0,1]$? de $[0,1[$?

