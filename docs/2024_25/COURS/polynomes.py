polynome = list # alias de type pour préciser qu'on travaille avec des polynômes mais ils spnt représentés par des listes

def normalise(P: polynome) -> polynome:
    assert len(P) >= 1, "la liste vide ne peut pas représenter un polynôme"
    while P[-1] == 0 and len(P) > 1:
        P = P[:-1]
    return P

def deg(P: polynome) -> int:
    P = normalise(P)
    if P == [0]:
        return -float("inf")
    else:
        return len(P) - 1

def plus(A: polynome, B: polynome) -> polynome:
    A = normalise(A)
    B = normalise(B)
    P_max, P_min = (A, B) if len(A) >= len(B) else (B, A)
    n_min = len(P_min)
    P_som = P_max
    for k in range(n_min):
        P_som[k] += P_min[k]
    return P_som

def fois(A: polynome, B: polynome) -> polynome:
    # attention aux pièges 
    A = normalise(A)
    B = normalise(B)
    if A == [0] or B == [0]:
        return [0]
    # danger : le degré d'une poly non nul c'est la longueur du normalisé  - 1
    na, nb = len(A), len(B)
    n = na + nb - 1 # deg(C) = deg(A) + deg(B) donc len(C) - 1 = len(A) - 1 + len(B) - 1 = len(A) + len(B) - 2
                    # donc len(C) = len(A) + len(B) - 1
    A = A + [0]*(n - na) # on complète par des 0 pour ne pas avoir des "IndexError"
    B = B + [0]*(n - nb) # idem
    C = [0 for _ in range(n)] # le polynôme produit final
    for k in range(n): # C est de longueur n
        for i in range(k + 1): # i va de 0 jusqu'à k compris
            C[k] += A[i]*B[k - i]
    return C


    
