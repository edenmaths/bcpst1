{{ chapitre(5, "Trigonométrie")}}







Cours au format 

- [PDF](../LATEX/Poly_Trigo_24.pdf) 

- et ses sources [TEX](../LATEX/trigo_24.tex)










!!! note "" 
	![Placeholder](./IMG/fourier2.jpg){ align=right width=500}
	La  trigonométrie  est  une  des parties  les  plus  anciennes  des
	mathématiques,  il y  a plus  de 4000  ans. Mais  c'est aussi  la branche  des
	mathématiques dont  les applications sont  les plus  nombreuses. On ne  peut y
	échapper car  on la  retrouve dans l'acoustique,  l'architecture, l'astronomie
	(et ainsi la navigation,  sur les océans, dans les airs  et dans l'espace), la
	biologie, la cartographie, la chimie, la compression d'images, de vidéos, de
	sons, le génie civil, l'infographie, la
	géophysique,  la cristallographie,  les sciences  économiques (en  particulier
	dans l'étude  des marchés financiers), l'électrotechnique,  l'électronique, la
	topographie et  la géodésie, beaucoup  de sciences physiques,  la construction
	mécanique, l'imagerie médicale (tomographie  axiale calculée et ultrasons), la
	météorologie,  la théorie  de  la  musique, la  théorie  des  nombres (et  par
	conséquent la cryptographie), l'océanographie, l'optique, la pharmacologie, la
	phonétique, la théorie  des probabilités, la psychologie,  la séismologie, les
	statistiques,  et  la  perception visuelle...La  Transformation  de  Fourier
	Rapide est par exemple l'algorithme qui a eu un impact révolutionnaire et est
	utilisé des milliards de fois chaque jour sur la planète.

	La trigonométrie,  et en particulier l'étude  des fonctions cosinus et  sinus, est
	primordiale dans pratiquement tous les domaines de la science...c'est pourquoi, 
	en toute logique, c'est une des  branches les plus martyrisée par le nouveau
	programme du secondaire. Nous allons y remédier.
	
	
# Promenade sur le cercle

## Enroulons la droite des réels

![Placeholder](./IMG/Metapost/trigo-5.pdf_b.svg){ align=right width=300}
Pour aller se promener, il est peu  pratique d'emmener la droite des réels telle
quelle : elle prend
trop de  place. C'est pourquoi nous  allons l'enrouler autour d'un  cercle, mais
comment faire ?  

On  considère  un   cercle  de  rayon  1  et  de   centre  l'origine  du  repère
$(O,\overrightarrow{i},\overrightarrow{j})$. On « colle » l'origine de
la droite des réels sur le point I de coordonnées $(1,\, 0)$, et on enroule. Enfin, on imagine, car
il va être difficile de trouver le « bout » de la droite correspondant à $-\infty$...Sans compter
qu'il va nous  falloir pas mal de  temps avant d'avoir fini  de l'enrouler, mais
ceci est un autre problème... 

![Placeholder](./IMG/Metapost/trigo-6.pdf_b.svg)



Comme vous  le savez bien, le  périmètre du cercle unité  vaut $2\times\pi\times \text{rayon}=2\pi$
puisque le rayon vaut 1. 

Donc,  le point  d'abscisse $2\pi$  de la  droite  des réels  vient se  «~coller~» sur  0, le  point
d'abscisse $2\pi+1$ de la droite des réels vient se «~coller~» sur 1 et plus
généralement, tout point d'abscisse $x$ voit  se coller sur lui $x+2\pi$, $x-2\pi$, $x+2\times2\pi$,
$x+3\times2\pi$, $\dots$


Nous pouvons  de plus  observer que nous  allons ainsi associer  à chaque  élément de la  droite des
réels, donc à chaque nombre réel $x$, un unique point sur le cercle. 


## Cercle trigonométrique


!!! {{ definition("Cercle trigonométrique") }}
    Dans un repère orthonormé  $(O,\overrightarrow{i},\overrightarrow{j})$, Le cercle trigonométrique est le cercle de  centre O, de rayon 1,
    muni d'une orientation. Le sens positif est le sens inverse des aiguilles
    d'une montre.  



![Placeholder](./IMG/Metapost/trigo-17.pdf_b.svg)


## La galette des Rois


Sachant qu'un « tour » correspond à $2\pi$, on peut facilement savoir à quels points du cercle
trigonométrique correspondent les fractions de $\pi$


![Placeholder](./IMG/Metapost/trigo-7.pdf_b.svg)



Bien sûr, on pourrait placer les points correspondant à n'importe quel réel, mais c'est un peu moins
pratique (Essayez donc de couper une galette des Rois en $\sqrt{2}$ parts...). 



## Le radian




Soit $\mathcal{C}$ le cercle trigonométrique (de rayon 1).

Soit $\widehat{RST}$ un angle géométrique; on effectue la translation de vecteur
$\overrightarrow{SO}$ de  cet angle: on obtient  l'angle $\widehat{R'OT'}$, qui est  de même
mesure que $\widehat{RST}$.

Soient $A$ et $M$ les points d'intersection respectifs des demi-droites $[OR ')$ et $[OT ')$ avec le cercle $\mathcal{C}$. On a alors $\widehat{RST}=\widehat{R'OT'}=\widehat{AOM}$ 



![Placeholder](./IMG/Metapost/trigo-18.pdf_b.svg)


!!! {{ definition("Angle non orienté") }}
     Une mesure en **radians** de  l'angle géométrique $\widehat{RST}$ est égale
     à la longueur de l'arc $\overparen{AM}$ 
	 
	 
!!! {{ definition("angle orienté") }}
     Une  mesure  en **radians**  de  l'angle  orienté $\bigl(  \overrightarrow{SR},\overrightarrow{ST}
	\bigr)$    est   égale    à    la   longueur    de   l'arc **orienté**
	$\overset{\curvearrowright}{AM}$ (Attention au sens)

 Demandez aux latinistes l'étymologie du mot *radian*...



!!! {{ definition("Mesure principale") }}
     La mesure  principale d'un angle orienté  est la mesure en  radian comprise
     dans l'intervalle $]-\pi, \pi]$.


!!! {{ exercice()}}

    === "Énoncé"

         1.  Quelle  est la  mesure  principale  de  l'angle orienté  de  mesure
            $\dfrac{129\pi}{34}$ ?
		 2. Quelle   est  la   mesure   principale  de   l'angle   orienté  de   mesure
		    $\frac{125\pi}{34}$ ? 

	=== "Indications"
		1. $
		   \dfrac{129}{34}=\dfrac{4\times 34+3}{34}=4+\dfrac{3}{34}
	       $ donc
	       $
		   \dfrac{129\pi}{34}=\dfrac{3\pi}{34}+2\times2\pi
		   $
           La mesure principale cherchée est donc $\frac{3\pi}{34}$

	    1. $-\dfrac{\pi}{34}$

## Correspondance entre degrés et radians



Il y a **proportionnalité** entre la mesure des  angles en degrés et mesure en radians; il faut
juste retenir que $180$ degrés correspondent à  $\pi$ radians et on retrouve alors facilement que le
coefficient de proportionnalité vaut...

| Deg  | 180  | 30 | 45| 60  | 90  | 360  |
|:----:|:-:|:-:|:-:|:-:|:-:|:-:|
| Rad  | π  | π/6  | π/4  | π/3  | π/2  | 2π  |


##  Relation de Chasles


Tout est dans le titre :


!!! {{ theoreme("Relation de Chasles")}}

    $$
	\left(\overrightarrow{u},\overrightarrow{v}      \right)     +      \left(\overrightarrow{v}
	,\overrightarrow{w} \right) = \left(\overrightarrow{u} ,\overrightarrow{w} \right)
	$$
	
	
# Sinus et Cosinus


## Approche géométrique de collège

!!! {{ exercice("Origine des lignes trigonométriques")}}


         
	Dans un repère orthonormé, on a tracé un cercle de rayon 1, l'origine $ O$
	du repère, le point $A$
	de coordonéées $(1;0)$, le point $B$ de coordonnées $(0;1)$, un point $M$ quelconque du cercle et
	quelques points associés à $ M$:

	- T l'intersection de $(OM)$ avec la perpendiculaire à $(OA)$ en $A$, 
	- U l'intersection de la perpendiculaire à $(OB)$ en $B$ avec la perpendiculaire
	  à $(OA)$ en $A$, 
	- $\Delta$ la droite $(OU)$, 
	- P le point de $(OA)$ de même abscisse que M,
	- $Q$ le point de $(OB)$ de même ordonnée que M.

	On note $\alpha$ l'angle $\bigl( \overrightarrow{OA} ,\overrightarrow{OM}  \bigr)$

	On définit quelques fonctions de la variable $\alpha$:


	- la sécante de l'angle $\alpha$ : $\sec(\alpha)=\dfrac{OT}{OA}$ ;
	
	- la tangente de l'angle $\alpha$ : $\tan(\alpha)=\dfrac{AT}{OA}$ ;
	
	- le sinus de l'angle $\alpha$ : $\sin(\alpha)=\dfrac{PM}{OA}$ ;
	
	-  on appelle  cosécante, cotangente,  cosinus de  l'angle $\alpha$  et on  note
	  ${\rm cosec }(\alpha)$,   ${\rm cotan }(\alpha)$  et   $\cos(\alpha)$  respectivement   la
	  sécante,  la  tangente  et  le   sinus  du  *complémentaire*  de  l'angle
	  $\alpha$. 

    Répondez aux questions suivantes :

    1. Que pouvez-vous dire à propos de la droite $(\Delta)$ ? 
	
	1. Construisez les symétriques des points $M$, $P$, $A$, $O$ par rapport à $(\Delta)$.
	   
	
	1. On note $\beta$ l'angle
		   $\left(\overrightarrow{OA},\overrightarrow{OM'}\right)$ : quel rapport existe
		   entre $\alpha$ et $\beta$ ?

	1.  Démontrez que $\sec(\alpha)=\dfrac{1}{\cos(\alpha)}$.
	
	1.  Démontrez que $\tan(\alpha)=\dfrac{\sin(\alpha)}{\cos(\alpha)}$.
	
	1.  Démontrez que ${\rm cotan }(\alpha)=\dfrac{1}{\tan(\alpha)}$.
	
	1.  Démontrez que ${\rm cosec }(\alpha)=\dfrac{1}{\sin(\alpha)}$.
	
	1.  Exprimez simplement $\sin(\alpha)\times\sec(\alpha)$ et $\cos(\alpha)\times{\rm cosec }(\alpha)$.

## Définition générale

Nous venons de voir qu'il est simple de repérer des points associés à des
sous-multiples de $\pi$, moins pour les autres. 

Pour y remédier, nous allons revenir à notre bon vieux système d'abscisses et d'ordonnées.

Pour les désigner, nous allons choisir  des petits noms qui sonnent bien, par exemple...*sinus*
et *cosinus* ! Ces noms vous disent-ils quelque chose ? Pourtant au collège, vous n'aviez pas
du tout parlé de radian, de droite des réels qui tourne autour d'un cercle et autres complications. 





![Placeholder](./IMG/Metapost/trigo-8.pdf_b.svg)


         

!!! {{ definition("Définition") }}
      Quelque soit le réel $x$, on appelle **cosinus** et **sinus** du
      réel $x$ l'abscisse et l'ordonnée du point du cercle trigonométrique associé à $x$. 
	  
	  On les note $\cos(x)$ et $\sin(x)$.
	



![Placeholder](./IMG/Metapost/trigo-9.pdf_b.svg)

Y aurait-il malgré tout un lien ? 
Mmmmm... regardons la figure précédente de plus près... 

Pour des valeurs de  $x$ comprises entre 0 et $\frac{\pi}{2}$, on
 retrouve  le  *soca*  du  *socatoa* vu  au  collège,  donc  au  moins  il n'y  a  pas  de
 contradiction avec ce qui a été vu dans le passé. 

Nous  avons   même  vu     comment  calculer
géométriquement les lignes trigonométriques de quelques valeurs particulières. 

Ce  petit  dessin   nous  permet  en  plus  de   vérifier  certaines  propriétés
importantes.  Par exemple :


!!! {{ theoreme("1ère formule de trigonométrie")}}

    Pour tout réel $x$
	
	$$
    (\cos x)^2+(\sin x)^2=1
	$$
	
	
On peut encore remarquer que :

!!! {{ theoreme("Majoration et minoration des lignes trigonométriques")}}

    Pour tout réel $x$, 
	
	$$
	-1 \leqslant \cos x \leqslant  1\quad \text{et}\quad -1
    \leqslant \sin x \leqslant 1
	$$



On remarquera également que les réels $x$ et  $x+2\pi$ sont associés au même point, donc ont le même
cosinus et le même sinus. 

 
!!! {{ theoreme("Périodicité")}}

    Pour tout réel $x$, 
	 
	$$
	 \cos(x+2\pi)=\cos(x)\quad \text{et}\quad\sin(x+2\pi)=\sin(x)
	$$
	 


Retenez les valeurs particulières suivantes :
 
 



![Placeholder](./IMG/Metapost/trigo-4.pdf_b.svg)

	


ou encore :


![Placeholder](./IMG/Metapost/unit_circle_b.svg)


## Angles associés



Que vous inspirent les dessins suivants :


![Placeholder](./IMG/Metapost/trigo-11.pdf_b.svg)

![Placeholder](./IMG/Metapost/trigo-19.pdf_b.svg)

![Placeholder](./IMG/Metapost/trigo-20.pdf_b.svg)



# Formulaire de trigonométrie

## Les formules

 $\sin ^{2}x+\cos ^{2}x=1$
 
 $1+\tan^2 x= \dfrac{1}{\cos^2 x} $

$\cos (a+b)=\cos a\cos b-\sin a\sin b$

$\cos (a-b)=\cos a\cos b+\sin a\sin b$

$\sin (a+b)=\sin a\cos b+\sin b\cos a$

$\sin (a-b)=\sin a\cos b-\sin b\cos a$

$\tan (a+b)=\dfrac{\tan a+\tan b}{1-\tan a\tan b}$, pour $a+b\neq \frac{\pi}{2}+k\pi$, $k\in ℤ$

$\tan (a-b)=\dfrac{\tan a-\tan b}{1+\tan a\tan b}$, pour $a-b\neq \frac{\pi}{2}+k\pi$, $k'\in ℤ$


$\cos (2x)=\cos ^{2}x-\sin ^{2}x=2\cos^{2}x-1=1-2\sin ^{2}x$

$\sin (2x)=2\cos x\sin x $

$\tan (2x)=\dfrac{2\tan x}{1-\tan ^{2}x}$, $x\neq\frac{\pi}{4}+k\frac{\pi}{2}$ pour $k\in ℤ$


## La formule magique

La  formule  qui  permet  d'obtenir  toutes les  autres  sera  pour  nous  $\cos
(a-b)=\cos a\cos b+\sin a\sin b$.

!!! {{ exercice()}}

    Démontrer pourquoi toutes les autres formules découlent de celle-ci.
	

## Un rappel de première : le produit scalaire


Le produit scalaire joue un rôle essentiel  en physique et vous l'avez étudié en
1ère :

$$
\overrightarrow{OA}\cdot \overrightarrow{OB}=OA×OB×\cos\left(\overrightarrow{OA},\overrightarrow{OB}\right)   
$$



En vous inspirant de la figure suivante :



![Placeholder](./IMG/Metapost/trigo-21.pdf_b.svg)



calculez  le  produit  scalaire  $\overrightarrow{OA}\cdot  \overrightarrow{OB}$
directement et en utilisant la relation de Chasles : 
$\left(\overrightarrow{OC}+\overrightarrow{CA}\right)\cdot\left(\overrightarrow{OD}
+\overrightarrow{DB} \right)$ et retrouvez la formule $\cos
(a-b)=\cos a\cos b+\sin a\sin b$.


## Formule spéciale onde


!!! {{ theoreme("Transformation de $a\cos x+b\sin x$")}}
	
    Soit $(x,a,b)∈ℝ^3$ avec $(a,b)\neq (0,0)$.
	Il existe $A∈ℝ^*_+$ et $φ∈ℝ$ tels que :
	
	$$
	a\cos x+b\sin x=A\cos(x-φ)
	$$
	
	avec $A=\sqrt{a^2+b^2}$, $\cos φ=\dfrac{a}{A}$ et $\sin φ=\dfrac{b}{A} $
	
	
Ça se démontre...





# Fonctions sinus et cosinus

## Définitions


Nous venons  de voir qu'à chaque réel  $x$, nous pouvons associer  une unique valeur de  $\cos x$ et
$\sin x$. Nous allons donc pouvoir définir deux *fonctions* cosinus et sinus


!!! {{ definition("Fonctions sinus et cosinus") }}

    $$
	\cos  :\begin{array}{rll} ℝ  &\to  & [-1,  1]  \\ x  &  \mapsto &  \cos
	x\end{array} \qquad\qquad\qquad \sin :\begin{array}{rll}  ℝ &\to & [-1,
	1] \\ x & \mapsto & \sin x\end{array}
	$$


## Parité

!!! {{ definition("Fonction paire") }}
     Dire qu'une fonction définie sur un ensemble $\cal D$ est PAIRE signifie que :

	1. $\cal D$ est symétrique par rapport à zéro.
	
	1. $(∀x ∈ \cal D)(f (-x)=f(x))$


Dans ce cas, la courbe représentative de $f$ dans un repère *orthogonal* est **symétrique
par rapport à l'axe des ordonnées**



![Placeholder](./IMG/Metapost/trigo-12.pdf_b.svg)




!!! {{ definition("Fonction impaire") }}
     Dire qu'une fonction définie sur un ensemble $\cal D$ est IMPAIRE signifie que :

	1. $\cal D$ est symétrique par rapport à zéro.
	
	1. $(∀x ∈ \cal D)(f (-x)=-f(x))$


Dans ce cas, la courbe représentative de $f$ dans un repère *orthogonal* est **symétrique
par rapport à l'origine du repère**



![Placeholder](./IMG/Metapost/trigo-13.pdf_b.svg)


!!! {{ exercice()}}

    Étudiez la parité des fonction cos et sin.

## Périodicité



Vous  avez bien sûr  remarqué que  $\cos(x+2\pi)=\cos(x)$ et  $\sin(x+2\pi)=\sin(x)$ pour  tout réel
$x$. 

Cela  se  comprend  à  la  fois   sur  le  cercle  trigonométrique  du  fait  de
l'enroulement et sur la représentation graphique des fonctions :



![Placeholder](./IMG/Metapost/trigo-16.pdf_b.svg)






## Variations




![Placeholder](./IMG/Metapost/TabCos_b.svg)


![Placeholder](./IMG/Metapost/trigo-14.pdf_b.svg)



![Placeholder](./IMG/Metapost/TabSin_b.svg)


![Placeholder](./IMG/Metapost/trigo-15.pdf_b.svg)

Dérivées ?





# Résolution d'équations et d'inéquations trigonométriques



## $\cos x = \cos α$
![Placeholder](./IMG/Metapost/trigo-24.pdf_b.svg){align=right}

!!! {{ theoreme("Arccosinus")}}

    Soit $t∈[-1,1]$.  L'équation $\cos x  = t$  d'inconnue $x$ admet  une unique
    solution **dans [0,π]**. On note cette solution $\arccos(t)$.





!!! {{ theoreme("$\cos x = \cos α$")}}

    - $\cos x = \cos α$ si, et seulement si, $x = α[2π]$ ou $x = -α[2π]$
	
	- $\cos x = a$ si, et seulement si, $x = \arccos a [2π]$ ou $x = -\arccos a[π]$
	






## $\sin x = \sin α$
![Placeholder](./IMG/Metapost/trigo-22.pdf_b.svg){align=right}

!!! {{ theoreme("Arcsinus")}}

    Soit $t∈[-1,1]$.  L'équation $\sin x  = t$  d'inconnue $x$ admet  une unique
    solution **dans [-π/2, π/2]**. On note cette solution $\arcsin(t)$.





!!! {{ theoreme("$\sin x = \sin α$")}}

    - $\sin x = \sin α$ si, et seulement si, $x = α[2π]$ ou $x = π-α[2π]$
	
	- $\sin x = a$ si, et seulement si, $x = \arcsin a [2π]$ ou $x = π-\arcsin a[2π]$
	


## $\tan x = \tan α$
![Placeholder](./IMG/Metapost/trigo-23.pdf_b.svg){align=right}


!!! {{ theoreme("Arctan")}}

    Soit $t∈ℝ$.  L'équation $\tan x  = t$  d'inconnue $x$ admet  une unique
    solution **dans ]-π/2, π/2[**. On note cette solution $\arctan(t)$.





!!! {{ theoreme("$ \\tan x = \\tan α$")}}

    - $\tan x = \tan α$ si, et seulement si, $x = α[π]$
	
	- $\tan x = a$ si, et seulement si, $x = \arctan a [π]$
	




## Équations se ramenant aux équations précédentes

La plupart du temps,  on essaiera de se ramener à l'un  des cas précédents après
des transformations selon le formulaire.


Pour résoudre une équation de la forme  $a\cos(t) + b\sin(t) = c$, on transforme le membre de gauche en $r\cos(t+\varphi)$ et on est ramené à une équation de la forme $\cos(x)=d$.


!!! {{ exercice("Pendule simple en Physique")}}

	On  considère un  pendule  simple  de longueur  $l=0.5m$  dans  le champ  de
	pesanteur terrestre pour lequel $g=9.81m\cdot s^{-2}$.  

    On  se  donne  comme   conditions  initiales  $\theta(0)=\dfrac{\pi}{6}$  et
    $\theta'(0)=1m \cdot s^{-1}$.  
	
	À quels instants  le pendule sera-t-il à la  position initiale, c'est-à-dire
	$\theta(t)=\dfrac{\pi}{6}$ ? 

