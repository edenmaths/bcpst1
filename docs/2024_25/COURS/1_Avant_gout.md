{{ chapitre(1, "Un avant-goût des mathématiques en BCPST")}}


# La modélisation en dynamique des populations

Peut-on prédire  combien il  y aura  d'habitants sur Terre  en l'an  2050 ?
Peut-on prévoir l'évolution du nombre d'atomes de carbone 14 dans une roche
radioactive? Combien de lapins peut espérer posséder un éleveur un an après
l'achat d'un couple? On ne peut bien  sûr pas répondre de manière exacte et
certaine à ces  questions, mais dans les  cas simples et à  condition de se
limiter à un  futur pas trop éloigné, on pourra  quand même prévoir l'ordre
de grandeur du nombre de Terriens, d'atomes de carbone 14 ou de lapins. 

Dans les trois  cas, il s'agit d'étudier l'évolution  du nombre d'individus
d'une population et donc de prédire les valeurs d'une grandeur dépendant du
temps. Vous  avez déjà  été confronté  à un problème  analogue en  cours de
physique lorsque vous cherchiez à déterminer la trajectoire d'un projectile
connaissant sa position et sa  vitesse initiale. Comment avez-vous procédé?
Vous avez d'abord choisi un cadre à  votre étude : probablement celui de la
mécanique classique, ce qui permettait  d'utiliser les lois de Newton, puis
en choisissant de négliger ou non les frottements de l'air. Vous avez alors
obtenu des équations différentielles faisant intervenir $x(t), y(t), z(t)$,
les  coordonnées   du  projectile  à   l'instant  $t$.  Si   la  résolution
mathématique de ces équations était  possible, vous avez pu exprimer $x(t),
y(t)$ et $z(t)$ en fonction de $t$, et ainsi donner une approximation de la
trajectoire du projectile. 

La  démarche physique  que nous  venons de  voir peut-elle  s'appliquer aux
problèmes de dynamique des populations qui nous intéressent? 

Première question :  quelles sont les lois qui régissent  l'évolution de la
population?  C'est là  qu'est le  premier  écueil, car  contrairement à  la
Mécanique pour  laquelle les lois  de Newton s'appliquent très  souvent, il
n'y a pas  de lois générales en dynamique des  populations. Dans chaque cas
particulier,  le  démographe  ou  le biologiste  doit  trouver  quelle  loi
d'évolution est la plus adaptée à la population qu'il étudie. 

Deuxième question, qui aurait d'ailleurs aussi pu se poser pour le problème
de la  trajectoire du projectile :  allons-nous utiliser un modèle  à temps
discret ou modèle à temps continu? 

Dans  un modèle  à  temps discret,  la population  étudiée  est recensée  à
intervalles réguliers, par  exemple tous les ans. Les  dates de recensement
sont  notées $0,1,2,  \ldots$, et  le  temps décrit  donc $\mathbb{I}$.  En
notant  $u_n$ le  nombre d'individus  à la  date $n$,  ou à  l'instant $n$,
l'évolution de la population est représentée par la suite 

$$
\left(u_n\right)_{n \in \mathbb{N}}=\left(u_0, u_1, u_2, \ldots\right)
$$

Au contraire, dans un modèle à  temps continu, le temps décrit l'intervalle
$[0,+\infty[$, et en  notant $N(t)$ le nombre d'individus  à l'instant $t$,
c'est la fonction 

$$
N:[0,+\infty[\rightarrow \mathbb{R}, t \mapsto N(t)
$$

qui  représente l'évolution  de la  population. On  utilise par  exemple un
modèle à  temps continu pour étudier  une population de bactéries  que l'on
peut considérer comme proliférant continûment. 

Une  fois  que l'on  a  choisi  entre ces  deux  types  de modèle,  la  loi
d'évolution de  la population permet  d'obtenir des conditions  que doivent
satisfaire la  suite $\left(u_n\right)_{n  \in \mathbb{N}}$ ou  la fonction
$N$ suivant le cas. On pourra  alors parfois calculer le nombre d'individus
dans la  population à chaque  instant, ceci dans  le cas où  ces conditions
permettent  de trouver  une formule  donnant $u_n$  en fonction  de $n$  ou
$N(t)$  en  fonction  de  $t$.  Comme  nous  allons  le  voir,  les  outils
mathématiques utilisés pour obtenir ces formules sont très variés. 

Mais il arrive aussi qu'on ne  puisse pas résoudre le problème mathématique
associé, c'est-à-dire qu'on ne puisse pas exprimer $u_n$ en fonction de $n$
ou $N(t)$  en fonction de  $t$. Dans ce cas,  il faudra se  contenter d'une
étude qualitative  du problème  en donnant  des propriétés  de $u_n$  ou de
$N(t)$, par exemple leurs variations ou leurs limites à l'infini. 

Enfin,  il  faudra confronter  les  résultats  mathématiques aux  résultats
expérimentaux  pour  pouvoir  valider  le modèle.  Si  les  résultats  sont
proches, on  pourra considérer que  le modèle est  bon, et donc  prévoir le
nombre d'individus à une date ultérieure avec une bonne approximation. Mais
attention, il ne faut pas croire que les prévisions pourront porter sur une
très grande durée. Plus la date  de la prévision sera éloignée, moins bonne
sera la prévision.




## Les modèles à temps discret



Si $\boldsymbol{u}_n$ désigne  le nombre d'individus d'une  population à la
date  $\boldsymbol{n}$, alors  l'accroissement de  la population  entre les
dates $n$  et $n+1$ est égal  à $u_{n+1}-u_n$. Cet accroissement  peu: être
positif  ou négatif,  et,  en l'absence  d'immigration, il  est  égal à  la
différence entre le  nombre de naissances pendant l'intervalle  de temps $]
n, n+1$ ] et le nombre de décès pendant cette même période. 

### Modèle de base

Dans ce modèle, nous faisons I'hypothèse très simplificatrice que le nombre
de naissances et le nombre de  décès entre $n$ et $n+1$ sont proportionnels
à  $u_n$, le  nombre d'individus  à la  date $n$.  Ainsi, si  la population
double, il  y aura  deux fois  plus de  naissances et  de morts.  En notant
$\lambda$ le  taux de natalité  et $\boldsymbol{\mu}$ le taux  de mortalité
$^4$, on a 

$$
\forall n \in \mathbb{N } u_{n+1}-u_n=\lambda u_n-\mu u_n
$$

Donc dans le modèle de base, la suite $\left(u_n\right)_{n \in \mathbb{N}}$ est donc une suite géométrique $^5$, et on a

$$
\forall n \in \mathbb{N} \quad u_n=(1+\lambda-\mu)^n u_0
$$

II s'agit donc d'un cas où l'on peut exprimer $u_n$ en fonction de $n$, et on peut prévoir l'évolution de la population à long terme :

* si $\lambda>\mu$, c'est-à-dire s'il y  a plus de naissances que de morts,
  alors  $1+\lambda-\mu>1$  et la  population  croît  indéfiniment :  c'est
  l'explosion démographique ; 

* si au contraire $\lambda<\mu$, alors $0<1+\lambda-\mu<1$ et la population
  décroît jusqu'à l'extinction. 


### Modèle avec nourriture limitée


Ce modèle convient au cas où la croissance de la population est limitée par
la quantité de nourriture disponible.  Vous pouvez par exemple imaginer une
colonie de cochons  sauvages sur une petite île déserte  qui ne peuvent pas
trop proliférer au risque d'épuiser toute la nourriture. 

Pour chercher une  loi d'évolution, commençons par noter  $Q_T$ la quantité
totale  de nourriture  disponible  et $Q_N(x)$  la  quantité de  nourriture
nécessaire pour  nourrir une  population de  taille $x$.  à un  instant $n$
donné, deux cas peuvent se présenter : 

* si $Q_N\left(u_n\right)<Q_T$, alors la  population peut manger à sa faim,
  et  nous  supposerons  que  dans ce  cas,  elle  prolifère,  c'est-à-dire
  $u_{n+1}-u_n>0$; 
  
* si $Q_N\left(u_n\right)>Q_T$, alors il n'y a pas assez à manger pour tout
  le monde,  et nous supposerons  que dans  ce cas, la  population diminue,
  c'est-à-dire $u_{n+1}-u_n<0$. 

Il  faut  maintenant  proposer   une  expression  pour  l'accroissement  de
population $u_{n+1}-u_n$.

D'après  ce  que l'on  vient  de  voir, il  doit  être  de même  signe  que
$Q_T-Q_N\left(u_n\right)$, ce qui incite à prendre 

$$
u_{n+1}-u_n=\phi\left(u_n\right)\left(Q_T-Q_N\left(u_n\right)\right)
$$

où  $\phi$ est  une fonction  positive à  déterminer. Dans  le modèle  avec
nourriture  limitée  le  plus  simple,  on  choisit  $\phi\left(u_n\right)$
proportionnel à  $u_n$, un peu  pour les mêmes  raisons que dans  le modèle
précédent.  D'autre part,  il  est  naturel de  supposer  que $Q_N(x)$,  la
quantité de  nourriture nécessaire  pour nourrir  une population  de taille
$x$, est proportionnelle à $x$. Finalement 

$$
\forall n \in \mathbb{N} \quad u_{n+1}-u_n=p u_n\left(Q_T-q u_n\right)
$$

où $p$ et $q$ sont des coefficients de proportionnalité positifs.
La  suite $\left(u_n\right)_{n  \in \mathbb{N}}$  est une  suite récurrente
puisque 

$$
\forall n \in \mathbb{N} \quad u_{n+1}=f\left(u_n\right)
$$

où $f$  est la  fonction définie par  $f(x)=x+p x\left(Q_T-q  x\right)$. La
plupart du temps,  il n'est pas possible d'exprimer le  terme général $u_n$
d'une  suite récurrente  en  fonction de  $u_0$ et  $n$.  Cela n'avait  été
possible pour la suite  géométrique du modèle de base que  parce que le cas
$f(x)=(1+\lambda-\mu) x$ se prêtait bien  aux calculs. Dans le cas général,
on peut simplement remarquer 

$$
\begin{aligned}
u_1 & =f\left(u_0\right) \\
u_2 & =f\left(u_1\right)=f\left(f\left(u_0\right)\right)=f \circ f\left(u_0\right) \\
& \vdots \\
u_n & =\underbrace{f \circ \cdots \circ f}_{n \text { fois }}\left(u_0\right)
\end{aligned}
$$

mais  cette expression  de  $u_n$ ne  nous avance  pas  tellement. D'où  la
nécessité de développer des techniques  d'étude des suites récurrentes. Ces
techniques  s'appuient sur  de  nombreux outils  d'analyse  : limite  d'une
suite,  continuité d'une  fonction, dérivée  d'une fonction,  inégalité des
accroissements finis, suites extraites d'une suite, etc. 

### Modèle avec temps de gestation

Les belettes ont  une période de gestation  de deux mois, et  on choisit de
recenser une population  de belettes tous les mois, ce  qui signifie que la
date $n$ correspond au $n$-ième mois.  Dans ce cas, le nombre de naissances
au mois  $n+1$ est proportionnel  non pas à $u_n$  comme dans le  modèle de
base, mais à l'effectif de la population deux mois plus tôt, c'est-à-dire à
$u_{n-1}$. En effet,  seules les belettes déjà présentes au  mois $n-1$ ont
pu avoir  des petits  au mois  $n+1$. Donc, en  notant encore  $\lambda$ et
$\mu$ les taux de natalité et de mortalité, on a 

$$
\forall n \in \mathbb{N}^* u_{n+1}-u_n=\lambda u_{n-1}-\mu u_n
$$

Ainsi, la suite $\left(u_n\right)_{n  \in \mathbb{N}}$ vérifie une relation
de récurrence linéaire à deux termes du type 

$$
\forall n \in \mathbb{N}^* \quad u_{n+1}=a u_n+b u_{n-1}
$$

où $a$ et $b$ sont deux réels fixés.

On voit bien que  pour calculer tous les termes d'une  telle suite, il faut
d'abord  connaître  les  valeurs  de  ses deux  premiers  termes  $u_0$  et
$u_1$. On pourra alors calculer $u_2=a  u_1+b u_0$, puis $u_3=a u_2+b u_1$,
etc、 

Mais ensuite,  que faire? On  peut d'abord  essayer de deviner  une formule
exprimant $u_n$ en fonction de $n$  à l'aide du calcul des premiers termes,
puis   chercher  à   démontrer  que   cette  formule   est  la   bonne  par
récurrence. Mais cette  méthode aboutit rarement. Pour  vous en convaincre,
vous pouvez la tester sur la célèbre suite de Fibonacci définie par 

$$
F_0=1, F_1=1 \text { et } \forall n \in \mathbb{ N}^* F_{n+1}=F_n+F_{n-1}
$$

Je vous mets au  défi de trouver la formule générale  à partir des premiers
termes 

$$
1,1,2,3,5,8,13,21,34,55
$$

Revenons au cas général. Pour exprimer $u_n$ en fonction de $n$ à partir de
la relation  (1) et des  conditions initiales  $u_0$ et $u_1$,  nous allons
considérer  un autre  problème et  qui  est, en  apparence seulement,  plus
compliqué  : déterminer  l'ensemble $E_{a,  b}$  de toutes  les suites  qui
vérifient l'équation (1).  Nous oublions donc momentanément  les valeurs de
$u_0$  et  de $u_1$,  et  nous  n'utiliserons  ces  deux valeurs  que  pour
sélectionner  dans   la  liste  des   éléments  de  $E_{a,  b}$   la  suite
$\left(u_n\right)_{n  \in \mathbb{N}}$  cherchée. Et  ce qui  est étonnant,
c'est que  le nouveau problème  est résoluble! On  peut en effet  donner la
liste  des éléments  de $E_{a,  b}$, et  ceci en  utilisant des  techniques
d'algèbre linéaire. 

Cette théorie  très importante étudie  des ensembles de vecteurs  ainsi que
des fonctions  définies sur ces  ensembles. Elle s'applique ici  car $E_{a,
b}$ a  une structure  d'espace vectoriel, ce  qui signifie  pour simplifier
que, si  $\left(v_n\right)_{n \in \mathbb{N}}$ et  $\left(w_n\right)_{n \in
\mathbb{N}}$ vérifient (1) et si $\alpha$ et $\beta$ sont deux réels, alors
la  suite $\left(\alpha  v_n+\beta w_n\right)_{n  \in \mathbb{N}}$  vérifie
encore (1).  On utilisera ensuite des  théorèmes sur les bases  des espaces
vectoriels de dimension finie pour déterminer $E_{a, b}$. 

On obtient par exemple par  cette méthode qu'une suite $\left(v_n\right)_{n
\in \mathbb{N}}$ appartient  à $E_{1,1}$ si et seulement si  il existe deux
réels $c$ et $d$ tels que 

$$
\forall n \in \mathbb{ N} \quad v_n=c\left(\frac{1+\sqrt{5}}{2}\right)^n+d\left(\frac{1-\sqrt{5}}{2}\right)^n
$$

Pour calculer, le $n$-ième terme de  la suite de Fibonacci, il suffit alors
d'utiliser les  conditions initiales $F_0=1$  et $F_1=1$ pour  calculer les
coefficients $c$ et $d$ associés, et on en déduit 

$$
\forall n \in \mathbb{ N} \quad F_n=\frac{5+\sqrt{5}}{10}\left(\frac{1+\sqrt{5}}{2}\right)^n+\frac{5-\sqrt{5}}{10}\left(\frac{1-\sqrt{5}}{2}\right)^n
$$

Il faut avouer  que sans l'algèbre linéaire, nous aurions  eu peu de chance
de deviner cette formule! 

### Modèle avec tranches d'âge

Pour tenir  compte de certains phénomènes  liés à l'âge des  individus, par
exemple le fait que les êtres humains de plus de 60 ans ont moins d'enfants
que ceux de la tranche 20-40  ans, on utilise des modèles avec distribution
d'âges. Considérons  par exemple un  modèle simplifié avec  quatre tranches
d'âge, 

| tranche | 1 | 2 | 3 | 4 |
| :---: | :---: | :---: | :---: | :---: |
| âge | 0 à 20 ans | 20 à 40 ans | 40 à 60 ans | plus de 60 ans |

L'unité  de  temps  sera  20  ans, ce  qui  signifie  qu'entre  deux  dates
consécutives  $n$  et $n+1$  s'écoulent  20  années,  et  que la  date  $n$
correspond à la date initiale plus $n$ fois 20 ans. 

Pour $i=1,2,3$ ou 4 , on notera $\lambda_i$ et $\mu_i$ les taux de natalité
et  de  mortalité de  la  tranche  $i$,  et  $N_i(n)$ désignera  le  nombre
d'individus  de  la  tranche  $i$  à  la date  $n$.  On  aura  par  exemple
$\lambda_2>\lambda_1$ et $\mu_4>\mu_3$. 

Les individus  de la tranche  1 présents à la  date $n+1$ sont  les enfants
d'individus présents à la date $n$, ainsi 

$$
N_1(n+1)=\lambda_1 N_1(n)+\lambda_2 N_2(n)+\lambda_3 N_3(n)+\lambda_4 N_4(n)
$$

Les individus de la  tranche 2 présents à la date  $n+1$ sont les individus
qui étaient dans la tranche 1 à la  date $n$ et qui ne sont pas morts entre
temps, ainsi 

$$
N_2(n+1)=N_1(n)-\mu_1 N_1(n)
$$

De la même manière,

$$
N_3(n+1)=N_2(n)-\mu_2 N_2(n)
$$

Enfin, les  individus de  la tranche 4  présents à la  date $n+1$  sont les
individus qui étaient dans les tranches 3 ou 4 à la date $n$ et qui ne sont
pas morts entre temps, ainsi 

$$
N_4(n+1)=N_3(n)-\mu_3 N_3(n)+N_4(n)-\mu_4 N_4(n)
$$

La complexité de ces quatre  équations rend apparemment difficile le calcul
des $N_i(n)$  en fonction de $n$.  Et pourtant, c'est possible  en général,
grâce encore à des techniques  d'algèbre linéaire que nous allons détailler
un peu maintenant. 

Commençons par  quelques notations. On  définit d'abord, pour tout  $n$, un
vecteur  dont les  coordonnées représentent  les effectifs  des différentes
tranches d'âge à la date $n$, 

$$
\overrightarrow{N(n)}=\left(\begin{array}{l}
N_1(n) \\
N_2(n) \\
N_3(n) \\
N_4(n)
\end{array}\right)
$$

Ensuite, on définit la fonction

$$
\begin{aligned}
& f: \mathbb{R}^4 \longrightarrow \mathbb{R}^4 \\
& \left(\begin{array}{c}
x \\
y \\
z \\
t
\end{array}\right) \longmapsto\left(\begin{array}{c}
\lambda_1 x+\lambda_2 y+\lambda_3 z+\lambda_4 t \\
\left(1-\mu_1\right) x \\
\left(1-\mu_2\right) y \\
\left(1-\mu_3\right) z+\left(1-\mu_4\right) t
\end{array}\right) \\
&
\end{aligned}
$$

Cette fonction  associe à un  vecteur $\vec{u}$ de $\mathbb{R}^4$  un autre
vecteur de  $\mathbb{R}^4$ dont  on calcule  les coordonnées à  partir de
celles  de $\vec{u}$.  Cette fonction  $f$ est  remarquable, car  c'est une
application linéaire, ce qui signifie 

$$
f(\alpha \vec{u}+\beta \vec{v})=\alpha f(\vec{u})+\beta f(\vec{v})
$$

pour tous vecteurs $\vec{u}$ et $\vec{v} \in \mathbb{ R}^4$, et tous réels
$\alpha$ et ${\beta}$. 

Grâce à ces notations, les équations (2) à (5) qui régissent l'évolution de
la population peuvent s'écrire sous une forme très simple, 

$$
\forall n \in \mathbb{ N} \quad \overrightarrow{N(n+1)}=f(\overrightarrow{N(n)})
$$

On retrouve encore une suite récurrente, mais cette fois-ci, ses termes sont des vecteurs. D'après (6), on a

$$
\begin{aligned}
\overrightarrow{N(1)} & =f(\overrightarrow{N(0)}) \\
\overrightarrow{N(2)} & =f(f(\overrightarrow{N(0)}))=f \circ f(\overrightarrow{N(0)}) \\
& \vdots \\
\overrightarrow{N(n)} & =\underbrace{f \circ \cdots \circ f}_{n \text { fois }}(\overrightarrow{N(0)})
\end{aligned}
$$

Les conditions initiales, à savoir le vecteur $\overrightarrow{N(0)}$, sont
connues, et exprimer  $\overrightarrow{N(n)}$ en fonction de  $n$ se ramène
donc au calcul de $f \circ \cdots  \circ f$. Pour cela, on utilise un autre
outil fondamental de l'algèbre linéaire,  les matrices, qui sont en quelque
sorte des tableaux  de nombres. On associe à l'application  linéaire $f$ la
matrice 

$$
A=\left(\begin{array}{cccc}
\lambda_1 & \lambda_2 & \lambda_3 & \lambda_4 \\
1-\mu_1 & 0 & 0 & 0 \\
0 & 1-\mu_2 & 0 & 0 \\
0 & 0 & 1-\mu_3 & 1-\mu_4
\end{array}\right)
$$

On pourra définir le produit de  deux matrices, la puissance $n$-ième d'une
matrice.  Calculer $f  \circ \cdots  \circ  f$ reviendra  alors à  calculer
$A^n$,  la puissance  $n$-ième  de la  matrice $A$,  et  les techniques  de
diagonalisation  des matrices  permettront dans  bien des  cas ces  calculs
explicites de puissances de matrices. 

Le  chemin est  encore  long avant  de pouvoir  donner  les expressions  de
$N_1(n)$, $N_2(n), N_3(n)$ et $N_4(n)$ en  fonction de $n$. Mais vous serez
capables  après  deux  années  d'études de  mathématiques  de  résoudre  ce
problème. 


## Les modèles à temps continu




Dans  les   modèles  à   temps  continu,   le  temps   décrit  l'intervalle
$[0,+\infty[$, et  on note  $N(t)$ le nombre  d'individus de  la population
étudiée à l'instant $t$. Poùr déterminer la loi d'évolution, on considèrera
un  «   petit  »  intervalle  de   temps  $[t,  t+\Delta  t]$,   ainsi  que
l'accroissement de  population pendant cet intervalle  de temps $N(t+\Delta
t)-N(t)$. Dans tous les modèles, nous supposerons que cet accroissement est
proportionnel à $\Delta  t$, la durée qui s'est écoulée  entre les instants
$t$ et $t+\Delta t$. Cela traduit  l'idée que pendant l'intervalle $[t, t+2
\Delta t]$, il y aura approximativement  deux fois plus de naissances et de
morts  que pendant  l'intervalle $[t,  t+\Delta t]$.  Mais attention,  ceci
n'est valable  que si $\Delta  t$ est petit.  Sur des durées  plus longues,
cette hypothèse serait trop restrictive. 

### Modèle de base

Comme dans le modèle de base à temps discret, on suppose que pour tout $t$,
l'accroissement  de population  $N(t+\Delta t)-N(t)$  est proportionnel  au
nombre d'individus  à l'instant  initial, c'est-à-dire ici  proportionnel à
$N(t)$. On obtient donc 

$$
N(t+\Delta t)-N(t)=k \Delta t N(t)
$$

où $k$ est le taux de croissance de la population, différence entre le taux
de natalité et le taux de mortalité. Cela s'écrit aussi 

$$
\frac{N(t+\Delta t)-N(t)}{\Delta t}=k N(t)
$$

et on reconnaît  alors dans le membre de gauche  le taux d'accroissement de
la fonction $N$ entre $t$ et $t+\Delta t$. En supposant que la fonction $N$
est dérivable sur  $\mathbb{R}^{+}$, on a donc, en  faisant tendre $\Delta
t$ vers 0 , 

$$
\forall t \in \mathbb{R}^{+} N^{\prime}(t)=k N(t)
$$

Donc la  fonction inconnue $N$  est solution d'une  équation différentielle
linéaire du  premier ordre  à coefficients constants.  La résolution  de ce
type d'équation est classique, et on obtient 

$$
\forall t \in \mathbb{ R}^{+} N(t)=e^{k t} N(0)
$$

Ce modèle peut être appliqué à l'évolution d'une population de bactéries, à
la désintégration  d'un élément  radioactif, $N(t)$ représentant  le nombre
d'atomes  de cet  élément, à  l'évolution  d'un réactif  chimique dans  une
solution, $N(t)$ représentant alors le  nombre de molécules du réactif, ou,
encore au calcul d'un capital $N(t)$ placé avec un taux d'intérêt $k$. 

Tout  cela est  très joli,  mais  si on  y regarde  de  plus près,  il y  a
apparemment une erreur dans ce que l'on vient de faire, car la fonction $N$
n'est certainement pas  dérivable sur $\mathbb{R}^{+}$. En  effet, s'il y
avait par exemple 32 individus dans la  population à l'instant 0 , puis une
naissance à l'instant  $t_0$, puis une autre aux instants  $t_1, t_2, t_3$,
puis une  mort aux instants  $t_4, t_5, t_6$, etc,  alors le graphe  de $N$
serait de la forme 

![lpjp1](./IMG/lpjp1.png)

Vous voyez que le graphe de la fonction $N$ ne se trace pas d'un trait
continu, ce  qui est incompatible avec  le fait que $N$  soit dérivable sur
$\mathbb{R}^{+}$. 

Pour remédier  à ce  défaut de dérivabilité,  nous pouvons  approcher cette
courbe  par  celle  d'une   nouvelle  fonction  $\widetilde{N}$,  dérivable
celle-là, comme sur ce dessin. 

![lpjp1](./IMG/lpjp2.png)



En identifiant $N$ et $\widetilde{N}$, on peut donc « faire comme si $N$
était  dérivable  », ce  qui  permet  d'effectuer  la fin  du  raisonnement
précédent. 

Ce  procédé peut  paraître  curieux,  mais on  peut  toujours rajouter  des
hypothèses dans la  phase de modélisation, à condition  de vérifier ensuite
si  les  résultats  mathématiques   sont  compatibles  avec  les  résultats
expérimentaux. Si tel  est le cas, I'hypothèse de dérivabilité  de $N$ sera
donc justifiée a posteriori. 


### Modèle avec taux de croissance variable


On ne suppose plus  que le taux de croissance $k$  est constant, mais qu'il
varie  avec  le  temps.  Par  exemple, ce  taux  peut  augmenter  pour  une
population humaine  avec les progrès  de la  médecine; de même,  la vitesse
d'une réaction chimique peut varier si l'on chauffe la solution. 

Par le  même procédé  que dans  le modèle précédent,  on obtient  alors une
équation différentielle linéaire du $1^{\text  {er }}$ ordre à coefficients
non constants, 

$$
\forall t \in \mathbb{ R}^{+} N^{\prime}(t)=k(t) N(t)
$$

Si la fonction $k$ est une fonction continue, la théorie du calcul intégral
permet résoudre cette équation, et on obtient 

$$
\forall t \in \mathbb{R}^{+} N(t)=N(0) \exp \left(\int_0^t k(u) d u\right)
$$

### Modèle avec nourriture limitée



Pour étudier  l'évolution d'une population  dont la croissance  est limitée
par la  quantité de  nourriture disponible, on  peut également  utiliser un
modèle à temps  continu. Comme dans le modèle analogue  à temps discret, on
va  noter $Q_T$  la  quantité  totale de  nourriture,  et  supposer que  la
quantité de nourriture nécessaire pour nourrir une population de taille $x$
est donnée par $Q_N(x)=q x$. 

Si $Q_N(x)<Q_T$, alors  la population augmente, alors  que si $Q_N(x)>Q_T$,
la population diminue par manque de nourriture. Un peu comme dans le modèle
à temps discret, on va alors supposer que 

$$
N(t+\Delta t)-N(t)=p \Delta t N(t)\left(Q_T-q N(t)\right)
$$

où  $p$ est  une  constante de  proportionnalité  strictement positive.  On
obtient  donc   une  équation  différentielle  non   linéaire  à  variables
séparables 

$$
\forall t \in \mathbb{R}^{+} N^{\prime}(t)=N(t)(a-b N(t))
$$

avec $a=p Q_T>0$ et $b=p q>0$.

La résolution de cette équation  différentielle fait intervenir la fraction
rationnelle 

$$
\frac{1}{X(a-b X)}
$$

ainsi que sa décomposition en éléments simples

$$
\frac{1}{X(a-b X)}=\frac{1}{a X}+\frac{b}{a(a-b X)}
$$

On obtient cette fois-ci encore une solution explicite,

$$
\forall t \in \mathbb{R}^{+} N(t)=\frac{a}{b\left(1+c e^{-a t}\right)}
$$

avec $c=-1+a / b N(0)$. Et par conséquent,

$$
\lim _{t \rightarrow+\infty} N(t)=a / b=Q_T / q
$$

Ainsi, dans  le modèle avec  quantité de nourriture limitée,  la population
tend à se stabiliser vers $Q_T / q$, ce qui correspond à une population qui
consommerait exactement la nourriture disponible.


### Modèle de proies et prédateurs


Jusqu'à présent, nous  n'avons étudié que des populations  vivant en milieu
fermé,    c'est-à-dire    insensibles     à    l'influence    des    autres
espèces.  Intéressons-nous  maintenant  à l'évolution  simultanée  de  deux
espèces vivant dans  un petit périmètre, par exemple les  poissons qui sont
des proies et les requins qui sont des prédateurs. 

Essayons de conjecturer, c'est-à-dire de deviner, comment la situation peut
évoluer. S'il  y a beaucoup  de poissons, les  requins peuvent en  manger à
volonté et le nombre de poissons diminue,  ceci jusqu'à ce qu'il n'y en ait
plus  assez   pour  nourrir  les   requins,  dont  le   nombre  grandissait
jusque-là. à ce moment-là, le nombre de requins va commencer à diminuer, et
il diminuera  jusqu'à ce qu'il y  ait suffisamment peu de  requins pour que
les poissons recommencent à proliférer. Après un certain temps, les requins
pourront à nouveau manger à leur faim, ils vont se multiplier, le nombre de
poissons  va diminuer,  etc. Le  phénomène semble  donc périodique,  et une
bonne   modélisation  permettra   peut-être  de   retrouver  ce   caractère
périodique. 

On  va noter  $P(t)$ et  $R(t)$ les  nombres de  poissons et  de requins  à
l'instant $t$. On  suppose que la quantité de plancton  dont se nourrissent
les poissons  est illimitée,  et en l'absence  de requins,  c'est-à-dire si
$R(t)=0$, on aurait donc 

$$
P(t+\Delta t)-P(t)=a \Delta t P(t)
$$

Le  taux de  croissance $a$  est strictement  positif puisque  les poissons
prolifèrent lorsque les requins ne sont pas là. 

Tenons maintenant compte  des requins en faisant l'hypothèse  que le nombre
de poissons mangés entre $t$ et $t+\Delta t$ est proportionnel au nombre de
poissons et au nombre de requins. Ainsi, pour un certain $b>0$, on a 

$$
P(t+\Delta t)-P(t)=a \Delta t P(t)-b \Delta t P(t) R(t)
$$

Passons  maintenant à  l'étude  des  variations du  nombre  de requins.  En
l'absence  de poissons,  c'est-à dire  si $P(t)=0$,  les requins  tendent à
disparaître, donc, pour un certain $c>0$, 

$$
R(t+\Delta t)-R(t)=-c \Delta t R(t)
$$

Mais  s'il y  a des  poissons, il  faut rajouter  un terme  proportionnel à
$R(t)$,  et nous  supposerons pour  simplifier que  ce terme  est également
proportionnel à $P(t)$. On a donc, pour un certain $d>0$, 

$$
R(t+\Delta t)-R(t)=-c \Delta t R(t)+d \Delta t P(t) R(t)
$$

Comme  précédemment, en  divisant par  $\Delta  t$ puis  en faisant  tendre
$\Delta t$ vers 0 , on déduit de (8) et (9) que, pour tout $t$, 

$$
\left\{\begin{array}{l}
P^{\prime}(t)=a P(t)-b P(t) R(t) \\
R^{\prime}(t)=-c R(t)+d P(t) R(t)
\end{array}\right.
$$

II  s'agit-là d'un  système différentiel  non linéaire,  connu sous  le nom
d'équations  de  Lotka-Volterra.  Ce  système n'a  malheureusement  pas  de
solutions explicites, c'est-à-dire  qu'on ne peut pas  obtenir des formules
permettant d'exprimer $P(t)$ et $R(t)$ en  fonction de $t$. On a alors deux
possibilités : 

* utiliser un  ordinateur et résoudre l'équation de  manière approchée, par
  exemple par la  méthode d'Euler; même si c'est  moins satisfaisant qu'une
  résolution exacte, cela peut suffire pour faire des prévisions: 
  
* se  contenter d'une étude qualitative  qui consiste, à partir  du système
  (10), à établir quelques propriétés des fonctions $P$ et $R$, par exemple
  leur sens de variation, leur périodicité, leur limite en $+\infty$... 
  
  
Essayons  par  exemple   de  retrouver  la  périodicité   que  nous  avions
conjecturée. Posons d'abord 

$$
P_{\mathrm{e}}=c / d \text { et } R_{\mathrm{e}}=a / b
$$

et   remarquons  que   les  fonctions   constantes  $P=P_{\mathrm{e}}$   et
$R=R_{\mathrm{e}}$ sont  solutions du système  (10). Cela correspond  à une
position   d'équilibre   :   s'il   y  a   $P_{\mathrm{e}}$   poissons   et
$R_{\mathrm{e}}$ requins,  le nombre  de poissons et  le nombre  de requins
resteront  constants. II  s'agit  bien  sûr d'un  résultat  théorique et  «
idéalisé  》  que  le  biologiste  interprétera  en  disant  que  les  deux
populations varient peu autour de $P_{\mathrm{e}}$ et $R_{\mathrm{e}}$. 

Dans la suite, nous allons nous limiter  au cas où $P(t)$ et $R(t)$ restent
《 proches » de cette position d'équilibre, c'est-à-dire que nous supposons
qu'il existe  deux fonctions $p$  et $r$ prenant  de « petites»  valeurs et
telles que, pour tout $t$, 

$$
\left\{\begin{array}{l}
P(t)=P_{\mathrm{e}}+p(t) \\
R(t)=R_{\mathrm{e}}+r(t)
\end{array}\right.
$$

En reportant ces relations dans (10), on obtient après quelques calculs,

$$
\left\{\begin{array}{l}
p^{\prime}(t)=-\operatorname{er}(t)-b p(t) r(t) \\
r^{\prime}(t)=f p(t)+d p(t) r(t)
\end{array}\right.
$$

avec $e=b P_{\mathrm{e}}>0$ et $f=d R_{\mathrm{e}}>0$.

Ce  système  différentiel n'a  pas  plus  de  solutions explicites  que  le
précédent, mais  on peut le simplifier  en négligeant certains termes  à la
manière des physiciens : comme $p(t)$  est petit, la quantité $b p(t) r(t)$
est négligeable  devant $-e  r(t)$, et  de même,  nous négligerons  $d p(t)
r(t)$  devant  $f p(t)$.  On  pourra  donc approcher  $p$  et  $r$ par  les
solutions $\bar{p}$ et $\bar{r}$ du système 

$$
\left\{\begin{array}{l}
\bar{p}^{\prime}(t)=-e \bar{r}(t) \\
\bar{r}^{\prime}(t)=f \bar{p}(t)
\end{array}\right.
$$

Nous avons ainsi obtenu un système différentiel linéaire dont la résolution
est maintenant possible. En effet, pour tout $t$, on a 

$$
\left\{\begin{array}{l}
\bar{p}^{\prime \prime}(t)=-e \bar{r}^{\prime}(t)=-e f \bar{p}(t) \\
\bar{r}^{\prime \prime}(t)=f \bar{p}^{\prime}(t)=-e f \bar{r}(t)
\end{array}\right.
$$

Donc  $\bar{p}$ et  $\bar{r}$ sont  solutions de  l'équation différentielle
linéaire du second ordre à coefficients constants 

$$
y^{\prime \prime}+\omega^2 y=0
$$

avec $\omega=\sqrt{e f}$. Il existe donc  des constantes $A, \phi, B, \psi$
telles que, pour tout $t$, 

$$
\left\{\begin{array}{l}
\bar{p}(t)=A \cos (\omega t+\phi) \\
\bar{r}(t)=B \cos (\omega t+\psi)
\end{array}\right.
$$

Et voilà : comme $\bar{p}$ et  $\bar{r}$ sont périodiques, on en déduit que
$P$ et $R$ le sont aussi! Notre  objectif est donc atteint, à ceci près que
notre preuve n'est valable que dans le cas particulier où $P$ et $R$ sont «
proches 》 de la position d'équilibre. 

On peut maintenant suivre l'évolution dans  le temps de $P(t)$ et $R(t)$ au
voisinage  du point  d'équilibre,  et ceci  grâce à  l'étude  de la  courbe
paramétrée 

$$
\left\{\begin{array}{l}
P(t)=P_{\mathrm{e}}+A \cos (\omega t+\phi) \\
R(t)=R_{\mathrm{e}}+B \cos (\omega t+\psi)
\end{array}, t \in \mathbb{ R}^{+}\right.
$$

On montre que cette courbe est une ellipse, et on retrouve ainsi les quatre phases dont nous avions conjecturé l'existence :

* phase 1 , les requins prolifèrent au détriment des poissons;

* phase 2, il n'y a plus assez de poissons et le nombre de requins diminue ;

* phase 3 , le nombre de requins a suffisamment chuté pour que les poissons
  recommencent à proliférer ; 
  
* phase  4, les poissons sont  à nouveau suffisamment nombreux  pour que le
  nombre de requins augmente. 
  
En suivant les flèches sur cette ellipse, on voit clairement les quatre phases.

![lpjp1](./IMG/lpjp3.png)
