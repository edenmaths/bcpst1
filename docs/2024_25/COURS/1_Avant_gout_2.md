{{ chapitre(1, "Pourquoi de l'informatique en BCPST ? Un exemple : les L-systèmes")}}


En  1968, un  biologiste hongrois,  Aristid Lindenmayer,  met au  point une
*grammaire formelle* afin de modéliser le développement et la prolifération
des plantes,  algues, champignons,  levures, bactéries :  nous sommes  à la
jonction de la biologie, des mathématiques et de l'informatique.



![linden](./IMG/linden.jpg)


La motivation est simple : un brin d'ADN décrit le développement d'une
plante au fil du temps (la *morphogénèse*). Au lieu d'analyser le code génétique des plantes,
Lidenmayer a tenté de deviner par lui-même leurs règles de croissance, puis
de les vérifier.


Pour décrire une grammaire on doit donner 

-  un alphabet  : c'est  l'ensemble des  *variables* (nous  utiliserons par
  exemple B pour bourgeon, T pour tige,...
  
- des  constantes : des  symboles qui sont  des instructions pour  tracer :
  avancer, tourner, remplir, etc.

- un axiome : c'est létat de départ

- un ensemble de  règles de réécriture permettant de passer  d'un état à un
  autre.
  
  
Pour  tracer, nous  allons utiliser  une tortue  peintre qui  est munie  de
pinceaux et  de peinture.  Pour cela,  on utilisera  le module  `turtle` de
Python:

```python
import turtle
```

Supposons par exemple qu'une branche  fertile (notons-la F) donne naissance
à deux branches fertiles (l'une en  tournant à gauche, l'autre à droite) et
devient stérile (S).

Cela peut se résumer à cela :

- alphabet = $\{S, F\}$
- constantes = $\{+, -, [, ]\}$
- axiome = F (une branche fertile au départ)
- règle : $F \to S[+F][-F]$


Avec pour conventions à donner à notre tortue :

- F : avancer d'une unité en traçant en vert
- S : avancer d'une unité en traçant en marron
- $+$ : tourner à gauche
- $-$ : tourner à droite
- [ : sauvegarder la position
- ] : restaurer la position

Ainsi `F[+F]F[-F]` donne:

![ls0](./IMG/l-systeme_img0.png)


On obtient avec `S[+F][-F]`:

![ls1](./IMG/l-systeme_img1.png)


En Python:

```python
def LS1(n, regle, axiome):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole == "F":
                new_mot = new_mot + regle
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot
	
def trace_LS(n, d, a, regle, axiome):
    gen = Turtle()
    gen.speed(1000)
    gen.fillcolor("yellow")
    gen.left(90)
    gen.penup()
    gen.backward(500)
    gen.pendown()
    positions = []
    mot = LS1(n, regle, axiome)
    for symb in mot:
        if symb == "F":
            gen.color("lightgreen")
            gen.forward(d)
        elif symb == "S":
            gen.color("black")
            gen.forward(d)
        elif symb == "+":
            gen.left(a)
        elif symb == "-":
            gen.right(a)
        elif symb == "[":
            positions.append((gen.position(), gen.heading()))
        elif symb == "]":
            pos, ang = positions.pop()
            gen.penup()
            gen.setposition(pos)
            gen.setheading(ang)
            gen.pendown()
    exitonclick()
```

On peut penser que la nature  n'a pas forcément un rapporteur à disposition
pour tracer des angles et que les angles peuvent varier un peu.


```python
elif symb == "+":
            gen.left(randint(int(a*0.9), int(a*1.1) + 1))
```

et on appelle notre fonction avec :

```python
trace_LS(4, 100, 20, "S[-F][+F]", "F")
```

mais bon, ce n'est pas très réaliste encore.


Voici un dessin plus sophistiqué:

```python
trace_LS(5, 5, 20, 'FF+[+F-F-F]-[-F+F+F]', 'F')
```



![ls6](./IMG/l-systeme_img6.png)



Pour certains modèles, il peut être pratique de tenir compte des bourgeons,
représentés par des petits carrés bleus  sur les figures. Ceux-ci vont agir
comme    points    d’ancrage    pour    faire    pousser    de    nouvelles
branches. Définissons «B» comme un  bourgeon. Nous avons mentionné plus tôt
qu’il faut souvent  plus d’une règle de croissance pour  obtenir une plante
réaliste.  Voici  les règles  d’une  plante  où  chaque segment  double  de
longueur (règle  1) et  chaque bourgeon  « B »  donne naissance  à d’autres
branches « F » qui contiennent elles-mêmes quatre bourgeons (règle 2).


Or, il  semble n’y avoir que  trois bourgeons dans la  représentation de la
règle  2.  En  réalité,  les  deux bourgeons  du  bas  sont  superposés  et
pousseront avec  une orientation différente. On  peut voir la plante  à ses
premières étapes de croissance. Dans cette illustration, on voit que l’état
de départ, soit l’itération 0 ne  contient qu’un seul bourgeon. Celui-ci va
ensuite  se  développer  et  donner naissance  à  d’autres  bourgeons.  Les
segments  doublent  de longueur  à  chaque  itération, mais  le  changement
d’échelle à chaque image semble annuler cet effet.





```python
def LS2(n, regle, axiome):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole == "B":
                new_mot = new_mot + regle
            elif symbole == "F":
                new_mot = new_mot + "FF"
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot
	
	
def trace_LS(n, d, a, regle, axiome):
    gen = Turtle()
    gen.speed(1000)
    gen.fillcolor("yellow")
    gen.left(90)
    gen.penup()
    gen.backward(500)
    gen.pendown()
    positions = []
    mot = fonc(n, regle, axiome, regle2, proba)
    for symb in mot:
        if symb == "F":
            gen.color("lightgreen")
            gen.forward(d)
        elif symb == "S":
            gen.color("black")
            gen.forward(d)
        elif symb == "B":
            gen.color("blue")
            gen.dot()
        elif symb == "+":
            gen.left(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "-":
            gen.right(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "[":
            positions.append((gen.position(), gen.heading()))
        elif symb == "]":
            pos, ang = positions.pop()
            gen.penup()
            gen.setposition(pos)
            gen.setheading(ang)
            gen.pendown()
    exitonclick()
```


Par exemple avec 

```python
 trace_LS(4, 20, 20, "F[[-B][+B]]F[+FB]-B", "B")
```

On obtient



![ls3](./IMG/l-systeme_img3.png)







On peut rajouter des feuilles.  Pour cela nous pouvons introduire « { » et
«  } »  qui définissent  une  surface pleine  limitée par  ses côtés,  plus
fréquemment une feuille ou un pétale. 


Par exemple :


![ls2](./IMG/l-systeme_img2.png)


La règle « F→FF » est importante  parce qu’elle fait doubler la longueur de
chaque branche  à chaque itération de  la plante. Par contre,  les feuilles
seraient affectées aussi  par cette règle (elles contiennent des  « F ») et
deviendraient  de tailles  démesurées. Il  faudra alors  définir une  autre
instruction pour  la tortue: «  f », qui  est identique à  « F »,  mais qui
n’est pas affectée par la règle « `F→FF` ». Les feuilles deviendront: 

`f{[-f+f+f][+f-f-f]} `


Il est  aussi possible de  rajouter de la  variation dans les  plantes pour
rehausser le  réalisme. Certaines règles  de croissance peuvent  avoir deux
possibilités que l’on tire au hasard chaque fois. Dans l’exemple suivant,
un bourgeon  peut se  transformer en  deux structures  différentes, chacune
ayant 50% de chances de survenir. 



![ls4](./IMG/l-systeme_img4.png)



```python
def LS3(n, regle1,  axiome, regle2, proba):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole in "BX":
                if random() < 0.5:
                    new_mot = new_mot + regle1
                else:
                    new_mot = new_mot + regle2
            elif symbole == "F":
                new_mot = new_mot + "FF"
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot

    
def trace_LS(n, d, a, regle, axiome, fonc, regle2 = None, proba = None):
    gen = Turtle()
    gen.speed(1000)
    gen.fillcolor("yellow")
    gen.left(90)
    gen.penup()
    gen.backward(500)
    gen.pendown()
    positions = []
    mot = fonc(n, regle, axiome, regle2, proba)
    for symb in mot:
        if symb in "Ff":
            gen.color("lightgreen")
            gen.forward(d)
        elif symb == "S":
            gen.color("black")
            gen.forward(d)
        elif symb == "B":
            gen.color("lightblue")
            gen.dot()
        elif symb == "+":
            gen.left(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "-":
            gen.right(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "[":
            positions.append((gen.position(), gen.heading()))
        elif symb == "]":
            pos, ang = positions.pop()
            gen.penup()
            gen.setposition(pos)
            gen.setheading(ang)
            gen.pendown()
        elif symb == "{":
            gen.begin_fill()
        elif symb == "}":
            gen.end_fill()
    exitonclick()
```


Par exemple 


```python
 trace_LS(4, 10, 20, 'F[[-B][+B]]F[+FB]-B[-f{[-f+f+f][+f-f-f]}]', 'B', LS3, "F[-B]F[-FB]+B[+f{[-f+f+f][+f-f-f]}]", 0.5) 
```

donne


![ls5](./IMG/l-systeme_img5.png)


## Encore un exemple




```python
 trace_LS(7, 2.5, 20, 'F[+X]F[-X]+X', 'X', LS2)
```

![ls7](./IMG/l-systeme_img7.png)




## Le code


[Le code complet](./PYTHON/L_systemes.py)


## Une grammaire pour compter


Voici une autre grammaire : qu'en pensez-vous ?




**expression:**

![expression](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22157%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2061%201%2057%201%2065%22%2F%3E%3Cpolygon%20points%3D%2217%2061%209%2057%209%2065%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2258%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2258%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3Eterme%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2230%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2230%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E%2B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2061%20h2%20m20%200%20h10%20m58%200%20h10%20m-98%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-24%20q0%20-10%2010%20-10%20m78%2044%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-24%20q0%20-10%20-10%20-10%20m-78%200%20h10%20m30%200%20h10%20m0%200%20h28%20m23%2044%20h-3%22%2F%3E%3Cpolygon%20points%3D%22147%2061%20155%2057%20155%2065%22%2F%3E%3Cpolygon%20points%3D%22147%2061%20139%2057%20139%2065%22%2F%3E%3C%2Fsvg%3E)

```
expression
         ::= terme ( '+' terme )*
```

**terme:**

![terme](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22163%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2061%201%2057%201%2065%22%2F%3E%3Cpolygon%20points%3D%2217%2061%209%2057%209%2065%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2264%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2264%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3Efacteur%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E.%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2061%20h2%20m20%200%20h10%20m64%200%20h10%20m-104%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-24%20q0%20-10%2010%20-10%20m84%2044%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-24%20q0%20-10%20-10%20-10%20m-84%200%20h10%20m24%200%20h10%20m0%200%20h40%20m23%2044%20h-3%22%2F%3E%3Cpolygon%20points%3D%22153%2061%20161%2057%20161%2065%22%2F%3E%3Cpolygon%20points%3D%22153%2061%20145%2057%20145%2065%22%2F%3E%3C%2Fsvg%3E)

```
terme    ::= facteur ( '.' facteur )*
```


**facteur:**

![facteur](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22281%22%20height%3D%22125%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2282%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2282%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3Econstante%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2270%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3Evariable%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2291%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2289%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22109%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%2297%22%20y%3D%2291%22%20width%3D%2290%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2295%22%20y%3D%2289%22%20width%3D%2290%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22105%22%20y%3D%22109%22%3Eexpression%3C%2Ftext%3E%3Crect%20x%3D%22207%22%20y%3D%2291%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22205%22%20y%3D%2289%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22215%22%20y%3D%22109%22%3E%29%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m82%200%20h10%20m0%200%20h100%20m-222%200%20h20%20m202%200%20h20%20m-242%200%20q10%200%2010%2010%20m222%200%20q0%20-10%2010%20-10%20m-232%2010%20v24%20m222%200%20v-24%20m-222%2024%20q0%2010%2010%2010%20m202%200%20q10%200%2010%20-10%20m-212%2010%20h10%20m70%200%20h10%20m0%200%20h112%20m-212%20-10%20v20%20m222%200%20v-20%20m-222%2020%20v24%20m222%200%20v-24%20m-222%2024%20q0%2010%2010%2010%20m202%200%20q10%200%2010%20-10%20m-212%2010%20h10%20m26%200%20h10%20m0%200%20h10%20m90%200%20h10%20m0%200%20h10%20m26%200%20h10%20m23%20-88%20h-3%22%2F%3E%3Cpolygon%20points%3D%22271%2017%20279%2013%20279%2021%22%2F%3E%3Cpolygon%20points%3D%22271%2017%20263%2013%20263%2021%22%2F%3E%3C%2Fsvg%3E)

```
facteur  ::= constante
           | variable
           | '(' expression ')'
```

**variable:**

![variable](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22127%22%20height%3D%22125%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3Ex%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2265%22%3Ey%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2291%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2289%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22109%22%3Ez%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m28%200%20h10%20m-68%200%20h20%20m48%200%20h20%20m-88%200%20q10%200%2010%2010%20m68%200%20q0%20-10%2010%20-10%20m-78%2010%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m26%200%20h10%20m0%200%20h2%20m23%20-88%20h-3%22%2F%3E%3Cpolygon%20points%3D%22117%2017%20125%2013%20125%2021%22%2F%3E%3Cpolygon%20points%3D%22117%2017%20109%2013%20109%2021%22%2F%3E%3C%2Fsvg%3E)

```
variable ::= 'x'
           | 'y'
           | 'z'
```


**constante:**

![constante](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22157%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%2219%22%20width%3D%2258%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2217%22%20width%3D%2258%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2237%22%3Echiffre%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m58%200%20h10%20m-98%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m78%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-78%200%20h10%20m0%200%20h68%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22147%2033%20155%2029%20155%2037%22%2F%3E%3Cpolygon%20points%3D%22147%2033%20139%2029%20139%2037%22%2F%3E%3C%2Fsvg%3E)

```
constante
         ::= chiffre+
```

**chiffre:**

![chiffre](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22127%22%20height%3D%22433%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E0%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2265%22%3E1%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2291%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2289%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22109%22%3E2%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22135%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22133%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22153%22%3E3%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22179%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22177%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22197%22%3E4%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22241%22%3E5%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22267%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22265%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22285%22%3E6%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22311%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22309%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22329%22%3E7%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22355%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22353%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22373%22%3E8%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22399%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22397%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22417%22%3E9%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m28%200%20h10%20m-68%200%20h20%20m48%200%20h20%20m-88%200%20q10%200%2010%2010%20m68%200%20q0%20-10%2010%20-10%20m-78%2010%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m-58%20-10%20v20%20m68%200%20v-20%20m-68%2020%20v24%20m68%200%20v-24%20m-68%2024%20q0%2010%2010%2010%20m48%200%20q10%200%2010%20-10%20m-58%2010%20h10%20m28%200%20h10%20m23%20-396%20h-3%22%2F%3E%3Cpolygon%20points%3D%22117%2017%20125%2013%20125%2021%22%2F%3E%3Cpolygon%20points%3D%22117%2017%20109%2013%20109%2021%22%2F%3E%3C%2Fsvg%3E)

```
chiffre  ::= '0'
           | '1'
           | '2'
           | '3'
           | '4'
           | '5'
           | '6'
           | '7'
           | '8'
           | '9'
```

## 
![rr-2.0](data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2216%22%20height%3D%2216%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23001407%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%23001414%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001407%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%2300140E%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23001414%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23003311%3B%20stroke%3A%20%23003311%3B%7D%20rect.terminal%20%7Bfill%3A%20%234DFF88%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%234DFFC7%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%234DF9FF%3B%20stroke%3A%20%23003311%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20transform%3D%22scale%280.178%29%22%3E%3Ccircle%20cx%3D%2245%22%20cy%3D%2245%22%20r%3D%2245%22%20style%3D%22stroke%3Anone%3B%20fill%3A%23FFCC00%22%2F%3E%3Ccircle%20cx%3D%2245%22%20cy%3D%2245%22%20r%3D%2242%22%20style%3D%22stroke%3A%23332900%3B%20stroke-width%3A2px%3B%20fill%3A%23FFCC00%22%2F%3E%3Cline%20x1%3D%2215%22%20y1%3D%2215%22%20x2%3D%2275%22%20y2%3D%2275%22%20stroke%3D%22%23332900%22%20style%3D%22stroke-width%3A9px%3B%22%2F%3E%3Cline%20x1%3D%2215%22%20y1%3D%2275%22%20x2%3D%2275%22%20y2%3D%2215%22%20stroke%3D%22%23332900%22%20style%3D%22stroke-width%3A9px%3B%22%2F%3E%3Ctext%20x%3D%227%22%20y%3D%2254%22%20style%3D%22font-size%3A26px%3B%20font-family%3AArial%2C%20Sans-serif%3B%20font-weight%3Abold%3B%20fill%3A%20%23332900%22%3ER%3C%2Ftext%3E%3Ctext%20x%3D%2264%22%20y%3D%2254%22%20style%3D%22font-size%3A26px%3B%20font-family%3AArial%2C%20Sans-serif%3B%20font-weight%3Abold%3B%20fill%3A%20%23332900%22%3ER%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fsvg%3E)
