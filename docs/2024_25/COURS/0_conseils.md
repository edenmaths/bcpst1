# Conseils pour apprendre son cours de mathématiques 



L'apprentissage et l'approfondissement du cours sont ce qui vous prendra le
plus de  temps (plus de  cinq heures par semaine  en plus des  cours), mais
sont primordiaux  pour espérer  progresser. Voyez-le comme  la construction
des fondations sur lesquelles vous ferez reposer tous vos raisonnements. 

## 1. Ne trichez pas sur le sommeil et sur l'attention

Le sommeil est  une étape essentielle dans le processus  de mémorisation du
cerveau, il ne  faut pas rogner sur  vos heures de repos.  Bien dormir vous
permettra de plus  d'être plus réveillés et donc plus  efficaces pendant la
journée. 

Ménagez-vous des  pauses : il  est difficile de maintenir  sa concentration
sur de  longues périodes d'affilée.  Des blocs d'une demi-heure  de travail
intense suivis de cinq minutes de pause sont un bon point de départ pour se
lancer,  vous pourrez  ensuite allonger  ces durées  quand vous  aurez pris
l'habitude d'un travail efficace. 

Quand  vous   travaillez,  coupez-vous  de  toute   source  de  distraction
extérieure     (installez-vous     seul,     éteignez     téléphones     et
ordinateurs...). Sinon,  vous serez bien trop  tentés d'y jeter «  juste un
coup d'œil de temps en temps » et vous perdrez beaucoup en efficacité. 

## 2. Pendant les cours

Assurez-vous d'arriver en  cours en ayant repris et compris  le cours de la
veille, autant que possible. 

Soyez actifs pendant les heures de cours, ça facilitera la mémorisation des
résultats et théorèmes : 

- Je pose souvent des questions à la classe. Interrompez votre recopie à ce
  moment-là (je vous laisserai ensuite le  temps de terminer) et tentez d'y
  répondre. Même si vous n'êtes pas sûrs de la réponse! 
- N'hésitez pas à  poser des questions (à moi, pas à  votre voisin) si vous
  ne comprenez  pas. Même si le  problème vous paraît stupide  : dites-vous
  que vous n'êtes  sûrement pas le seul  à bloquer sur ce  point. Il arrive
  parfois qu'on trouve la réponse tout seul au moment de poser la question,
  c'est tout à fait normal. 
- Soyez attentifs aux questions de vos camarades et aux réponses qui y sont
  apportées. 
-  Prenez  en note  mes  remarques  informelles,  qui  peuvent aider  à  la
  compréhension  des   notions  (même  si   elles  ne  sont   pas  toujours
  rigoureuses). 

Je vous  laisserai régulièrement  du temps pour  réfléchir aux  exemples du
cours, sortez un brouillon et ne vous contentez pas d'attendre la réponse :
c'est plus fatigant,  mais ça ancrera bien mieux les  techniques dans votre
mémoire à long terme. 

## 3. En dehors des cours

Le bachotage n'est efficace que sur  la mémoire à très court terme. Réviser
en bloc  la veille  des échéances  vous paraîtra  peut-être utile  en début
d'année, quand  la quantité de  choses à réviser restera  raisonnable, mais
cette stratégie  se révélera de moins  en moins rentable au  fil de l'année
(chaque devoir  surveillé portera sur l'intégralité  des chapitres). Gardez
en tête que votre  objectif est de passer les concours  : vous devez ancrer
vos  connaissances  dans  la  mémoire  à long  terme,  pour  éviter  d'être
submergés sous la masse des choses à apprendre. 

Les stratégies suivantes devraient s'avérer plus efficaces que le bachotage
sur le long terme: 

- Travaillez  de manière régulière, si  possible un peu tous  les soirs. Ne
  tentez pas  de tout apprendre  en bloc,  de petites durées  répétées sont
  plus efficaces. 
- Donnez-vous des objectifs de temps plutôt que de résultats (par exemple :
  « ce soir, je fais une heure de  maths » plutôt que « ce soir, j'apprends
  tout le chapitre  »). Il est plus rentable de  reprendre en profondeur un
  demi-chapitre que de survoler la totalité des résultats. 
- Interrogez-vous  sur le cours  plutôt que  de le relire  passivement. Par
  exemple,  vous  pouvez  commencer  votre séance  d'apprentissage  sur  un
  chapitre en listant tout ce dont vous vous souvenez. Ça vous permettra de
  ne pas perdre de temps sur des choses déjà sues et de vous concentrer sur
  les détails problématiques. 
-  Demandez  à  un  camarade  de  vous  interroger  sur  des  résultats  au
  hasard. Vérifiez  que vous  en connaissez bien  toutes les  hypothèses et
  toutes les conclusions. 



Quelques conseils pour vous approprier les résultats les plus récalcitrants :
     
-  Cherchez  des  exemples  d'objets qui  illustrent  ce  résultat  ou
	   représentez le résultat par un dessin. 
- Si vous avez du mal  à mémoriser la liste des hypothèses, retirez-en
	   une et cherchez des contre-exemples.  Vous comprendrez bien mieux la
	   nécessité de  l'hypothèse en  question. C'est cependant  un exercice
	   difficile :  si en dix minutes  de recherche, vous ne  trouvez aucun
	   contre-exemple, ne vous obstinez pas et  venez me voir à la fin d'un
	   cours, je vous aiderai.
- Reprenez  les démonstrations du  cours : comprendre pourquoi  un résultat
  est  vrai aide  à  le retenir.  Cela  vous entraînera  de  plus pour  les
  exercices, où  on retrouve  souvent des  raisonnements qui  ressemblent à
  ceux des démonstrations. 
- Reformulez le  résultat avec vos propres mots (ou  passez du français aux
  quantificateurs et réciproquement) pour vous l'approprier. 
-  Comparez le  résultat avec  ceux  qui lui  ressemblent le  plus dans  le
  chapitre (au niveau des hypothèses ou des conclusions) et mettez en avant
  leurs différences. 
- Construisez-vous une association d'idées  avec le théorème : associez-lui
  une image  (qui peut  ne rien  avoir à voir  avec les  mathématiques), un
  personnage  de votre  série préférée,  une  musique ...  En cas  d'oubli,
  repasser par  votre association  d'idées vous  permettra de  retrouver le
  résultat beaucoup plus facilement. 

Laissez-vous régulièrement du temps pour  réviser de vieilles notions (vous
pouvez par exemple créer de petites cartes avec une question au recto et la
réponse au verso).
Vous vous remercierez ensuite vous-même quand viendra le moment
de réviser pour les concours blancs (sans parler des concours eux-mêmes, où
vous aurez deux années de cours à réviser). 

## 4. Quelques conseils d'anciens

-  «  Bien  travailler  le  cours. Refaire  les  exercices  du  cours  pour
  comprendre les  méthodes de base. Ne  pas hésiter à poser  des questions,
  même très basiques, au risque de trouver la réponse tout seul.» 

- «  Relire le  cours de  la journée chaque  soir, voire  tout de  suite en
  sortant du cours,  pour toujours avoir les idées claires  sur ce que l'on
  fait et ne pas accumuler un quelconque retard. » 

- « Apprendre le cours tout le temps, sans prendre de retard. »

-  « L'entraînement  et la  connaissance  du cours  de façon  intelligente,
  c'est-à-dire en  comprenant les méthodes  et en retenant les  points clés
  des démonstrations, est ce qui m'a fait le plus progresser. Il faut noter
  tous  les conseils  qui sont  donnés à  l'oral pendant  les cours  et qui
  pourront servir plus tard lors d'un exercice. » 

- « Notez le  plus de méthodes, d'astuces, de remarques,  qui sont dites en
  cours mais  qui ne  sont pas  écrite sur le  polycopié, ils  sont souvent
  d'une grande aide. » 

- «  Travailler le cours  en fonction des interros  de cours de  la semaine
  suivante  (revoir le  cours et  refaire les  exercices des  chapitres qui
  tomberont  à l'interro)  permet de  se tenir  à un  programme de  travail
  précis, mélangeant anciens chapitres et chapitres récents. » 

- «Essayer d'organiser son programme  de révision des chapitres en fonction
  du programme  des interros de cours  pour la semaine suivante.  Comme les
  chapitres des interros  de cours reviennent le nombre  de fois nécessaire
  pour être vraiment assimilés, il est nécessaire de suivre ce rythme. » 

- « Le plus important est d'apprendre  la leçon. C'est dur de le comprendre
  en début  d'année :  on sort  de Terminale, le  plus efficace  c'était de
  faire des exercices  pour s'entraîner et on avait des  bonnes notes assez
  facilement sauf  que ici, ce  n'est pas  du tout le  cas. Il ne  faut pas
  prendre à la  légère les interrogations de cours car  elles permettent de
  vraiment maîtriser la leçon si on le fait sérieusement. » 

- « Bien se servir de ses fiches et petites cartes pour réviser le cours. »

- «Apprendre le  cours parfaitement dès les tous premiers  cours. Au début,
  cela  semble très  abstrait.  Mais  c'est avec  le  temps  que l'on  peut
  comprendre le réel intérêt de connaitre sur le bout des doigts toutes les
  définitions et les théorèmes (avec évidemment toutes les hypothèses!)» 

- «Ne pas négliger le cours, y consacrer du temps mais surtout vérifier que
  cet apprentissage  est efficace  dès les  premières semaines.  Essayer de
  comprendre les  démonstrations et d'improviser  ensuite en colle  pour la
  rédaction,  ne pas  l'apprendre au  mot près  au risque  d'oublier l'idée
  principale de la preuve. Après chaque chapitre, il est utile de faire une
  fiche  avec  les  formules,  les théorèmes  importants  et  les  méthodes
  spécifiques ou  les astuces récurrentes  pour ne pas devoir  les chercher
  dans tous les polycopiés à la fin de l'année. » 

