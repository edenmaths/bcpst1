{{ chapitre(1, "Bases pour raisonner")}}




Cours au format 

- [PDF](../LATEX/Poly_logic_24.pdf) 

- et ses sources [TEX](../LATEX/Logic24.tex)




!!! exo ""
	![Placeholder](./IMG/evilKermit.jpg){ align=right height=250}
	Vous arrivez en  pleine forme après une année de  Terminale pendant laquelle
	vous avez brillé.  Avant d'explorer  de nouvelles contrées mathématiques, il
	s'agit d'établir quelques bases solides afin de consolider nos futurs apprentissages :
  
	- comprendre un énoncé en le traduisant selon la logique des propositions et des prédicats;
	- utiliser des rudiments de théorie des ensembles et faire le lien avec la logique;
	- découvrir dans le contexte familier des résolutions d'inéquations l'intérêt, la rigueur et la mise au point de théorèmes et de démonstrations;
	- revoir, développer et préciser le raisonnement par récurrence vu au lycée;
	- s'entraîner au calcul algébrique et investir les points vus précédemment dans le contexte de calcul de sommes. 
	

## Mise en évidence des problèmes

### Rapports de jury

Voici quelques extraits  de rapports de jurys qui mettent  en évidence certaines
lacunes que nous allons tenter de combler dans ce chapitre:

- *Comme l’an dernier,  le niveau est très hétérogène  et l’impression générale
    ressentie à la lecture des copies amène  à penser que les questions les plus
    subtiles, qui demandent une compréhension fine  de la théorie, quel que soit
    le  domaine concerné,  échappent à  presque tous  les candidats.  La plupart
    d’entre eux  ont acquis des techniques  et des réflexes mais  ne comprennent
    pas forcément en profondeur ce qu’ils font.*
- *La plupart  des étudiants  arrivent à  reproduire des  séquences vues  en classe
durant leurs deux années de préparation  mais ils le font souvent sans maîtriser
les concepts mathématiques qui y sont  liés et préfèrent réciter par cœur plutôt
que de s’adapter à l’énoncé de  l’exercice qu’ils traitent. On note encore cette
année des  étudiants « hors-sujet  » plaquant sur une  question une «  recette »
apprise  par cœur  mais  inadaptée à  la  question posée.[...]Curieusement,  les
étudiants  sont plus  à l’aise  avec les  concepts qui  n’ont été  abordés qu’en
classe  préparatoire   (matrices,  calcul   intégral,  variables   aléatoires  à
densité). En revanche, certaines lacunes du  lycée n’ont pas été comblées. C’est
le cas de l’étude des fonctions. 
On  note également  un  nombre  important de  candidats  (environ  un quart)  en
difficulté  sur   des  notions   de  collège  :   mise  au   même  dénominateur,
factorisation, parenthèses, règles de priorité, confusion entre les opérations.*
- *Les opérations  élémentaires et  les notions de  base sont de  plus en  plus mal
maîtrisées : addition et multiplication  sont régulièrement confondues (on passe
de* $2x = y$ à $x = y–2$ ;  *on confond diviser par* $2n$ *et diviser par* $n^2$, *etc
.)[...]La  manipulation  des fractions  a  été  un obstacle  insurmontable  dans
beaucoup de copies.[...] Beaucoup de candidats utilisent la formule (fausse) :* $m^{1/2} m^{1/3} =
m^{1/6}$.*[...]
De plus, les raisonnements doivent être clairs et précis, les affirmations étant
étayées par  une argumentation solide. Par  exemple, le recours trop  fréquent à
des phrases  du type  « il  est clair  que… »  doit être  évité au  profit d’une
justification correcte fondée  sur un apprentissage rigoureux et  une très bonne
maîtrise du cours.*
- *Le jury est stupéfait du nombre d’erreurs intervenant dans l’étude d’une fonction simple (question 5a) : erreurs sur
les variations (notamment résoudre dérivée = 0 au lieu de dérivée > 0), erreurs manifestes sur la représentation
graphique (allure aberrante, courbe avec un pic, valeur incorrecte pour t = 0 ou pour t → +∞, tangente horizontale
à t = 0...), pas d’indication de l’asymptote ou de la tangente horizontales... Il est assez préoccupant de constater que
de nombreux étudiants ne sont pas capables de réaliser cette question parfaitement, alors qu’elle ne dépasse pas le
niveau  de Terminale  Scientifique et  qu’un usage  raisonné de  la calculatrice
permet *
- *Le symbole ⇔ est souvent mal  utilisé et de nombreux étudiants confondent une
  équivalence et une implication, voired’éviter de nombreuses erreurs.*
  

### Francis Bacon et le Grand Toral


En  1605,   Francis  BACON   dénonçait  déjà  l'existence   du  Grand   Oral  en
Mathématiques:

*Logic differeth from rhetoric...in this,  that logic handleth reason 
  exact and in truth, and rhetoric handleth it as it is planted in popular opinions
  and manners*
  
  
### Les BCPST1 au Pays des Merveilles 



- « Reprenez donc un peu de thé » propose le Lièvre de Mars.

- « Je n'ai rien pris du tout, je ne saurai donc reprendre de rien ! »

- « Vous voulez  dire que vous ne sauriez reprendre de  quelque chose » répartit
  le Chapelier.
  
- « Quand il n'y a rien, ce n'est pas facile d'en reprendre ».


-  Alors comme ça, vous êtes étudiante?
-  Oui, en mathématique par exemple.
-  Alors que vaut cette fraction : un sur deux sur trois sur quatre?
-  Eh bien ...
-  Elle vaut deux tiers, la devança le Loir.
-  Ou trois huitièmes si vous préférez, ajouta le Lièvre de Mars.
-  Ou encore un sur vingt-quatre, affirma le Chapelier.
-  En fait, je crois que...
-  Aucune importance! Dîtes-nous plutôt combien vous voulez de sucre dans votre thé?
-  Deux ou trois, ça dépend de la taille de la tasse.
-  Certainement pas, car de toute façon, deux ou trois c'est pareil.
-  Parfaitement! approuva le Loir en fixant Alice qui écarquillait les yeux.
-  Ce n'est pourtant pas ce qu'on m'a appris, fit celle-ci.
- Pourtant, ce n'est pas compliqué  à comprendre, en voici une démonstration des
  plus élémentaires.
  On sait que pour tout entier $n$ on a successivement
  
$$
    (n+1)^2=n^2+2n+1
$$
  
$$
  (n+1)^2-2n-1=n^2
$$
  
  Retranchons $n(2n+1)$ des deux côtés
                
$$
  (n+1)^2-(n+1)(2n+1)=n^2-n(2n+1)
$$
  
  Mézalor, en ajoutant $(2n+1)^2/4$, on obtient

$$
  (n+1)^2-(n+1)(2n+1)+\dfrac{(2n+1)^2}{4}=n^2-n(2n+1)+\dfrac{(2n+1)^2}{4}
$$
  
  Soit
  
$$
  \left((n+1)-\dfrac{2n+1}{2}\right)^2=\left(n-\dfrac{2n+1}{2}\right)^2
$$

  En passant à la racine carrée, on obtient
  
$$
  (n+1)-\dfrac{2n+1}{2}=n-\dfrac{2n+1}{2}
$$
  
  d'où
  
$$
  n+1=n
$$
  
  Et si je prends $n=2$, j'ai aussitôt $3=2$
  
-  Alors, qu'est-ce que vous en dites?
-  Je...commença Alice.
-  D'ailleurs, cela prouve que tous les entiers sont égaux, la coupa le Lièvre de Mars.
-  Pas mal du tout! Qu'en dites-vous mademoiselle la mathématicienne?
-  Je vais vous dire tout de suite ce que j'en pense.
-  Ah non! Nous préférerions de loin que vous pensiez ce que vous allez nous dire.
-  C'est pareil! grinça Alice qui commençait à en avoir assez.
-  Comment  ça, c'est  pareil? Dire  ce que  l'on pense  ce serait
  pareil que penser ce que l'on dit? S'étrangla le Lièvre de Mars. 
-  Incroyable! Et manger ce qu'on voit ce serait pareil que voir ce qu'on mange?
-  Mais...
-  Et respirer quand on dort pareil que dormir quand on respire?
-  En logique, nous vous mettons 3 sur 5.
-  Autant dire moins que un.
-  C'est à dire zéro, puisque si 2=3 alors 1=0.
-  Parce que chez vous, 3 c'est moins que 1? s'indigna Alice.
- On se  demande ce qu'on vous apprend  à l'école!  Tiens, et je  peux même vous
  donner une démonstration très simple. 
  Prenez deux nombres non nuls et égaux:
  
$$
  a=b
$$ 

  Alors

$$
  a^2=ab 
$$

  et

$$
  a^2-b^2=ab-b^2
$$

  Un peu d'identité remarquable:

$$
  (a+b)(a-b)=b(a-b)
 $$

  Je simplifie:

$$ 
  a+b = b
$$

  Mais comme $ a=b$:

$$ 
  2b=b
$$

  et je simplifie:

$$
  2=1 
$$

-  C'est de la folie pure, pensa Alice...


### Souvenirs...

| date | Cas (% augmentation)| Décès (% augmentation)|
|---|---|---|
|2020-03-01 | 35(52%) | | 
|2020-03-02 |40(14%)| | 	
|2020-03-03 | 51(28%)| | 	
|2020-03-04 | 85(67%)| | 	
|2020-03-05 | 114(34%)| 	1(n.a.) |
|2020-03-06 | 160(40%) | 2(100%) |
|2020-03-07 | 206(29%) | 2 |
|2020-03-08 | 271(32%) | 3(50%) |
|2020-03-09 | 321(18%) | 7(133%) |
|2020-03-10 | 373(16%) | 7 |
|2020-03-11 | 456(22%) | 9(29%) |
|2020-03-12 | 590(29%) | 10(11%) |
|2020-03-13 | 797(35%) | 28(180%) |
|2020-03-14 | 1,061(33%) | 43(54%) |
|2020-03-15 | 1,391(31%) | 65(51%) |
|2020-03-16 | 1,543(11%) | 81(25%) |
|2020-03-17 | 1,950(26%) | 115(42%) |
|2020-03-18 | 2,626(35%) | 158(37%) |
|2020-03-19 | 3,269(24%) | 194(23%) |
|2020-03-20 | 3,983(22%) | 250(29%) |
|2020-03-21 | 5,018(26%) | 285(14%) |
|2020-03-22 | 5,683(13%) | 359(26%) |
|2020-03-23 | 6,650(17%) | 508(42%) *annonce du confinement prenant effet le 26* |
|2020-03-24 | 8,077(21%) | 694(37%) |
|2020-03-25 | 9,529(18%) | 877(26%) |
|2020-03-26 | 11,658(22%) | 1,161(32%) |
|2020-03-27 | 14,548(25%) | 1,455(25%) |
|2020-03-28 | 17,104(18%) | 1,669(15%) |
|2020-03-29 | 19,606(15%) | 2,043(22%) |
|2020-03-30 | 22,271(14%) | 2,425(19%) |
|2020-03-31 | 25,521(15%) | 3,095(28%) |

**Cas de Covid et décès au Royaume-Uni au mois de mars 2020**
  [**Source Wikipedia**](https://fr.wikipedia.org/wiki/Pand\%C3\%A9mie_de_Covid-19_au_Royaume-Uni)



Boris JOHNSON annonce

- **le 12 mars** : *We are considering the question of banning major public events such as sporting fixtures. The scientific advice as we’ve said over the last couple of weeks is that banning such events will have little effect on the spread.*
- **le 16 mars** : *And without drastic  action, cases could double every 5 or 6
  days.* 

[Source](https://www.gov.uk/government/speeches/pm-statement-on-coronavirus-16-march-2020)


Voici  quelques  lignes  en  Python   que  vous  allez  exécuter,  commenter  et
expliquer :

```python
import matplotlib.pyplot as plt
from math import log

d = [1,1,0,1,4,0,2,1,18,15,22,16,34,43,36,56,35,74,149,187,183,284,294,214,374,382]

s = [0,1,2,2,3,7,7, 9,10,28,43,65,81,115,158,194,250,285,359,508,695,878,1162,1456,1670,2044]

t = [k for k in range(6,32)]

ls = [log(x) for x in s]

plt.bar(t,d)
#plt.plot(t,s)
#plt.plot(t,ls)
plt.show()
```


## Un brin de logique


!!! quote "Through the looking glass - Lewis CARROLL"

	Contrariwise, continued Tweedledee, if it was so, it might be; and if it 
	were so, it would be; but as it isn't, it ain't. That's logic. 
	
	
	
	
 
### Contexte



On appellera **proposition** tout  énoncé dont on peut décider s'il
est vrai ou faux indépendamment de ses composantes.

On  distinguera   la  logique  des   propositions  de  la   logique  des
**prédicats** qui  introduit des variables dans  les assertions qui
peut  les rendre  donc parfois  vraies et  parfois fausses.


Par  exemple  «  *Je  suis  actuellement  en salle  3*  »  est  une  proposition
susceptible d'être vraie  ou fausse dans toute situation alors  que la valeur de
vérité de « *x est actuellement en salle 3* » dépend de $x$.





En informatique, **Vrai** et **Faux** sont appelées des 
**booléens** (Qui était George BOOLE?)  notées `True` 
et `False` en Python :

```python
In [54]: 2 > 3
Out[54]: False

In [55]: 2 <= 3
Out[55]: True

In [56]: 2 + 2 == 4
Out[56]: True

In [57]: len("Papa") == 4
Out[57]: True
```


### Implication

!!! {{ exercice()}}

    Soit un entier naturel $n$ inférieur à 20.  On considère la proposition « si
    $n$ est pair, alors son successeur est premier ». 
	
	Quels sont les entiers qui rendent cette proposition vraie?  
 



!!! {{ definition("Implication") }}
     $A$ et $B$ étant deux propositions, on note $A ⟹ B$ ($A$ *implique* $B$) la
     proposition qui  est toujours  vraie sauf  quand $A$ est  vraie et  $B$ est
     fausse. 
	 
	 
- Que penser de l'exercice précédent en terme d'implication?
- Que penser de cette citation de Pierre DAC (in
  *Y'a du mou dans la  corde à noeuds*): « Avec le mot SI  on peut faire tout ce
  qu'on ne peut pas faire.* » ?
  




On  peut  résumer  la  situation  dans une  **table  de  vérité**  car  les
propositions $A$ et $B$ ne peuvent prendre que deux valeurs donc il n'y a que quatre possibilités pour les valeurs du *couple* $(A,B)$:

| $A$ | $B$ | $A⟹B$|
|:---:|:---:|:----:|
| F | F | V |
|  F | V | V |
|  V | V | V |
| V | F | F |

Ainsi, le cas  où $A$ est fausse ne  nous intéresse pas vraiment car  on est sûr
que l'implication sera vraie quelque soit la valeur de vérité de $B$. 

Ainsi, $A⟹B$ étant vraie, on peut dire que:

- SI $A$ (est vraie) ALORS $B$ (est vraie)
- IL SUFFIΤ que $A$ soit vraie pour que $B$ soit vraie
- IL FAUT que $B$ soit vraie pour que $A$ soit vraie
- $A$ est une **condition suffisante** de $B$
- $B$ est une **condition nécessaire** de $A$

!!! exemple

	Soit  $A$  la  proposition  :  $x$  est un  entier  naturel.   Soit  $B$  la
	proposition: $x$ est positif.
	
	Les propositions suivantes sont vraies: 

	- SI $x$ est un entier naturel ALORS $x$ est positif
    - IL SUFFIΤ que $x$ soit un entier naturel pour que $x$ soit positif
    - IL FAUT que $x$ soit positif pour que $x$ soit un entier naturel
    - $x$ entier naturel est une **condition suffisante** de $x$ positif
    - $x$ positif est une **condition nécessaire** de $x$ entier naturel

	On a bien $x$ est un entier naturel implique que $x$ est positif ($A ⟹ B$ est vraie).


### Contraposée


Reprenons notre exemple: SI $x$ **n'est pas** positif ALORS $x$ **ne** peut **pas** 
être un entier naturel. 

On note $\lnot A$ la **négation** de $A$. L'observation précédente signifie
que $\lnot B ⟹ \lnot A$. Est-ce toujours le cas? Dressons la table!


| $A$ | $B$ | $A⟹B$ | $\lnot B$ | $\lnot A$ | $\lnot B ⟹ \lnot A$ |
|:---:|:---:|:-------:|:---:|:---:|:-------:|
| F | F | **V** | V | V | **V** |
| F | V | **V** | F | V | **V** |
| V | V | **V** | F | F | **V** |
| V | F | **F** | V | F | **F** |


Ainsi, dans  tous les cas, $A⟹B$  et $\lnot B ⟹  \lnot A$ ont la  même valeur de
vérité. 


!!! tip "À retenir"

	 Ainsi, dans certains  cas, pour démontrer une implication,  il sera parfois
	 plus simple de démontrer la contraposée. 
	 
	 
!!! info "Remarque"

	Dans un «  théorème » du cours  de mathématique sous la  forme *SI hypothèse
	ALORS conclusion*, on a en fait une implication *hypothèse $⟹$ conclusion*.

  
	-  si on  a l'hypothèse vérifiée et  le théorème démontré alors  on peut en
    déduire que la conclusion est vraie. C'est le **modus ponens**.
    -  si on a le contraire de la conclusion vérifié et le théorème démontré,
      alors on peut e déduire que le contraire de l'hypothése est vrai. C'est le
      **modus tollens**.
  

    Par exemple, en supposant que la proposition « $x$ est un entier naturel
    implique  que $x$  est positif »  est un  théorème, 3  étant un  entier
    naturel, on en déduit que 3 est positif.

    On peut aussi dire  que comme $-3$ n'est pas positif, on  en déduit que $-3$
    n'est pas un entier naturel.
	
	
	

### Contraire (négation) d'une implication



On voudrait savoir si la proposition « *un nombre est positif
implique que ce nombre est un entier naturel* » est vraie.

On « sent » bien que non.  Par exemple $\sqrt{2}$ est bien positif
(hypothèse vraie) mais
pourtant ce n'est pas un entier naturel (conclusion fausse).

Si on formalise on a $A$ ET $¬B$. Est-ce que par hasard $A$ ET $¬B$ ne serait pas
la négation de $A⟹B$?

C'est quoi ET? Pour être en accord avec le « bon sens », on dira que $A$ ET
$B$  est  vraie   seulement  quand  les  deux  propositions  $A$   et  $B$  sont
 simultanément vraies. On note cet opérateur $\wedge$.
 
 
| $A$  | $B$  |  $¬B$ | $A\wedge ¬B$ | $A⟹B$ |
|:-:|:-:|:-:|:-:|:-:|
|   F | F | V | F | V|
|  F | V | F | F | V|
|  V | V | F | F | V|
|  V | F | V | V | F|




Bingo! Les  deux propositions sont  bien contraires.  Or une proposition  et son
contraire ne peuvent pas être vraies simultanément (c'est ce qu'on appelle le **tiers exclus**). 

Puisque  $A\wedge¬B$ est  vraie  (dans le  cas  de $\sqrt{2}$),  c'est que  son
contraire, $A⟹B$ est faux.



### Quantificateurs


#### Un pour tous : $\forall$


Mmmmmm..... Y  a quelque  chose de  pas très  clair cependant.  On a  parlé d'un
certain $x$, ou d'un certain entier...C'est un peu flou.

Il vaudrait mieux écrire que notre théorème est en fait:

«  *Quelque soit  le nombre  $x$, si  $x$  est un  entier naturel  alors il  est
positif* »

Plus formellement, on a l'habitude de noter cela:

$$
(\forall x)(x\in\mathbb N ⟹ x ≥0)
$$

!!! info "Remarque"

     Ce symbole $\forall$ se lit « *pour tout* » (Notation introduite par l'Allemand GENTZEN} en 1933: *für Alle* d'où le A à l'envers).


#### Un pour l'existence : $\exists$



Nous avons établi qu'il existait **au moins** un nombre positif ($\sqrt{2}$) qui
n'était pas un entier naturel. Cela s'écrit formellement:

$$
(∃x)(x≥0 ∧ x ∉ \mathbb N)
$$


Ce symbole se lit « *il existe* **AU MOINS** » (On le doit au britannique
Bertrand RUSSELL dans ce contexte).

Il existe une variante: $(∃!)$ qui signifie « il existe un **UNIQUE** élément »

#### Quantificateurs et négation

Quand ce n'est pas vrai pour tous, c'est que c'est faux pour au moins un.

Quand ce n'est pas vrai pour au moins un, c'est que c'est faux pour tous.

Ainsi, si on note $P(x)$ une certaine propriété dépendant de $x$ :

$$
¬((∃x)(P(x)) ≡ (∀x)(¬P(x))
$$

$$
¬((∀x)(P(x)) ≡ (∃x)(¬P(x))
$$


La dernière proposition renvoie à  la recherche de **contre-exemple**: pour
montrer que  quelque chose n'est  pas toujours vrai, il  faut exhiber un  cas où
cette chose est fausse.

!!! tip "À retenir"
	
	Bien repérer les quantificateurs dans les énoncés de propositions permet de donner des pistes de démonstrations.


!!! info "Remarque"
	
	Souvent, on contracte l'écriture $(∀x)(x ∈ \mathbb R)$ en $(∀x ∈ \mathbb R)$



### Réciproque / Équivalence

Lorsqu'on  a établi  qu'une  implication  était vraie,  on  a  souvent envie  de
vérifier l'implication *réciproque* (i.e. avec la « flèche dans l'autre
sens »).

Si c'est le cas, on utilise le symbole $⟺$:

$$
(A⟹B) ∧ (B⟹A) ≡ (A⟺B)
$$

On lit « *A est équivalent à B* » ou « *A* **si, et seulement si** *B* ».

!!! {{ exercice()}}

    === "Énoncé"

         Est-ce que deux nombres sont égaux **si, et seulement si** leurs carrés
         sont égaux ?

	=== "Indications"

         Nous voudrions donc voir si la proposition suivante est vraie :
          
          $
		  (∀x∈ℝ)(∀y∈ℝ)(x²=y²⟺x=y)
          $
		  
	     *Pour démontrer une équivalence on procède souvent par* **double inclusion**.

	     - **$\fbox{x=y  ⟹ x²=y²}$**  Ce sens est  assez évident  puisque, pour
	       tous réels $x$ et $y$:
		   
		    $$
		     x²=x×x=y×y (\text{ car }x=y) = y²
		    $$
			
			Ainsi:

            $$
		     (∀x∈ℝ)(∀y∈ℝ)(x=y⟹x²=y²) 
		    $$
		   
         -  **$\fbox{x=y ⟸  x²=y²}$**  on sent  qu'il  y a  u  problème en  tant
           qu'élève bien entraîné(e).  En  effet, $(-2)²=2²$ et pourtant $-2\neq
           2$.  Plus formellement:

	        $$
		     (∃x∈ℝ)(∃y∈ℝ)(x²=y² ∧ ¬(x=y))
		    $$
			
			ou encore:

            $$
		     ¬\bigl( (∀x∈ℝ)(∀y∈ℝ)(x²=y²⟹x=y) \bigr) 
		    $$

           ce qui  traduit que l'implication dans  ce sens est fausse,  donc que
           l'équivalence est fausse. 

!!! {{ exercice()}}

    Démontrer que $(∀x∈ℝ_+)(∀y∈ℝ_+)(x²=y²⟺x=y) $


## Récurrence


### Récursion, récurrence, induction





...des mots  qui tournent autour du même
thème mais qu'il faut savoir distinguer.

L'**induction**  consiste   à  conclure  à   partir  d'observations
préalables: cela correspond à  la démarche expérimentale classique. Sans
précautions, cela peut conduire  à certaines contre-vérités. Par exemple
3, 5 et 7 sont premiers  donc on pourrait en déduire, par induction, que
tous les nombres impairs à partir de 3 sont premiers et pourtant...


L'**induction    mathématique**    ou   **raisonnement    par
récurrence** corrige  ce défaut. On  part toujours d'un  résultat observé
mais on  *démontre* qu'il  est vrai pour  tous les  éléments d'un
ensemble donné,  éventuellement infini. C'est  l'induction expérimentale
corrigée par la rigueur du raisonnement mathématique.

En  informatique, une  **fonction récursive**  (ou  un *type
récursif*)  est  une  fonction  (ou   un  type)  qui  fait  référence  à
elle(lui)-même  dans sa définition.  Ce mécanisme  est très  puissant et
permet de condenser l'écriture d'un programme.


```python
In [12]: def yati1adans(mot):
    ...:     if len(mot) == 0:
    ...:         return False
    ...:     elif mot[0] == 'a':
    ...:         return True
    ...:     else:
    ...:         return yati1adans(mot[1:])
    ...: 

In [13]: yati1adans("Gertrude")
Out[13]: False

In [14]: yati1adans("Germaine")
Out[14]: True
```




C'est l'induction mathématique qui
nous   intéresse    ici.   Rappelons
quelques principes.


Pour démontrer qu'une proposition $\cal P_n$ dépendant uniquement
d'un paramètre $n$ est vraie pour tout $n \geqslant n_0$, il faut
vérifier que:


- $\cal P_{n_0}$ est vraie (on parle parfois d'initialisation);

- pour tout $n \geqslant n_0$, $\cal P_n \Longrightarrow \cal P_{n+1}$ est une
  implication  vraie (on parle parfois d'hérédité). 



!!! note "remarque"
    
	 Vous vous souvenez que $A⟹B$ est toujours vraie si la proposition A est
     fausse.  Donc le seul cas intéressant à envisager est celui où A est
     vraie. Alors, pour que l'implication soit vraie, il faut que B soit vraie aussi.

     C'est pourquoi en Terminale on vous a souvent dit qu'il fallait démontrer que
     si $\cal P_n$ est vraie alors $\cal P_{n+1}$ est vraie aussi. Cela **suffit** à
     démontrer que l'implication est vraie.

Quand on a  besoin de supposer que non seulement  la proposition est vraie
pour un certain $n$ mais aussi pour tous les entiers qui lui sont
inférieurs (pas  seulement le  père mais aussi  tous les  ascendants) on
parle alors de *récurrence forte*.


Voyons un exemple...

### Un jeu



Voici un  test de fin d'étude  maternelle en Syldavie :  prenez un cube,
placez en-dessous  deux autres cubes, et encore  en-dessous trois cubes,
etc.




Combien y a-t-il de cubes bleus au total sur un dessin avec trois lignes ? On
peut encore les compter à la main,  mais que faire si je vous demande le
nombre de cubes lorsqu'on a placé 100 rangées ? $n$ rangées ?

Le dessin nous donne une idée : si nous complétons la figure pour former
un  rectangle, il  y a  deux fois  plus de  cubes, mais  maintenant nous
pouvons  les compter. Il  y en  a en  effet $\dfrac{4\times  (4+1)}{2}$, et
donc 

$$
1+2+3+4=\dfrac{4\times(4+1)}{2}
$$


Reprenons la méthode évoquée plus haut :

- Nous allons essayer de prouver que la proposition suivante est vraie pour tout entier naturel $n$
  
$$
  \cal P_n : {\rm «} 0 + 1+2+3+\cdots+n=\dfrac{n(n+1)}{2} {\rm »}
$$

- Il est facile de vérifier que $0=\dfrac{0(0+1)}{2}$, donc la deuxième étape de
  notre raisonnement est vérifiée 
  
$$
  \cal P_0\ {\rm est\ vraie}
$$

- Supposons  qu'une «  génération »,  appelons-la par  exemple la  $k$-ème, soit
  «  infectée  ».   Plus sobrement  on  dira  :  soit  $k$ un  entier  supérieur
  à 1. Supposons que $\cal P_k$ soit vraie et essayons alors de démontrer que cela
  implique que la génération suivante ,  la $k+1$-ème, sera elle aussi infectée,
  ce qui impliquera que :
  
$$
\cal P_k\ \Longrightarrow \cal P_{k+1}
$$

Il    s'agit   donc    de    calculer    $1+2+3+\cdots+k+(k+1)$   sachant    que
$1+2+3+\cdots+k=\dfrac{k(k+1)}{2}$, or :

$$
\begin{align*}
\cal P_k ⟹& 1+2+3+\cdots+k+(k+1) &=& \displaystyle\underbrace{1+2+3+\cdots+k}&+(k+1) \\
&&=&\ \frac{k(k+1)}{2}  & +(k+1)\\
&&= &\ (k+1)\left(\frac{k}{2}+1\right) & \\
&&=&\ (k+1)\left(\frac{k+2}{2}\right)  &\\
&&=&\ \frac{(k+1)(k+1+1)}{2}\\
      &&&  {\displaystyle\underbrace{\hspace{9em}}}&\\
  &&&{\hspace{3em} \cal P_{k+1}}&\\
\end{align*}
$$

Nous en déduisons que $(\forall k \in ℕ)(\cal P_k⟹\cal P_{k+1})$.


- Nous avons vérifié que la proposition était vraie au rang 0 et
qu'elle était héréditaire.  Nous allons donc en déduire que la proposition
sera toujours vraie, quelque soit  l'entier naturel $n$ grâce au
théorème admis suivant:


### Les théorèmes


!!! {{ theoreme("(admis) Principe de récurrence simple")}}

    Soit $\cal P_n$ une proposition ne dépendant que d'un entier $n$.
    
	SI :
	
	- il existe un entier $n_0$ tel que $\cal P_{n_0}$ soit vraie ;
	
	- $(\forall n \geqslant n_0)(\cal P_n⟹\cal P_{n+1})$

    ALORS :
	
	$$
	(\forall n \geqslant n_0)(\cal P_n ) {\rm (sous-entendu\ « \ est \ vraie »)}
	$$


On retiendra donc la méthode suivante :

!!! note "À retenir"

    Pour démontrer que $\cal P_n$ est vraie pour tous les entiers naturels $n$
    supérieurs à un certain $n_0$
	
	- On expose clairement la proposition $\cal P_n$.
	
	- On  vérifie que  $\cal P_{n_0}$ est vraie  : c'est le  **pas initial**  de la
	  récurrence.
	  
	- On démontre ensuite que $\cal P_k ⟹ \cal P_{k+1}$ pour n'importe quel entier $k
	  \geqslant n_0$ :  c'est le passage du  rang $k$ au rang  $k+1$ qui exprime
	  que la proposition $\cal P_n$ est **héréditaire**.
	  
	- Il  reste à  **conclure** en  annonçant que,  d'après le  principe de
	  récurrence simple,  la proposition est  vraie pour tout entier  naturel $n
	  \geqslant n_0$ 


!!! {{ theoreme("(admis) Principe de récurrence à deux pas")}}

    Soit $\cal P_n$ une proposition ne dépendant que d'un entier $n$.
  
	SI:
  
	- il existe un entier $n_0$ tel que $\cal P_{n_0}$ et $\cal P_{n_0+1}$ soient vraies;
    
	- $(\forall n \geqslant n_0)(\cal P_n ∧ \cal P_{n+1}⟹\cal P_{n+2})$
  
	ALORS:
	
	$$
	(\forall n \geqslant n_0)(\cal P_n ) 
	$$
	
	
!!! {{ exercice()}}

    On pose $F_0=F_1=1$ et $(\forall n \geqslant 0)(F_{n+2}=F_{n+1}+F_n)$.

	Démontrer que $(\forall n \geqslant 0)(0\leqslant F_n \leqslant \left(\frac{5}{3}\right)^n)$



!!! {{ theoreme("(admis) Principe de récurrence forte")}}

	Soit $\cal P_n$ une proposition ne dépendant que d'un entier $n$.
  
	SI:
	
	- il existe un entier $n_0$ tel que $\cal P_{n_0}$  soit vraie;
  
	- $(\forall n \geqslant n_0)(\cal P_{n_0} ∧ \cal P_{n_0+1} ∧ \cdots ∧ \cal P_{n-1}∧\cal P_n⟹\cal P_{n+1})$
  
	ALORS:
  
	$$
	(\forall n \geqslant n_0)(\cal P_n ) 
	$$


!!! {{ exercice()}}

	On        pose        $a_1=3$        et        $(\forall        n        \in
	ℕ^*)(a_{n+1}=\dfrac{2}{n}\displaystyle\sum_{k=1}^na_k)$.  Démontrer que  $(\forall n
	\in ℕ^*)(a_n=3n)$
	
	
	
	
!!! warning "Attention !"

	-  Attention à vérifier  que la proposition ne dépend que  d'un entier et pas
    d'un réel quelconque ;
	
    - Attention à ne pas tomber dans le piège classique de rédaction : « 
      supposons que la proposition soit vraie **pour tout** $n$ »
	  au lieu de « pour **un certain** $n$ » ;
    
	-   Si  vous démontrez  $\cal P_n⟹\cal P_{n+1}$  sans  utiliser le  fait  que
        $\cal P_n$  soit vraie  c'est  que  vous n'avez  pas  besoin d'utiliser  le
        principe de récurrence et qu'une démonstration directe suffit.



# Les grands types de raisonnements : le résumé

Voici  résumées les  méthodes classiques  de raisonnement  qui seront  utiles en
mathématiques et dans la « vraie vie »



## Avec des quantificateurs

-  Pour  montrer que  quelque  soit  $x \in  E,  P(x)$,  on choisit  un  élément
  **quelconque** $x$ de $E$ et on  montre la propriété $P(x)$. Ceci se fait
  à  l'aide de  la phrase  «  Soit $x  \in  E$  »  suivie d'un  raisonnement
  abstrait (ou  d'un calcul littéral)  considérant $x$ comme une  quantité fixée
  une bonne fois pour toutes. 
  
- Pour  montrer qu'il existe  un $x \in E$  vérifiant $P(x)$, on  l'exhibe. Soit
  explicitement, soit à l'aide d'une construction abstraite plus difficile... 
  
- Pour  montrer qu'un *unique* $x  \in E$ vérifie  $P(x)$, la plupart du  temps on
  choisit deux éléments $x$ et $y$ de $E$ et on montre que la véracité de $P(x)$
  et de $P(y)$ implique $x=y$ 
  
- Pour montrer qu'il n'existe pas de $x  \in E $ vérifiant $P(x)$, on montre que
  $\forall x \in E, \text{ non }P(x)$ 
  
  
## Pour démontrer une implication

- Le  raisonnement direct.  De la  propriété $P$, on  arrive à  déduire l'énoncé
  $Q$.  Techniquement,  on  suppose  $P$  vraie  et  on  démontre  que  $Q$  est
  (nécessairement) vraie.
  
- La raisonnement par la **contraposée** : ce type de raisonnement repose
   sur la règle : $(A \Rightarrow B) = (\overline{B} \Rightarrow \overline{A})$.
   
!!! {{ exercice()}}

       Soit $ a \in ℝ$. Montrons que $(\forall \varepsilon > 0, |a|<\varepsilon) \Rightarrow a=0$.

- Le raisonnement par l'**absurde**. On prend comme hypothèse à la fois l'énoncé
   $P$ et l'énoncé $non(Q)$, c'est-à-dire  la négation de l'implication que l'on
   cherche  à  démontrer.  Il  s'agit  de montrer  qu'une  telle  hypothèse  est
   absurde.  Si la  négation  de l'implication  est absurde,  on  en déduit  que
   l'implication est vraie. 
   
!!! {{ exercice()}}

       Montrer que  pour tout $n  \in ℕ$, on  a l'implication suivante  : $n^2$
       pair $\Rightarrow$ $n$ pair 

- Une  **disjonction de cas**.  Ce raisonnement est  acceptable à condition  de bien
   faire une étude de tous les cas possible. 
   
## Un raisonnement par récurrence

Déjà abondamment commenté

## Équivalence

- On peut démontrer une équivalence par **double implication** 

- Procéder par **Analyse-Synthèse** :
    - *Phase d'analyse* : on recherche des conditions ($\mathcal{C}$) nécessaires
      pour une certaine hypothèse ($\mathcal{H}$). 
    - *Phase de synthèse* : on contrôle le caractère suffisant (ou pas) de ces
      conditions. Une  fois trouvées des conditions  nécessaires et suffisantes,
      on conclut : $(\mathcal{H}) \Leftrightarrow (\mathcal{C})$ 
	  
!!! {{ exercice()}}

      Démontrer que  toute fonction $f :  ℝ \rightarrow ℝ$ s'écrit  de manière
      unique comme somme d'une fonction paire et d'une fonction impaire. 

