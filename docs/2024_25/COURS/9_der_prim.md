{{ chapitre(9, "Dérivées et primitives: rappels")}}


Cours au format 


- [PDF](../LATEX/Poly_der_prim_24.pdf) 


- et ses sources [TEX](../LATEX/der_prim_24.tex)
