

## 2024/2025

<!--

### Le Poly entier


- Le [Poly](../LATEX/PolyTotalB1_23.pdf)

- et ses sources [TeX](../PolyTotalB1_23.tex)

-->

### Les chapitres

- [Des conseils pour étudier le cours](0_conseils.md)

- [Un avant-goût des mathématiques en BCPST](1_Avant_gout.md)

- [Pourquoi des maths et de l'info en BCPST ?](1_Avant_gout_2.md)

- [1. Fonctions : rappels](1_Fonctions.md)

- [2. Bases pour raisonner](1_Bases_logique_sommes.md)

- [3. Ensembles](2_sets.md)

- [4. Réels](3_reels.md)

- [5. Trigonométrie](4_trigo.md)

- [6. Complexes](5_complex.md)

- [7. Dénombrements](6_denomb.md)

- [8. Suites réelles : rappels](7_suites_reelles.md)

- [9. Dérivées et primitives : rappels](9_der_prim.md)

- [10. Calcul intégral : rappels](10_Integrales21.md)

- [11. Équations différentielles : rappels](11_EquaDiffs.md)

- [12. Calcul matriciel](12_Matrices.md)

- [13. Systèmes linéaires](13_Systemes.md)

- [14. Statistique descriptive](15_Statistique.md)

- [15. Geometrie: rappels](14_Geometrie.md)

- [16. Polynomes](16_Polynomes.md)

- [17. Espaces vectoriels](17_EV.md)

- [18. Suites réelles : suite](18_Suites_conv.md)

- [19. Limites et continuité](19_Limites_cont.md)

- [20. Dérivation approfondissement](20_Der_App.md)

- [21. Développements limités](21_DL.md)

- [22. Intégration II](22_Integ_2.md)

- [23. Applications linéaires](23_AppLin.md)

- [24. Probabilités I](24_probas_I.md)

- [25. VAR sur un univers fini](25_VAR.md)

- [26. Fonctions de 2 variables](26_f2v.md)

<!-- [Graphes I](27_graphes.md)-->
