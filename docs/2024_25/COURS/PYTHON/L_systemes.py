from turtle import Turtle, exitonclick
from numpy.random import random, randint



def LS1(n, regle, axiome, regle2 = None, proba = None):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole == "F":
                new_mot = new_mot + regle
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot

def LS2(n, regle, axiome, regle2 = None, proba= None):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole in "BX":
                new_mot = new_mot + regle
            elif symbole == "F":
                new_mot = new_mot + "FF"
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot

def LS3(n, regle1,  axiome, regle2, proba):
    mot = axiome
    for etape in range(n):
        new_mot = ""
        for symbole in mot:
            if symbole in "BX":
                if random() < 0.5:
                    new_mot = new_mot + regle1
                else:
                    new_mot = new_mot + regle2
            elif symbole == "F":
                new_mot = new_mot + "FF"
            else:
                new_mot = new_mot + symbole
        mot = new_mot
    return mot


        
def trace_LS(n, d, a, regle, axiome, fonc, regle2 = None, proba = None):
    gen = Turtle()
    gen.speed(1000)
    gen.fillcolor("yellow")
    gen.left(90)
    gen.penup()
    gen.backward(500)
    gen.pendown()
    positions = []
    mot = fonc(n, regle, axiome, regle2, proba)
    for symb in mot:
        if symb in "FfX":
            gen.color("lightgreen")
            gen.forward(d)
        elif symb == "S":
            gen.color("black")
            gen.forward(d)
        elif symb == "B":
            gen.color("lightblue")
            gen.dot()
        elif symb == "*":
            gen.left(a)
        elif symb == "/":
            gen.right(a)
        elif symb == "+":
            gen.left(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "-":
            gen.right(randint(int(a*0.8), int(a*1.2) + 1))
        elif symb == "[":
            positions.append((gen.position(), gen.heading()))
        elif symb == "]":
            pos, ang = positions.pop()
            gen.penup()
            gen.setposition(pos)
            gen.setheading(ang)
            gen.pendown()
        elif symb == "{":
            gen.begin_fill()
        elif symb == "}":
            gen.end_fill()
    exitonclick()

# trace_LS(3, 5,20, 'FF+[+F-F-F]-[-F+F+F]', 'F', LS1)
#trace_LS(6, 5, 25.7, "F[+B]F[-B]+B", "B", LS2)
#trace_LS(2, 50, 90, 'F[F]-F+F[--F]+F-F', 'F', LS1)
#trace_LS(5, 5,20, 'FF+[+F-F-F]-[-F+F+F]', 'F', LS1)
#trace_LS(4, 5, 20, '-f{[-f+f+f][+f-f-f]}F[[-B][+B]]F[+FB]-B', 'F', LS3, "+f{[-f+f+f][+f-f-f]}F[-B]F[-FB]+B", 0.5) 


