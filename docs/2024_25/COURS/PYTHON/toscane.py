from numpy.random import randint

def toscane(nb_parties: int) -> list[float]:
    freqs = [0 for _ in range(0, 19)]
    for partie in range(nb_parties):
        som = randint(1, 7) + randint(1, 7) + randint(1, 7)
        freqs[som] += 1
    return [f/nb_parties for f in freqs]

res = toscane(100000)

print(f"Les fréquences: {[(i, res[i]) for i in range(19)]}")

print(f"\nExpérimentalemant 9: {res[9]}, 10: {res[10]}\n\nEn théorie 9: {25/6**3} 10: {27/6**3}" )
