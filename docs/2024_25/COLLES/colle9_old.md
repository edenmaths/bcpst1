---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 9

**Semaine du 27 novembre au 1er décembre 2023**

## [Cours](../../LATEX/PolyTotal21.pdf)





### Généralités sur les suites réelles

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).


### Généralités sur les fonctions


#### Prérequis (cf chapitre 1)

- Cours de 2nde : définition d'une fonction, lecture graphique, sens de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
  
#### Dérivées - Primitives


- Commandes Python permettant de tracer des représentations graphiques.
- Approche empiriquo-historique - Nombre dérivé - Dérivabilité en un réel -
  Application au calcul de certaines limites - Calcul des dérivées usuelles
  à partir  de la définition -  Approximation affine - Dérivabilité  sur un
  intervalle - Lien avec les variations - Fonctions strictement monotones -
  Extremum local - Dérivée d'une composée - Primitives.
- **Remarque**  : les  calculs de  limites *ne  sont pas*  un objectif  de ce
  chapitre. On insistera  sur le calcul de  dérivées/primitives, l'étude de
  sens de variation, le calcul  d'équations de tangentes, la représentation
  graphique  via une  allure rapide  « à  la main  » PUIS  avec Python,  la
  recherche d'extrema.
 

### [Python](../../INFORMATIQUE/3_Recursion/)

- Exemples de boucles : calculs de sommes et de produits - compteurs - test
  de présence de caractères dans une chaîne - Recherche d'extremum. 
- Fonctions récursives.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

-    On    considère    la    suite   $(u_n)$    définie    par    :    $u_0=1$,
  $u_1=\sqrt{3}-\dfrac{1}{2}$     et      $\forall     n     \in      ℕ,     \:
  u_{n+2}=2\sqrt{2}u_{n+1}-4u_{n}$. Exprimer son terme général en fonction de $n$. 
- $\displaystyle\lim_{x\to0} \dfrac{\tan(x)}{x}$
- Calcul de la dérivée de $\sqrt{1+\sqrt{1+x^2}}$ + sens de variation
- Primitive de $\dfrac{1+\tan^2(2x)}{\bigl(42+\tan(2x)\bigr)^{42}}$
- Une boîte fermée en forme de pavé droit à base carrée a un volume de
  500m$^3$. Déterminer les dimensions qui minimisent la quantité de matière
  utilisée pour la fabriquer. 


## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 8 et 9 du
  Poly     et      utiliser     des     exemples     du      [cahier     de
  calcul](https://edenmaths.gitlab.io/bcpst1/2022_23/COURS/Revisions_BCPST.pdf). 

- **On commencera par demander  de tracer la représentation graphique d'une
  fonction ou d'une suite à l'aide de Python en moins de 3 minutes sinon la note sera
  inférieure  à  5.   Les  candidats  auront donc  leur  ordinateur  ou  un
  ordinateur de la salle 16 prêt à l'emploi**
  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


