---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 3

**Semaine du 30 septembre au 4 octobre2024**

## Cours



### [Bases pour raisonner](../LATEX/Poly_logic_24.pdf)

Logique - Implication - Contraposée -  Négation - Quantificateurs - Réciproque -
Équivalence - Récurrence simple, double, forte - Analyse Synthèse.


### [Ensembles - Applications]((../LATEX/Poly_sets_24.pdf))

Appartenance - Inclusion - Égalité - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition  - Produit  cartésien -  Fonction -  Fonction totale,  injective,
surjective,  bijective  (théorème  de   la  bijection  non  vu),  bijection
réciproque.



### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/1_Premiers_Pas/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
-FONCTIONS - **Calcul de sommes et de produits, compteurs**.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous:

1.   Caractériser  à   l'aide  de   quantificateurs  l'injectivité   et  la
   surjectivité.  Comment démontrer  qu'un fonction  n'est pas  injective ?
   surjective ?

1.  Écrire une  fonction `inter(ens1: list, ens2: list) ->  list` qui renvoie
  l'intersection   de   deux  listes   au   sens   de  l'intersection   des
  ensembles. Faites de même pour la réunion.
  
1. Déterminer une fonction `images(dep: list, Gr: list, x) -> list` qui prend un
   ensemble  de départ  `dep` modélisé  par une  liste et  le graphe  `Gr` d'une
   relation modélisé par une liste de couples et qui renvoie la liste des images
   de l'élément `x` donné en paramètre à la fonction.
   
   Proposer un ou plusieurs exemples.
   
1. À l'aide  de la fonction précédente supposée connue,  déterminer une fonction
   `est_fonction(dep: list,  Gr: list) ->  bool` qui détermine si  une relation
   déterminée par son ensemble de départ et son graphe est une fonction. 

1. Tout exerice du [DS1](../DS/DS1_24.pdf) (notamment toute question tournant autour du 2nd degré) 
 

## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 2 et 3 ou s'y apparentant.


