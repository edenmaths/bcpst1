---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 8

**Semaine du 18 au 22 novembre 2024**

## Cours










### [Dénombrements](../LATEX/Poly_denomb_24.pdf)

Calculs de sommes et produits - Notation Σ et Π - Sommes avec Python - Sommes des $k$,
$k^2$, $k^3$ - Somme des $x^k$ - Linéarité de Σ - Sommes doubles -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 

### [Généralités sur les suites réelles](../LATEX/Poly_suites1_24.pdf)

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).  **Remarque** :  les notions  de  sens de  variation et  de
convergence seront abordées dans un prochain chapitre.


### Python

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
- FONCTIONS - **Calcul de sommes et de produits, compteurs** - MAXIMUM -
Histogramme d'une liste et autres exercices sur les
parcours de listes et de chaînes de caractères.




### [Python](../../INFORMATIQUE/3_Recursion/)

Calculs de sommes, de produits, compteurs, puissance entière.





## Questions à préparer

On commencera la colle par un exercice librement choisi et préparé par le(la) candidat(e).

## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 7 et 8 ou s'y apparentant.


