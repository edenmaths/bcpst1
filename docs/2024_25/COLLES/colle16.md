---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 16

**Semaine du 3 au 7 février 2024**

## Cours




### [Polynômes](../../COURS/16_Polynomes/)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.



### [Espaces vectoriels (début)](../../LATEX/Poly_EV_24_court.pdf)

Sous-espaces vectoriels - Caractérisations - 
SEV  engendré par  une  famille -  Famille génératrice  -  Famille libre  -
bases - Étude à l'aide du rang de la matrice de la famille.




### Python


[Calcul matriciel](../../INFORMATIQUE/4_Calcul_matriciel/)

Fonctions `zeros(n: int, p: int) ->  list`, `eye(n: int) -> list`, `prod(A:
list, B:  list) -> list`,  transposée, produit  scalaire de deux  listes et
application au calcul du produit de deux matrices.


## Exercices

- Vous pouvez  poser en colle tout  exercice de la liste du  chapitre 16 et
  ceux du chapitre 17 au programme.

