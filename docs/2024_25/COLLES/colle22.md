---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 22

**Semaine du 2 au 5 avril 2024**

## Cours







### [Développements limités](../../LATEX/Poly_DL_23.pdf)


Fonction négligeable devant une autre au voisinage  d'un point - DL - Formule de
Taylor-Young  -  DL de  référence  -  Opérations (somme,  produit,  intégration,
substitution) - Applications à l'étude locale
d'une fonction (asymptotes, tangentes, positions relatives,...).


### [Intégration 2](../../LATEX/Poly_Int_2_23.pdf)

Rappels sur  le chapitre 10 (définition  en termes d'aire, relation  de Chasles,
linéarité,  IPP, changement  de variable)  -  Intégrales et  relation d'ordre  -
Valeur moyenne  - Théorème fondamental de  l'analyse - Fonction définie  par une
intégrale -  Somme de Riemann :  théorème admis pour les  fonctions continues et
démonstration  pour des  fonctions  de classe  $\mathscr  C^1$ -  Interprétation
géométrique.


### Python



#### [Approximation du calcul d'intégrales](../../INFORMATIQUE/11_approxint/)

Méthodes  des  rectangles   -  des  trapèzes  -  de  Simpson   -  Un  atome
d'arithmétique des nombres flottants.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

1. $f(x)=\dfrac{1}{\sin x}- \dfrac{1}{x}$. Démontrer  que $f$ peut être prolongée
  par continuité en 0, que ce prolongement  par continuité est dérivable en 0,
  former  l'équation de  la tangente  $(T)$ à  $\mathscr{C}_f$ et  déterminer la
  position relative de $(T)$ et de $\mathscr{C}_f$ au voisinage de 0. 

1. Règles de calcul sur les $\mathscr o$. Preuve d'au moins 3 d'entre elles.


1.  $DL_3(0)$  de $\tan(x)$  :  par  Taylor-Young  direct et  en  utilisant
   $\dfrac{\sin(x)}{\cos(x)}$. 
   
1. Fonctions Python calculant les approximations des sommes de Riemann d'une
  fonction donnée en  paramètre ainsi que les bornes de  l'intervalle et le
  nombre $n$ d'intervalles de la subdivision.

1. Convergence des sommes de Riemann : démonstration de la convergence de la
  suite $(S_n)$ dans le cas d'une fonction $\mathscr C^1$ sur un intervalle $[a,b]$.

1.     Sens     de    variation    de    la     fonction    $G:    x\mapsto
    \displaystyle\int_x^{2x}\dfrac{dt}{t^2+t+1}$.       (Attention      aux
    justifications diverses).

## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres
  21 et 22 du Poly.  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


