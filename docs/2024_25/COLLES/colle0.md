# Conseils pour aborder les colles de mathématiques 


## 1. Déroulement d'une colle de mathématiques

Trois élèves (un «  trinôme de colle ») sont évalués  en même temps pendant
une heure  (sur des  sujets différents) au  tableau , chaque élève dispose  alors d'un
tiers du tableau pour réfléchir.  Si  par  malchance vous  vous
retrouvez    seul,   essayez    d'intégrer    un    autre   groupe    cette
semaine-là. Prévenez vos colleurs en cas d'absence par email:

- [g.connan@externat-chavagnes.com](mailto:g.connan@externat-chavagnes.com)
- [j.laurentin@externat-chavagnes.com](mailto:j.laurentin@externat-chavagnes.com)


En  classe préparatoire  BCPST, le  rythme est  d'un oral  de mathématiques
toutes les deux semaines. Cette fréquence vous oblige à travailler le cours
régulièrement et vous  aide à ne pas prendre de  retard. Chaque colle porte
alors  sur deux  ou  trois chapitres  du  cours, le  détail  des notions  à
connaître étant indiqué sur le [programme en ligne](index.md). 

Le déroulement d'une colle suit les étapes suivantes :

-   Une  question   de   cours,  avec   éventuellement  restitution   d'une
  démonstration à connaître ou un exercice de cours.
- La résolution d'un ou plusieurs exercices.
- Un bilan avec le colleur.

Une  part importante  de la  note  finale évalue  la maîtrise  du cours  (à
travers  la question  de  cours  et la  résolution  des  exercices). Si  la
question de cours n'est pas maîtrisée, la note sera inférieure à 10.


Le but des colles est multiple:

- vous permettre de vérifier que vous êtes au point sur un chapitre;
- vous entraîner pour les épreuves d'admission;
- vous  permettre d'être interrogé  par des personnes différentes  qui vous
  apporteront des éclairages différents;
- préparer à fond certains exercices  et rédiger très sérieusement ceux qui
  vous seront proposés.
  

**Remarque** :  vous serez également  interrogé sur  les TP `Python`  ou les
notions `Python` vues en cours.


## 2. Aborder les exercices

Les  exercices de  colle  sont à  traiter  dans l'ordre  où  ils vous  sont
proposés. Leur difficulté  est souvent supérieure à ceux  traités en cours,
c'est normal :  les colles sont là  pour vous apprendre à réagir  face à de
nouveaux exercices et le colleur vous guidera dans vos raisonnements. 

Il peut arriver que le colleur vous propose un exercice déjà connu. Dans ce
cas, signalez-le et demandez-en un  nouveau (sauf si l'exercice en question
n'a pas  été bien  compris) :  recracher un corrigé  connu peut  vous faire
gagner un  ou deux  points sur  la note finale,  mais ça  ne vous  fera pas
progresser. 

Une  colle  est un  oral,  on  attend donc  de  vous  une interaction  avec
l'examinateur : vous  devez lui exposer vos pistes et  raisonnements au fur
et  à  mesure de  votre  réflexion,  même si  vous  n'êtes  pas sûr  qu'ils
aboutiront. Ne  tentez pas de cacher  vos pistes et calculs  qui n'auraient
pas abouti  : ils sont  intéressants et font  partie de votre  réflexion au
même titre que  le reste. Si cela vous rassure,  vous pouvez vous délimiter
un coin  « brouillon »sur votre  tableau et préciser à  l'examinateur qu'il
s'agit de pistes et de calculs non vérifiés. 

L'examinateur est là  pour réagir à vos idées, vous  donner des indications
et  vous guider.  Il ne  faut cependant  pas vous  montrer trop  passifs et
attendre qu'il vous dicte quoi faire : vous seriez sanctionnés au niveau de
la note. 

Soyez honnête : si vous vous rendez compte qu'on attend de vous un résultat
que vous  ne connaissez  pas, dites-le  tout de suite,  le colleur  vous le
réexpliquera et ça vous évitera de perdre  du temps dessus (même si ce sera
bien évidemment pris  en compte au moment de l'attribution  de la note). De
même, n'hésitez  pas à demander des  précisions ou explications si  vous ne
comprenez pas  certaines des indications :  il ne faut pas  sortir de colle
sans avoir tout compris! 

N'hésitez pas à  demander au colleur un  rapide bilan de la  colle avant de
partir  (s'il ne  vous  le  propose pas  spontanément)  :  il est  toujours
intéressant de  comparer ce que vous  pensez être vos forces  et faiblesses
avec  la vision  de quelqu'un  d'extérieur. S'il  le désire,  il peut  vous
donner votre note à ce moment-là (mais ce n'est pas une obligation). 

## 3. Reprise de colle

Il est important  de reprendre votre colle  au calme, le soir  même ou plus
tard dans la semaine (le plus tôt possible sera souvent le mieux). 

Vous devrez **obligatoirement** rédiger le  compte-rendu de votre colle qui
contiendra :

- les énoncés;
- l'indication de ce que vous avez traité;
-  la rédaction  complète des  exercices donnés,  même si  vous n'avez  pas
  terminé pendant  l'oral. Il  s'agit donc de  devoirs-maison personnalisés
  dont il faudra particulièrement soigner la rédaction.



Cette reprise de colle poursuit plusieurs objectifs :

- Vous  assurer que vous  avez bien  compris l'exercice et  les indications
  éventuelles du  colleur (si  vous savez  le refaire,  c'est a  priori que
  c'est le cas). 
- Vous entraîner à bien rédiger.  Pendant la colle, les grandes lignes d'un
  raisonnement peuvent  suffire, mais  il est  aussi important  de vérifier
  qu'on sait écrire les détails. 
- Comprendre les points qui ont posé  problème et les mettre en évidence de
  manière à faciliter les révisions ultérieures. 

Un [exemple de reprise de colle](./retour_colle.jpg) rédigé par mes soins est disponible.

## 4. Quelques conseils d'anciens

- « Ne partez jamais de colle sans avoir tout compris à la colle. »

- « Profitez des colles pour approfondir des exercices. »

- « Il ne faut pas hésiter à  refaire les exercices de TD et de cours, mais
  il est  aussi important  de ne  pas négliger ceux  de colle,  qui donnent
  souvent  des méthodes  moins  directes  et permettent  de  faire de  gros
  progrès. Il ne faut donc pas bâcler les reprises de colles. » 

