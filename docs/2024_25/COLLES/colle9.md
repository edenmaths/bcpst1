---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 9

**Semaine du 25 au 29 novembre 2024**

## Cours










### [Généralités sur les suites réelles](../LATEX/Poly_suites1_24.pdf)

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).  **Remarque** :  les notions  de  sens de  variation et  de
convergence seront abordées dans un prochain chapitre.



### Généralités sur les fonctions


#### [Prérequis (cf chapitre 1)](../LATEX/PolyGenFonc24.pdf)


- Cours de 2nde : définition d'une fonction, lecture graphique, sens de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
  
#### [Dérivées - Primitives](../LATEX/Poly_der_prim_24.pdf)


- Commandes Python permettant de tracer des représentations graphiques.
- Approche empiriquo-historique - Nombre dérivé - Dérivabilité en un réel -
  Application au calcul de certaines limites - Calcul des dérivées usuelles
  à partir  de la définition -  Approximation affine - Dérivabilité  sur un
  intervalle - Lien avec les variations - Fonctions strictement monotones -
  Extremum local - Dérivée d'une composée - Primitives.
- **Remarque**  : les  calculs de  limites *ne  sont pas*  un objectif  de ce
  chapitre. On insistera  sur le calcul de  dérivées/primitives, l'étude de
  sens de variation, le calcul  d'équations de tangentes, la représentation
  graphique  via une  allure rapide  « à  la main  » PUIS  avec Python,  la
  recherche d'extrema.



### Python

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
- FONCTIONS - **Calcul de sommes et de produits, compteurs** - MAXIMUM -
Histogramme d'une liste et autres exercices sur les
parcours de listes et de chaînes de caractères.




### [Python](../../INFORMATIQUE/3_Recursion/)

Calculs de sommes, de produits, compteurs, puissance entière.






## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 8 et 9 ou s'y apparentant.


