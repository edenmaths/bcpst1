


# Colle 3

**Semaine du 2 au 6 octobre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)




### Ensembles - Applications

Appartenance - Inclusion - Égalité - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition  - Produit  cartésien -  Fonction -  Fonction totale,  injective,
surjective,  bijective  (théorème  de   la  bijection  non  vu),  bijection
réciproque.



### Réels

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue.


### [Premiers pas en Python](../INFORMATIQUE/1_Premiers_Pas/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `List` - Chaînes  de caractères - Boucles - Parcours
de listes - Fonctions.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :


1.   Caractériser  à   l'aide  de   quantificateurs  l'injectivité   et  la
   surjectivité.  Comment démontrer  qu'un fonction  n'est pas  injective ?
   surjective ?
2.  Déterminer une  fonction  en *Python*  de paramètre  un  entier `n`  et
  renvoyant  les $n$ premiers
  termes de la suite de Fibonacci. 
  On pourra créer dans cette fonction  une variable locale liée à une liste
  `F` contenant deux fois l'entier `1` suivis de `n - 2` fois l'entier `0`. 
3.  Écrire une  fonction `inter(ens1: list, ens2: list) ->  list` qui renvoie
  l'intersection   de   deux  listes   au   sens   de  l'intersection   des
  ensembles. Faites de même pour la réunion.
4. Résoudre dans $ℝ$ $\sqrt{x-1} < \sqrt{3-2x}$.
5. Énoncer et démontrer l'inégalité  triangulaire pour les valeurs absolues
   et étudier le cas d'égalité.
