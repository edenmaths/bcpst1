


# Colle 2

**Semaine du 25 au 30 septembre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)

### Rappels de lycée : fonctions usuelles

Racine carrée,  logarithme, exponentielles  de bases  quelconques, fonction
puissance α.  On insistera sur la partie
algébrique, notamment la résolution d'équations avec le formalisme du cours
de  logique pour  en  assurer  la rigueur,  mais  on  pourra s'appuyer  sur
quelques résultats  analytiques (sens  de variation,  allure de  la courbe,
limites) pour guider l'intuition.


### Bases pour raisonner

Logique - Implication - Contraposée -  Négation - Quantificateurs - Réciproque -
Équivalence - Récurrence simple, double, forte - Analyse Synthèse.



### Ensembles

Appartenance - Inclusion - Égalité - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition  -  Produit  cartésien



### [Premiers pas en Python](../INFORMATIQUE/1_Premiers_Pas/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `List` - Chaînes  de caractères - Boucles - Parcours
de listes - Fonctions.

## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :

1. Écrire une  fonction `inter(ens1: list, ens2: list) ->  list` qui renvoie
  l'intersection   de   deux  listes   au   sens   de  l'intersection   des
  ensembles. Faites de même pour la réunion.
1.  Soit  $A=\bigl\{0,1\bigr\}$.  Déterminer  $\left(A^2\setminus
        \bigl\{(0,0)\bigr\}\right)\otimes A$. Est-ce que $A⊗B⊗C=(A⊗B)⊗C=A⊗(B⊗C)$?
1. Tout exercice de la 2e [interro de cours](../DS/Test_2_23.pdf)
1. Sachant que $\ln(a\cdots b)= \ln(a)\cdots\ln(b)$ (à compléter) et que $\ln(1)=0$, démontrer
  les  autres propriétés  algébriques  du  logarithme népérien  (puissance,
  inverse, quotient, racine carrée...).
1. Résolution dans le plus grand ensemble de $x^{\left(x^x\right)}=\left(x^x\right)^x$
1. Écrire  à l'aide des  quantificateurs $\exists$ et $\forall$  sachant que
  $f$ est une fonction définie sur $\mathbb{R}$:
     
	 - $f$ n'est pas la fonction nulle
	 - L'équation $f(x)=0$ admet une unique solution réelle
	 - $f$ ne s'annule pas 
	 - $f$ prend toute valeur réelle comprise entre $-1$ et $1$
	 - $f$ prend toutes ses valeurs entre $-1$ et $1$



