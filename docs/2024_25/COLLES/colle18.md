---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 18

**Semaine du 3 au 7 mars 2025**

## Cours




### [Suites (suite)](../../LATEX/Poly_Suites_Conv_24_court.pdf)
t
Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des suites monotones - Croissances comparées -  Suites  équivalentes -
Définition - Caractérisation par la limite  du rapport - Rapports avec le signe,
les limites  - Opérations sur les  équivalents - Équivalents usuels  à partir de
suites convergeant  vers 0  (limites de taux  de variations) -  Limite de  $(1 +
1/n)^n$ - Suites adjacentes.


### [Limites et continuité](../../LATEX/Poly_Lim_Cont_24_court.pdf)

Notion de  voisinage - Définitions  des différentes  limites - Continuité  en un
réel - Opérations sur les limites -
Composition -  Relation d'ordre  - Encadrement -  Prolongement par  continuité -
Limites et  suites - Théorème de  la limite monotone -  Fonctions équivalentes -
Opérations sur les  équivalents - Équivalents usuels - Fonction  continue sur un
intervalle - Fonctions continues par morceaux  - TVI - Image d'un intervalle par
une  fonction continue  -  Théoreme des  bornes  - Théorème  de  la bijection  -
Arctan - Résolution d'équations numériques par dichotomie. 

### Python


#### Dichotomie

   Résolution d'équation - Exemple de recherche d'une valeur approchée de $\sqrt{2}$.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


14.  $\displaystyle\lim _{n\to+\infty} \left( \sqrt{n+\sqrt{n}}-\sqrt{n} \right)$ et
     $ \displaystyle\lim _{n\to+\infty} \left( 1 + \frac{1}{n} \right)^n $ 

15. Soit $(u_n)$ une suite convergeant vers $0$.  Donner des équivalents
    simples de $\sin(u_n)$, $\cos (u_n)-1$  et $\tan(u_n)$. En admettant le
    premier démontrer les deux suivants.
	

	
1.  Fonction Python `dicho(f, a, b,  eps)` calculant une approximation de $f(x)=0$
  sur un intervalle 
  $[a,b]$ où $f$ est continue et strictement croissante avec une précision
  arbitraire   `eps`   (supérieure   à   $2^{52}$)  à   l'aide   d'une   méthode
  dichotomique.   Appliquer  à   la   recherche   d'une  approximation   de
  $\sqrt{37}$.

1. Énoncer les deux formes du TVI et le théorème de la bijection. 

1. Étude  de la fonction  Arctan.

## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres
  18 et 19 du Poly.  On évitera les exercices exigeant
  des démonstrations « avec des $ε$ ».



