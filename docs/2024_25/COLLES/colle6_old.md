


# Colle 6

**Semaine du 6 au 10 novembre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)



### Complexes

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé - Formes  trigonométriques  et exponentielles  d'un
nombre  complexe  - Formules  d'Euler  -  Factorisation de  $1-e^{iα}$  par
l'exponentielle de l'angle moitié.




### Dénombrements

Calculs de  sommes -  Notation Σ  - Sommes  avec Python  - Sommes  des $k$,
$k^2$, $k^3$ - Somme des $x^k$ - Linéarité de Σ.




### [Algorithmes basiques](../INFORMATIQUE/2_Premiers_exos/)

sommes -  compteurs -  Maximum -  Histogramme et  autres exercices  sur les
parcours de listes et de chaînes de caractères.




## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :

1. Toute question tiré du [DS3](../../DS/DS3_23.pdf)

1. Fonction Python qui renvoie le maximum/minimum d'une liste de nombres.

1.  Déterminer les nombres complexes dont le carré vaut $3+4{\rm i}$
   (veillez particulièrement à la rédaction).

5. Déterminer  l'argument de $z×z'$ en  fonction des arguments de  $z$ et de
  $z'$.  On  donnera une  démonstration  sans  utiliser les  exponentielles
  complexes. Déterminer alors  l'argument de $1/z$ en fonction  de celui de
  $z$.
   
1. Démontrer de deux manières différentes la formule donnant $\displaystyle
   \sum_{k=0}^{k=n}x^k$
   
1. On représente un nombre complexe par la donnée d'un couple de deux réels
   qui seront ses parties réelles et imaginaires.
   Par exemple, `(3, 4)` représentera le nombre $3+4i$.  Déterminer une
   fonction `mulc(c1: tuple, c2: tuple) -> tuple` qui renvoie le produit de
   deux complexes donnés en arguments. 


## Exercices

**ON INSISTERA ENCORΕ SUR LES CALCULS ET LA PRÉSENTATION RIGOUREUSE DES RAISONNEMENTS.**

**REMARQUE**: **Toute question de cours non
résolue  sans  aide  et  rapidement  (5 minutes  maxi)  entraîne  une  note
inférieure à la moyenne, voire largement  sous la moyenne en fonction de la
performance.**
**Tout programme Python doit fonctionner et ne pas
laisser penser qu'il n'a pas été testé à la maison.**
**Une question de cours ne doit pas  seulement être plus ou moins apprise par
cœur : elle doit être comprise et parfaitement maîtrisée.**
