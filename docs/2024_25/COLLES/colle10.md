---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 10

**Semaine du 2 au 6 décembre 2024**

## Cours



### Généralités sur les fonctions


#### [Prérequis (cf chapitre 1)](../LATEX/PolyGenFonc24.pdf)


- Cours de 2nde : définition d'une fonction, lecture graphique, sens de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
  
#### [Dérivées - Primitives](../LATEX/Poly_der_prim_24.pdf)


- Commandes Python permettant de tracer des représentations graphiques.
- Approche empiriquo-historique - Nombre dérivé - Dérivabilité en un réel -
  Application au calcul de certaines limites - Calcul des dérivées usuelles
  à partir  de la définition -  Approximation affine - Dérivabilité  sur un
  intervalle - Lien avec les variations - Fonctions strictement monotones -
  Extremum local - Dérivée d'une composée - Primitives.
- **Remarque**  : les  calculs de  limites *ne  sont pas*  un objectif  de ce
  chapitre. On insistera  sur le calcul de  dérivées/primitives, l'étude de
  sens de variation, le calcul  d'équations de tangentes, la représentation
  graphique  via une  allure rapide  « à  la main  » PUIS  avec Python,  la
  recherche d'extrema.



#### Initiation au calcul intégral

Définition en  tant qu'aire -  Relation de Chasles  - Intégration par  parties -
Changement de variable (il sera donné s'il n'est pas affine).





### Python




### [Récursion](../../INFORMATIQUE/3_Recursion/)

Calculs de sommes, de produits, compteurs, puissance entière.

### [Oral concours](../../INFORMATIQUE/31_TB_algo/)

Épreuves de TB et préparation aux oraux G2E (questions rapides 1 à 5, exercice 2)





## Exercices

- Vous  pouvez poser  en colle tout  exercice des  chapitres 9 et  10 et  du TP5
  d'informatique.

