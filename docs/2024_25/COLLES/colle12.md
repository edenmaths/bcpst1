---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 12

**Semaine du 8 au 13 janvier 2024 : joyeuse année $10111×100×1011×10$**

## [Cours](../../LATEX/PolyTotalB1_23.pdf)


### [Équations différentielles](../../LATEX/Poly_ED_23.pdf)

Équations différentielles linéaires  du 1er ordre - Équations  homogènes et avec
second  membre  - Principe  de  superposition  des  solutions  - Méthode  de  la
variation de  la constante. Équations  linéaires du second ordre  à coefficients
constants - Équation  caractéristique associée - Pour les  équations avec second
membre,  on  donnera  la  forme d'une  solution  particulière.  Résolution  avec
SymPy.  Tracé d'une famille de solutions avec NumPy après la résolution (`odeplot` n'a pas
encore été vu). La méthode d'Euler sera étudiée plus tard. 

### [Calcul matriciel](../../LATEX/Poly_Mat_23.pdf)

Approche  historique -  Définition -  Liens avec  Python :  `list` et  `array` -
Matrices  particulières -  Opérations sur  les matrices  - Formule  du binôme  -
Matrices inversibles.



### [Systèmes linéaires](../../LATEX/Poly_Systemes_23.pdf)

Exemple historique - Écriture matricielle  - Systèmes équivalents - Recherche de
l'inverse  avec le  tableau  complété -  Rang d'une  matrice  - Méthode  de
Gauss-Jordan -  Application à la  résolution de systèmes -  Rang, inconnues
principales et non principales - Utilisation de `NumPy` et `SymPy`. 


### Python




#### [Algorithmique](../../INFORMATIQUE/31_TB_algo/)

- Exemples de boucles : calculs de sommes et de produits - compteurs - test
  de présence de caractères dans une chaîne - Recherche d'extremum. 
- Fonctions sur les chaines et les listes (cf épreuves du concours TB).

#### [Calcul matriciel](../../INFORMATIQUE/4_Calcul_matriciel/)
-  Fonctions Python  pour effectuer  les opérations  de base  : somme,  produit,
  puissance
- Fonctions renvoyant l'identité, la matrice nulle
- tests pour savoir si une matrice est symétrique, diagonale, triangulaire supérieure,...





## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Principe de superposition des solutions des équations différentielles linéaires du premier
  ordre : énoncé et démonstration.
- Méthode de la variation de  la constante : présentation et explication de
  son mode de fonctionnement dans le **cas général**.
- Écrire une fonction qui prend une liste d'entiers entre 0 et 9 et renvoie
  la liste des distributions de ces entiers. Par exemple `distrib([4, 7, 7,
  1]` renvoie `[0, 1, 0, 0, 1, 0, 0, 2, 0, 0]`.
- Fonction renvoyant le produit de deux matrices : proposer au moins deux solutions.
- Fonction Python testant si une matrice est symétrique/antisymétrique/diagonale.
- Résoudre dans $ℝ^3$ le système $\left\{\begin{array}{c}mx+y+z=m\\
x+my+z=m\\
x+y+mz=m
\end{array}\right.$
- La matrice $\mathbb I_n$ est neutre pour le produit matriciel.


## Exercices

- Vous pouvez poser  en colle tout exercice de la liste des  chapitres 11, 12 et
  13 du
  Poly.
  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


