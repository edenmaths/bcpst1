---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 27 

**Semaine du 28 au 31 mai 2024**

## Cours





### [Variables aléatoires réelles finies](../../LATEX/Poly_VAR_23.pdf)

Loi de probabilité -  SCE lié à une VAR - Existence d'une  loi de valeurs prises
connues - Fonction de répartition - Fonction d'une VAR - Espérance - Théorème de
transfert  - Linéarité  de  l'espérance  - Moments  d'ordre  $r$ -  Moments
centrés - Variance -  Théorème de K-H -  $\mathbb V(aX=b)$ -
VAR centrée réduite - VA certaine - Loi uniforme sur $[\![ 1, n]\!]$,
sur $[\![ a, b ]\!]$ - Loi binomiale.



### Python



#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulles, tri par sélection, tri par insertion.

#### Probas

Modélisations d'expériences aléatoires.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

	

1. Tri à bulles : code et commentaires.

1. Tri par sélection : code et commentaires.

1.  Calcul de  l'espérance  d'une loi  binomiale :  calcul  avec sommes  de
   coefficients binomiaux et  calcul à partir de sommes de  VAR suivant une
   loi de Bernoulli.
   
1.  Formule de KH : énoncé et démonstration

1. $\mathbb E(aX+b)$ et $\mathbb V(aX+b)$ : énoncé et démonstration

1. Loi uniforme sur $[\![ 1, n]\!]$,  espérance et variance. Application à la loi
  uniforme sur $[\![ a, b]\!]$. 


## Exercices

- Vous pouvez poser en colle tout  exercice de la liste du chapitre
    25
   **
   et proposer des simulations de VAR avec Python**.
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


