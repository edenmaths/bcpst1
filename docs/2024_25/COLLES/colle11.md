---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 11

**Semaine du 11 au 15 décembre 2023**

## [Cours](../../LATEX/PolyTotalB1_23.pdf)


 

### Initiation au calcul intégral

Définition en  tant qu'aire -  Relation de Chasles  - Intégration par  parties -
Changement de variable (il sera donné s'il n'est pas affine).





### Équations différentielles

Équations différentielles linéaires  du 1er ordre - Équations  homogènes et avec
second  membre  - Principe  de  superposition  des  solutions  - Méthode  de  la
variation de  la constante. Équations  linéaires du second ordre  à coefficients
constants - Équation  caractéristique associée - Pour les  équations avec second
membre,  on  donnera  la  forme d'une  solution  particulière.  Résolution  avec
SymPy.  Tracé d'une famille de solutions avec NumPy après la résolution (`odeplot` n'a pas
encore été vu). La méthode d'Euler sera étudiée plus tard. 




### [Python](../../INFORMATIQUE/31_TB_algo/)

- Exemples de boucles : calculs de sommes et de produits - compteurs - test
  de présence de caractères dans une chaîne - Recherche d'extremum. 
- Fonctions sur les chaines et les listes (cf épreuves du concours TB).


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- $ I=\displaystyle\int_0^{\pi}\sin(x){\rm e}^{-x}\,{\rm d} x $
- $J= \displaystyle\int_{-1}^{1}\dfrac{e^x}{e^{2x}+1}{\rm d} x\ (x=\ln t)$
- $K= \displaystyle\int_{\pi/6}^{\pi/4}\cos^5t\,{\rm d}t$
- $L=\displaystyle\int_{a}^{b} \frac{1}{t^2+6t+10} {\rm d}t$
- Principe de superposition des solutions des équations différentielles linéaires du premier
  ordre : énoncé et démonstration.
- Méthode de la variation de  la constante : présentation et explication de
  son mode de fonctionnement dans le **cas général**.
- Écrire une fonction qui prend une liste d'entiers entre 0 et 9 et renvoie
  la liste des distributions de ces entiers. Par exemple `distrib([4, 7, 7,
  1]` renvoie `[0, 1, 0, 0, 1, 0, 0, 2, 0, 0]`.
  

## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 10 et 11 du
  Poly     et      utiliser     des     exemples     du      [cahier     de
  calcul](https://edenmaths.gitlab.io/bcpst1/2022_23/COURS/Revisions_BCPST.pdf). 

  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


