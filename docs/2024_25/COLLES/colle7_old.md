


# Colle 7

**Semaine du 13 au 17 novembre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)



### Complexes

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé - Formes  trigonométriques  et exponentielles  d'un
nombre  complexe  - Formules  d'Euler  -  Factorisation de  $1-e^{iα}$  par
l'exponentielle de l'angle moitié.




### Dénombrements

Calculs de sommes et produits - Notation Σ et Π - Sommes avec Python - Sommes des $k$,
$k^2$, $k^3$ - Somme des $x^k$ - Linéarité de Σ - Sommes doubles -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 


## Python

### [Algorithmes basiques](../INFORMATIQUE/2_Premiers_exos/)

sommes -  compteurs -  Maximum -  Histogramme et  autres exercices  sur les
parcours de listes et de chaînes de caractères 


### [Python](../../INFORMATIQUE/3_Recursion/)

Calculs de sommes, de produits, compteurs, puissance entière.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :

1. Toute question tiré du [DS3](../../DS/DS3_23.pdf)


5. Déterminer  l'argument de $z×z'$ en  fonction des arguments de  $z$ et de
  $z'$.  On  donnera une  démonstration  sans  utiliser les  exponentielles
  complexes. Déterminer alors  l'argument de $1/z$ en fonction  de celui de
  $z$.
   
1. Démontrer de deux manières différentes la formule donnant $\displaystyle
   \sum_{k=0}^{k=n}x^k$
   
1.  Calcul de $\displaystyle \sum_{i=1}^p\left(\sum_{k=1}^ik×i\right)$.

1. Calcul de la somme des éléments d'une liste d'entiers avec Python : en donner une
  version récursive et une version avec boucle.
  
1. Relation de Pascal: démonstration ensembliste et par le calcul.

## Exercices

**CETTΕ SEMAINE**

- **On commencera par la résolution d'une équation du second degré qui doit
  être résolue  sans faute  et EN  MOINS DE  3 MINUTES  sinon la  note sera
  inférieure à 5.**
  
- On posera au moins un exercice impliquant l'écriture d'une fonction en Python.

**REMARQUE**: **Toute question de cours non
résolue  sans  aide  et  rapidement  (5 minutes  maxi)  entraîne  une  note
inférieure à la moyenne, voire largement  sous la moyenne en fonction de la
performance.**
**Tout programme Python doit fonctionner et ne pas
laisser penser qu'il n'a pas été testé à la maison.**
**Une question de cours ne doit pas  seulement être plus ou moins apprise par
cœur : elle doit être comprise et parfaitement maîtrisée.**
