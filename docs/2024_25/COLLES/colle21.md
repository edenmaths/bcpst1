---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 21

**Semaine du 25 au 29 mars 2024**

## Cours








### [Dérivation : approfondissement](../../LATEX/Poly_Der_App_23.pdf)

Révisions  sur  les  chapitres  8  et 9  (nombre  dérivé,  calcul  de  dérivées,
dérivation et  variations) - Dérivées  successives :  la formule de  Leibniz est
hors  programme  mais a  été  vue  (on peut  la  rappeler  pour un  exercice)  -
Dérivation et extrema  - Théorème de Rolle - Égalité  des accroissements finis -
L'inégalité ne doit pas être sue mais  doit pouvoir être redémontrée à partir de
l'EAF - Application aux suites définies par une relation de récurrence.




### [Développements limités](../../LATEX/Poly_DL_23.pdf)


Fonction négligeable devant une autre au voisinage  d'un point - DL - Formule de
Taylor-Young  -  DL de  référence  -  Opérations (somme,  produit,  intégration,
substitution) - Applications à l'étude locale
d'une fonction (asymptotes, tangentes, positions relatives,...).




### Python



#### [Suites](../../INFORMATIQUE/a_suites/)

Modélisations de diverses suites.

#### Dichotomie

   Résolution d'équation -  Exemple de recherche d'une  valeur approchée de
   $\sqrt{2}$.
   
#### Suites logistiques

Tracer de l'escargot 

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

1. Tout exrait du [DS 7](../../DS/DS7_23.pdf)
	
1.  Égalité des accroissements finis : énoncé et démonstration.  Application à
  l'inégalité des accroissements finis.

1. Théorème de Rolle : énoncé et démonstration.

1. Dérivée de $\ln\circ\ln\circ\cdots\circ\ln$ ($n$ compositions avec $n∈ℕ$).

1. $f(x)=\dfrac{1}{\sin x}- \dfrac{1}{x}$. Démontrer  que $f$ peut être prolongée
  par continuité en 0, que ce prolongement  par continuité est dérivable en 0,
  former  l'équation de  la tangente  $(T)$ à  $\mathscr{C}_f$ et  déterminer la
  position relative de $(T)$ et de $\mathscr{C}_f$ au voisinage de 0. 

1. Règles de calcul sur les $\mathscr o$. Preuve d'au moins 3 d'entre elles.



## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres
  20 et 21 du Poly.  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


