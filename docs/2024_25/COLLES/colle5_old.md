


# Colle 5

**Semaine du 16 au 20 octobre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)




### Trigonométrie

Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais on n'a pas étudié les fonctions).




### Complexes - Début

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé.




### [Algorithmes basiques](../INFORMATIQUE/2_Premiers_exos/)

sommes - compteurs




## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :

1. Toute question tiré du [DS2](../../DS/DS2_23.pdf)

1.  Déterminer les nombres complexes dont le carré vaut $3+4{\rm i}$
   (veillez particulièrement à la rédaction).

1.  Connaissant la formule donnant $\cos(a -b)$, démontrer les formules donnant
  $\sin(a-b)$, $\tan(a-b)$ et $cos(2x)$.
  
1.  Démontrer la formule donnant $\cos(a -b)$ en utilisant le produit scalaire.



5.  Énoncer  et démontrer  l'inégalité  triangulaire  pour les  modules  de
   nombres cpmplexes
   et étudier le cas d'égalité.
   
6. Fonction Python renvoyant le nombre de 'a' contenus dans une chaîne de
  caractères donnée en argument.
  
  

## Exercices

**ON  INSISTERA SUR  LES CALCULS  ET  LA PRÉSENTATION  RIGOUREUSE DES  RÉSOLUTIONS
D'(IN)ÉQUATIONS.**

**REMARQUE**: **à partir de cette semaine, toute question de cours non
résolue  sans  aide  et  rapidement  (5 minutes  maxi)  entraîne  une  note
inférieure à la moyenne, voire largement  sous la moyenne en fonction de la
performance.**
**Tout programme Python doit fonctionner et ne pas
laisser penser qu'il n'a pas été testé à la maison.**
**Une question de cours ne doit pas  seulement être plus ou moins apprise par
cœur : elle doit être comprise et parfaitement maîtrisée.**
