---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 16

**Semaine du 5 au 9 février 2024**

## Cours




### [Polynômes](../../COURS/16_Polynomes/)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.



### [Espaces vectoriels (début)](../../LATEX/Poly_EV_23.pdf)

Définition d'un $\mathbb K$-espace vectoriel (pour information - pas d'exercices
dessus). Cinq exemples de référence. Sous-espaces vectoriels - Caractérisations. 
SEV  engendré par  une famille  - Famille  génératrice -  Sur-famille d'une
famille génératrice - Famille libre (Juste
la définition - pas
encore les bases).




### Python



#### [Images et matrices](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que matrice :  fonction agissant sur
les pixels, retournements.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

5.  Fonction  Python   déterminant  le  degré  d'un   polynôme  (liste  des
   coefficients dans  l'ordre croissant des  degrés) en créant  d'abord une
   fonction   qui  normalise   la  liste   (en  enlevant   les  zéros   non
   significatifs).
   
6.  Fonction  Python  qui  additionne deux  polynômes  en  disposant  d'une
   fonction `degré` et `normalise`.
   
7. Fonction Python qui multiplie deux polynômes en disposant d'une fonction
   `degré` et `normalise`.
   
8.   Soit  $\mathscr   F$   une   famille  de   vecteurs   d'un  $\mathbb   K$-EV
  E. Vect($\mathscr F$) est un SEV de E.

9.  On note $H=\left\{ ( x,y,z) \in \mathbb{R}^{3}\mid
x+2y+3z=0\right\}$, démontrer que $H$ est un sev de $\mathbb{R}^{3}$
en déterminant $\mathbf{u}$ et $\mathbf{v}$, deux vecteurs de $\mathbb{R}^{3}$, de
sorte que $H={\rm Vect}{( \mathbf{u},\mathbf{v})}$.
On note $\mathbf{w}=(1,2,3)$. Est-ce que $\mathbf{w}\in H$?

10. L'intersection de 2 SEV est encore un SEV. Quid de la réunion?

11.  L'ensemble des suites arithmétiques est un sous-espace vectoriel de
  l'espace vectoriel des suites réelles.


## Exercices

- Vous pouvez  poser en colle tout  exercice de la liste du  chapitre 16 et
  ceux du chapitre 17 au programme. 
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


