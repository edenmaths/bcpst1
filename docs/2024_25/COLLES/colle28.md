---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 1

**Semaine du 16 au 20 septembre 2024**

## Cours


### [Généralités sur les fonctions ](../../LATEX/Poly_f2var_23.pdf)

Définition d'une  fonction -  En Python  `def` et  `lambda` -  Notations et
vocabulaire  -   Représentation  graphique   -  Lien  avec   la  résolution
d'équations et d'inéquations - Parité -  Périodicité - Bornes - Monotonie -
Opération sur les fonctions -  Racine carrée, logarithme, exponentielles de
bases quelconques, fonction puissance α.


### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/1_Premiers_Pas/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `List` - Chaînes de caractères.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous:

0.  Tout exercice du [test de rentrée](../DS/INT_1_24.pdf)

1. Théorème de composition de fonctions monotones : énoncé et
   démonstration.

2. Définir et représenter graphiquement une (ou plusieurs) fonction(s) avec Python : le faire
   sur un exemple donné par l'interrogateur.
   
3. Déterminer les fonctions polynomiales de degré 3 impaires.

4. Calculer  Calculez th$(x)$  en fonction  de $e^x$  et $e^{-x}$,  puis en
   fonction de $e^{2x}$, enfin en fonction de $e^{-2x}$. 

## Exercices

- Vous pouvez poser en colle tout exercice du chapitre 1.


