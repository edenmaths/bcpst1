---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 20

**Semaine du 18 au 22 mars 2024**

## Cours





### [Limites et continuité](../../LATEX/Poly_Lim_Cont_23.pdf)

Notion de  voisinage - Définitions  des différentes  limites - Continuité  en un
réel - Opérations sur les limites -
Composition -  Relation d'ordre  - Encadrement -  Prolongement par  continuité -
Limites et  suites - Théorème de  la limite monotone -  Fonctions équivalentes -
Opérations sur les  équivalents - Équivalents usuels - Fonction  continue sur un
intervalle - Fonctions continues par morceaux  - TVI - Image d'un intervalle par
une  fonction continue  -  Théoreme des  bornes  - Théorème  de  la bijection  -
Arctan - Résolution d'équations numériques par dichotomie. 



### [Dérivation : approfondissement](../../LATEX/PolyDerApp22.pdf)

Révisions  sur  les  chapitres  8  et 9  (nombre  dérivé,  calcul  de  dérivées,
dérivation et  variations) - Dérivées  successives :  la formule de  Leibniz est
hors  programme  mais a  été  vue  (on peut  la  rappeler  pour un  exercice)  -
Dérivation et extrema  - Théorème de Rolle - Égalité  des accroissements finis -
L'inégalité ne doit pas être sue mais  doit pouvoir être redémontrée à partir de
l'EAF - Application aux suites définies par une relation de récurrence.



### Python



#### [Suites](../../INFORMATIQUE/a_suites/)

Modélisations de diverses suites.

#### Dichotomie

   Résolution d'équation -  Exemple de recherche d'une  valeur approchée de
   $\sqrt{2}$.
   
#### Suites logistiques

Tracer de l'escargot 

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

	
1.  Fonction Python `dicho(f, a, b,  eps)` calculant une approximation de $f(x)=0$
  sur un intervalle 
  $[a,b]$ où $f$ est continue et strictement croissante avec une précision
  arbitraire   `eps`   (supérieure   à   $2^{52}$)  à   l'aide   d'une   méthode
  dichotomique.   Appliquer  à   la   recherche   d'une  approximation   de
  $\sqrt{37}$.

1. Énoncer les deux formes du TVI et le théorème de la bijection. 

1. Étude  de la fonction  Arctan et en particulier  détail du calcul  de sa
   fonction dérivée.
   
1.  Égalité des  accroissements finis  : énoncé  et démonstration.  Application à
  l'inégalité des accroissements finis.

1. Théorème de Rolle : énoncé et démonstration.

1. Dérivée de $\ln\circ\ln\circ\cdots\circ\ln$ ($n$ compositions avec $n∈ℕ$).


## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres
  19 et 20 du Poly.  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


