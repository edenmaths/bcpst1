---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 23

**Semaine du 8 au 12 avril 2024**

## Cours



### [Intégration 2](../../LATEX/Poly_Int_2_23.pdf)

Rappels sur  le chapitre 10 (définition  en termes d'aire, relation  de Chasles,
linéarité,  IPP, changement  de variable)  -  Intégrales et  relation d'ordre  -
Valeur moyenne  - Théorème fondamental de  l'analyse - Fonction définie  par une
intégrale -  Somme de Riemann :  théorème admis pour les  fonctions continues et
démonstration  pour des  fonctions  de classe  $\mathscr  C^1$ -  Interprétation
géométrique.


### [Applications linéaires (début)](../../LATEX/Poly_App_lin_23.pdf)

Définition - Image d'une CL - Conservation  du neutre - Image d'une famille liée
/ libre  - Caractérisation d'une  AL - $\mathscr  L(V,W)$ - Compositions  d'AL -
Noyau - Noyau et injectivité - Image - Image et surjectivité.

### Python



#### [Approximation du calcul d'intégrales](../../INFORMATIQUE/11_approxint/)

Méthodes  des  rectangles   -  des  trapèzes  -  de  Simpson   -  Un  atome
d'arithmétique des nombres flottants.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

   
1. Fonctions Python calculant les approximations des sommes de Riemann d'une
  fonction donnée en  paramètre ainsi que les bornes de  l'intervalle et le
  nombre $n$ d'intervalles de la subdivision.

1. Convergence des sommes de Riemann : démonstration de la convergence de la
  suite $(S_n)$ dans le cas d'une fonction $\mathscr C^1$ sur un intervalle $[a,b]$.

1.     Sens     de    variation    de    la     fonction    $G:    x\mapsto
    \displaystyle\int_x^{2x}\dfrac{dt}{t^2+t+1}$.       (Attention      aux
    justifications diverses).
	
1.  Démontrer  que  l'application  $f$   définie  de  $\mathfrak{M}_3(ℂ)$  dans
  $\mathfrak{M}_3(ℂ)$, par $ f(M) = M^T $ est une application linéaire.


1. Image d'une famille libre/liée par une AL.

1. Lien entre noyau et injectivité (énoncé et preuve).


## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres
  22 et 23 du Poly.  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


