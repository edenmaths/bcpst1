---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 17

**Semaine du 12 au 16 février 2024**

## Cours




### [Espaces vectoriels (début)](../../LATEX/Poly_EV_23.pdf)

Définition d'un $\mathbb K$-espace vectoriel (pour information - pas d'exercices
dessus). Cinq exemples de référence. Sous-espaces vectoriels - Caractérisations. 
SEV  engendré par  une famille  - Famille  génératrice -  Sur-famille d'une
famille génératrice - Famille libre (Juste
la définition - pas
encore les bases).





### [Suites (suite)](../../LATEX/Poly_Suites_Conv_23.pdf)

Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des suites monotones -  Croissances comparées (Pas encore d'équivalents
ni de suites adjacentes).


### Python



#### [Images et matrices](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que matrice :  fonction agissant sur
les pixels, retournements.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

   
8.   Soit  $\mathscr   F$   une   famille  de   vecteurs   d'un  $\mathbb   K$-EV
  E. Vect($\mathscr F$) est un SEV de E.

9.  On note $H=\left\{ ( x,y,z) \in \mathbb{R}^{3}\mid
x+2y+3z=0\right\}$, démontrer que $H$ est un sev de $\mathbb{R}^{3}$
en déterminant $\mathbf{u}$ et $\mathbf{v}$, deux vecteurs de $\mathbb{R}^{3}$, de
sorte que $H={\rm Vect}{( \mathbf{u},\mathbf{v})}$.
On note $\mathbf{w}=(1,2,3)$. Est-ce que $\mathbf{w}\in H$?

10. L'intersection de 2 SEV est encore un SEV. Quid de la réunion?

11.  L'ensemble des suites arithmétiques est un sous-espace vectoriel de
  l'espace vectoriel des suites réelles.

12. $\displaystyle\lim_{n\to+\infty}s_n$ avec $s_n=\displaystyle\sum_{k=1}^n\dfrac{n}{n^2+k}$ et fonction
Python  donnant une  approximation de  $s_n$ pour  un entier  $n$ donné  en
paramètre.

13. Théorème d'encadrement des limites : Ènoncé **précis** et démonstration **soignée**.



## Exercices

- Vous pouvez poser en colle tout  exercice de la liste des chapitres 17 et
  18 (sauf suites  adjacentes) du Poly.  On évitera  les exercices exigeant
  des démonstrations « avec des $ε$ ».

- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


