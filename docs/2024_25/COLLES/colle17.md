---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 17

**Semaine du 24 au 28 février 2024**

## Cours




### [Espaces vectoriels (début)](../../LATEX/Poly_EV_24_court.pdf)

Sous-espaces vectoriels - Caractérisations - 
SEV  engendré par  une  famille -  Famille génératrice  -  Famille libre  -
bases - Étude à l'aide du rang de la matrice de la famille.





### [Suites (suite)](../../LATEX/Poly_Suites_Conv_24_court.pdf)

Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des suites monotones - Croissances comparées -  Suites  équivalentes -
Définition - Caractérisation par la limite  du rapport - Rapports avec le signe,
les limites  - Opérations sur les  équivalents - Équivalents usuels  à partir de
suites convergeant  vers 0  (limites de taux  de variations) -  Limite de  $(1 +
1/n)^n$ - Suites adjacentes.



### Python


#### [Calcul matriciel](../../INFORMATIQUE/4_Calcul_matriciel/)

Fonctions `zeros(n: int, p: int) ->  list`, `eye(n: int) -> list`, `prod(A:
list, B:  list) -> list`,  transposée, produit  scalaire de deux  listes et
application au calcul du produit de deux matrices.




#### [Images et matrices](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que matrice:  fonction agissant sur
les pixels, retournements.




## Exercices

- Vous pouvez  poser en colle tout  exercice de la liste du  chapitre 17 et
  ceux du chapitre 18 au programme.

