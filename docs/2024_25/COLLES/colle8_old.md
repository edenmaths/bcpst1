


# Colle 8

**Semaine du 20 au 24 novembre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)






### Dénombrements

Calculs de sommes et produits - Notation Σ et Π - Sommes avec Python - Sommes des $k$,
$k^2$, $k^3$ - Somme des $x^k$ - Linéarité de Σ - Sommes doubles -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 



### Généralités sur les suites réelles

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).  **Remarque** :  les notions  de  sens de  variation et  de
convergence seront abordées dans un prochain chapitre.


## Python



### [Python](../../INFORMATIQUE/3_Recursion/)

Calculs de  sommes, de produits,  compteurs, puissance entière à  l'aide de
fonctions récursives.



## Questions à préparer


On commencera la colle par une des questions proposées ci-dessous:


- Calcul de $\displaystyle \sum_{i=1}^p\left(\sum_{k=1}^ik×i\right)$.
- Calcul  de la somme  des éléments d'une liste  d'entiers : en  donner une
  version récursive et une version avec boucle.
- Relation de Pascal: démonstration ensembliste et par le calcul.
- Calcul de $ \displaystyle\sum _{k=0} ^n k \binom{n}{k} $.
-  $ u_0  = 2  $ et  $ \forall  n  \in ℕ$,  $ u_{n+1}  = -2  u_n +  3 $.  Donner
  l'expression de $u_n$ en fonction de $n$. 
-    On    considère    la    suite   $(u_n)$    définie    par    :    $u_0=1$,
  $u_1=\sqrt{3}-\dfrac{1}{2}$     et      $\forall     n     \in      ℕ,     \:
  u_{n+2}=-u_{n+1}-u_{n}$. Exprimer son terme général en fonction de $n$. 
  
  
## Exercices

**CETTΕ SEMAINE**

- **On commencera par la résolution d'une équation du second degré qui doit
  être résolue  sans faute  et EN  MOINS DE  3 MINUTES  sinon la  note sera
  inférieure à 5.**
  
- On posera au moins un exercice impliquant l'écriture d'une fonction en Python.

**REMARQUE**: **Toute question de cours non
résolue  sans  aide  et  rapidement  (5 minutes  maxi)  entraîne  une  note
inférieure à la moyenne, voire largement  sous la moyenne en fonction de la
performance.**
**Tout programme Python doit fonctionner et ne pas
laisser penser qu'il n'a pas été testé à la maison.**
**Une question de cours ne doit pas  seulement être plus ou moins apprise par
cœur : elle doit être comprise et parfaitement maîtrisée.**
