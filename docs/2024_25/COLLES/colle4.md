---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 4

**Semaine du 7 au 11 octobre2024**

## Cours


### [Ensembles - Applications](../LATEX/Poly_sets_24.pdf)

Appartenance - Inclusion - Égalité - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition  - Produit  cartésien -  Fonction -  Fonction totale,  injective,
surjective,  bijective  (théorème  de   la  bijection  non  vu),  bijection
réciproque.




### [Réels](../LATEX/Poly_sets_24.pdf)

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue - Partie entière - Majorant, PGE, Sup. 




### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
-FONCTIONS - **Calcul de sommes et de produits, compteurs**.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous:

1.   Caractériser  à   l'aide  de   quantificateurs  l'injectivité   et  la
   surjectivité.  Comment démontrer  qu'un fonction  n'est pas  injective ?
   surjective ?

1. Énoncer et démontrer l'inégalité triangulaire pour les valeurs absolues.

1.  Soit $f:x\mapsto |2x+6|-|x-2|$.
	Donner  une expression  de $f$  comme fonction  affine par  morceaux. Tracer
	l'allure de la courbe représentative dans un repère bien choisi. 
  
1. Résoudre dans $ℝ$ $\sqrt{4-x^2}=\sqrt{3}x$.

 
1.   Déterminer une  fonction  qui  prend une  chaîne  et  un caractère  en
    arguments et  qui renvoie `True`  si le  caractère est présent  dans la
		chaîne  et  `False` sinon.  Adaptez  pour  obtenir l'indice  de  la
		première occurence du caractère 
    dans la chaîne et renvoyer $-1$ si le caractère n'est pas présent. 

## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 3 et 4 ou s'y apparentant.


