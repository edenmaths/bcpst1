---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 15

**Semaine du 27 au 31 janvier 2025**

## Cours



### Géometrie


Vecteurs du plan et  de l'espace - Droites et cercles dans le  plan - Droites et
plans dans l'expace - Projection orthogonale et produit scalaire.


### [Polynômes](../../COURS/16_Polynomes/)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.



### Python

[Calcul matriciel](../../INFORMATIQUE/4_Calcul_matriciel/)







## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 15 et 16 du
  Poly ou tout autre en relation avec le programme de la semaine. 


