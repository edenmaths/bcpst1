---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 6

**Semaine du 4 au 8 novembre 2024**

## Cours





### [Trigonométrie](../LATEX/Poly_Trigo_24.pdf)


Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $\cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais on n'a pas étudié les fonctions).




### [Complexes](../LATEX/Poly_complex_24.pdf)

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé - Formes  trigonométriques  et exponentielles  d'un
nombre  complexe  - Formules  d'Euler  -  Factorisation de  $1-e^{iα}$  par
l'exponentielle de l'angle moitié.





### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
- FONCTIONS - **Calcul de sommes et de produits, compteurs** - MAXIMUM -
Histogramme d'une liste et autres exercices sur les
parcours de listes et de chaînes de caractères.



## Questions à préparer

On commencera la colle par un exercice librement choisi et préparé par le(la) candidat(e).

## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 5 et 6 ou s'y apparentant.


