---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 10

**Semaine du 4 au 8 décembre 2023**

## [Cours](../../LATEX/PolyTotalB1_23.pdf)






### Généralités sur les fonctions


#### Prérequis (cf chapitre 1)

- Cours de 2nde : définition d'une fonction, lecture graphique, sens de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
  
#### Dérivées - Primitives


- Commandes Python permettant de tracer des représentations graphiques.
- Approche empiriquo-historique - Nombre dérivé - Dérivabilité en un réel -
  Application au calcul de certaines limites - Calcul des dérivées usuelles
  à partir  de la définition -  Approximation affine - Dérivabilité  sur un
  intervalle - Lien avec les variations - Fonctions strictement monotones -
  Extremum local - Dérivée d'une composée - Primitives.
- **Remarque**  : les  calculs de  limites *ne  sont pas*  un objectif  de ce
  chapitre. On insistera  sur le calcul de  dérivées/primitives, l'étude de
  sens de variation, le calcul  d'équations de tangentes, la représentation
  graphique  via une  allure rapide  « à  la main  » PUIS  avec Python,  la
  recherche d'extrema.
 

#### Initiation au calcul intégral

Définition en  tant qu'aire -  Relation de Chasles  - Intégration par  parties -
Changement de variable (il sera donné s'il n'est pas affine).


### [Python](../../INFORMATIQUE/3_Recursion/)

- Exemples de boucles : calculs de sommes et de produits - compteurs - test
  de présence de caractères dans une chaîne - Recherche d'extremum. 
- Fonctions récursives.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Calcul de la dérivée de $\sqrt{1+\sqrt{1+x^2}}$ + sens de variation
- Une boîte fermée en forme de pavé droit à base carrée a un volume de
  500m$^3$. Déterminer les dimensions qui minimisent la quantité de matière
  utilisée pour la fabriquer. 
- $ I=\displaystyle\int_0^{\pi}\sin(x){\rm e}^{-x}\,{\rm d} x $
- $J= \displaystyle\int_{-1}^{1}\dfrac{e^x}{e^{2x}+1}{\rm d} x\ (x=\ln t)$
- $K= \displaystyle\int_{\pi/6}^{\pi/4}\cos^5t\,{\rm d}t$
- $L=\displaystyle\int_{a}^{b} \frac{1}{t^2+6t+10} {\rm d}t$


## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 9 et 10 du
  Poly     et      utiliser     des     exemples     du      [cahier     de
  calcul](https://edenmaths.gitlab.io/bcpst1/2022_23/COURS/Revisions_BCPST.pdf). 

- **On commencera par demander  de tracer la représentation graphique d'une
  fonction ou d'une suite à l'aide de Python en moins de 3 minutes sinon la note sera
  inférieure  à  5.   Les  candidats  auront donc  leur  ordinateur  ou  un
  ordinateur de la salle 16 prêt à l'emploi**
  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


