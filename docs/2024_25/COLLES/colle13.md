---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 13

**Semaine de DS du 15 au 20 janvier 2024**

## [Cours](../../LATEX/PolyTotalB1_23.pdf)



### [Calcul matriciel](../../LATEX/Poly_Mat_23.pdf)

Approche  historique -  Définition -  Liens avec  Python :  `list` et  `array` -
Matrices  particulières -  Opérations sur  les matrices  - Formule  du binôme  -
Matrices inversibles.



### [Systèmes linéaires](../../LATEX/Poly_Systemes_23.pdf)

Exemple historique - Écriture matricielle  - Systèmes équivalents - Recherche de
l'inverse  avec le  tableau  complété -  Rang d'une  matrice  - Méthode  de
Gauss-Jordan -  Application à la  résolution de systèmes -  Rang, inconnues
principales et non principales - Utilisation de `NumPy` et `SymPy`. 





### [Statistique descriptive](../../LATEX/Poly_Stats_23.pdf)

Statistique univariée et bivariée. 

### Python



#### [Calcul matriciel](../../INFORMATIQUE/4_Calcul_matriciel/)
-  Fonctions Python  pour effectuer  les opérations  de base  : somme,  produit,
  puissance
- Fonctions renvoyant l'identité, la matrice nulle
- tests pour savoir si une matrice est symétrique, diagonale, triangulaire supérieure,...


#### [statistiques](../../INFORMATIQUE/5_Statistiques/)

Représentations graphiques. Moyenne, moyenne du produit puis calcul de la covariance avec König-Huyghens puis
de la variance.  Détermination du coefficient de corrélation et  de la droite de
régression.
Toute fonction de ce TP est à chercher et peut être demandée en colle.




## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


- Formule et démonstration de $\mathbb V(x+y)$.
- Formule de König-Huyghens pour le calcul de $\mathbb S(x,y)$.
- Fonction renvoyant le produit de deux matrices : proposer au moins deux solutions.
- Fonction déterminant la covariance avec toute fonction intermédiaire nécessaire.
- Fonction Python prenant une matrice sous forme de liste de listes
  d'entiers  en paramètres  et renvoyant  la  même matrice  avec une  ligne
  supplémentaire contenant la  somme des éléments de chaque  colonne et une
  colonne supplémentaire contenant la somme des éléments de chaque ligne.
  
- Résoudre dans $ℝ^3$ le système $\left\{\begin{array}{c}mx+y+z=m\\
x+my+z=m\\
x+y+mz=m
\end{array}\right.$


## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 12, 13
  et 14 du Poly.
- En  statistique, on  posera une question  impliquant l'écriture  d'une ou
  plusieurs fonctions en Python. 
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


