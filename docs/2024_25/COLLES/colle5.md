---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 5

**Semaine du 14 au 18 octobre2024**

## Cours





### [Réels](../LATEX/Poly_sets_24.pdf)

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue - Partie entière - Majorant, PGE, Sup. 




### [Trigonométrie](../LATEX/Poly_Trigo_24.pdf)


Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $\cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais on n'a pas étudié les fonctions).




### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
-FONCTIONS -  **Calcul de sommes  et de  produits, compteurs** -  MAXIMUM -
Histogramme d'une liste.



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous:

1.  Soit $f:x\mapsto |2x+6|-|x-2|$.
	Donner  une expression  de $f$  comme fonction  affine par  morceaux. Tracer
	l'allure de la courbe représentative dans un repère bien choisi. 
  
1. Résoudre dans $ℝ$ $\sqrt{4-x^2}=\sqrt{3}x$.

 
1.   Déterminer une  fonction  qui  prend une  chaîne  et  un caractère  en
    arguments et  qui renvoie `True`  si le  caractère est présent  dans la
		chaîne  et  `False` sinon.  Adaptez  pour  obtenir l'indice  de  la
		première occurence du caractère 
    dans la chaîne et renvoyer $-1$ si le caractère n'est pas présent. 

1.  Connaissant  la formule donnant  $\cos(a -b)$, démontrer les  formules donnant
  $\sin(a-b)$, $\tan(a-b)$ et $cos(2x)$.
  
1.  Démontrer la formule donnant $\cos(a -b)$ en utilisant le produit scalaire.


## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 4 et 5 ou s'y apparentant.


