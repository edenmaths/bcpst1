


# Colle 4

**Semaine du 9 au 13 octobre 2023**

## [Cours](../LATEX/PolyTotalB1_23.pdf)






### Réels

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue - Partie entière - Majorant, PGE, Sup. 



### Trigonométrie

Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais on n'a pas étudié les fonctions).



### [Algorithmes basiques](../INFORMATIQUE/2_Premiers_exos/)

sommes - compteurs




## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous :

1.  Soit $f:x\mapsto |2x+6|-|x-2|$.
	Donner  une expression  de $f$  comme fonction  affine par  morceaux. Tracer
	l'allure de la courbe représentative dans un repère bien choisi. 

1.  Connaissant  la formule donnant  $\cos(a -b)$, démontrer les  formules donnant
  $\sin(a-b)$, $\tan(a-b)$ et $cos(2x)$.
  
1.  Démontrer la formule donnant $\cos(a -b)$ en utilisant le produit scalaire.

2.  Déterminer une fonction en *Python* de paramètre un entier `n` et
  renvoyant  les $n$ premiers
  termes de la suite de Fibonacci. 
  On pourra créer dans cette fonction  une variable locale liée à une liste
  `F` contenant deux fois l'entier `1` suivis de `n - 2` fois l'entier `0`. 

5. Énoncer et démontrer l'inégalité  triangulaire pour les valeurs absolues
   et étudier le cas d'égalité.
   
6. Fonction Python renvoyant le nombre de 'a' contenus dans une chaîne de
  caractères donnée en argument.

**Remarque**: **à partir de cette semaine, toute question de cours non
résolue  sans  aide  et  rapidement  (5 minutes  maxi)  entraîne  une  note
inférieure à la moyenne, voire largement  sous la moyenne en fonction de la
performance.**
**Tout programme Python doit fonctionner et ne pas
laisser penser qu'il n'a pas été testé à la maison.**
**Une question de cours ne doit pas  seulement être plus ou moins apprise par
cœur : elle doit être comprise et parfaitement maîtrisée.**
