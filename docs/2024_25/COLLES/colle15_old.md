---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 15

**Semaine du 29 janvier au 2 février 2024**

## Cours



### [Géometrie](../../LATEX/Poly_Geo_23.pdf)



Rappels sur le calcul  vectoriel de 2nde - Rappels sur  le produit scalaire dans
le  plan  - Définition  et  autres  formulations  -  Bilinéarité et  symétrie  -
Orthogonalité - Produit scalaire dans  l'espace et applications - Vecteur normal
à un  plan - Distance  d'un point à un  plan - Représentations  paramétriques de
droites et de plans - Équation cartésienne d'un plan - Cercles et sphères.



### [Polynômes](../../COURS/16_Polynomes/)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.



### Python



#### [Images et matrices](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que matrice :  fonction agissant sur
les pixels, retournements.:



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Une ou plusieurs démonstration du Vrai ou Faux suivant:
L'espace est muni d'un repère orthonormal.

On considère la droite ($d$) dont un système d'équations paramétriques est :
 $\left\{\begin{array}{l c l}
 x&=&2 - \dfrac{t}{2}\\
 y &=& 1\\ 
 z &=&5 - \dfrac{3t}{2}\\
\end{array}\right. ~~(t \in \mathbb R)$

On note  A le  point de  coordonnées $(2~;~-1~;~1)$, B  le point  de coordonnées
$(4~;~-2~;~2)$ et C le point de ($d$) d'abscisse $1$.

1. La droite ($d$) est parallèle à l'axe $\left(\text{O}~;~\overrightarrow{\jmath}\right)$.

2.  Le plan $P$ d'équation $x+ 3z - 5=0$ est le plan passant par A et orthogonal à ($d$).

3. La mesure de l'angle géométrique $\widehat{\text{BAC}}$ est $\dfrac{\pi}{3}$ radians.

4. La sphère de centre C et passant par B coupe le plan $P$ d'équation $x+3z -5 = 0$.

5.  Fonction  Python   déterminant  le  degré  d'un   polynôme  (liste  des
   coefficients dans  l'ordre croissant des  degrés) en créant  d'abord une
   fonction   qui  normalise   la  liste   (en  enlevant   les  zéros   non
   significatifs).
   
6.  Fonction  Python  qui  additionne deux  polynômes  en  disposant  d'une
   fonction `degré` et `normalise`.
   
7. Fonction Python qui multiplie deux polynômes en disposant d'une fonction
   `degré` et `normalise`.
   


## Exercices

- Vous pouvez poser en colle tout exercice de la liste des chapitres 15 et 16 du
  Poly. 
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  Une question  de cours ne doit  pas seulement être plus  ou moins apprise
  par cœur : elle doit être comprise et parfaitement maîtrisée.


