---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 7

**Semaine du 12 au 18 novembre 2024**

## Cours





### [Complexes](../LATEX/Poly_complex_24.pdf)

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé - Formes  trigonométriques  et exponentielles  d'un
nombre  complexe  - Formules  d'Euler  -  Factorisation de  $1-e^{iα}$  par
l'exponentielle de l'angle moitié.






### [Dénombrements](../LATEX/Poly_denomb_24.pdf)

Calculs de sommes et produits - Notation Σ et Π - Sommes avec Python - Sommes des $k$,
$k^2$, $k^3$ - Somme des $x^k$ - Linéarité de Σ - Sommes doubles -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 


### Python

### [Premiers pas en Python](../INFORMATIQUE/2_Premiers_exos/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles  -  Type  `List`  -  Chaînes  de  caractères  -  `while`,  `for`
- FONCTIONS - **Calcul de sommes et de produits, compteurs** - MAXIMUM -
Histogramme d'une liste et autres exercices sur les
parcours de listes et de chaînes de caractères.




### [Python](../../INFORMATIQUE/3_Recursion/)

Calculs de sommes, de produits, compteurs, puissance entière.





## Questions à préparer

On commencera la colle par un exercice librement choisi et préparé par le(la) candidat(e).

## Exercices

- Vous pouvez poser en colle tout exercice des chapitres 6 et 7 ou s'y apparentant.


