---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 18

**Semaine du 19 au 23 février 2024**

## Cours




### [Suites (suite)](../../LATEX/Poly_Suites_Conv_23.pdf)

Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des suites monotones - Croissances comparées -  Suites  équivalentes -
Définition - Caractérisation par la limite  du rapport - Rapports avec le signe,
les limites  - Opérations sur les  équivalents - Équivalents usuels  à partir de
suites convergeant  vers 0  (limites de taux  de variations) -  Limite de  $(1 +
1/n)^n$ - Suites adjacentes.


### Python



#### [Suites](../../INFORMATIQUE/a_suites/)

Modélisations de diverses suites.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


11.  L'ensemble des suites arithmétiques est un sous-espace vectoriel de
  l'espace vectoriel des suites réelles.

12. $\displaystyle\lim_{n\to+\infty}s_n$ avec $s_n=\displaystyle\sum_{k=1}^n\dfrac{n}{n^2+k}$ et fonction
Python  donnant une  approximation de  $s_n$ pour  un entier  $n$ donné  en
paramètre.

13. Théorème d'encadrement des limites : Ènoncé **précis** et démonstration
    **soignée**.
	
14.  $\displaystyle\lim _{n\to+\infty} \left( \sqrt{n+\sqrt{n}}-\sqrt{n} \right)$ et
     $ \displaystyle\lim _{n\to+\infty} \left( 1 + \frac{1}{n} \right)^n $ 

15. Soit $(u_n)$ une suite convergeant vers $0$.  Donner des équivalents
    simples de $\sin(u_n)$, $\cos (u_n)-1$  et $\tan(u_n)$. En admettant le
    premier démontrer les deux suivants.
	
16.         Pour         tout        $n\geqslant2$,         on         pose
    $a_n=2^n\sin\left(\frac{\pi}{2^n}\right)\quad\hbox{et}\quad
    b_n=2^n\tan\left(\frac{\pi}{2^n}\right)$
	
	Démontrer que ces suites sont adjacentes.

## Exercices

- Vous pouvez poser en colle tout  exercice de la liste du chapitre
  18  du Poly.  On évitera  les exercices exigeant
  des démonstrations « avec des $ε$ ».

- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


