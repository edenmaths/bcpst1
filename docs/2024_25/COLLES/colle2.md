---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 2

**Semaine du 21 au 26 septembre 2024**

## Cours


### [Généralités sur les fonctions ](../../LATEX/Poly_f2var_23.pdf)

Définition d'une  fonction -  En Python  `def` et  `lambda` -  Notations et
vocabulaire  -   Représentation  graphique   -  Lien  avec   la  résolution
d'équations et d'inéquations - Parité -  Périodicité - Bornes - Monotonie -
Opération sur les fonctions -  Racine carrée, logarithme, exponentielles de
bases quelconques, fonction puissance α.



### Bases pour raisonner

Logique - Implication - Contraposée -  Négation - Quantificateurs - Réciproque -
Équivalence - Récurrence simple, double, forte - Analyse Synthèse.



### Python

### Fonctions

Définition  de fonctions  simples  liées au  chapitre  1 -  Représentation
graphique de fonction numérique.

### [Premiers pas en Python](../INFORMATIQUE/1_Premiers_Pas/)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `List` - Chaînes de caractères - `while`, `for`



## Questions à préparer

On commencera la colle par une des questions proposées ci-dessous:

0.  Tout exercice du [test de rentrée](../DS/INT_1_24.pdf)

1. Théorème de composition de fonctions monotones : énoncé et
   démonstration.

2. Définir et représenter graphiquement une (ou plusieurs) fonction(s) avec Python : le faire
   sur un exemple donné par l'interrogateur. 
   

5. Déterminer une **FONCTION** qui prend  en argument une chaîne de caractères et
   qui **renvoie**  la chaîne avec ses  caractères de rang pair  en majuscules et
   les autres en minuscules. L'examinateur pourra ensuite demander une variante.
   
3. Démontrer que pour tout entier naturel $n$, si $n^2$ est pair alors $n$ l'est
   aussi.

3. Déterminer une fonction Python qui prend un entier naturel $n$ en argument et
   renvoie la somme des entiers de 1 à $n$. On devra y lire le mot `for`. 

## Exercices

- Vous pouvez poser en colle tout exercice du chapitre 1 et 2 ou s'y apparentant.


