---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 26

**Semaine du 21 au 22 mai 2024**

## Cours






### [Probabilités sur un ensemble fini](../../LATEX/Poly_Intro_Probas_23.pdf)

Expérience  aléatoire -  Vocabulaire  - Système  complet  d'évènements -  Espace
probabilisé  : 3  axiomes de  Kolmogorov -  Équiprobabilité -  Probabilité d'une
union  - Croissance  d'une  mesure  de probabilité  -  Formule des  probabilités
totales -  Conditionnement -  Formule des probabilités  composées -  Formules de
Bayes - Indépendance : mutuelle et 2 à 2.


### [Variables aléatoires réelles finies](../../LATEX/Poly_VAR_23.pdf)

Loi de probabilité -  SCE lié à une VAR - Existence d'une  loi de valeurs prises
connues - Fonction de répartition - Fonction d'une VAR - Espérance - Théorème de
transfert  - Linéarité  de  l'espérance  - Moments  d'ordre  $r$ -  Moments
centrés - Variance - Loi binomiale : expression de la loi et calcul de l'espérance.


### Python



#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulles, tri par sélection, tri par insertion.

#### Probas

Modélisations d'expériences aléatoires.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

	

1. Tri à bulles : code et commentaires.

1. Tri par sélection : code et commentaires.

1. À partir des trois axiomes de Kolmogorov, démontrer :
    
	1.  $\mathbb P \left(\complement_\Omega{A}\right)=1-\mathbb P(A)$;
    2. $\mathbb P(\emptyset)=0$;
    3. $\forall(A,B)\in  {\mathscr T}\times{\mathscr T},\
      A\subset B \Longrightarrow \mathbb P(A)\leqslant \mathbb P(B)$;
    4.  $\forall(A,B)\in {\mathscr T}\times{\mathscr T},\
      \mathbb P(A\cup B)=\mathbb P(A)+\mathbb P(B)-\mathbb P(A\cap B)$ (cas
      particulier de la formule du crible de Poincaré). 
	  
1. 2 versions de la FPΤ et démonstrations.

1. Simulation à l'aide de Python du problème du Duc de Toscane.

1. Calcul de l'espérance d'une loi binomiale.


## Exercices

- Vous pouvez poser en colle tout  exercice de la liste du chapitre
   24  du Poly  et ce  qui  correspond au  programme dans  le chapitre  25,
   **notamment des exercices faisant intervenir des notions de dénombrement
   et des simulations avec Python**.
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


