---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 24

**Semaine du 15 au 20 avril 2024 (semaine de DS)**

## Cours



### [Applications linéaires (suite)](../../LATEX/Poly_App_lin_23.pdf)

Définition - Image d'une CL - Conservation  du neutre - Image d'une famille liée
/ libre  - Caractérisation d'une  AL - $\mathscr  L(V,W)$ - Compositions  d'AL -
Noyau - Noyau et injectivité - Image - Image et surjectivité  -  Image
réciproque -  Homomorphismes particuliers - Puissances  d'endomorphismes - Image
d'une   famille  libre/génératrice   -   DIMENSION   FINIE  Détermination   d'un
homomorphisme à l'aide de l'image d'une base - Caractérisation des isomorphismes
(image  d'une  base  est  une  base)  - Deux  espaces  isomorphes  ont  la  même
dimension  -  Rang d'un  homomorphisme  -  Théorème  du  rang -  Injectivité  et
surjectivité en  dimension finie -  Noyau d'une  forme linéaire -  Matrice d'une
famille  dans une  base -  Matrice  d'un homomorhisme  - Image  d'un vecteur  et
produit  matriciel  -  Endomorphisme  canoniquement associé.

### Python



#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulles, tri par sélection, tri par insertion.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

	
1.  Démontrer  que  l'application  $f$   définie  de  $\mathfrak{M}_3(ℂ)$  dans
  $\mathfrak{M}_3(ℂ)$, par $ f(M) = M^T $ est une application linéaire.


1. Image d'une famille libre/liée par une AL.

1. Lien entre noyau et injectivité (énoncé et preuve).

1. On note T l'homomorphisme de $ℝ_3[X]$ dans $ℝ_2[X]$ défini par
  $T(P)=XP''+P(1)$,  $\mathcal  B_2$  la  base  canonique  de  $ℝ_2[X]$  et
  $\mathcal B_3$ la base
  canonique  de  $ℝ_3[X]$.   Déterminez ${\rm  Mat}_{\mathcal  B_2,\mathcal
  B_3}$ ainsi qu'une base 
  de l'image et du noyau de $T$

1. Tri à bulles : code et commentaires.

1. Tri par sélection : code et commentaires.


## Exercices

- Vous pouvez poser en colle tout  exercice de la liste du chapitre
   23 du Poly.  
- Toute  question de cours non  résolue sans aide et  rapidement (5 minutes
  maxi) entraîne une note inférieure à  la moyenne, voire largement sous la
  moyenne en fonction de la performance.
-  Tout programme  Python en question  de cours  doit fonctionner et  ne pas
  laisser penser qu'il n'a pas été testé à la maison.    
-  **UNE QUESTION  DE COURS NE DOIT  PAS SEULEMENT ÊTRE PLUS  OU MOINS APPRISE
  PAR CŒUR : ELLE DOIT ÊTRE COMPRISE ET PARFAITEMENT MAÎTRISÉE.**


