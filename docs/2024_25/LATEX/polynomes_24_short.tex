%% Last modified: <polynomes_24_short.tex modifié par  Guillaume Connan le mercredi 15 janvier 2025 à 13h 22min 47s>


 \chapterimage{abel}
 \chaptertext{Le programme de BCPST-1:\\
  \textbf{ Polynômes, règles de calcul.} \\
  - Notation $P: x \mapsto \displaystyle\sum_{k=0}^{n} a_{k} x^{k}$.\\
  - Monômes, coefficients. Polynôme nul.\\
  -  Cas   particuliers:  polynômes  constants,   fonctions  affines,
    fonctions puissances entières. \\
  - Les  opérations usuelles (combinaison linéaire,  produit, composée)
    sur les polynômes fournissent des polynômes. \\
  - Unicité  de l'écriture des  polynômes: un polynôme  à coefficients
    dans $\mathbf{R}$ est nul si et seulement si tous ses coefficients sont
    nuls.\\
  - Coefficient dominant. Degré d'un polynôme.\\ 
  - Degré d'une somme, d'un produit de polynômes.\\
  - Polynôme dérivé. Degré du polynôme dérivé.\\
 \textbf{Racines et factorisation.}  \\
  - Racines réelles (ou zéros réels) d'un polynôme.\\ 
  - Un nombre réel $\alpha \in \mathbb{R}$ est racine d'un polynôme $P$\\
    si et  seulement s'il existe  un polynôme $Q$ tel  que $P(x)=(x-\alpha)
    Q(x)$ pour tout $x \in \mathbb{R}$.
  - Généralisation à plusieurs racines distinctes.\\
  - Le nombre de racines distinctes d'un polynôme non nul est majoré par son degré. \\
  - Tout polynôme de degré impair a au moins une racine réelle. \\
  - Un  polynôme  de  degré  $n   \in  \mathbb{N}^{*}$  possédant  $n$  racines
    distinctes     s'écrit     sous     la      forme     $P:     x     \mapsto
    a_{n}\left(x-\alpha_{1}\right) \cdots\left(x-\alpha_{n}\right)$.\\
 \textbf{Racines multiples}\\
  - Ordre de multiplicité d'une racine.\\ 
  - Une racine  $\alpha$ d'un polynôme $P$ est une  racine multiple si et
    seulement si $P^{\prime}(\alpha)=0$. 
}



%   Les équations polynomiales sont au  c\oe ur des mathématiques, tant
%   au niveau  des applications  concrètes qu'à celui  purement abstrait.  Ce sont
%   deux jeunes héros de  l'esprit morts tragiquement à 20 et  26 ans qui verront,
%   en avance sur leur temps, que la  résolution concrète passe par un voyage dans
%   l'abstraction:  il s'agit  du  norvégien Niels  \textsc{Abel}  et du  français
%   Évariste \textsc{Galois}. Ils démontrent  indépendamment l'un de l'autre qu'il
%   n'est  pas  possible d'obtenir  des  formules  simples donnant  les  solutions
%   générales d'une équation  à l'aide des opérations arithmétiques de  base et de
%   racines $n$-èmes  de nombres  entiers (on dit  \textit{à l'aide  de radicaux}).
%   \textsc{Galois} ira 
%   plus  loin et  déterminera un  critère permettant  de savoir  si une  équation
%   donnée  peut  avoir  des  solutions   exprimables  par  radicaux.  Deux  héros
%   romantiques  (nous sommes  autour  de 1830)  ont  donc ouvert  la  voie à  des
%   théories qui vont bouleverser le monde.
%   À notre petit niveau, pour entamer notre second semestre, nous nous
%   contenterons  d'explorer quelques  résultats très  simples qui  serviront
%   d'introduction au cours de deuxième année.}

\chapter{Polynômes}



%\section{Programme de BCPST-1}



\section{Quelques définitions}

\subsection{Définition générale}

\begin{definition}[Polynôme à une indéterminée]
  Un \textbf{polynôme} à une indéterminée,  à coefficients dans $\mathbb R$
  est une fonction pouvant s'écrire sous la forme
%
\[ P: x\mapsto \sum _{k=0} ^{n} \alpha_i x^i = \alpha_0 + \alpha_1 x + \cdots + \alpha_n x^n ,\]
%

où $ \alpha_i \in \mathbb R $ pour tout $ i \in [\![ 0 , n ]\!] $.
Les $\alpha_i$ s'appellent les \textbf{coefficients} du polynôme $ P $. 
\end{definition}

\subsection{Polynômes particuliers}

\begin{itemize}
\item  Le polynôme  nul est  l'unique  polynôme vérifiant:  $(∀t
  ∈\mathbb R)(P(t)=0)$.
  Il est souvent noté $Θ$.

\item Tout polynôme vérifiant $(∀t ∈\mathbb R)(P(t)=a)$  avec $a$ un élément de $\mathbb R$
  fixé est un \textit{polynôme constant}.
\item  Un  polynôme  n'ayant  qu'un  seul coefficient  non  nul  est  appelé  un
  \textit{monôme}.
\item  Un  polynôme  n'ayant  que  deux coefficients  non  nuls  est  appelé  un
  \textit{binôme}.
\item  Un  polynôme  n'ayant  que  trois coefficients  non  nuls  est  appelé  un
  \textit{trinôme}.
\end{itemize}

\subsection{Degré d'un polynôme}


\begin{definition}[Degré d'un polynôme]
  Soit P un polynôme  non nul. Si $P(x)=\displaystyle\sum_{k=0}^na_ix^i$ avec
  \fbox{$a_n\neq 0$}, alors $n$ est appelé \textbf{degré du polynômne} P.
  On le note $\deg P = n$.

  Le  monôme $a_nx^n$  est  alors appelé  \textbf{terme
   dominant} et $a_n$ est le \textbf{coefficient dominant}.

 Si le coefficient dominant est 1, on dit que P est un \textbf{polynôme unitaire}.
\end{definition}

Le degré est en fait la plus grande puissance des monômes de l'écriture de P dont le
coefficient est non nul.
\textit{Par convention} on note $\deg(Θ)=-\infty$. On en déduit le théorème suivant:

\begin{theoreme}
  P est un polynôme constant \textit{si, et seulement si} $\deg(P) \leqslant 0$.
\end{theoreme}



\begin{recherche}
  On peut représenter sur Python un polynôme par la liste de ses coefficients dans
  le sens croissant des degrés des monômes.

  Par exemple $P: x\mapsto 42 + x^2 +3x^3 + 12x^5$ sera représenté par:

  \begin{pythoncode}
    P = [42, 0, 1, 3, 0, 12]
  \end{pythoncode}

  Déterminer                 alors                  une                 fonction
  \mintinline[fontsize=\small]{python}{def degre(P: list) -> int} 
  qui renvoie le degré d'un polynôme \texttt{P}.  Pour moins l'infini, on peut
  utiliser                                                               au
  choix
  \mintinline[fontsize=\small]{python}{-float('inf')},
  \mintinline[fontsize=\small]{python}{-np.inf}          ou
  encore \mintinline[fontsize=\small]{python}{-math.inf}
  



\end{recherche}

\subsection{Opérations sur les polynômes}

Dans    cette   section,    $P:x\mapsto   \displaystyle\sum_{i=0}^na_ix^i$,
$Q:x\mapsto \displaystyle\sum_{i=0}^mb_ix^i$ sont
deux polynômes de degrés $n$ et $m$ respectivement.
On supposera par exemple que $m \leqslant n$.

\begin{definition}
  \begin{description}
  \item[Addition]
    $P+Q:x\mapsto a_nX^n+a_{n-1}x^{n-1}+\cdots+a_{m+1}x^{m+1}+(a_m+b_m)x^m+\cdots+(a_1+b_1)x+(a_0+b_0)$.
   \item[Produit                         externe]                        $λ\cdot
     P:x\mapsto (λ×a_n)x^n+(λ×a_{n-1})x^{n-1}+\cdots+(λ×a_1)x+(λ×a_0)$
   \item[Multiplication] $P×Q:x\mapsto =\displaystyle\sum_{i=0}^{n+m}c_ix^i$ avec $c_k=\displaystyle\sum_{i+j=k}a_i×b_j=\displaystyle\sum_{i=0}^ka_ib_{k-i}$.
  \end{description}
\end{definition}


% \begin{recherche}
%   Déterminer une fonction Python pour chacune des opérations précédemment définies.
% \end{recherche}




\begin{theoreme}[Polynômes égaux]

  Deux polynômes sont égaux si, et seulement si, ils ont les mêmes coefficients
  
\end{theoreme}



\begin{recherche}
  \begin{enumerate}
  \item       Quel       est       le      degré       de       $P:x\mapsto
    (x-1)\displaystyle\sum_{i=0}^nx^i$? Quel est son terme 
    dominant?   Son    coefficient   dominant?    Quelles   sont    ses   autres
    caractéristiques?
   \item $P:x\mapsto ax^3+bx^2+cx+d$ et  $Q:x\mapsto αx^2+βx+γ$. Que valent
     $P+Q$, $P×Q$, $P\circ Q$? À quelle condition a-t-on $P=Q$?
  \end{enumerate}
\end{recherche}






\begin{theoreme}[Degré de la somme et du produit]
Soit $P$ et $Q$ deux polynômes  et $ λ \in \mathbb R^* $. On a:
\begin{itemize}
\item $ \deg (P+Q) = $ 
\item $ \deg ( P Q ) = $

\item $ \deg ( λ P ) = $ .
\end{itemize}
  
\end{theoreme}




\begin{theoreme}[Parité (en tant que fonction)]
  \begin{enumerate}
  \item Le polynôme P est pair (en tant que fonction) si, et seulement si, tous ses
    coefficients d'indices impairs 
    sont nuls.
  \item Le polynôme P est impair (en tant que fonction) si, et seulement si, tous ses
    coefficients d'indices pairs 
    sont nuls. 
  
  \end{enumerate}
  
\end{theoreme}



\begin{recherche}
  Parmi les polynômes suivants, y a-t-il des polynômes pairs ? Impairs?
  
  $x\mapsto x^5+x$,
  $x\mapsto 5x^8+7x^4+3x^2$, $x\mapsto 3x^3+2x^2$.

  
\end{recherche}




















\section{Racines d'un polynôme}


% \subsection{Étymologie}



% \setindent{0cm}
% \begin{flushleft}
% \begin{minipage}[t]{3cm}
% \begin{center}
%   \includegraphics[width=1.5cm]{al-khwarizmi}
% %\caption{Al Khwarizmi\\(780 - 850)}
% \end{center}
% \end{minipage}
% \end{flushleft}
% \setindent{\indentationcours}
% \vspace{-3cm}
% \begin{minipage}[t]{\linewidth}
% \end{minipage}


% Abu  'Abdallah  Muhammad  ibn  Musa  {\large al-Khwarizmi}  dans  son  \oe  uvre
% \textit{Al-Kitab  al-mukhtasar  fi  hisab \emph{\large  al-jabr}  wa-l-muqabala}
% introduit  le terme  \textit{jidhr},  qui signifie  \textit{racine,  ce qui  est
%   caché}, pour désigner ce que nous  notons $x$ dans une équation polynomiale du
% type $x^3-3x^2+10x-5=0$.


\subsection{Définitions}



\begin{definition}[Racine d'un polynôme]
Soit $P$ un polynôme  et $ α  \in \mathbb R $ un réel. On dit que $α$
est une \textbf{racine} du polynôme $P$ si, et seulement si, $P(α) = 0$ .
\end{definition}


\begin{recherche}
  Combien le polynôme nul admet-il de racines?
\end{recherche}



\begin{definition}[Multiple, diviseur]
  Soit $A$ et $B$ deux polynômes avec $B$ non nul. S'il existe un polynôme $Q$ tel que 
  $A = B×Q$ alors on dit que:
  \begin{itemize}
  \item le polynôme $B$ est un \textbf{diviseur} du polynôme $A$,
  \item ou le polynôme $B$ \textbf{divise} le polynôme $A$,
  \item ou le polynôme $A$ est un \textbf{multiple} du polynôme $B$,
  \item ou le polynôme $A$ est \textbf{divisible} par polynôme $B$,
  \end{itemize}
\end{definition}

Par exemple, si  $ P(x)=x + 1 $, $  Q(x) = x - 1  $ et $ R(x) = x^2  - 1 $,
alors $P$ et $Q$ ....... $R$.




\subsection{Racines et divisibilité}


% Ceci nous  permet d'introduire  un des  théorèmes importants  de ce  chapitre en
% deux étapes:


% \begin{theoreme}[Division par $X-α$]\label{th::div_rac}
%   Soit  $P$ un  polynôme de  $\mathbb K[X]$ et  $α  \in \mathbb K$  alors le  reste de  la
%   division de P par $X-α$ est $P(α)$
% \end{theoreme}


% \begin{proof}
%   \GrosTrou{
%     Écrivons la division euclidienne de P par $X-α$:
%     $$P=(X-α)× Q+R \text{ avec } \deg(R)<\deg(X-α)=1$$
%   On en déduit que R est un polynôme constant. Notons $c$ cette constante:
%   $$P=(X-α)× Q+c \text{ avec } c∈\mathbb K$$
%   On  en déduit  que  $P(α)=(α-α)×Q(α)+c=c$.  Or $\deg(P(α))<=0<\deg(X-α)$  donc
%   $P(α)$ est bien le reste cherché.  La division euclidienne de P par $X-α$
%   s'écrit donc:
%   $$P=(X-α)× Q+P(α) $$
%   }
% \end{proof}

\begin{theoreme}[Racines et divisibilité (admis)]

Soit $P$ un polynôme et $α \in \mathbb R$. Le réel $α$ est racine du
polynôme $P$ si et seulement si $X - α$ divise P, c'est-à-dire
%
\[P(α)=0 ⟺ x - α \text{ divise } P\]
\end{theoreme}
%\bclampe Utilisez le théorème précédent.

% \begin{proof}
%   \GrosTrou{
%     Écrivons la division euclidienne de P par $X-α$:
%     $$P=(X-α)× Q+P(α) $$
%   Alors on obtient l'équivalence suivante:
%   $$P(α)=0⟺X-α \text{ divise } P$$
%   }
% \end{proof}


\begin{theoreme}
Soit $P$ un polynôme et $α_1$, $α_2$, \dots, $α_m$ des éléments deux à deux
distincts de $\mathbb R$. Le polynôme $x\mapsto (x-α_1) ×(x-α_2)× \cdots× (x-α_m) $ divise $P$ si et seulement si $α_1$, $α_2$, \dots, $α_m$ sont des racines de $P$.
\end{theoreme}

%\bclampe Récurrence sur le nombre de racines pour la réciproque.

% \begin{proof}
% \GrosTrou{
% L'implication directe est évidente, il suffit donc de montrer la réciproque. On suppose que $α_1$, $α_2$, \dots, $α_m$ sont des racines de $P$, et on pose $ \forall k \in [\![ 1 , m ]\!] $,
% %
% \[ H(k) = \text{\og{}} (X-α_1) ×(X-α_2)× \cdots× (X-α_k) \text{ divise } P \text{\fg{}} \]
% %
% L'initialisation de la récurrence est directe par la propriété précédente: $H(1)$ est vraie.\\
% Soit $k \in [\![ 1 , m-1 ]\!]$ un entier naturel fixé, on suppose que $ H(k) $ est vraie: il existe $A \in \mathbb K[X]$ tel que
% %
% \[ P = (X-α_1) (X-α_2) \dots (X-α_k) A .\]
% %
% Comme $ k+1 \leq m $, alors $ α_{k+1} $ est racine de $P$ et $P(α_{k+1}) = 0 $. Mais comme les $α_i$ sont supposés distincts deux à deux $(α_{k+1}-α_1) ×(α_{k+1}-α_2)× \cdots× (α_{k+1}-α_k) \neq 0 $. Donc nécessairement $ A( α_{k+1} ) = 0 $ et $α_{k+1}$ est racine de $A$. Par la proposition précédente, il existe alors $ C \in \mathbb K[X] $ tel que $ A  = ( X - α_{k+1} ) ×C $. Donc
% %
% \[ P = (X-α_1) ×(X-α_2) ×\cdots× (X-α_k) ×( X - α_{k+1} )× C .\]
% %
% Ce qui montre $H(k+1)$.\\
% Donc $ \forall k \in [\![ 1 , m ]\!] $, $H(k)$ est vraie. En particulier $H(m)$ est vraie, ce qui termine la preuve.
% }
% \end{proof}


\begin{recherche}
Par exemple $P: x\mapsto x^4-2x^2+1$ admet 1 et $-1$ comme racines évidentes. On peut donc
le factoriser par $(x-1)×(x+1)$...terminez la factorisation de P.  
\end{recherche}



\begin{theoreme}[Nombre maximum de racines / Caractérisation du polynôme nul]
  Un polynôme  de degré inférieur ou  égal à $n$  et qui possède au  moins $n+1$
  racines distinctes est le polynôme nul.

  Par contraposée, tout polynôme non nul de degré $n$ admet au plus $n$ racines.
\end{theoreme}


\begin{theoreme}[Racines des polynômes de degré impair]
  Tout polynôme de degré impair a au moins une racine réelle.
\end{theoreme}



\subsection{Ordre de multiplicité d'une racine}

\begin{definition}[Ordre de multiplicité]
Soit $P$ un polynôme, $p$ un entier naturel non nul et $α \in \mathbb R $ un réel. On dit que $r$ est une racine d'\textbf{ordre de multiplicité} $p$ du polynôme $P$ lorsque
%
\[ x\mapsto ( x - α ) ^p \text{ divise } P \quad \text{ et } \quad x\mapsto
  ( x - α ) ^{p+1} \text{ ne divise pas } P .\] 
\end{definition}

\begin{NB}
Autrement dit, $α$ est une racine d'ordre de multiplicité $p$ du polynôme $P$ lorsqu'il existe un polynôme $Q$  tel que
%
\[  P(x) = ( x - α ) ^p  Q (x) \text{ et }  Q(α) \neq 0 .\]
\end{NB}

\begin{DANGER}
Attention! Pour  démontrer que $α$  est une racine  d'ordre $p$ de  $P$, il
faut penser à vérifier la deuxième condition: que $x\mapsto ( x - α ) ^{p+1}$ ne divise pas $P$.
\end{DANGER}

Si l'ordre vaut 1, on dit que $α$ est une \textit{racine simple}, sinon si l'ordre
vaut 2, c'est  une \textit{racine double} et de manière  générale c'est une
\textit{racine multiple}.

\begin{theoreme}[Caractérisation des racines multiples]
  Une  racine $\alpha$  d'un polynôme  $P$ est  une racine  multiple si  et
  seulement si $P^{\prime}(\alpha)=0$. 
\end{theoreme}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% E X E R C I C E S
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\modeexercice


\section{Quelques petits jeux}


\subsection{Second degré}


\begin{exercice}[ Développer, réduire et ordonner]
  
  
  \begin{multicols}{4}
    \begin{enumerate}
\item $(4x-5)^2$
\item $(2x-3)(2x+3)$
\item $(4x-5)(2x+3)$
\item $(5x+1)^2$
\end{enumerate}
\end{multicols}
\end{exercice}












\begin{exercice}[Factoriser]
  Factoriser (sans avoir à développer):
  \begin{multicols}{3}
    \begin{enumerate}
\item $(4x-3)(x+4)-(4x-3)(3x+1)$
\item $(3x+2)^2-25$
\item $(3x+4)^2-(2x+1)^2$
\item $(2x+3)(x+5)-(x+5)^2$
\end{enumerate}
\end{multicols}
\end{exercice}







\begin{exercice}[Complétion du carré et discriminant]
Résoudre les équations suivantes en complétant  le carré ET en utilisant le
discriminant.  Proposer  des  phrases   utilisant  le  mot  \textit{racine}
correspondant à chacune de ces équations.

\begin{multicols}{4}
  \begin{enumerate}
  \item $8x^2-8x+12=0$
  \item $x^2+8x=84$
  \item $9x^2+13x=10$
  \item $3x^2+4x=5$
  \item $x^2+17x-18=0$
  \item $3x^2+10x+3=0$
  \item $11x^2-12=x$
  \item $8x+2=-7x^2$
  \item $x^2+111x=3400$
  \item $7x^2-26x=1008$
  \end{enumerate}
  
\end{multicols}

\end{exercice}




% \begin{exercice}
  
% Pour chaque fonction,
% \begin{itemize}
% \item retrouver sa forme canonique;
% \item donner l'allure de sa courbe;
% \item donner son tableau de variations.
% \end{itemize} 

% \begin{multicols}{3}
% \begin{enumerate}
% \item $f(x)=-2x^2+20x+7$
% \item $g(x)=x^2+8x-3$
% \item $h(x)=5x^2-10x+13$
% \end{enumerate}
% \end{multicols}

% NB : on pensera à vérifier avec Python

% \end{exercice}




\begin{exercice}[Étude de fonctions du second degré]
  
Pour chaque fonction,
\begin{itemize}
\item retrouver sa forme développée ;
\item donner l'allure de sa courbe ;
%\item donner son tableau de variations.
\end{itemize} 

\begin{multicols}{4}
  \begin{enumerate}
\item $f(x)=3(x-4)^2-5$
\item $g(x)=-2(x-3)^2+6$
\item $h(x)=(x+1)^2-8$
\item $k(x)=-5(x-2)^2+4$
\end{enumerate}
\end{multicols}
%NB : on pensera à vérifier avec Python.
\end{exercice}





\begin{exercice}[Equations du 2nd degré particulières]
  Résoudre dans $ℝ$:

  \begin{multicols}{3}
  \begin{enumerate}
  \item $x^2=9$
  \item $x^2=-9$
  \item $(x-5)^2=3$
  \item $(2x-1)^2 + x(1-2x)=4x^2-1$
  \item $(3x+5)^2=(x+1)^2$
  \item $(5x-4)^2-(3x+7)^2=0$
 % \item $2016x^2+x-2017=0$
  \end{enumerate}
\end{multicols}

\end{exercice}



\begin{exercice}[Allure d'une parabole]
  
Soit $f(x)=ax^2+bx+c$.
Dessiner une courbe possible de $ ({\scr C}_f) $ sachant que $a<0$ et $\Delta<0$.

\end{exercice}









% \begin{exercice}
  
% Donner le tableau de signes de la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=-5(2x-3)(-x+4)$.

% \end{exercice}












\begin{exercice}[Propriétés des paraboles]
  
Soit $f$ une fonction du second degré.\\
Sa courbe $ ({\scr C}_f) $  a pour sommet $S(4~;~9)$ et elle passe par le point $A \left( \dfrac{5}{2}~;~0 \right)$.\\

\begin{enumerate}
\item Quel est l'axe de symétrie de cette parabole ?
\item En déduire le deuxième point d'intersection de $ ({\scr C}_f) $ avec l'axe des abscisses.
\item Déterminer la forme canonique de $f(x)$.
\item Déterminer sa forme développée.
\item Déterminer sa forme factorisée.
\item Vérifier que la réponse de la question 2) est juste.
\end{enumerate}

\end{exercice}
















\begin{exercice}[Inéquations du 2nd degré]
 
 Résoudre les inéquations $3x^2-4x-15 \geqslant 0$ et $-2x^2-12x-10 \leqslant 0$.
  
\end{exercice}




\begin{exercice}[Inéquation cachée]
  Quel est l'ensemble de définition de $x\mapsto \frac{1}{\sqrt{x^2-4x+7}}$?
\end{exercice}






\begin{exercice}[2nd degré: choisir la forme adapatée au problème]
  
Soit $f$ une fonction du second degré de courbe représentative $ ({\scr C}_f) $. \\

\begin{enumerate}
\item Pour retrouver les points d'intersection éventuels de $ ({\scr C}_f) $ avec l'axe des abscisses, est-il préférable d'avoir sa forme canonique, sa forme développée ou sa forme factorisée ?
\item Pour retrouver le sommet $S$ de $ ({\scr C}_f) $, est-il préférable d'avoir sa forme canonique, sa forme développée ou sa forme factorisée ?
\end{enumerate}


\end{exercice}





\begin{exercice}[Parabole à partir de la forme factorisée]
  
On donne $f(x)=-3(x+4)(x-2)$.

\begin{enumerate}
\item Comment s'appelle la courbe représentative $ ({\scr C}_f) $  de cette fonction ? Justifier.
\item Que peut dire de la courbe $ ({\scr C}_f) $ à partir de la forme donnée ?
\item Déterminer sa forme canonique.
\item En déduire son tableau de variations.
\end{enumerate}
\end{exercice}



\begin{exercice}[Relations entre coefficients et racines]
\begin{enumerate}
\item   On note $P(x)=ax^2+bx+c$ avec $a$ non nul.  Démontrez que si $a$ et $c$ sont de
  signes opposés alors P admet au moins une racine réelle.
\item  Soit $P:x\mapsto  ax^2+bx+c$ ($a\neq  0$) un  polynome du  2nd degré
  admettant deux racines rélles $α$ et  $β$. Quelle est la forme factorisée
  de $P$? En la développant trouver des relations entre les coefficients et
  les racines.
\end{enumerate}
\end{exercice}


\begin{exercice}[Problèmes se ramenant à du 2nd degré]
\begin{enumerate}
\item   Peut-on trouver 3 carrés dont les  côtés sont des entiers consécutifs et dont
  la somme des aires vaut 15125 ?
  \item   Déterminez  les  triangles   rectangles  dont  les  côtés   sont  des  entiers
  consécutifs. 
\item Deux petits  poneys parcourent  195km. La  vitesse moyenne  de l'un  est 4km/h
  supérieure à  celle de  l'autre et il  est arrivé une  heure avant  son petit
  copain. Quelles sont les vitesses des petits poneys?
  \item Trouver deux nombres dont la somme vaut 57 et le produit 540.
\end{enumerate}
\end{exercice}























% \begin{exercice}
%   Résoudre dans $ℝ$

%   \begin{multicols}{3}
%   \begin{enumerate}
%   \item $6x^4-5x^2+1=0$
%   \item $(x^2+2x+1)^2<16$
%   \item $-2x^2+  7x-5 \leqslant 0$
%   \item $\frac{3x^2+x+1}{x^2-3x-10}>0$
%   \item $4x^2(\sqrt{6}+4\sqrt{3})x+\sqrt{18}$
%   \end{enumerate}
% \end{multicols}
% \end{exercice}




\begin{exercice}[Fractions rationnelles]
  Résoudre dans $ℝ$:
\begin{multicols}{2}
  \begin{enumerate}
  \item $\frac{2x-5}{x-1}=\frac{x-1}{x+1}$
    \item $\frac{x^2-x+1}{x+2}=2x+3$
    \end{enumerate}
  \end{multicols}
\end{exercice}



% \begin{exercice}
%   Résolvez dans $ℝ$ les équations:
%   \begin{multicols}{2}
%   \begin{enumerate}
%   \item $\sqrt{3x^2-4x+1}=4$
%   \item $x+1 = \sqrt{2x-1}$
%   \end{enumerate}
% \end{multicols}
% \end{exercice}







\begin{exercice}[Problème comptable]
  Une entreprise  syldave produit  des matraques à  condensation nucléaire
hyperstatique pour le ministère de la culture de Syldavie. 

On note la fonction coût total $C$ et la fonction
recette totale $R$.

On sait que  $C(q)=0,2q^2+q+90$ et $R(q)=12q$ avec $q$  la quantité de
matraques fabriquées par jour exprimée  en milliers, les recettes et les
coûts étant exprimés en Mzlrt\v{c}z\ss t\dj.
 
\begin{enumerate}
\item Exprimez $B(q)$, le bénéfice correspondant en Mzlrt\v{c}z\ss t\dj.
\item Déterminez par le calcul le nombre de matraques qu'il faut fabriquer pour que
  l'entreprise soit rentable.
\item Étudiez le sens de variation de B et dressez son tableau de
  variation. 
\item Déterminez quelle production permet d'obtenir un bénéfice maximum.
\end{enumerate}


\end{exercice}











% \begin{exercice}
  
% Début 2014, Klow, la capitale syldave, comptait  12000 habitants. En Janvier 2015, on s'aperçoit que
% Klow  a perdu  un certain  pourcentage (noté  $t$)  de sa  population qui  a fuit  vers la  Bordurie
% voisine. Mais l'année suivante, les émigrants se sont  aperçu que la vie étaient pire en Bordurie et
% reviennent en fraude avec quelques Bordures et Klow voit ainsi sa population croître de nouveau~:~en Janvier 2016, elle a gagné $(2t)\%$ en population par rapport à Janvier 2015. Elle compte ainsi, en Janvier 2016, 12285 habitants
% \begin{enumerate}
% 	\item Déterminer (en fonction de $t$) le coefficient multiplicateur correspondant à une diminution de population de $t\%$ suivie d'une augmentation de $(2t)\%$.
% 	\item Expliquer avec soin pourquoi $t$ est solution de l'équation $-2.4t^2+120t-285=0$ (\textit{si vous trouvez une autre équation dont $t$ est solution, vous pouvez bien entendu la donner ici, en justifiant avec soin})
% 	\item Calculer le pourcentage $t$ sachant que la population de la ville n'est jamais descendue en-dessous des 10000 personnes.
%         % \item Question subsidiaire pour  départager les ex-\ae quos~:~un syldave
%         %   mesure 1m88, fume 1 paquet de Zrälùkz par jour: calculer le pourcentage de
%         %   mots d'origine bordure dans la phrase «~Czesztot wzryzkar nietz on waghabontz! Czesztot bätczer yhzer kzömmetz noh dascz gendarmaskaïa ~»
% \end{enumerate}


% \end{exercice}




% \begin{exercice}




% %\begin{figure}
% \begin{center}
%   \includegraphics[width=.5\linewidth]{tank}
% \end{center}
% %\end{figure}



  
%   A Syldavian  fleet hire  company leases  tanks to private  armies on  a yearly
%   basis, with  a percentage discount in  the leasing charge proportional  to the
%   number of tanks leased.

%   \begin{enumerate}
%   \item How  many tanks must  the fleet hire company  lease to a  single private
%     army to maximise income?
%   \item How would  your decision be affected  if the fleet hire  company wish to
%     maximise profit?
%   \end{enumerate}

%   We will need to make certain assumption:
%   \begin{itemize}
%   \item the income I and the profit P depend only on the fleet size S;
%   \item the nominal lease charge is £200000 per tank and per year;
%   \item the fleet hire company has a special offer: the lease charge per tank is
%     discounted by 1 per cent for each tank in the fleet, e.g. a fleet size of 20
%     tanks earns a 20 per cent discount;
%   \item each tank depreciates by £100000 each year.
%   \end{itemize}
% \end{exercice}







% \begin{exercice}
%   A  quadratic  function   is  defined  by  $f(x)=x^2+kx+9$,  where   $k$  is  a
%   constant.  It is  given  that  the equation  $f(x)=0$  has  two distinct  real
%   roots. Find the set of values that $k$ can take.


%   For the case where $k=-4\sqrt{3}$,
%   \begin{enumerate}
%   \item express  $f(x)$ in the form  $a(x+a)^2+b$ stating the values  of $a$ and
%     $b$, and hence wrire down the leats value taken by $f(x)$.

%     \item Solve  the equation $f(x)=0$  expressing your  answer in the  terms of
%       surds, simplified as far as possible.
%   \end{enumerate}
% \end{exercice}







\begin{exercice}[Vitesse optimale sur une autoroute]


On  considère un  modèle  très  simplifié d'autoroute.  Les  voitures roulent  à
vitesse  constante sur  une route  droite et  infinie. On  appelle \textit{débit
  stabilisé} le rapport entre la vitesse des voitures (constante) et la distance
entre les deux pare-chocs avant de ces voitures (constante):

$$D=\frac{v}{\ell}$$

Vérifiez que cela
nous donne le nombre de voitures par unité de temps.

On a l'impression que plus on va vite, plus le débit augmente mais il faut tenir
compte de la distance  d'arrêt: plus on va vite, plus  cette distance est longue
ce qui est dangereux.

Or la distance de freinage est proportionnelle à l'énergie de la voiture induite
par  sa   vitesse  et   sa  masse   (c'est  son   énergie  cinétique   qui  vaut
$\frac{1}{2}mv^2$ ) et il  faut lui ajouter la distance induite  par le temps de
réaction du conducteur qu'on supposera constant.

Il faut donc se placer à une distance de:

$$\ell=Kv^2+t_0v+L$$


avec K  un certain coefficient  de proportionnalité  constant $t_0$ le  temps de
réaction constant et L la longueur des véhicules qu'on supposera constante.



Exprimez alors le débit en fonction de $v$ et des paramètres.



\textbf{Comment déterminer \textit{v} pour avoir un débit maximum?}


Pour l'application  numérique, on prendra  une longueur  de voiture de  4m. Pour
calculer le coefficient K, on se reportera au tableau reproduit dans les manuels
de code de la route:

\begin{center}
  
\begin{tabular}{|l||ccccccc|}
  \hline
  Vitesse (km/h)& 20 &30 &50  &70 &90 &110  &130 \\
\hline
  Distance de freinage (m)& 2  &5  & 14  & 28  & 46  & 68  & 95 \\
  \hline
\end{tabular}

\end{center}
\end{exercice}



% \begin{exercice}[London's burning]
  
% \begin{multicols}{2}
  
% %\begin{figure}
% \begin{center}
%   \includegraphics[scale=2]{WalkieTalkie}
% \end{center}
% %\end{figure}

% The City of London building, officially  named 20 Fenchurch Street but popularly
% known as  the 'Walkie  Talkie' due to  its design, has  been dubbed  the 'Walkie
% Scorchie' after  the office tower made  headlines for reflecting light  from its
% glass front, burning parked cars underneath.

% During the summer heatwave in 2012, the ongoing construction project also
% sparked fires in the streets below and caused paint and tiles to fall off nearby
% buildings,  while  enterprising Londoners  found  they  could  fry eggs  on  the
% scolding pavements around the tower. 

% \textbf{In order to  understand this amazing phenomenon,  whether check, explain
%   or use one or several of the following results:}
% %\begin{figure}
% \begin{center}
%   \includegraphics[scale=0.89]{mp-tex-PraTW-1}
% \end{center}
% %\end{figure}

  
% \end{multicols}



% \begin{enumerate}
% \item  Let M  be a  point of  the  parabola whose  equation is  $y=x^2$. If  its
%   abscissa is $a$, the slope-intercept equation of  the tangent line
%   $(T_a)$ to $y=x^2$ at M is $y=2ax - a^2$.
% \item Let  $(N_a)$ be the  straight line whose slope  (or gradient) is  equal to
%   $-\frac{1}{2a}$ and which contains point $M$.

%   We can verify that
%   $(N_a)\bot(T_a)$ and that the slope-intercept equation of $(N_a)$ is $y=-\frac{1}{2a}x+a^2+\frac{1}{2}$.
% \item Let $b$ be a positive real, $F$ the point of coordinates $(0,b)$, $F'$ the
%   point of coordinates $(a,-b)$ as
%   shown above.  Let us assume that
%   $M$ is the middle point of $[F'F'']$.

%   The slope-intercept equation of
%   the straight line $(FF'')$ is $y=2ax+b$.
% \item Let $I$ be the intersection point of $(FF'')$ and $(N_a)$.
%  Let us assume that  $\widehat{F''MI}=\widehat{IMF}$. The
%   line $(MI)$ is therefore an angle bisector of the triangle $MFF''$. Thus the Point $I$ is the
%   middle point of the line $[FF'']$. \textbf{We can deduce that}
%   $\boldsymbol{b}$ \textbf{is equal to} $\boldsymbol{\frac{1}{4}}$.
% \item Think of $(F''M)$ as a sunbeam reflecting on a mirror in $M$. %$

% The \textit{law of reflection} states that for each incident ray the angle of incidence equals the angle of reflection. 
  
% \textbf{How to
%   explain the phenomenon observed in front of the 20 Frenchurch Street building %$
%   according to  the result found above about $F$?}
%  \item The point  $F$ is called the  \textit{focus} of the red  parabola. Do you
%    have any idea of its etimology?
% \end{enumerate}


  
% \end{exercice}


% \newpage


% \begin{exercice}[Projectile]



% %\begin{figure}
% \begin{center}
%   \includegraphics[height=4cm]{projectile}
% \end{center}
% %\end{figure}


% Un projectile lancé depuis le sol est  supposé avoir sa trajectoire dans un plan
% vertical. On ne tiendra pas compte  de la résistance de l'air. Cette trajectoire
% dépend alors de deux paramètres:
% \begin{itemize}
%   \item sa vitesse initiale $v_0$
%   \item l'angle de lancement par rapport à l'horizontale $\alpha$
%   \end{itemize}

  
% Le temps est exprimé en secondes, le  lancement a lieu à l'instant 0. La hauteur
% à l'instant $t$ est donnée par:


% $$h(t) = -\frac{1}{2}gt^2 + v_0t\sin \alpha$$



  
%   \begin{enumerate}
%   \item On suppose que $v_0=100 m.s^{-1}$. Calculez dans chacun des cas suivants
%     le  temps  que mettra  le  projectile  pour  toucher  le sol  lorsque  $\alpha\in
%     \bigl\{\pi/6, \pi/4, \pi/3\bigr\}$.

%     Pour quelle valeur de $t$ la hauteur est-elle maximale?


%     \item On  veut atteindre  un point  situé à  1 km.  Quel angle  choisir avec
%       toujours le même $v_0$?

%       \item Cas général?
%   \end{enumerate}

% \end{exercice}



\subsection{Polynômes de degré quelconque}

\begin{exercice}[Exploiter la connaissance de certaines racines]
  Vérifiez que $1+\sqrt{2}$ et $1-\sqrt{2}$ sont des racines de P avec:
$$P(x)=x^4-3x^3+3x^2-3x-2$$

En déduire une factorisation de P.
\end{exercice}




\begin{exercice}[Savoir déterminer le degré d'un polynôme]
  Donner le degré et le coefficient dominant de $P: x\mapsto (x+1)^{n} - (x-1)^{n}$ (ou $n \in \N^{*}$).
\end{exercice}




\begin{exercice}[Savoir déterminer la multiplicité d'une racine]
  Soit $P$ un polynôme défini par $P(x)=x^5+3x^4+4x^3+4x^2+3x+1$.  Vérifiez
  que $-1$ est une racine de 
  P. Quelle est son ordre de multiplicité?
\end{exercice}




% \begin{exercice}
%   Factoriser les polynômes suivants:
% \begin{enumerate}
% \item $X^4-X^2+1$ dans $ℂ[X]$ puis $\R[X]$.
% \item $X^4-2X^3+2X^2-2X+1$ dans $\R[X]$.
% \item $X^6-3X^5+2X^4-X^2+3X-2$ dans $\R[X]$ sachant que $\I$ est racine dans $ℂ$.
% \end{enumerate}
% \end{exercice}




% \begin{exercice}
%   Factoriser dans $ℂ[X]$ les polynômes suivants:

%   \begin{multicols}{3}
%     \begin{enumerate}
%     \item $(X+\I)^4-(X-\I)^4$.
%     \item $X^4-1$.
%     \item $X^3+1$.
%     \item $X^2-2\cos(α)X+1$.
%     \item $X^2-2\sin(α)X+1$.
%     \item $(X+2\I)^4-(X-2\I)^4$.
%     \end{enumerate}
    
%   \end{multicols}
% \end{exercice}



\begin{exercice}[Savoir exploiter des équations polynomiales en pensant au degré]
  Déterminez  \textbf{tous}  les   polynômes  P  de  $ℝ[X]$   qui  vérifient  les
  conditions suivantes. Vous raisonnerez par \textbf{Analyse-Synthèse}.
  \begin{multicols}{3}
    \begin{enumerate}
    \item $P(x^2)=(x^2+1)×P(x)$
    
    \item $P(x)-P'(x)=x$
    \item $P(x)-P'(x)=x^2$
    % \item $P-P'=X^n$
    %   \item $XP'=P$
    % \item $(1-X)P'-P=X$
    % \item $P'$ divise $P$
    
    % \item $(X^2+1)P''-6P=Θ$  
    \end{enumerate}
    
  \end{multicols}
\end{exercice}


% \begin{exercice}[Oral Agro-Véto 2016 : polynômes de Tchebychev]
% On définit une suite de polynômes $(T_n)_{n\in\N}$ de $\R[X]$ par $T_0=1$, $T_1=X$ et:
% $$\forall n\in\N,\quad T_{n+2}=2XT_{n+1}-T_n$$
% \begin{enumerate}
% \item
%   \begin{enumerate}
%   \item Donner l'expression de $T_2$ et $T_3$.
%   \item  Pour tout  entier  naturel $n$,  donner  le degré  de  $T_n$ ainsi  que
%     l'expression du coefficient devant le terme de plus haut degré.
%   \item Pour tout entier naturel  $n$, donner l'expression de chaque coefficient
%     de $T_{n+2}$ en fonction des coefficients de $T_{n+1}$ et de $T_n$.
%   \end{enumerate}
% \item
%   \begin{enumerate}
%   \item\label{deuza}  On décide  de  coder un  polynôme  sous forme  d'une  liste: celle  des
%     coefficients   de   ce   polynôme    classés   dans   l'ordre   des   degrés
%     croissants. Écrire une fonction qui retourne le degré d'un polynôme.
%   \item            Écrire            une           fonction            d'en-tête
%     \mintinline[fontsize=\small]{python}
%     {def etape(L: list, M: list) -> list} qui donne en sortie la liste associée
%     au polynôme
%     \mintinline[fontsize=\small]{python}{T = 2X*M - L} où
%     \texttt{L} et \texttt{M} sont des listes définies comme en \vref{deuza}.
%   \item Écrire une fonction d'en-tête
%     \mintinline[fontsize=\small]{python}{def tchebychev(n: int) -> list} 
%     qui, à partir d'un entier $n$, donne en sortie la liste associée au polynôme
%     $T_n$.
%   \item Écrire une fonction d'en-tête
%     \mintinline[fontsize=\small]{python}{def  evalue(P: list,  x: float)  -> float}  qui, étant  donné un  polynôme
%     \texttt{P}  (défini sous  forme  d'une  liste) et  un  réel  $x$, évalue  ce
%     polynôme  en $x$,  c'est-à-dire donne  une valeur  pour $P(x)$.  (\textit{On
%       essaiera  de  concevoir  un  algorithme utilisant  le  moins  d'opérations
%       algébriques possible}).
%   \item Écrire une fonction d'en-tête
%     \mintinline[fontsize=\small]{python}{def TraceTchebychev(n: int) -> None} qui,  à partir d'un entier $n$, trace
%     la   représentation   graphique   du  polynôme   $T_n$   sur   l'intervalle
%     $[-1,1]$. Quelles observations peut-on faire?
%   \end{enumerate}
%   \item
%     \begin{enumerate}
%     \item Pour tout couple de réels $(a,b)∈ℝ^n$, démontrer l'égalité:
%       $$
%       \cos(a)\cos(b)=\frac{1}{2}\bigl(\cos(a+b)+\cos(a-b)\bigr)
%       $$
%     \item Pour tout réel $a$ et tout entier naturel $n$, démontrer que:
%       $$
%       T_n(\cos(a))=\cos(na)
%       $$
%     \item Pour  tout entier naturel $n$,  démontrer que le polynôme  $T_n$ admet
%       $n$ racines distinctes, toutes appartenant à $[-1,1]$.
%     \end{enumerate}


% \end{enumerate}
  
% \end{exercice}

% \begin{exercice}[Savoir montrer qu'un polynôme est nul]
%   Pour $n \in \N$ montrer qu'il existe au plus un polynôme $T_{n} \in \R[X]$ tel que $\forall x \in \R$, $T_{n}(\cos{x}) = \cos(nx)$.
% \end{exercice}


% \begin{exercice}[Oral Agro-Véto 2015 (extrait)]
%   Pour tout entier $p∈\llbracket 0,n \rrbracket$, on considère les polynômes:
%   $$
%   T_p=X^p(1-X)^p\qquad\text{ et } \qquad L_p=\frac{1}{p!}(T_p)^{(p)}
%   $$
%   où $(T_p)^{(p)}$ désigne la dérivée d'ordre $p$ de $T_p$.

%   Fixons un entier naturel $p$.

%   \begin{enumerate}
%   \item Déterminer le degré et le coefficient dominant de $L_p$.
%   \item Notons $L_p$ sous la forme $L_p=\displaystyle\sum_{k=0}^pa_{k,p}X^k$. Établir que:
%     $$
%     ∀k∈\llbracket 0,p \rrbracket, a_{k,p}=(-1)^k\binom{p}{k}\binom{p+k}{k}
%     $$
%   \item\label{relrec} Déterminer une relation entre $a_{k+1,p}$ et $a_{k,p}$ pour $k∈\llbracket 0,p-1 \rrbracket $.
%     Préciser la valeur de $a_{0,p}$.
%   \item  En vous  appuyant sur  la question  \vref{relrec}, écrire  une fonction
%     Python  qui prend  comme  argument  un entier  naturel  $p$  et renvoie  les
%     coefficients $a_{0,p}, a_{1,p},...,a_{p,p}$ de  $L_p$. Tester cette fonction
%     dans le cas où $p∈\bigl\{0,1,2\bigr\}$.
    
%   \end{enumerate}
% \end{exercice}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Poly_poly_24"
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
