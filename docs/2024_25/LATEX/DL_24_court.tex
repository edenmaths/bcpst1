%% Last modified: <DL_24_court.tex modifié par  Guillaume CONNAN le mercredi 26 février 2025 à 10h 09min 57s>


\modenormal 

\chapterimage{young}
\chaptertext{En 1910, l'anglais William Henry \textsc{Young} publie \textit{The
    Fundamental Theorems of the Differential  Calculus} dans lequel il introduit
  \textit{The Expansion Theorem} que 
	nous appelons en  France \textit{Formule de Taylor-Young}.  Celle-ci sera d'une
        portée \textbf{locale} et 
	va  nous  permettre d'avoir  une  approximation  polynomiale \textbf{locale}  d'une
	fonction. Ce théorème nous aidera calculer des limites qui jusqu'alors
	nous résistaient. Le chapitre va comporter énormément de résultats et peu de
	démonstrations...
	
	Nous allons  dans un premier  temps étendre   aux fonctions
        négligeables ce  que nous avons  vu sur les fonctions  équivalentes mais
        attention! Il y a de nombreuses  différences! Les unes privilégient 1 et
        les produits, les autres 0 et les sommes: vous devinez lesquelles?
      }
      

\chapter{Développements limités et applications}

\section{ Fonction négligeable devant une autre au voisinage d'un point}


On notera $\overline{ℝ}=ℝ\cup\{-\infty, +\infty\}$ et $\scr V_{x_0}$ un voisinage de $x_0$. 


\begin{definition}[Fonction négligeable]
  
    Soit $x_0 \in \overline{ℝ}$, $f$ et $g$ deux fonctions à valeurs réelles,
    définies sur un voisinage $\scr V_{x_0}$ de $x_0$. 
	On dit que $f$ est \textbf{négligeable devant} $g$ au voisinage de $x_0$ lorsqu'il existe une
    fonction $ε$ définie sur $\scr V_{x_0}$ telle que 
	
	$$
	\forall x \in {\scr V}_{x_0},\quad f(x) = g(x)×ε(x)\quad {\rm avec } \displaystyle\lim _{x\to x_0}ε(x) = 0
	$$
		
	On note alors $ f(x) \underset{x \to x_0}{=} {
          \text{\large\textcursive{o}}}(g(x))  $ qui  se lit:  \og $f(x)$  est un
        \textit{petit o} de $g(x)$
        au voisinage de $x_0$\fg{}

\end{definition}



Par  exemple, soit  $f$  et  $g$ les  fonctions  définies  sur $\mathbb{R}$  par
$f(x)=x^2$ et $g(x)=x^5$.

\begin{itemize}
\item  $g(x)=x^3×f(x)$ et $ \displaystyle\lim_{x\to0}x^3=0$ donc $g(x)\underset{x \to
0}{=} \text{\large\textcursive{o}}\left(f(x)\right)$ soit encore  $x^5\underset{x \to
0}{=} \text{\large\textcursive{o}}\left(x^2\right)$

\item  $f(x)=\dfrac{1}{x^3} ×g(x)$ et $ \displaystyle\lim_{x\to+\infty}\dfrac{1}{x^3} =0$ donc $f(x)\underset{x \to
+\infty}{=} \text{\large\textcursive{o}}\left(g(x)\right)$ soit encore  $x^2\underset{x \to
+\infty}{=} \text{\large\textcursive{o}}\left(x^5\right)$

\end{itemize}


Le  théorème suivant  nous  donnera souvent  un moyen  pratique  de définir  une
relation de négligeabilité:


\begin{theoreme}[critère de négligeabilité]
   Soit $x_0 \in \overline{\mathbb{R}}$, $f$ et $g$ deux fonctions à valeurs
	    réelles, définies sur un voisinage $\cal V_{x_0}$ de $x_0$. 
		
		Si $g$ ne s'annule pas sur $\cal V_{x_0}$, alors 
		
		$$
		f(x)\underset{x \to x_0}{=} \text{\large\textcursive{o}}(g(x))   \Longleftrightarrow   \displaystyle\lim_{x\to   x_0}
		\dfrac{f(x)}{g(x)} = 0
		$$
\end{theoreme}

On se souviendra en particulier que $ f(x)\underset{x \to x_0}{=}  \text{\large\textcursive{o}}\left(1\right)⟺ \displaystyle\lim_{x\to x_0}f(x)=0$.


\begin{recherche}
  
	Comparer les  fonctions suivantes au voisinage de 0 puis  de $+\infty$
	pour la relation de négligeabilité:

	$$
	f:x\mapsto \dfrac{x}{|\ln(x)|} \quad g:x\mapsto \sqrt{x} \quad h:x\mapsto x^3-x^2
	$$


\end{recherche}



\begin{theoreme}[règles de calcul]
  
	Dans  tout  ce  qui  suit, $x_0∈  \overline{\mathbb{R}}$  et  les  fonctions
	introduites sont définie sur un voisinage $\cal V_{x_0}$ de $x_0$.
	\begin{description}
	\item[Transitivité]:   $   f(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right) ∧ g(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(h(x)\right) ⟹ f(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(h(x)\right)$
	\item[Produit] $  f_1(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g_1(x)\right) ∧ f_2(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g_2(x)\right) ⟹ f_1(x)×f_2(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g_1(x)×g_2(x)\right)$
	\item[Somme]: $ f_1(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right) ∧ f_2(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right) ⟹ f_1(x)+f_2(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right)$
	\item[Produit   par   un    scalaire]   $f(x)\underset{x   \to   x_0}{=}
          \text{\large\textcursive{o}}\left(g(x)\right) ⟹ (∀λ∈ℝ)(λ\cdot f(x)\underset{x \to x_0}{=} \text{\large\textcursive{o}}\left(g(x)\right))$
	\item[Produit par une fonction] $ f(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right)  ⟹ f(x)×h(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)×h(x)\right)$
	\item[Produit par une fonction bornée] Si $h$ est bornée sur $\cal V_{x_0}$
	    alors $\underset{x \to x_0}{=} {\Large \scr
	   o}\left(g(x)\right)  ⟹ f(x)×h(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right)$
	\item[Valeur absolue]  $f(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(g(x)\right)  ⟹ |f|(x)\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left(|g|(x)\right)$
	\item[Puissance positive] $f(x)\underset{x \to x_0}{=} {\Large \scr
	   o}\left(g(x)\right)  ⟹ (∀α>0)((f(x))^α\underset{x  \to   x_0}{=}   \text{\large\textcursive{o}}\left((g(x))^α\right))$ sous couvert de définition des puissances.
       \end{description}
       
\end{theoreme}



Il  n'y  a  plus qu'à  trouver  des  exemples  et  à démontrer  chacune  de  ces
propositions.



\begin{theoreme}[croissances comparées]
  
    $$
	\begin{array}{rlll}
	x^\alpha       & \underset{x \to +\infty}{=}& \text{\large\textcursive{o}}(x^\beta)           & \text{ lorsque } \alpha<\beta , \\
	x^\beta        & \underset{x \to 0}{=}      & \text{\large\textcursive{o}}(x^\alpha)          & \text{ lorsque } \alpha<\beta , \\
	(\ln x) ^\beta &  \underset{x \to +\infty}{=}& \text{\large\textcursive{o}}(x^\alpha)          & \text{ lorsque } \alpha>0 \text{ et } \beta>0 , \\
	(\ln|x|)^\beta& \underset{x \to 0}{=}   & \text{\large\textcursive{o}}(\frac{1}{x^\alpha}) & \text{ lorsque } \alpha>0 \text{ et } \beta>0 , \\
	x^\alpha       &  \underset{x \to +\infty}{=}& \text{\large\textcursive{o}}(a^x)                & \text{ lorsque } \alpha>0 \text{ et } a > 1 ,   \\
	a^x            &  \underset{x \to +\infty}{=}& \text{\large\textcursive{o}}(b^x)                & \text{ lorsque } |a| < |b| .
	\end{array}
	$$
\end{theoreme}


Par exemple,  $ \displaystyle\lim  _{\underset{x>0}{x \to  0}} x^\alpha  (\ln x)
^\beta = 0$ car $\displaystyle\lim_{\underset{x>0}{x \to 0}} x^\alpha (\ln x) ^\beta \overset{y=\frac{1}{x}}{=} \displaystyle\lim_{y \to +\infty} \frac{ (-\ln(y))^\beta }{y^\alpha} = 0$


\begin{theoreme}[Relation entre équivalence et négligeabilité]
  
    Soit $x_0 \in  \overline{ℝ}$, et $f$, $g$ deux fonctions  à valeurs réelles,
    définies sur un voisinage $\cal V_{x_0}$ de $x_0$. Alors:
	\begin{itemize}
	\item   $ f(x)  \underset{x \to  x_0}{\sim} g(x)  \Longleftrightarrow (f  - g)(x)
	  \underset{x \to x_0}{=} \text{\large\textcursive{o}}\left(g(x)\right) $. 
	\item  Si $ f(x)\underset{x \to x_0}{=}  \text{\large\textcursive{o}}\left(g(x)\right) $ et si
	  $  g(x) \underset{x  \to x_0}{\sim}  h(x)  $ alors  $ f(x)\underset{x  \to
	  x_0}{=} \text{\large\textcursive{o}}\left(h(x)\right) $.
      \end{itemize}
    \end{theoreme}


    
    \section{Développements limités}

\subsection{(Re-)découverte des développements limités aux ordres 0 et 1}


%\begin{figure}
\begin{center}
  \includegraphics[height=5cm]{~/PREPA/EDEN/Other/ecs1/docs/2020_21/COURS/IMG/jourdain.jpg}
\end{center}
%\end{figure}

\begin{quote}
  {\itshape
L'Agronome Mathématicien - Acte II Scène 4
  }
\end{quote}

\begin{description}
\item[MAÎTRE DE MATHÉMATIQUES] La voix, O, se forme en rouvrant les mâchoires, et rapprochant les lèvres par les deux coins, le haut et le bas, O.

\item[MONSIEUR JOURDAIN] O, O. Il n’y a rien de plus juste. A, E, I, O, I, O. Cela est admirable ! I, O, I, O.

\item[MAÎTRE DE MATHÉMATIQUES] L’ouverture de la bouche fait justement comme un petit rond qui représente un O.

\item[MONSIEUR JOURDAIN] O, O, O. Vous avez  raison, O. Ah la belle chose, que de
	savoir quelque chose !
	
\item[MAÎTRE DE MATHÉMATIQUES] Je vous expliquerai à fond toutes ces curiosités.
	
\item[MONSIEUR JOURDAIN]  Par ma foi ! il y a plus de trois ans que je calcule des
	développements limités sans que j’en susse rien, et je vous suis le plus obligé du monde de m’avoir appris cela.
\end{description}

\subsubsection{Ordre 0}



Considérons   une   fonction   continue   en  $x_0$.   Cela   signifie   que
$\displaystyle\lim_{x\to     x_0}f(x)    =     f(x_0)$     ou    encore     que
$\displaystyle\lim_{x\to x_0}f(x)-f(x_0) = 0$ soit, en utilisant le langage vu à
la section précédente: $ f(x)-f(x_0)\underset{x \to x_0}{=} \text{\large\textcursive{o}}\left(1\right)$ c'est-à dire
 
$$
f(x)\underset{x \to x_0}{=} f(x_0) + \text{\large\textcursive{o}}\left(1\right)
$$


\textbf{Nous venons d'écrire le développement limité à l'ordre 0 de} $\mathbf{f}$ \textbf{au voisinage de} $\mathbf{x_0}$


\subsubsection{Ordre 1}

À vous  de jouer? À  votre avis, quelle  forme devrait prendre  un développement
limité  à l'ordre  1 ?  Quelle  propriété qu'on  demanderait à  $f$ de  vérifier
pourrait nous y amener?



\subsection{Un peu d'ordre: notion de DL}


Voici  une  définition  qui  va  nous permettre  d'ordonner  nos  intuitions  et
d'avancer:


\begin{definition}[$\operatorname{DL}_n(x_0)$]
   Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$.Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$. On dit que $f$ \textbf{admet un développement
    limité} d'ordre $n$ en $x_0$ (${\rm DL}_n(x_0)$ pour les intimes) lorsqu'il existe $ (a_0,a_1,\cdots,a_n) \in
    ℝ^{n+1} $ tel que: 

	$$
	f(x) \underset{x \to x_0}{=} a_0 + a_1 (x-x_0) + \cdots + a_n (x-x_0)^n +  \text{\large\textcursive{o}}\left((x-x_0)^n\right)
	$$

	Le polynôme  $ a_0 + a_1  (x-x_0) + \cdots +  a_n (x-x_0)^n $ est  appelé la
	\textbf{partie     régulière}     du     développement     limité     et
        $\text{\large\textcursive{o}}\left((x-x_0)^n\right)$ son \textbf{reste}.

\end{definition}


Par  exemple,  on  sait  que  $  \sin(x) \underset{x  \to  0}{\sim}  x$  i.e.  $
\sin(x)\underset{x \to  0}{=}x +\text{\large\textcursive{o}}\left(x\right)$ qui  constitue un
${\rm DL}_1(0)$ de sin, la partie régulière étant $x$.



\begin{recherche}
  \begin{enumerate}
         \item  Déterminez le DL$_2(0)$ de $f:x\mapsto x^3\sin \dfrac{1}{x} $.
         \item   Déterminez le DL$_1(0)$ de $g:x\mapsto (1+x)^{42}$
         \item   Déterminez le DL$_n(0)$ de $h: x\mapsto \dfrac{1}{1-x} $.
         \end{enumerate}

         \begin{Aide}
           \begin{enumerate}
           \item  $f(x) = x^2ε(x)$
		\item  Binôme de Newton
		 \item  Suite géométrique
           \end{enumerate}
         \end{Aide}

       \end{recherche}

       \begin{theoreme}[Unicité du DL]
         
         Soient $n\inℕ$, $x_0\inℝ$ et $f$ une fonction à valeurs réelles définie au voisinage de $x_0$. Si $f$ admet un développement limité à l'ordre $n$ en $x_0$, alors ce développement est unique.

       \end{theoreme}

       \bclampe On suppose comme d'habitude qu'il y en a deux....

    \begin{theoreme}[Détermination d'un équivalent à partir d'un DL]
      
Soit $f$ une fonction définie au voisinage de $0$ et admettant un DL$_n(0)$.

\centerline{Si $f(x)=a_px^p+a_{p+1}x^{p+1}+\ldots+a_nx^n+\text{\large\textcursive{o}}\left(x^n\right)$ et $a_p\neq 0$ alors $f(x)\underset{x\to 0}{\sim}a_px^p$.}
    \end{theoreme}



 \subsection{Développement asymptotique}

Il existe le pendant des DL en l'infini:


\begin{definition}[$\operatorname{DA}_n(+\infty)$]
     Soient  $n\inℕ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $+\infty$. On dit que $f$ \textbf{admet un développement
    asymptotique} d'ordre $n$ en $+\infty$ (${\rm DA}_n(+\infty)$ pour les intimes) lorsqu'il existe $ (a_0,a_1,\cdots,a_n) \in
    ℝ^{n+1} $ tel que: 

	$$
	f(x) \underset{x \to +\infty}{=} a_0 +  \dfrac{a_1}{x}  + \cdots + \dfrac{a_n}{x^n}  + \text{\large\textcursive{o}}\left(\dfrac{1}{x^n} \right)
	$$

\end{definition}

En fait,  on se  ramène le  plus souvent  à des DL  au voisinage  de 0  grâce au
théorème suivant:


\begin{theoreme}[Équivalence de l'étude en 0]
  
	Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$. 
	
	Soit  $g$ la  fonction par $g:  h\mapsto f(h  +
	x_0)$. Alors  $g(x-x_0) = f(x_0  + x-x_0) = f(x)$.  Donc $g$ est  définie au
	voisinage de 0 et
	
	$
	f(x)\underset{x \to x_0}{=}a_0+a_1(x-x_0)+\cdots+a_n(x-x_0)^n+
        \text{\large\textcursive{o}}\left((x-x_0)^n\right) ⟺\\ g(h)\underset{h \to 0}{=}a_0+a_1h+\cdots+a_nh^n+ \text{\large\textcursive{o}}\left(h^n\right)
	$
	
      \end{theoreme}


	
En $+\infty$ on posera $g(h)=f\left(\dfrac{1}{h} \right)$.



\subsection{Formule de Taylor-Young}


La formule suivante va nous donner une \textbf{condition suffisante} d'existence d'un DL:

% %\begin{figure}
% \begin{center}
%   \includegraphics[width=0.8\linewidth]{/home/moi/PREPA/EDEN/Other/ecs1/docs/2020_21/COURS/IMG/FTY.png}
% \end{center}
% % \end{figure}

%  ou dans la langue de Molière:

 \begin{theoreme}[Formule de Taylor-Young (admis)]
   	Soit $ n \in ℕ $.  \textbf{SI} $f$ est une fonction de classe $\scr C^n$ sur un
		intervalle $I$ et $x_0\in I$,  \textbf{ALORS} $f$ admet pour développement limité
		d'ordre $n$ au voisinage de $x_0$:
		
		$$
		f( x_0 + h ) \underset{h \to 0}{=} \displaystyle\sum_{k=0}^n \dfrac{ h^k }{ k! } f^{(k)}(x_0) + \text{\large\textcursive{o}}\left(h^n\right)
		$$
         
 \end{theoreme}


      \begin{DANGER}
        Vous  avez bien  noté que  ce théorème  est une  implication et  pas une
        équivalence. Il existe  des fonctions qui admettent un DL  à l'ordre $n$
        sans être de classe $\scr C^n$.

        Pour les fonctions pathologiques, les DL ne servent pas à prouver qu'elles
        sont $\scr C^n$ comme le montre l'exemple qui suit.
      \end{DANGER}


      \begin{recherche}
        Soit $f:x\mapsto x^3\sin(1/x)$.
        \begin{enumerate}
        \item      Démontrer      que       $f(x)\underset{x      \to      0}{=}
          \text{\large\textcursive{o}}\left(x^2\right)$. Que  peut-on en déduire
          point de vue DL?
        \item Peut-on prolonger par continuité cette fonction en 0?
          \item Si oui, est-elle dérivable en 0? De dérivée continue en 0?
        \end{enumerate}
      \end{recherche}
 


      
Cette formule nous donnera la clé des DL de référence que vous allez avoir le
plaisir de démontrer:

\subsection{DL$_n$(0) de référence}

%\begin{theoreme}[DL$_n(0)$ de référence]
  \begin{itemize}
    \item   $\exp(x)  \underset{x \to 0}{=}   1  +  x  +  \dfrac{x^2}2  +   \dfrac{x^3}{3!}  +  \cdots  +
      \dfrac{x^n}{n!} + \text{\large\textcursive{o}}\left(x^{n}\right)
	  \underset{x   \to  0}{=}   \displaystyle\sum   _{k=0}^n  \dfrac{x^k}{k!}   +  {\Large   \scr
	  o}\left(x^{n}\right) $
	\item  $  \ln(1+x) \underset{x \to 0}{=} x  - \dfrac{x^2}{2} +  \dfrac{x^3}{3} + \cdots  + (-1)^{n-1}
	  \dfrac{x^n}{n}  + \text{\large\textcursive{o}}\left(x^{n}\right)
	  \underset{x \to 0}{=}  \displaystyle\sum _{k=1}^n  (-1)^{k-1}  \dfrac {x^k}{k}  +
	  \text{\large\textcursive{o}}\left(x^{n}\right)$
	\item  $\dfrac1{1-x} \underset{x \to 0}{=} 1 + x + x^2 +  \cdots + x^n + \text{\large\textcursive{o}}\left(x^{n}\right)
		\underset{x \to 0}{=} \displaystyle\sum _{k=0}^n x^k +
	  \text{\large\textcursive{o}}\left(x^{n}\right)$
	\item  $\dfrac1{1+x} \underset{x \to 0}{=} 1 - x + x^2 +  \cdots + (-1)^n x^n + \text{\large\textcursive{o}}\left(x^{n}\right)
		\underset{x \to 0}{=} \displaystyle\sum _{k=0}^n
		(-1)^k x^k + \text{\large\textcursive{o}}\left(x^{n}\right)$
	\item    $(1+x)^{\alpha}  \underset{x \to 0}{=}   1   +  \alpha   \dfrac{x}{1!}  +   \alpha(\alpha-1)
	  \dfrac{x^2}{2!}    +     \cdots    +    \alpha(\alpha-1)\cdots(\alpha-n+1)
	  \dfrac{x^n}{n!} + \text{\large\textcursive{o}}\left(x^{n}\right)$
	\item   $ \sin(x)  \underset{x \to 0}{=} x  -  \dfrac{x^3}{3!} +  \dfrac{x^5}{5!} +  \cdots +  (-1)^n
	  \dfrac{  x^{2n+1}  }{ (2n+1)!  }  +  \text{\large\textcursive{o}}\left(x^{2n+1}\right)  \underset{x \to 0}{=} \displaystyle\sum  _{k=0}^n  (-1)^k
	  \dfrac{ x^{2k+1} }{ (2k+1)! } + \text{\large\textcursive{o}}\left(x^{2n+1}\right)$
	\item   $ \cos(x)  \underset{x \to 0}{=} 1  -  \dfrac{x^2}{2!} +  \dfrac{x^4}{4!} +  \cdots +  (-1)^n
	  \dfrac{  x^{2n}   }{  (2n)!   }  + \text{\large\textcursive{o}}\left(x^{2n}\right)   \underset{x \to 0}{=}  \displaystyle\sum   _{k=0}^n  (-1)^k
	  \dfrac{ x^{2k} }{ (2k)! } + \text{\large\textcursive{o}}\left(x^{2n}\right)$

        \end{itemize}
%      \end{theoreme}




      
\subsection{Règles de calcul}

\subsubsection{$\text{\large\textcursive{o}}\left(x^n\right)$}

\begin{theoreme}[Sur la manipulation des o]
  \begin{multicols}{2}
  \begin{enumerate}
  \item $ \text{\large\textcursive{o}}\left(x^n\right) + \text{\large\textcursive{o}}\left(x^n\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^n\right)$
	\item $ \text{\large\textcursive{o}}\left(x^n\right) - \text{\large\textcursive{o}}\left(x^n\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^n\right)$
	\item $ \text{\large\textcursive{o}}\left(x^n\right) + \text{\large\textcursive{o}}\left(x^p\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^{\operatorname{min}(n,p)}\right)$
	\item $ x^p × \text{\large\textcursive{o}}\left(x^n\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^{p+n}\right)$
	\item $ \text{\large\textcursive{o}}\left(x^n\right) × \text{\large\textcursive{o}}\left(x^p\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^{n+p}\right)$
	\item $ \text{\large\textcursive{o}}\left(λ\cdot x^n\right) \underset{x \to 0}{=} \text{\large\textcursive{o}}\left(x^n\right)$
        \end{enumerate}
      \end{multicols}
    \end{theoreme}

\subsubsection{Linéarité}

\begin{theoreme}[Linéarité des DL]
  
    Soit $f$ et $g$ admettant des DL$_n(0)$ de parties régulières respectives $P(x)$
    et  $Q(x)$.  Alors, pour  tout  $λ∈ℝ$,  $λf+g$  admet  un DL$_n(0)$  de  partie
    régulière $λP(x)+Q(x)$.
\end{theoreme}


\begin{recherche}
  
    Déterminez le DL$_n(0)$ de 
	$\operatorname{ch}(x)=\dfrac{\operatorname{e}^x+\operatorname{e}^{-x}}{2}$ et de 
	$\operatorname{sh}(x)=\dfrac{\operatorname{e}^x-\operatorname{e}^{-x}}{2}$.
	
      \end{recherche}


      \subsubsection{Produit}
      \begin{theoreme}[Produit de DLs]
        
	Soit $f$ et $g$ admettant des DL$_n(0)$ de parties régulières respectives $P(x)$
    et $Q(x)$. Alors $f×g$  admet  un DL$_n(0)$  de  partie
    régulière le  produit $P(x)×Q(x)$  en ne  retenant que  les termes  de degré
    inférieur ou égal à $n$.
	
	
      \end{theoreme}


      \begin{recherche}
        \begin{enumerate}
        \item DL$_3(0)$ de $\operatorname{e}^{x}\sqrt{1+x}$.
	\item   DL$_2(1)$ de $ \dfrac{\exp x}{x} $.
	\item  DL$_7(0)$ de $\tan x$
        \end{enumerate}
      \end{recherche}


      \subsubsection{Substitution}

      \begin{theoreme}[Substitution]

        
	Si $  f(x)\underset{x \to x_0}{=}P(x) +\text{\large\textcursive{o}}\left(x^n\right)$ avec
	$P∈ℝ_n[X]$    et    si   $u:    t\mapsto    u(t)$    est   telle    que    $
	\displaystyle\lim_{t\to0}u(t)=t_0$ alors on a le développement asymptotique:
	
	$$
	f\bigl(u(t)\bigr)\underset{t \to t_0}{=}P\bigl(u(t)\bigr) + \text{\large\textcursive{o}}\bigl((u(t))^n\bigr)
	$$
        
      \end{theoreme}


      \begin{recherche}
        
	DL$_2(0)$ de $x\mapsto 2^x$.



      \end{recherche}


\section{Application des DLs}

\subsection{Calcul de limites}

\begin{recherche}
  
    $ \displaystyle\lim_{x\to 0}\dfrac{\sqrt{1+x}-\sqrt{1-x}}{\operatorname{e}^x-\operatorname{e}^{x^2}}$ 

  \end{recherche}
  


  Voir par exemple l'exercice~\vref{exolim}



\subsection{Recherche d'un équivalent de $f(x) - \displaystyle\lim_{x\to x_0}f(x)$}


  \begin{recherche}
    
On pose $\displaystyle{f(x)=\frac{1}{\sin^2(x)}-\frac{1}{x^2}}$.

\begin{enumerate}
	\item Déterminer un équivalent en $0$ de $x^2\sin^2(x)$ et de $x^2-\sin^2(x)$.
	\item En déduire que $f$ admet une limite finie $\ell$ en $0$.
	\item Déterminer un équivalent de $f(x)-\ell$ en $0$.
\end{enumerate}
  \end{recherche}


  
  \subsection{Asymptotes}

  \begin{definition}[Asymptotes]
    
\begin{enumerate}
	\item Si $f$ admet une limite infinie en $x_0 \in ℝ$ (éventuellement à droite ou à gauche), on dit que la droite d'équation $x=x_0$ est asymptote verticale à la courbe $\mathscr{C}_f$.
	\item Si $f$ admet une limite finie $\ell$ en $\pm\infty$, on dit que la
          droite  d'équation  $y=\ell$ est  asymptote  horizontale  à la  courbe
          $\mathscr{C}_f$ en $\pm\infty$ (respectivement). 
	\item Si $f(x)-(ax+b)$ tend vers $0$ en $\pm\infty$, on dit que la droite d'équation $y=ax+b$ est asymptote oblique à la courbe $\mathscr{C}_f$ en $\pm\infty$ (respectivement).
\end{enumerate}
    
\end{definition}

\begin{recherche}
  
Soit  $f$   définie  sur  $\R^*$  par   $f(x)=(2+x)\exp(-1/x)$.  Déterminer  les
éventuelles asymptotes à la courbe  $\mathscr{C}_f$ et étudier, lorsque cela est
possible, la position relative de $\mathscr{C}_f$ par rapport à ses asymptotes. 
\end{recherche}


  \begin{recherche}
    
 
Dans chaque cas, étudier les branches infinies de la fonction $f$:

\begin{multicols}{2}
\begin{enumerate}

\item
$f_1(x)=\dfrac{x^2+\ln(x)}{x+1}$
% \item
% $f_2(x)=\dfrac{x\text{e}^{x}+1}{x+1}$
% \item
% $f_3(x)=\frac{x\ln(x)+\ln(x)}{\sqrt{x+1}}$
\item
$f_2(x)=\sqrt{x^2-4x}$

\end{enumerate}
\end{multicols}
  \end{recherche}

  \subsection{Lien avec la continuité et la dérivabilité}

  \begin{theoreme}[Lien avec la continuité et la dérivabilité]
    
    Avec les hypothèses habituelles:
	\begin{enumerate}
	\item  $f$ est continue en $x_0$ si, et seulement si, $f$ admet un DL$_0(x_0)$
	\item  $f$ est dérivable en $x_0$ si, et seulement si, $f$ admet un DL$_1(x_0)$
        \end{enumerate}
 \end{theoreme}

      On retiendra aussi  que si on connaît un ${\rm  DL}_2(a)$ alors on obtient
      une équation de la  tangente au point d'abscisse $a$ et  la position de la
      courbe au voisinage de $a$ car:

      $$
      f(x) = f(a) + (x-a)f'(a) + \dfrac{1}{2}(x-a)^2f''(a) + \text{\large\textcursive{o}}\left((x-a)^2\right)
      $$

\begin{recherche}

Soit $f(x)=\dfrac{1}{\sin x}- \dfrac{1}{x}$

\begin{enumerate}
\item Montrer que $f$ peut être prolongée par continuité en 0.
\item Montrer que ce prolongement par continuité est dérivable en 0 et former l'équation de la tangente $(T)$ à $\mathscr{C}_f$.
\item Déterminer la position relative de $(T)$ et de $\mathscr{C}_f$ au voisinage de 0.
\end{enumerate}
\end{recherche}




\subsection{Avec Python}


\begin{pythoncode}
import sympy as sy
import sympy.plotting as splt
x = sy.symbols('x') 

def montre_dl(fx, a, b, n=3, vois=0):
    f = lambda x: fx
    reg = lambda x: sy.series(f(x), x, vois, n).removeO()
    p = splt.plot(f(x),reg(x),(x,a,b),show=False,axis_center=(0,0))
    sy.pprint(reg(x))
    p.show()

montre_dl(1/(sy.sin(x))**2 -1/x**2, -2, 2)
\end{pythoncode}



%\begin{figure}
\begin{center}
  \includegraphics[height=4cm]{DL}
\end{center}
%\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% E X E R C I C E S
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\modeexercice

\section{La main à la pâte}



\begin{exercice}[Calcul de développements limités]

Calculer les développements limités suivants :

\begin{multicols}{3}
\begin{enumerate}
\item DL$_3(0)$ de $(\cos x)\E^x$
\item DL$_4(0)$ de $x\ln\left( \dfrac{1+x}{1-x}\right)$
\item DL$_3(0)$ de $\dfrac{\sqrt{1+x}}{(1-x)^2}$
\item DL$_4(0)$ de $\ln(1+\cos(2x))$
\item DL$_2(0)$ de $\dfrac{\ln(1-x)}{\ln(1+x)}$
\item DL$_4(+\infty)$ de $\sqrt[3]{x^3+x}-\sqrt[3]{x^3-x}$
\item DL$_2(2)$ de $\dfrac{1}{x}$

\item DL$_2(0)$ de $\E^{\sin x}+\E^{\cos x}$
\item DL$_5(0)$ de $\tan x$
\end{enumerate}

\end{multicols}

\end{exercice}


\begin{exercice}[Calculs de limites]\label{exolim}

Déterminer les limites suivantes (en utilisant des développements limités) :

\begin{multicols}{2}
\begin{enumerate}
\item $\displaystyle\lim_{x \to 0} \dfrac{\E^{x^2}-\cos x}{x^2}$
\item $\displaystyle\lim_{x \to 0} \dfrac{\dfrac{\sqrt{1+x}-\sqrt{1-x}}{x}-1}{x}$
\item $\displaystyle\lim_{x \to 0} \dfrac{\sin x - x}{\tan x (\E^{2x}+1-2\E^x)}$
\item $\displaystyle\lim_{x \to 0} \dfrac{\E^x+2 \sin x -1}{\ln(1+x)+\sqrt{1+x}-1}$
\item $\displaystyle\lim_{x \to 0}\dfrac{1}{x}\ln\frac{\E^x+\E^{-x}}{2}$
\end{enumerate}
\end{multicols}

\end{exercice}



\begin{exercice}[Branches infinies]
  Étudier les braches infinies au voisinage de $\pm\infty$ de $f:x\mapsto x^2\ln\left(\dfrac{x+1}{x-1}\right)$.
\end{exercice}


\begin{exercice}[Branches infinies]
Soit $f: x\mapsto \dfrac{x^2}{x-1}\exp(1/x)
$. Dire si la courbe représentative de
$f$ admet une  asymptote au voisinage de $+\infty$ et  le cas échéant déterminer
la position de la courbe par rapport à l'asymptote au voisinage de $+\infty$.

\end{exercice}


\begin{exercice}[Branches infinies]
Soit $f: x\mapsto x\sqrt{\dfrac{1+x}{x-1}}$. Dire si la courbe représentative de
$f$ admet une  asymptote au voisinage de $+\infty$ et  le cas échéant déterminer
la position de la courbe par rapport à l'asymptote au voisinage de $+\infty$.
\end{exercice}


\begin{exercice}[Branches infinies]
Soit $f: x\mapsto x\left(1+\dfrac{1}{x}
\right)^x$. Dire si la courbe représentative de
$f$ admet une  asymptote au voisinage de $+\infty$ et  le cas échéant déterminer
la position de la courbe par rapport à l'asymptote au voisinage de $+\infty$.

\end{exercice}


\begin{exercice}[Étude locale au voisinage de 0]

  On considère la fonction $f$ définie par $f(x)=\dfrac{x}{\E^x-1}$.
  Démontrer  que  $f$  peut  être  prolongée  par continuité  en  0  et  que  ce
  prolongement est dérivable en 0. Déterminer à l'aide d'un développement limité
  une équation de sa tangente au point d'abscisse 0 ainsi que 
  la position de $\mathscr C_f$ par
  rapport à cette tangente.
\end{exercice}


\begin{exercice}[Étude locale au voisinage de 0]

  On considère la fonction $f$ définie par $f(x)=\dfrac{\ln(1 + x) - x}{x^2}$.
  Démontrer  que  $f$  peut  être  prolongée  par continuité  en  0  et  que  ce
  prolongement  est  dérivable  en  0.   À  l'aide  d'un  développement  limité,
  déterminer une équation de la tangente au point d'abscisse 0 et la position de
  $\mathscr C_f$ par rapport à cette tangente.
\end{exercice}



\begin{exercice}[DL de arctan]
  Soit $n∈ℕ$. Déterminer un $DL_{2n+1}(0)$ de la fonction $x\mapsto \arctan(x)$.
\end{exercice}


% \begin{exercice}[\'Etude d'une fonction avec logarithme]

% On considère la fonction $f$ définie sur $\R^{+*}$ par $f(t)=t-\ln t - \dfrac{1}{t}$.

% \begin{enumerate}
% \item \'Etudier les branches infinies de $f$.
% \item Faire une étude de variations de $f$ et donner l'allure de la représentation graphique de $f$.
% \item Résoudre l'équation $f(t)=0$.
% \end{enumerate}

% \end{exercice}






\begin{exercice}[Limites et équivalents]

Les fonctions $f$ et $g$ sont définies au voisinage de $0$ par:

\[ f(x)=\dfrac{\arctan(x)-x}{\tan(x)-x} \text{ et } g(x)=\left[\tan\left(x+\frac{\pi}{4}\right)\right]^{\frac{1}{\sin(x)}} \]

\begin{enumerate}
	\item Démontrer que $f$ admet une limite finie $\ell$ en $0$ et déterminer un équivalent de $f(x)-\ell$ en $0$.
	\item Même question pour $g$.
\end{enumerate}
\end{exercice}



\begin{exercice}[\'Etude d'une fonction définie avec des radicaux]

On considère la fonction $f$ définie par $f(x)=\dfrac{x\sqrt{x^2+1}}{x-1}$.


\begin{enumerate}
	\item Déterminer les domaines de définition, de continuité et de dérivabilité de $f$.
	\item \'Etudier ses variations et déterminer si $f$ admet des extrema locaux.
	\item Démontrer que la courbe $\mathscr{C}_f$ admet une tangente $(T_0)$ en son point d'abscisse $0$. Donner une équation de $(T_0)$ et préciser la position relative de $\mathscr{C}_f$ et $(T_0)$ au voisinage de $0$.
	\item Montrer que la courbe $\mathscr{C}_f$ admet une asymptote oblique $(D)$ en $+\infty$. Donner une équation de $(D)$ et préciser la position relative de $\mathscr{C}_f$ et $(D)$ au voisinage de $+\infty$.
	\item Montrer que la courbe $\mathscr{C}_f$ admet une asymptote oblique $\Delta$ en $-\infty$.\\ Donner une équation de $\Delta$ et préciser la position relative de $\mathscr{C}_f$ et $\Delta$ au voisinage de $-\infty$.
	\item Tracer l'allure de la courbe $\mathscr{C}_f$.
\end{enumerate}

\end{exercice}





\begin{exercice}[Pour les Stakhanovistes]
  
	Vérifiez les solutions proposées pour ces DL :



	
	\begin{align*}
	x/\sin x              &\underset{x\to 0}{=} 1 + x^2/6 + 7x^4/360 + \text{\large\textcursive{o}}(x^4)              \cr
	1/\cos x              &\underset{x\to 0}{=} 1 + x^2/2 + 5x^4/24 + \text{\large\textcursive{o}}(x^4)               \cr
	\ln(\sin x/x)         &\underset{x\to 0}{=} -x^2/6 -x^4/180 -x^6/2835  + \text{\large\textcursive{o}}(x^6)        \cr
	\exp(\sin x/x)        &\underset{x\to 0}{=} e(1 - x^2/6 + x^4/45) + \text{\large\textcursive{o}}(x^4)             \cr
	\sqrt{\tan x}         &\underset{x\to \pi/4}{=} 1 + h + h^2/2   + \text{\large\textcursive{o}}(h^2), h = x-\pi/4                   \cr
	\sin(x+x^2+x^3-x^4)   &\underset{x\to 0}{=} x + x^2 + 5x^3/6 -3x^4/2  + \text{\large\textcursive{o}}(x^4)           \cr
	\ln(x\tan(1/x))       &\underset{x\to 0}{=} x^{-2}/3 + 7x^{-4}/90  + \text{\large\textcursive{o}}(1/x^4)              \cr
	(1-\cos x)/(e^x-1)^2  &\underset{x\to 0}{=} 1/2 - x/2 + x^2/6   + \text{\large\textcursive{o}}(x^2)                 \cr
	\sin((\pi\cos x)/2)   &\underset{x\to 0}{=}1 -\pi^2x^4/32 + \pi^2x^6/192  + \text{\large\textcursive{o}}(x^6)     \cr
	\cos x\ln(1+x)        &\underset{x\to 0}{=} x - x^2/2 - x^3/6    + \text{\large\textcursive{o}}(x^4)                \cr
	(\sin x-1)/(\cos x+1) &\underset{x\to 0}{=} -1/2 + x/2 - x^2/8 + \text{\large\textcursive{o}}(x^2)                 \cr
	\ln(2\cos x+\tan x)   &\underset{x\to 0}{=} \ln2+x/2-5x^2/8+11x^3/24-59x^4/192  + \text{\large\textcursive{o}}(x^4) \cr
	e^{\cos x} &\underset{x\to 0}{=} e(1 - x^2/2 + x^4/6) + \text{\large\textcursive{o}}(x^5) \cr
	x\sqrt{(x-1)/(x+1)}   &\underset{x\to 2}{=} 1/\sqrt3(2 + 5h/3 + h^3/54)  + o(h^3), h = x-2              \cr
	\sqrt{1+\sqrt{1-x}}   &\underset{x\to 0}{=} \sqrt2(1 - x/8 - 5x^2/128 - 21x^3/1024) + \text{\large\textcursive{o}}(x^3)  \cr
	\sqrt{1-\sqrt{1-x^2}} &\underset{x\to 0}{=} |x|/\sqrt2(1 + x^2/8 + 7x^4/128) + \text{\large\textcursive{o}}(x^5)        \cr
	e^x-\sqrt{1+2x}       &\underset{x\to 0}{=} x^2 - x^3/3 + 2x^4/3 - 13x^5/15  + \text{\large\textcursive{o}}(x^5)        \cr
	(\sqrt[3]{x^3+x^2}+\sqrt[3]{x^3-x^2})/x &\underset{x\to +\infty}{=} 2 - 2x^{-2}/9 + \text{\large\textcursive{o}}(1/x^3) \cr
	(1-x+x^2)^{1/x}       &\underset{x\to 0}{=} e^{-1}(1 + x/2 + 19x^2/24) + \text{\large\textcursive{o}}(x^2)           \cr
	((1+x)/(1-x))^\alpha  &\underset{x\to 0}{=} 1 + 2\alpha x + 2\alpha^2x^2 + 2\alpha(2\alpha^2+1)x^3/3 + \text{\large\textcursive{o}}(x^3) \cr
	(\sin x/x)^{2/x^2}    &\underset{x\to 0}{=} e^{-1/3}(1 - x^2/90) + \text{\large\textcursive{o}}(x^3)                \cr
	(\sin x/x)^{3/x^2}    &\underset{x\to 0}{=} e^{-1/2}(1 - x^2/60 - 139x^4/151200)+ \text{\large\textcursive{o}}(x^4) \cr
	(1+\sin x)^{1/x}      &\underset{x\to 0}{=} e(1 - x/2 + 7x^2/24)  + \text{\large\textcursive{o}}(x^2)                \cr
	(1+\sin x + \cos x)^x &\underset{x\to 0}{=} 1 + x\ln2 + x^2(\ln^22+1)/2  + \text{\large\textcursive{o}}(x^2)         \cr
	(\sin x)^{\sin x}     &\underset{x\to \pi/2}{=} 1 - h^2/2 + 7h^4/24  + \text{\large\textcursive{o}}(h^4), h = x-\pi/2                 \cr
	(\tan x)^{\tan2x}     &\underset{x\to \pi/4}{=} e^{-1}(1 + 2h^2/3 + 4h^4/5)   + \text{\large\textcursive{o}}(h^4), h = x-\pi/4     \cr
	\end{align*}
	


  
\end{exercice}


\subsection*{Aux concours}


\begin{exercice}[Écrit G2E]
  Ce problème  est consacré  à l'étude d'une  fonction $f$,  solution d'une
  équation différentielle (partie A) dont  on détermine les coefficients du
  développement limité en 0 dans la partie B.

  
\subsubsection*{Partie A : Une fonction}
\begin{enumerate}
  \item On considère l'équation différentielle $(E)$ suivante à résoudre sur l'intervalle $]-\infty, 1[$ :

$$
y+(x-1) y^{\prime}=\mathrm{e}^{-x}
$$

\begin{enumerate}

\item  Résoudre l'équation différentielle homogène associée à $(E)$.

\item  Résoudre $(E)$.
\end{enumerate}
  \item On considère la fonction $f$ définie par:

$$
\forall x \in]-\infty, 1\left[, \quad f(x)=\frac{1}{\mathrm{e}^{x}(1-x)}\right.
$$
\begin{enumerate}
\item  Démontrer que $f$ est l'unique solution de $(E)$ satisfaisant à la condition initiale $y(0)=1$.

\item  Étudier les variations de $f$ et ses limites en $-\infty$ et 1 .
\end{enumerate}

\item Donner,  en justifiant,  le développement  limité à  l'ordre de  2 de
  $f(x)$ au voisinage de 0.

\end{enumerate}

\subsubsection*{Partie B : Une suite}
$f$ désigne toujours la fonction définie dans la partie $\mathrm{A}$. Pour tout $n \in \mathbb{N}$, on note $d_{n}$ le coefficient de $x^{n}$ dans le développement limité de $f(x)$ au voisinage de 0 à l'ordre $n$.

\begin{enumerate}
\item
  \begin{enumerate}
    \item  Justifier l'existence de $d_{n}$ pour tout $n \in \mathbb{N}$.

    \item Exprimer $d_{n}$ en fonction  de $f^{(n)}(0)$ et préciser $d_{0},
      d_{1}$ et $d_{2}$.
    \end{enumerate}

  \item \begin{enumerate}
      \item En dérivant $n$ fois l'équation $(E)$, démontrer que:

$$
\forall n \in \mathbb{N}, \forall x \in]-\infty, 1\left[, \quad(n+1) f^{(n)}(x)+(x-1) f^{(n+1)}(x)=(-1)^{n} \mathrm{e}^{-x}\right.
$$

On utilisera la formule donnant la dérivée n-ième d'un produit. 

\item  En déduire que:

$$
\forall n \in \mathbb{N}, \quad d_{n+1}=d_{n}+\frac{(-1)^{n+1}}{(n+1) !}
$$
\end{enumerate}
\end{enumerate}
\end{exercice}





\begin{exercice}[Écrit Agro-Véto - Niveau 2...]
  Considérons la fonction $f$ définie sur $\mathbb{R}_{+}$par:

$$
\forall x \in \mathbb{R}_{+}^{*}, f(x)=\frac{\sin (x)}{x} \text { et } f(0)=1
$$

\begin{enumerate}
  \item \textbf{Représentation graphique de la fonction $f$.}

\begin{enumerate}
\item  Démontrer que $f$ est de classe $\mathcal{C}^{1}$ sur $\mathbb{R}_{+}^{*}$ et calculer $f^{\prime}$ sur $\mathbb{R}_{+}^{*}$.

\item  Démontrer que $f$ est continue en 0 , dérivable en 0 et préciser $f^{\prime}(0)$. L'application $f$ est-elle de classe $\mathcal{C}^{1}$ sur $\mathbb{R}_{+} ?$

\item  Afin d'étudier les variations de $f$ sur $\mathbb{R}_{+}$, nous introduisons la fonction $g: x \mapsto x \cos (x)-\sin (x)$.

  \begin{enumerate}
\item  Étudier le signe de $g$ sur $[0, \pi]$ puis les variations de $f$ sur $[0, \pi]$.

\item  Soit $n$ appartenant à $\mathbb{N}^{*}$.

Démontrer que l'équation $\left(\mathcal{E}_{n}\right): x \cos (x)=\sin (x), x \in[n \pi,(n+1) \pi]$ admet une unique solution $x_{n}$,

et en déduire le signe de $g$ sur $[n \pi,(n+1) \pi]$ puis les variations de $f$ sur $[n \pi,(n+1) \pi]$.

\textit{Une discussion sur la parité de $n$ intervient.}
\end{enumerate}
\item  Étudier la limite de $f$ en $+\infty$ et préciser la nature de la branche infinie.

\item  Tracer l'allure de la courbe représentative de $f$ sur $[0,6 \pi]$.

\end{enumerate}
\item \textbf{Étude des dérivées successives de $f$ sur $\mathbb{R}_{+}$.}

\begin{enumerate}
\item  Démontrer que $f$ est de classe $\mathcal{C}^{\infty}$ sur $\mathbb{R}_{+}^{*}$.

\textit{Nous rappelons que pour tout entier naturel $n$, la dérivée $n^{\text {ème }}$ de $f$ se note $f^{(n)}$, la dérivée $n^{\text {ème }}$ de la fonction cosinus se note donc $\cos ^{(n)}$.
}
\item  Démontrer par récurrence, que pour tout entier naturel $n$:

$$
\forall x \in \mathbb{R}_{+}^{*}, f^{(n)}(x)=\frac{1}{x^{n+1}} \int_{0}^{x} u^{n} \cos ^{(n)}(u) \mathrm{d} u
$$

\item  Soit $n$ appartenant à $\mathbb{N}, x$ appartenant à $\mathbb{R}_{+}^{*}$, calculer: $\frac{1}{x^{n+1}} \int_{0}^{x} u^{n} \cos ^{(n)}(0) \mathrm{d} u$.

\textit{L'expression attendue dépend de $\cos ^{(n)}(0)$, valeur que vous ne chercherez ni à évaluer, ni à simplifier.
}
\item  Soit $n$ appartenant à $\mathbb{N}, x$ appartenant à $\mathbb{R}_{+}^{*}$, démontrer que:

$$
\left|f^{(n)}(x)-\frac{\cos ^{(n)}(0)}{n+1}\right| \leqslant \frac{1}{x^{n+1}} \int_{0}^{x} u^{n}\left|\cos ^{(n)}(u)-\cos ^{(n)}(0)\right| \mathrm{d} u .
$$

\item  Soit $n$ appartenant à $\mathbb{N}$, démontrer que:

$$
\forall u \in \mathbb{R}_{+},\left|\cos ^{(n)}(u)-\cos ^{(n)}(0)\right| \leqslant u
$$

\item  En déduire que pour tout entier naturel $n$:

$$
\forall x \in \mathbb{R}_{+}^{*},\left|f^{(n)}(x)-\frac{\cos ^{(n)}(0)}{n+1}\right| \leqslant \frac{x}{n+2}
$$

\item  Soit $n$ appartenant à $\mathbb{N}$, démontrer que $: \lim _{x \rightarrow 0^{+}} f^{(n)}(x)=\frac{\cos ^{(n)}(0)}{n+1}$

\item  Démontrer que $f$ est deux fois dérivable en 0 et que $: f^{\prime \prime}(0)=-\frac{1}{3}$, puis démontrer que $f$ est de classe $\mathcal{C}^{2}$ sur $\mathbb{R}_{+}$.

\item  Démontrer que pour tout entier naturel $n, f$ est de classe $\mathcal{C}^{n}$ sur $\mathbb{R}_{+}$et préciser la valeur de $f^{(n)}(0)$.

\textit{Nous avons donc montré que $f$ est de classe $\mathcal{C}^{\infty}$ sur $\mathbb{R}_{+}$.}

\end{enumerate}

\item \textit{Expression des dérivées successives de $f$ sur $\mathbb{R}_{+}^{*}$.}
\begin{enumerate}
\item  Démontrer que pour tout entier naturel $k: \forall x \in \mathbb{R}_{+}^{*}, \sin ^{(k)}(x)=\sin \left(x+k \frac{\pi}{2}\right)$.

\item  Considérons la fonction inverse $b: x \mapsto \frac{1}{x}$.

Démontrer que pour tout entier naturel $k: \forall x \in \mathbb{R}_{+}^{*}, b^{(k)}(x)=\frac{(-1)^{k} k !}{x^{k+1}}$.

\item  Pour $n$ appartenant à $\mathbb{N}$, exprimer la dérivée $n^{\text {ème }}$ de $f$ en fonction des dérivées successives des fonctions $\sin$ et $b$,

et en déduire que:

$$
\forall x \in \mathbb{R}_{+}^{*}, f^{(n)}(x)=\frac{1}{x^{n+1}} \sum_{k=0}^{n}\left(\begin{array}{l}
n \\
k
\end{array}\right)(-1)^{n-k}(n-k) ! \sin \left(x+k \frac{\pi}{2}\right) x^{k}
$$

\item  Soit $k$ appartenant à $\mathbb{N}, x$ appartenant à $\mathbb{R}_{+}^{*}$.

Exprimer $\sin ^{(2 k)}(x)$ en fonction de $\sin (x)$ et de $k$, puis $\sin ^{(2 k+1)}(x)$ en fonction de $\cos (x)$ et de $k$.

\item  Démontrer que pour tout entier naturel $n$ et tout réel strictement positif $x$:

$$
f^{(n)}(x)=\frac{n !}{x^{n+1}}\left(\sin (x) \sum_{0 \leqslant 2 k \leqslant n} \frac{(-1)^{n-k} x^{2 k}}{(2 k) !}+\cos (x) \sum_{0 \leqslant 2 k+1 \leqslant n} \frac{(-1)^{n-k-1} x^{2 k+1}}{(2 k+1) !}\right)
$$
\end{enumerate}
\end{enumerate}
\end{exercice}








% \modenormal
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Poly_DL_24_court"
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
