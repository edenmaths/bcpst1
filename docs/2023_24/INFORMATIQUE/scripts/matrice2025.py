import matplotlib.pyplot as plt
#import matplotlib.cm as cm
import numpy as np
#import matplotlib.colors as colors

def mat_pow(A, n):
    nl, nc = A.shape
    assert nl == nc
    P = np.eye(nl)
    for k in range(1, n+1):
        P = np.dot(P, A)
    return P


def nilpo(taille: int) -> np.ndarray: 
    return np.array([[1 if i < j else 0 for j in range(taille)] for i in range(taille)])

def montre(mat: np.ndarray):
	plt.clf()
	plt.imshow(mat)
	plt.show()

# Que se passe-t-il ici ?
def montre_nilpo(taille: int):
	for i in range(1, taille):
		montre(mat_pow(nilpo(taille), i))

def ix(n):
    A = np.zeros((n, n))
    for i in range(n):
        A[i, i] = 1 # on allume la diagonale
        A[i, (n - 1) - i] = 1 # et l'antidiagonale
    return A

def suede(n):
    A = np.zeros((n, 2*n))
    for j in range(2*n):
        A[n//2, j] = 1 # on allume la ligne du milieu
    for i in range(n):
        A[i, 2*n//3] = 1 # on allume la colonne au 1/3 de la largeur 
    return A

def damier(n):
    A = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            if i%2 == j%2: # si même parité
                A[i,j] = 1
    return A
    



